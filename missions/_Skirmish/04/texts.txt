$ dotdotdot
- ...


$ Intro1
- How much further do we have to go?
$ Intro2
- I don't know, but I think my compass broke. I'm pretty sure the sun doesn't set north.
$ Intro3
- So we've looped right back into American territory? Wonderful...
$ Intro4
- It's not so bad, we should be somewhere in the north. These places will not be well-defended, if at all.
$ Intro5
- No offense, but that's not very reassuring. While yes, the war is over and we probably won't be shot on sight, we're still an unwelcome party deep behind enemy lines.
$ Intro6
- What I'm saying is, I don't want them to assume the wrong thing.
$ Intro7
- Hey. Bit late.
$ Intro8
- American pigs!
$ Intro9-1
- Hold your fire, Sonia, this is Henry Lynch, my buddy from years back!
$ Intro9-2
- Appreciate the warm welcome, Ron. 
$ Intro10
- How did you find me all the way out here? We're way off the track.

$ Intro11
- Your radio is very easy to keep track of. 
$ Intro12
- How can we trust him? By his face alone, I can tell he's a yankee.
$ Intro13
- Well, I was, but my citizenship expired about 10 years ago. Now your buddies matter more than your nations. Then the Alliance became vestigial. Old enemies kept their grudges, and as the years moved on, so did we. Went north, tried to settle...
$ Intro14
- Long story short, North aint so friendly anymore. Harsh soil, no oil, wildlife. Pretty much everything that could go wrong with a survival scenario went wrong, except the rain.
$ Intro15
- Nobody got sick, and all our weapon still work. So, we intended to use them on Tau, a nearby American supply outpoust.
$ Intro16
- It backfired. The whole damned thing is automated. Not a single person in sight and it's mass-producing light units like nobody's buisness.
$ Intro17
- I say we can take it. 
$ Intro18
- Woman, are you crazy?! Who birthed you, Lisa Lawson? What makes you think you can take out a base by yourself?
$ Intro19
- Simple reason: Limited supplies. It doesn't matter how many defenses they have by now, their crates are gound to run out sooner or later.
$ Intro20
- We just need to hunker down and snipe whatever shippments come in. Better yet if we steal them ourselves.
$ Intro21
- A crazy plan, but it could work.
$ Intro22
- She's completely off her rocker! We don't even have engineers to build a base of our own!
$ Intro23
- We don't need it if all the robots have are scout drones. Plus, I still owe Ron. And frankly, I like the Russian uniforms better than ours. Everything beats the trash arabs wore.
$ Intro24
- Good to hear. I don't trust you, but you're better off following us anyway. I'm sure Alliance will shoot you as eagerly as their own.
$ Intro25
- ...Wait, what? What the fu-
$ Intro26
- There's a civil war brewing and we wanna get ahead of it. Check the truck if you want. It has an american alaskite missile loaded onto it.
$ Intro27
- You can come with us and break through south. We can't trust any of these capitalist pigs.
$ Intro28
- ...Well I'll be damned, the girl is right. There are markings here in inches, too. We've got no time to lose. Lead the way, crazy lady!
$ Intro29
- Great! Then let's take over Tau and arm ourselves to the teeth.
$ Intro30
- On that note, we'll save ourselves a lot of trouble if we don't raze the factory. It will be easier to make new vehicles with it.

$ Briefing1
- What are our weapons?
$ Briefing2
- Pretty much what you see here. We managed to hook up a few stolen cameras and survey some paths remotely.
$ Briefing3
- My buddy there carries some remote-detonated explosives. They won't do much against their speedster drones unless we figure out their paths.
$ Briefing4
- They follow a scripted path?
$ Briefing5
- From what little we've seen, it seems so.
$ Briefing6
- What about the enemy turrets?
$ Briefing7
- Only the basics. Light cannons, a few radars. They have a whole of one gatling gun on a jeep to chase off tigers.
$ Briefing8
- The weapons seem a little sluggish though. I think the base is underpowered.
$ Briefing9
- Hmm... I wonder if we can use that to our advantage somehow.

$ EnemyBaseSighted
- Here we are! Safeties off! FORWARD!
$ EnemyFire
- You call this shooting? This aim is terrible. My grandma could have done better.
$ Monkey
- Is it weird that I'm glad this monkey is following us? It seems competent with the gun at least.
$ TankSighted1
- You call that a JEEP!? This is a tank, comrade, not a toy!
$ TankSighted2
- It didn't sound like one from afar.
$ TankSighted3
- You mean you haven't actually SEEN it?!
$ TankSighted4
- We saw a lot of dead cats and heard a car zooming by, we put two and two together.
$ TankSighted5
- Shut up and keep shooting!

$ DepotDown
- Alright, that's done. We can gut their factory and salvage some parts. Then, it's off south, no rest stops.
$ FactoryDown
- This should buy us some breathing room. Now it's just a matter of time before the defenses fall.
$ BothDown
- Alright, that's done. We'll have to build our own factory, but there should be enough parts to salvage something.
 
$ WTF1 
- What was that? It didn't sound like a usual explosion.
$ WTF2
- That's because it wasn't. Man, that takes me back. Haven't heard this in forever.
$ WTF3 
- Yes, but what IS it?
$ WTF4 
- That's the thunder of stuff or people coming in from the future. As they materialize, thier sudden presence displaces the air. Hence, the BOOM.
$ WTF5 
- Though to be honest, I haven't heard that familiar clasp in a couple dozen years. I wonder if this is just a freak outlier or if we're gonna see a return of these things.
$ WTF6 
- I hope not. I'm going to have to start wearing earplugs to sleep at night again.
$ AnotherOne
- That boom again.
$ WTF7 
- Hello? Is anybody out there? This is Gryphon 8. I don't know where I am... If you can hear me please find me, I'm on an island...
$ WTF8 
- Wait, was that a person? Did they come from the future?
$ WTF9 
- Sounds like it. The noise came from somewhere south, I think.
$ WTF10 
- You and your compass better shut it!
$ WTFLOL1
- Ох ты ж ёб твою налево через корзину!
$ WTFLOL2
- HAHAHAHA! Are you alright?
$ WTFLOL3
- THAT THING ALMOST GAVE ME A HEART ATTACK! What WAS that?

$ HelloThere1
- Howdy there, comrade.
$ HelloThere2
- Ah! Who are you?
$ HelloThere3
- Relax, I'm not an enemy. You're an engineer, aren't you?
$ HelloThere4
- Yes, I am! I'm Timothy N. Kazantsev. Codename Gryphon 8. Do you know where General Yashin is?
$ HelloThere5
- He's dead, comrade. You missed a lot. Come along for now, we'll fill you in.
$ HelloThere6
- Okay! Lead the way, comrade.
$ Hellothere7
- We could use your tools and knowlege to build a base. Ideally somewhere we can place an oil derrick.
$ Hellothere7-NoDoctor
- We could use your tools and knowlege to build a base. Without oil we won't get far, so it's best if a scientist searches the area.
$ Hellothere7-NoOil
- We could use your tools and knowlege to build a base. Ideally somewhere we can place an oil derric. The doctor can help us find it.

$ WhatMiss1
- So what did I miss?
$ WhatMiss2
- Well, long story short, TAWAK is a little imprecise. You landed here quater a century after you were supposed to.
$ WhatMiss3
- That is a very long time. Are we still at war with the Americans?
$ WhatMiss4
- Sort of. You see, when the war started, the two sides have fought, had big victories, suffered heavy loses... It was an entire fuckfest.
$ WhatMiss5
- Eventually, there were deffectors on all sides. They formed an American-Russian union of scientists and bight-eyed idealists called The Alliance.
$ WhatMissKurt
- There were also Arabs, like me. We've taken over the American EON - that's their version of TAWAK from a different form of history - and made quite a mess when we arrived.
$ WhatMiss6
- The Alliance ultimately was the victorious side in the conflict. Thanks to them, the Alaskite will be owned by nobody.
$ WhatMiss7
- That's crazy, comrade! I had to abandon my life and family to fight a war that is already over!
$ WhatMiss8
- Not quite, comrade Tim. You see, it looks like some renegade Americans are back at it again. And you made it just in time to kick their asses back to the stone age.
$ WhatMissHenry
- Aren't we a little too advanced here to be calling it the stone age? I've seen rockets, computer-controlled vehicles, and lasers in action, sometimes all at once.
$ WhatMissMichail
- And yet, we are still doing manual labor.

$ BaseReady1
- And she's ready! We can build some cars and escape easily now.
$ BaseReady2
- We can, yes, but should we?
$ BaseReady3
- Yes, we need to hurry south, away from the yanks.
$ BaseReady4
- Except, Tau is still building units. They all have to be going somewhere.
$ BaseReady5
- And considering Americans are acting shady...
$ BaseReady6
- You're saying we need to wreck it anyway.
$ BaseReady7
- Precisely.
$ BaseReady8
- We still need to keep moving. Who knows what will happen if we linger too long here.
$ BaseReady9
- I suppose the decision is mine to make...

$ Harem1
- Hey, uh... Sonia? I just realized, you're the only woman in this group...
$ Harem2
- So?
$ Harem3
- Nevermind, I was just thinking... I'll shut up. 
$ Whatwhatwhat
- ???

 
 
# GoalUpdate1
Add Main HMS
- Sonia and Ron must survive.
Add Main TruckMS
- Do not let your transport be destroyed.
Add Main Goal01
- Destroy the american depot.
Add Secondary ZeroDeaths
- Try not to suffer any casualties.
#

# NewGirl
Add Secondary SaveMe
- Find the new arrival.
#

# NewGirlOut
Out Secondary SaveMe
#

$ Pingas
- Pingas!



? QWarning
If the truck explodes, the warhead contained within will follow.
- TRUCK MUST SURVIVE
?

| MCDied
One of the main characters is dead.
| TruckDied
Kaboom.
 