MAIN_DEFINITIONS
  SIZE 60 80
  TICK 0
  PLAYER_NUMBER 1
  FOG_TYPE 2
  FOGSIDES 1 2 3 4 5 6 7 8
  MUCHY_RECT 0 0 0 0
  EAGLES 0 50
  WAVES 15
  WATER_COLORS 707400 6 DEE7B5 3 FFE0D0
END_OF_MAIN_DEFINITIONS

HEXES
  LINE 0 0 59
    t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4
  LINE 1 1 60
    t4 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t4
  LINE 2 1 60
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 3 2 61
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 4 2 61
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 5 3 62
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 6 3 62
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 7 4 63
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 8 4 63
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 9 5 64
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 10 5 64
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 11 6 65
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 12 6 65
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 13 7 66
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 14 7 66
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 15 8 67
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" f2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 16 8 67
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 17 9 68
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 18 9 68
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 19 10 69
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 20 10 69
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 21 11 70
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 22 11 70
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 23 12 71
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 24 12 71
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 25 13 72
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 26 13 72
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" f2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 27 14 73
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 28 14 73
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 29 15 74
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 30 15 74
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 31 16 75
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 32 16 75
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 33 17 76
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 34 17 76
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 35 18 77
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 36 18 77
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 37 19 78
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 38 19 78
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 39 20 79
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 40 20 79
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 41 21 80
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 42 21 80
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 43 22 81
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 44 22 81
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 45 23 82
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 46 23 82
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 47 24 83
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 48 24 83
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 49 25 84
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 50 25 84
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 51 26 85
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 52 26 85
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 53 27 86
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 54 27 86
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 55 28 87
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 56 28 87
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 57 29 88
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 58 29 88
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 59 30 89
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 60 30 89
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 61 31 90
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 62 31 90
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 63 32 91
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 64 32 91
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 65 33 92
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 66 33 92
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 67 34 93
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 68 34 93
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 69 35 94
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 70 35 94
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 71 36 95
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 72 36 95
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 73 37 96
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 74 37 96
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 75 38 97
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 76 38 97
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 77 39 98
    t4 t2 "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" t2 t4
  LINE 78 39 98
    t4 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t2 t4
  LINE 79 40 99
    t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4 t4
END_OF_HEXES

EXTERNAL_NODES nodes.bin
ENVIROMENT
  SET 1 dummy
  SET 2 list-akatojerab
  SET 3 list-jerab-spal
  SET 4 list-platan
  SET 5 list-platan-spal
  SET 6 list-mocal-koreny
  SET 7 list-vrba
  SET 8 list-vrba-zluta
  SET 9 jehl-borovice
  SET 10 jehl-borov-vysoka
  SET 11 jehl-borovice-mala
  SET 12 stone_set_01
  SET 13 stone_set_02
  SET 14 stone_set_03
  SET 15 stone_set_04
  SET 16 stone_set_05
  SET 17 stone_set_06
  SET 18 stone_set_07
  SET 19 houby
  SET 20 mrcha
  SET 21 jehl-smrk-silny
  SET 22 jehl-smrk-stredni
  SET 23 jehl-smrk-uzky-spal
  SET 24 jehl-smrk-stredni
  SET 25 jehl-smrk-stredni-spal
  SET 26 jehl-smrk-stredni-suchy
  SET 27 list-dvoupatr
  SET 28 list-briza
  SET 29 list-paskvil
  SET 30 ker
  SET 31 jehl-borov-vysoka-spal
  SET 32 list-akat-vysoky-spal
  SET 33 plavun
  SET 34 cykas
  SET 35 list-kastan
  SET 36 mech
  SET 37 jehl-smrk-maly
  SET 38 ker-husty
  SET 39 ker-rosti
  SET 40 list-akat-rosti
  SET 41 list-briza-vyssi
  SET 42 list-sahara
  SET 43 list-akat-maly
  SET 44 list-dvoupatr-maly
  SET 45 parez
  SET 46 polosuche
  SET 47 list-dub
  SET 48 list-kastan-vanek
  SET 49 parezoid
  SET 50 branka
  SET 51 leknin
  SET 52 monolit
  SET 53 snowman
  SET 54 snowman_small
END_OF_ENVIROMENT

EXTERNAL_SECTORS sectors.bin
ATTITUDES
  PARAMS 8
  1 0 0 0 0 0 0 0 0
  0 1 2 2 2 2 2 2 2
  0 2 1 2 2 2 2 2 2
  0 2 2 1 2 2 2 2 2
  0 2 2 2 1 2 2 2 2
  0 2 2 2 2 1 2 2 2
  0 2 2 2 2 2 1 2 2
  0 2 2 2 2 2 2 1 2
  0 2 2 2 2 2 2 2 1
  FLAG 8
  T T T T T T T T T
END_OF_ATTITUDES

TECHS
  PARAMS 72 8
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
  F T F T T F T T F T T F T T F T T F T T F T T F T T F
END_OF_TECHS

RESTRICTS
  PARAMS 0 40 8
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
  T T T T T T T T T
END_OF_RESTRICTS

MISSION_OBJECTIVES_FULL 0

SPECS
  PARAMS 0 9
  FLAG 0 0
END_OF_SPECS

BASES
  PARAMS T T

  POOL 1

  BASE 1
    POOL_NUM 1
    SIDE 1
    UNIT 1
END_OF_BASES



RANDOMS
  MICROLOGIC 2 0 1
  RANDOM_SEED 1 -842784643
  RANDOM_SEED 2 -2115085983
  RANDOM_SEED 3 1902144189
  RANDOM_SEED 4 898151057
  RANDOM_SEED 5 -18222789
  RANDOM_SEED 6 1882865670
  RANDOM_SEED 7 -789110971
  RANDOM_SEED 8 1751026996
  RANDOM_SEED 9 -1051396069
  RANDOM_SEED 10 -1363405664
  RANDOM_SEED 11 -443074555
  RANDOM_SEED 12 911650
  RANDOM_SEED 13 364498371
  RANDOM_SEED 14 378023
  RANDOM_SEED 15 -304682128
  RANDOM_SEED 16 907772
  RANDOM_SEED 17 -652005513
  RANDOM_SEED 18 1234038047
  RANDOM_SEED 19 797399756
  RANDOM_SEED 20 1221242998
  RANDOM_SEED 21 -2092389119
  RANDOM_SEED 22 1828787413
  WIND_TIME 782845 782873 782934
  WIND_STRENGTH 36.7606167791381 52.0 53.3803083895691
  WIND_SCATTER 24.1066377870272
END_OF_RANDOMS

NEW_UNITS
  UNIT 1 BUILDING
    DEFINITION
      IDENT AmDepot
      POS 22 15 3
      SIDE 1
      NATION 1
      HP 1000
      BUILDING
        TYPE 1 LEVEL 1 BASE 1
    END_OF_DEFINITION
    GAME_STATE
      ACTION 0 T 1 1
      MOVE 22 15 3 6 F
      UNITMODE F 22 15
      STATE 4 24
      WEAPON_LOADING 0 0
      ATTACK 0 0 F 0
      PLAN 0 T
      ANIM_STATES 0 0 0 0
      MASHA_FLAG 0 DYING 0
      SELECTION 0 0 0 0
      DRAW 406 270 0
      FROM_TO_POS 22 15 0 22 15 0
      FROM_TO_XY 0 0 0 0
      LAST_ACTION 0
      BUILDING
    END_OF_GAME_STATE
  UNIT 2 BUILDING
    DEFINITION
      IDENT AmFac
      POS 28 26 2
      SIDE 1
      NATION 1
      HP 1000
      BUILDING
        TYPE 6 LEVEL 1 BASE 1
    END_OF_DEFINITION
    GAME_STATE
      ACTION 0 T 1 1
      MOVE 28 26 2 6 F
      UNITMODE F 28 26
      STATE 4 24
      WEAPON_LOADING 0 0
      ATTACK 0 30 F 0
      PLAN 0 T
      ANIM_STATES 0 0 0 0
      MASHA_FLAG 0 DYING 0
      SELECTION 0 0 0 0
      DRAW 420 468 0
      FROM_TO_POS 28 26 0 28 26 0
      FROM_TO_XY 0 0 0 0
      LAST_ACTION 0
      BUILDING
        USER_INTERFACE 1 1 1 1
    END_OF_GAME_STATE
END_OF_NEW_UNITS

COLDMEAT
END_OF_COLDMEAT

FOGOBJ
END_OF_FOGOBJ

MISSILES
END_OF_MISSILES

EXPLOSIONS
END_OF_EXPLOSIONS

CRATERS_NEW
END_OF_CRATERS_NEW

LAPSER_EFECTS
END_OF_LAPSER_EFECTS

LASER_EFECTS
END_OF_LASER_EFECTS

USER_INTERFACE
  VIEW 12 -96 1542 854 1
  MASHA_FLAG 2 F F
END_OF_USER_INTERFACE

MC_COMMANDS
END_OF_MC_COMMANDS

DEBRIEFING
SIDE 0 10
  LINES
SIDE 1 10
  LINES
SIDE 2 10
  LINES
SIDE 3 10
  LINES
SIDE 4 10
  LINES
SIDE 5 10
  LINES
SIDE 6 10
  LINES
SIDE 7 10
  LINES
SIDE 8 10
  LINES
END_OF_DEBRIEFING

AREAS
  ACTIVE 1
  LAST 10
END_OF_AREAS

SEXTRA
  1
    1

  255
  1
    2

  255
END_OF_SEXTRA

