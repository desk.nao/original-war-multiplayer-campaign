function FROMOW_MAP_END()
	clearRegistryAchievs()
end

createSync = function ()
    local self = { localPos={}, lastTick= 0, temp = {}, MVR = nil, pointers = {}, me = nil }

    self.colours = {RGB(0,0,255), RGB(255,255,0), RGB(255,0,0), RGB(0,255,255), RGB(255,125,0), RGB(255,0,255), RGB(0,255,0), RGB(255,255,255) }

    self.init = function ()
        local players = MULTI_PLAYERINFO_CURRENT_PLID
        for i, v in pairs(players) do
            if v.ALIVE then
                if v.NAME == MULTI_PLAYERINFO_CURRENT_PLID[MyID].NAME then
                    self.me = v.NAME
                end
                self.pointers[v.NAME] = getElementEX(gamewindow.overlay,anchorLTRB,XYWH(math.random(200,600),math.random(200,250),32,32),true,{nomouseevent=true,texture='Pointer.png',colour1=self.colours[v.COLOUR]})
                self.pointers[v.NAME].label = getLabelEX(self.pointers[v.NAME],anchorNone,XYWH(0,-35,120,74),Tahoma_16B,v.NAME,{font_colour=self.colours[v.COLOUR],font_style_outline=true,shadowtext=true,text_halign=ALIGN_MIDDLE})
            end
        end
        setVisible(self.pointers[self.me],false)
    end

    self.tick = function (FRAMETIME)

        if (OW_GAME_TICK-self.lastTick >= 4) then
            self.lastTick = OW_GAME_TICK
            self.temp = SGUI_getmousexy()
            self.MVR = OW_GETMVRXY()
            self.localPos = {self.MVR.X + self.temp.X, self.MVR.Y + self.temp.Y}
            OW_CUSTOM_COMMAND_SGUI(999, MyID, self.localPos[1], self.localPos[2])
        end

    end

    self.updatePos = function (ID, x, y)
        local v = MULTI_PLAYERINFO_CURRENT_PLID
        if v[ID].NAME == v[MyID].NAME then
            return
        else
            AddEventSlideX(self.pointers[v[ID].NAME].ID, x, 0.05, nil)
            AddEventSlideY(self.pointers[v[ID].NAME].ID, y, 0.05, nil)
        end

    end

    return self
end

function FROMOW_MAP_START(MAPPATH)
    registryAchievs({'ACH_CAD'})
    sync = createSync()
    sync.init()
    regTickCallback('sync.tick(%frametime)')
end

Query.dictionary = {
    ["QML1"] = {
        "The seminal Marxist work 'Das Kapital' (The Capital) was written by:",
        "- Marx brothers.",
        "- Karl Marx.",
        "- St. Paul.",
        "- Ask Gleb."
    },
    ["QML2"] = {
        "The middle name of V. I. Lenin is:",
        "- Ilyich.",
        "- Ivan.",
        "- Ignatius.",
        "- Ask Gleb."
    },
    ["QML3"] = {
        "The famous 'Communist Manifesto' by K. Marx and B. Engels was first published in:",
        "- 48 BC.",
        "- 1848.",
        "- 2048.",
        "- Ask Gleb."
    },
    ["QML4"] = {
        "The acronym USSR stands for:",
        "- Union of Sanctity, Serenity and Radiation.",
        "- Union of Shoplifters, Sycophants and Robbers.",
        "- Union of Soviet Socialist Republics.",
        "- Ask Gleb."
    },
    ["QML5"] = {
        "The Great October Socialist Revolution of 1917 that deposed the Tsar and created the first state of workers and peasants took place on:",
        "- 1st May.",
        "- 7th November.",
        "- 4th July.",
        "- Ask Gleb."
    },
    ["QML6"] = {
        "Who of the great leaders of the world's proletariat is the author of the prophetic quote: 'Alaskite is the only crutch of the ailing imperialistic world order'?",
        "- L. D. Trotsky.",
        "- V. I. Lenin.",
        "- Mao Tse-Tung.",
        "- Ask Gleb."
    },
    ["QEnd"] = {
        "You now have the support of four commanders in this area. You can end the mission or play on. Will you:",
        "- Continue.",
        "- End the mission."
    },
    ["QKill"] = {
        "You have secured support from all the important commanders of the Russian forces. Now you only have to find Platonov and rid the world of him.",
        "- OK."
    },
    ["QEndInfo"] = {
        "Platonov is dead and you can take the supreme command of all Russian forces. Going through his paperwork, you came across a terrible secret that the high command kept away from everybody: Nothing and nobody from future left after a certain date for which an experiment with increasing the temporal range of TAWAR was scheduled. All the evidence points to the experiment failing and the TAWAR being destroyed in the process. The stream of reinforcements is set to dwindle.",
        "- OK."
    }
}

dictionary = {
    ['MDestroy'] = {
        add_main = {"Find and eliminate general Platonov."},
        out = {"Kill general Platonov.", "Capture at least 4 bases in the area."}
    },
    ['MStart'] = {
        add_main = {"Capture at least 4 bases in the area."},
        add_alternative_main = {"Kill general Platonov."}
    },
    ['MGoss'] = {
        add_main = {"Save Artefact for Russia: capture and protect it."},
        add_secondary = {"Stop scientists from joining the Alliance."},
    },
    ['MGossArteOut'] = {
        out = {"Save Artefact for Russia: capture and protect it."}
    },
    ['MGossSciOut'] = {
        out = {"Stop scientists from joining the Alliance."}
    }
}