function FROMOW_MAP_START(MAPPATH)
	registryAchievs({'ACH_ADV'})
end

createSync = function ()
    local self = { localPos={}, lastTick= 0, temp = {}, MVR = nil, pointers = {}, me = nil }

    self.colours = {RGB(0,0,255), RGB(255,255,0), RGB(255,0,0), RGB(0,255,255), RGB(255,125,0), RGB(255,0,255), RGB(0,255,0), RGB(255,255,255) }

    self.init = function ()
        local players = MULTI_PLAYERINFO_CURRENT_PLID
        for i, v in pairs(players) do
            if v.ALIVE then
                if v.NAME == MULTI_PLAYERINFO_CURRENT_PLID[MyID].NAME then
                    self.me = v.NAME
                end
                self.pointers[v.NAME] = getElementEX(gamewindow.overlay,anchorLTRB,XYWH(math.random(200,600),math.random(200,250),32,32),true,{nomouseevent=true,texture='Pointer.png',colour1=self.colours[v.COLOUR]})
                self.pointers[v.NAME].label = getLabelEX(self.pointers[v.NAME],anchorNone,XYWH(0,-35,120,74),Tahoma_16B,v.NAME,{font_colour=self.colours[v.COLOUR],font_style_outline=true,shadowtext=true,text_halign=ALIGN_MIDDLE})
            end
        end
        setVisible(self.pointers[self.me],false)
    end

    self.tick = function (FRAMETIME)

        if (OW_GAME_TICK-self.lastTick >= 4) then
            self.lastTick = OW_GAME_TICK
            self.temp = SGUI_getmousexy()
            self.MVR = OW_GETMVRXY()
            self.localPos = {self.MVR.X + self.temp.X, self.MVR.Y + self.temp.Y}
            OW_CUSTOM_COMMAND_SGUI(999, MyID, self.localPos[1], self.localPos[2])
        end

    end

    self.updatePos = function (ID, x, y)
        local v = MULTI_PLAYERINFO_CURRENT_PLID
        if v[ID].NAME == v[MyID].NAME then
            return
        else
            AddEventSlideX(self.pointers[v[ID].NAME].ID, x, 0.05, nil)
            AddEventSlideY(self.pointers[v[ID].NAME].ID, y, 0.05, nil)
        end

    end

    return self
end

function FROMOW_MAP_END()
	clearRegistryAchievs()
end

Query.dictionary = nil

dictionary = {
    ['M1'] = {
        add_main = {"Construct a Siberite rocket."},
        add_secondary = {"Protect Hugh Stevens."}
    },
    ['M1a'] = {
        out = {"Protect Hugh Stevens."}
    },
    ['M2'] = {
        add_secondary = {"Stop Y. I. Gorky."}
    },
    ['M2a'] = {
        out = {"Stop Y. I. Gorky."}
    },
    ['M2b'] = {
        out = {"Stop Y. I. Gorky."}
    },
    ['M3'] = {
        out = {"Construct a Siberite rocket."},
        add_main = {"Build a warehouse and a barracks (both upgraded) in the outlined area."},
        add_main = {"The Siberite rocket must not fall into enemy hands."},
        add_secondary = {"Prepare a vehicle with a radar to monitor the explosion."}
    },
    ['M3a'] = {
        out = {"Prepare a vehicle with a radar to monitor the explosion."}
    },
    ['M3b'] = {
        out = {"Prepare a vehicle with a radar to monitor the explosion."}
    },
    ['M4'] = {
        out = {"Build a warehouse and a barracks (both upgraded) in the outlined area."},
        out = {"The Siberite rocket must not fall into enemy hands."},
        add_main = {"Fire the Siberite rocket at the warehouse to test it."},
        out = {"Prepare a vehicle with a radar to monitor the explosion."},
        add_secondary = {"Place a vehicle with a radar on the top of the northern ridge to monitor the warehouse during the explosion."}
    },
    ['M4a'] = {
        out = {"Prepare a vehicle with a radar to monitor the explosion."}
    },
    ['M4b'] = {
        out = {"Prepare a vehicle with a radar to monitor the explosion."}
    },
    ['M5'] = {
        out = {"Fire the Siberite rocket at the warehouse to test it."},
        add_main = {"One scientist must get to the marked area and then return to the Siberite lab."}
    }
}