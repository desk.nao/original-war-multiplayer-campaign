function FROMOW_MAP_START(MAPPATH)
	registryAchievs({'ACH_LS'})
    include('_python')
    AddSingleUseTimer(2,'include("_customQueries")')
    setVisible(MOUSEOVER,true)
    set_Callback(gamewindow.ID,CALLBACK_MOUSEMOVE,'slidePointerTowards(%x,%y)')
    set_Callback(gamewindow.ID,CALLBACK_MOUSECLICK,'if %b == 1 then changeDirection(%x,%y) else OW_CUSTOM_COMMAND(0, '..current_texture..', '..HA.X..', '..HA.Y..', '..current_direction..') end;')
end

function FROMOW_MAP_END()
	clearRegistryAchievs()
end