function FROMOW_MAP_START(MAPPATH)
	registryAchievs({'ACH_EMP'})
end

function FROMOW_MAP_END()
	clearRegistryAchievs()
end

dictionary = {
    ['MStart'] = {
        add_main = {"Prepare an attack on the Arab base New Samarkand."}
    },
    ['MAttack'] = {
        add_main = {"Destroy the New Samarkand base."}
    },
    ['MLeg'] = {
        add_main = {"Destroy the Legion base before they can spread the news."}
    },
    ['MBeh'] = {
        add_secondary = {"Build one Behemoth vehicle."}
    },
    ['MBehOut'] = {
        out = {"Build one Behemoth vehicle."}
    },
    ['MAttOut'] = {
        out = {"Destroy the New Samarkand base."}
    },
    ['MLegOut'] = {
        out = {"Destroy the Legion base before they can spread the news."}
    },
    ['MPrepOut'] = {
        out = {"Prepare an attack on the Arab base New Samarkand."}
    }
}

Query.dictionary = {
    ["QLegionHeike"] = {
        "The Legion’s commander, Heike Steyer, has offered help. How much help depends on the price you are willing to pay:",
        "- 3 men – 50 crates, 10 crystals of Alaskite.",
        "- 5 men – 75 crates, 15 crystals of Alaskite.",
        "- 7 men – 100 crates, 20 crystals of Alaskite.",
        "- Reject the offer."
    },
    ["QLegionFarmer"] = {
        "Legion commander Robert Farmer is offering help. How much help depends on what you’re prepared to pay:",
        "- 3 men – 60 crates, 15 crystals of Alaskite.",
        "- 5 men – 90 crates, 25 crystals of Alaskite.",
        "- 7 men – 120 crates, 30 crystals of Alaskite.",
        "- Reject the offer."
    },
    ["QLegionBergkamp"] = {
        "Legion soldiers are deserting and want to join your ranks. Do you want to:",
        "- Accept them.",
        "- Reject them."
    }
}