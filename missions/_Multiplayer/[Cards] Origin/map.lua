function FROMOW_MAP_START(MAPPATH)
	cards.State = 'Base Building'
	
	include('devmode')
	
	setWH(cards.ELE,cards.CARDWIDTH*12,cards.CARDHEIGHT)
end

AddSingleUseTimer(2,'Starter()')

function Starter()
InitCards()
ENT.STEP(1,ENT.PLAYERS.ME)

--ENT.ERASER = getElementEX(nil,anchorLTRB,XYWH(0,ScrHeight-200,1700,200))
setVisible(game.ui,false)
ENT.BLACK.MAP = getElementEX(nil,anchorLTRB,XYWH(getX(game.ui.minimap.map)-5,getY(game.ui.minimap.map)-5,getWidth(game.ui.minimap.map)+5,getHeight(game.ui.minimap.map)+5),true,{nomouseevent=true,colour1=BLACKA(100)})
end

function CreateFailed()
	setText(ENT.LABEL,'A building already exists here or it is outside your defined area! Please try again.')
end

Preview = getElementEX(nil,anchorLR,XYWH(0,0,158,216),false,{texture='SGUI/Nao/cards/Previews/ArabianWarehouse.png',})	

Preview.Textures = { 
-- AMERICAN 
{
	{ 176, 220, 'b-u-warehouse' },
	{ 84, 82, 'b-u-oilmine' },
	{ 84, 146, 'b-u-oilpower' },
	{ 80, 76, 'b-u-automaticturret' },
	{ 80, 76, 'b-u-automaticturret' }
},
-- RUSSIAN 
{
	{ 158, 216, 'b-r-warehouse' },
	{ 88, 122, 'b-r-oilmine' },
	{ 82, 102, 'b-r-oilpower' },
	{ 72, 98, 'b-r-automaticturret' },
	{ 72, 98, 'b-r-automaticturret' }
},
-- ARABIAN 
{
	{ 138, 208, 'b-a-warehouse' },
	{ 82, 158, 'b-a-oilmine' },
	{ 102, 164, 'b-a-oilpower' },
	{ 82, 72, 'a-a-bunker' },
	{ 82, 72, 'a-a-bunker' }
}

}

Preview.buildings = { 
	-- Preview.buildings[BID][1][1] STRING
	--[[0: Depot]] 
	{ 
		{ {'depot'} }, 
		{ {166,198}, {0,0}, {0,0} }, -- AM
		{ {128,118}, {0,0}, {0,0} }, -- AR
		{ {158,216}, {0,0}, {0,0} }  -- RU
	},

	--[[1: Warehouse]]
	{ 
		{ {'warehouse'} }, 
		{ {176,220}, {0,0}, {0,0} }, -- AM
		{ {128,147}, {0,0}, {0,0} }, -- AR
		{ {164,230}, {0,0}, {0,0} }  -- RU
	},

	--[[2: Workshop]]
	{ 
		{ {'workshop'} }, 
		{ {174,202}, {0,0}, {0,0} }, -- AM
		{ {161,155}, {0,0}, {0,0} }, -- AR
		{ {172,228}, {0,0}, {0,0} }  -- RU
	},
	
	--[[3: Factory]]
	{ 
		{ {'factory'} }, 
		{ {172,214}, {0,0}, {0,0} }, -- AM
		{ {159,180}, {0,0}, {0,0} }, -- AR
		{ {174,198}, {0,0}, {0,0} }  -- RU
	},
	
	--[[4: Armoury]]
	{ 
		{ {'armoury'} }, 
		{ {136,128}, {0,0}, {0,0} }, -- AM
		{ {157,135}, {0,0}, {0,0} }, -- AR
		{ {125,107}, {0,0}, {0,0} }  -- RU
	},
	
	--[[5: Barracks]]
	{ 
		{ {'barracks'} }, 
		{ {138,152}, {0,0}, {0,0} }, -- AM
		{ {134,131}, {0,0}, {0,0} }, -- AR
		{ {124,124}, {0,0}, {0,0} }  -- RU
	},
	
	--[[6: Laboratory]]
	{ 
		{ {'basiclab'} }, 
		{ {142,172}, {0,0}, {0,0} }, -- AM
		{ {116,146}, {0,0}, {0,0} }, -- AR
		{ {122,186}, {0,0}, {0,0} }  -- RU
	},
	
	--[[7: Half Lab.]]
	{ 
		{ {'bridgeupg'} }, 
		{ {78,148}, {0,0}, {0,0} }, -- AM
		{ {69,104}, {0,0}, {0,0} }, -- AR
		{ {72,120}, {0,0}, {0,0} }  -- RU
	},
	
	--[[8: Full Lab.]]
	{ 
		{ {'bridgeupg'} }, 
		{ {78,148}, {0,0}, {0,0} }, -- AM
		{ {69,104}, {0,0}, {0,0} }, -- AR
		{ {72,120}, {0,0}, {0,0} }  -- RU
	},
	
	--[[9: Basic Lab.]]
	{ 
		{ {'basiclab'} }, 
		{ {142,172}, {0,0}, {0,0} }, -- AM
		{ {116,146}, {0,0}, {0,0} }, -- AR
		{ {122,186}, {0,0}, {0,0} }  -- RU
	},

	--[[10: Weapon L.]]
	{ 
		{ {'weaponlab'} }, 
		{ {134,208}, {0,0}, {0,0} }, -- AM
		{ {117,148}, {0,0}, {0,0} }, -- AR
		{ {138,176}, {0,0}, {0,0} }  -- RU
	},
	
	--[[11: Siberite L.]]
	{ 
		{ {'siberiumlab'} }, 
		{ {134,206}, {0,0}, {0,0} }, -- AM
		{ {123,145}, {0,0}, {0,0} }, -- AR
		{ {128,174}, {0,0}, {0,0} }  -- RU
	},
	
	--[[12: Computer L.]]
	{ 
		{ {'computerlab'} }, 
		{ {172,214}, {0,0}, {0,0} }, -- AM
		{ {0,0}, {0,0}, {0,0} }, -- AR
		{ {122,184}, {0,0}, {0,0} }  -- RU
	},
	
	--[[13: Biological L.]]
	{ 
		{ {'biologicallab'} }, 
		{ {138,190}, {0,0}, {0,0} }, -- AM
		{ {138,190}, {0,0}, {0,0} }, -- AR
		{ {138,190}, {0,0}, {0,0} }  -- RU
	},

	--[[14: Space-time L.]]
	{ 
		{ {'spacetimelab'} }, 
		{ {132,204}, {0,0}, {0,0} }, -- AM
		{ {132,204}, {0,0}, {0,0} }, -- AR
		{ {132,204}, {0,0}, {0,0} }  -- RU
	},

	--[[15: Opto-electronics]]
	{ 
		{ {'optolab'} }, 
		{ {146,196}, {0,0}, {0,0} }, -- AM
		{ {127,147}, {0,0}, {0,0} }, -- AR
		{ {0,0}, {0,0}, {0,0} }  -- RU
	},
	
	--[[16: Ext. Tracks]]
	{ 
		{ {'trackext'} }, 
		{ {112,136}, {0,0}, {0,0} }, -- AM
		{ {117,84}, {0,0}, {0,0} }, -- AR
		{ {112,134}, {0,0}, {0,0} }  -- RU
	},
	
	--[[17: Ext. Guns]]
	{ 
		{ {'gunext'} }, 
		{ {130,124}, {0,0}, {0,0} }, -- AM
		{ {115,102}, {0,0}, {0,0} }, -- AR
		{ {118,104}, {0,0}, {0,0} }  -- RU
	},
	
	--[[18: Ext. Rockets]]
	{ 
		{ {'rocketext'} }, 
		{ {114,122}, {0,0}, {0,0} }, -- AM
		{ {110,87}, {0,0}, {0,0} }, -- AR
		{ {110,100}, {0,0}, {0,0} }  -- RU
	},

	--[[19: Ext. Civilian]]
	{ 
		{ {'non-combatext'} }, 
		{ {112,112}, {0,0}, {0,0} }, -- AM
		{ {115,95}, {0,0}, {0,0} }, -- AR
		{ {104,94}, {0,0}, {0,0} }  -- RU
	},
	
	--[[20: Ext. Radar]]
	{ 
		{ {'radarext'} }, 
		{ {110,136}, {0,0}, {0,0} }, -- AM
		{ {101,79}, {0,0}, {0,0} }, -- AR
		{ {110,102}, {0,0}, {0,0} }  -- RU
	},
	
	--[[21: Ext. Siberite]]
	{ 
		{ {'siberiumext'} }, 
		{ {114,112}, {0,0}, {0,0} }, -- AM
		{ {108,94}, {0,0}, {0,0} }, -- AR
		{ {116,112}, {0,0}, {0,0} }  -- RU
	},
	
	--[[22: Ext. Radio]]
	{ 
		{ {'radioext'} }, 
		{ {114,118}, {0,0}, {0,0} }, -- AM
		{ {114,70}, {0,0}, {0,0} }, -- AR
		{ {0,0}, {0,0}, {0,0} }  -- RU
	},
	
	--[[23: Ext. Stitch]]
	{ 
		{ {'solarext'} }, 
		{ {112,112}, {0,0}, {0,0} }, -- AM
		{ {113,91}, {0,0}, {0,0} }, -- AR
		{ {0,0}, {0,0}, {0,0} }  -- RU
	},

	--[[24: Ext. Computer]]
	{ 
		{ {'computerext'} }, 
		{ {110,150}, {0,0}, {0,0} }, -- AM
		{ {0,0}, {0,0}, {0,0} }, -- AR
		{ {112,102}, {0,0}, {0,0} }  -- RU
	},

	--[[25: Ext. Laser]]
	{ 
		{ {'laserext'} }, 
		{ {114,130}, {0,0}, {0,0} }, -- AM
		{ {0,0}, {0,0}, {0,0} }, -- AR
		{ {0,0}, {0,0}, {0,0} }  -- RU
	},

	--[[26: Oil Power]]
	{ 
		{ {'oilpower'} }, 
		{ {84,146}, {0,0}, {0,0} }, -- AM
		{ {98,103}, {0,0}, {0,0} }, -- AR
		{ {82,102}, {0,0}, {0,0} }  -- RU
	},

	--[[27: Solar Power]]
	{ 
		{ {'solarpower'} }, 
		{ {76,78}, {0,0}, {0,0} }, -- AM
		{ {81,92}, {0,0}, {0,0} }, -- AR
		{ {0,0}, {0,0}, {0,0} }  -- RU
	},
	
	--[[28: Siberite Power]]
	{ 
		{ {'siberiumpower'} }, 
		{ {72,150}, {0,0}, {0,0} }, -- AM
		{ {79,93}, {0,0}, {0,0} }, -- AR
		{ {92,114}, {0,0}, {0,0} }  -- RU
	},
	
	--[[29: Oil Mine]]
	{ 
		{ {'oilmine'} }, 
		{ {84,82}, {0,0}, {0,0} }, -- AM
		{ {79,93}, {40,156}, {24,-11} }, -- AR
		{ {88,122}, {0,0}, {0,0} }  -- RU
	},

	--[[30: Siberite Mine]]
	{ 
		{ {'siberiummine'} }, 
		{ {84,74}, {0,0}, {0,0} }, -- AM
		{ {0,0}, {0,0}, {0,0} }, -- AR
		{ {84,76}, {0,0}, {0,0} }  -- RU
	},
	
	--[[31: Breastworks]]
	{ 
		{ {'breastworks'} }, 
		{ {94,110}, {0,0}, {0,0} }, -- AM
		{ {74,86}, {0,0}, {0,0} }, -- AR
		{ {86,108}, {0,0}, {0,0} }  -- RU
	},
	
	--[[32: Bunker]]
	{ 
		{ {'bunker'} }, 
		{ {88,76}, {0,0}, {0,0} }, -- AM
		{ {82,66}, {0,0}, {0,0} }, -- AR
		{ {86,70}, {0,0}, {0,0} }  -- RU
	},
	
	--[[33: Turret]]
	{ 
		{ {'automaticturret'} }, 
		{ {80,76}, {0,0}, {0,0} }, -- AM
		{ {0,0}, {0,0}, {0,0} }, -- AR
		{ {72,60}, {0,0}, {0,0} }  -- RU
	},

	--[[34: Teleport]]
	{ 
		{ {'teleport'} }, 
		{ {138,154}, {0,0}, {0,0} }, -- AM
		{ {138,154}, {0,0}, {0,0} }, -- AR
		{ {138,154}, {0,0}, {0,0} }  -- RU
	},

	--[[35: Fort]]
	{ 
		{ {'bunker-s'} }, 
		{ {84,120}, {0,0}, {0,0} }, -- AM
		{ {84,120}, {0,0}, {0,0} }, -- AR
		{ {84,120}, {0,0}, {0,0} }  -- RU
	},

	--[[36: Control Tower]]
	{ 
		{ {'ct'} }, 
		{ {142,194}, {0,0}, {0,0} }, -- AM
		{ {142,194}, {0,0}, {0,0} }, -- AR
		{ {142,194}, {0,0}, {0,0} }  -- RU
	},

	--[[37: Behemoth]]
	{ 
		{ {'behemoth'} }, 
		{ {148,142}, {0,0}, {0,0} }, -- AM
		{ {148,142}, {0,0}, {0,0} }, -- AR
		{ {148,142}, {0,0}, {0,0} }  -- RU
	},

	--[[38: Eon]]
	{ 
		{ {'eon'} }, 
		{ {48,128}, {0,0}, {0,0} }, -- AM
		{ {48,128}, {0,0}, {0,0} }, -- AR
		{ {48,128}, {0,0}, {0,0} }  -- RU
	},

	--[[39: Alien Tower]]
	{ 
		{ {'alien'} }, 
		{ {200,270}, {0,0}, {0,0} }, -- AM
		{ {200,270}, {0,0}, {0,0} }, -- AR
		{ {200,270}, {0,0}, {0,0} }  -- RU
	}
					
}

ENT.DEPOTS = {}
ENT.PLAYERS = {}
ENT.COL = {RGB(100,100,255), RGB(255,255,100), RGB(255,100,100), RGB(100,255,255), RGB(255,125,100), RGB(255,100,255), RGB(100,255,100), RGB(255,255,255)}
function VerifyResources(DEPOT, CRATES, OIL, ALASKITE, SIDE)
	if DEPOT == 1 then ENT.DEPOTS[1][1] = CRATES ENT.DEPOTS[1][2] = OIL ENT.DEPOTS[1][3] = ALASKITE ENT.DEPOTS[1][4] = SIDE end
	if DEPOT == 2 then ENT.DEPOTS[2][1] = CRATES ENT.DEPOTS[2][2] = OIL ENT.DEPOTS[2][3] = ALASKITE ENT.DEPOTS[2][4] = SIDE end	
	if DEPOT == 3 then ENT.DEPOTS[1][1] = CRATES ENT.DEPOTS[1][2] = OIL ENT.DEPOTS[1][3] = ALASKITE end
	if DEPOT == 4 then ENT.DEPOTS[2][1] = CRATES ENT.DEPOTS[2][2] = OIL ENT.DEPOTS[2][3] = ALASKITE end	
end

if ENT.CONSOLE == 0 then 
	ENT.PLAYERS = {1, 2}
	ENT.PLAYERS.ME = 1
	ENT.PLAYERS.MY_LOCATION = 2
end

if ENT.CONSOLE == 1 then -- REVISE
	ENT.PLAYERS 	= {MULTI_PLAYERINFO_CURRENT_PLID[1].COLOUR,MULTI_PLAYERINFO_CURRENT_PLID[2].COLOUR} -- REVISE
	if ENT.PLAYERS[1] < ENT.PLAYERS[2] then ENT.PLAYERS.ME = 1 end -- REVISE
	if ENT.PLAYERS[1] > ENT.PLAYERS[2] then ENT.PLAYERS.ME = 2 end -- REVISE
	--ENT.PLAYERS.ME  = MULTI_PLAYERINFO_CURRENT_PLID[MyID].COLOUR -- REVISE
	
end

ENT.TITLE = getElementEX(nil,anchorLTRB,XYWH(200,-150+ScrHeight/2,ScrWidth-400,45),true,{nomouseevent=true,colour1=ENT.COL[ENT.PLAYERS.ME]})

ENT.LABEL = getLabelEX(ENT.TITLE,anchorNone,XYWH(0,10,ScrWidth-400,25),Tahoma_16B,'',{font_colour=ENT.COL[8],font_style_outline=true,shadowtext=true,text_halign=ALIGN_MIDDLE})

ENT.BLACK 		= getElementEX(nil,anchorLTRB,XYWH(0,ScrHeight-192-50,ScrWidth,50),true,{nomouseevent=true,colour1=BLACKA(150)})
ENT.BLACK.LABEL = getLabelEX(nil,anchorNone,XYWH(0,ScrHeight-192-50,ScrWidth,50),Tahoma_16B,'Sends x4 soldiers, x5 Heavy Guns.',{font_colour=ENT.COL[8],font_style_outline=true,shadowtext=true,text_halign=ALIGN_MIDDLE,nomouseevent=true})

ENT.RESOURCES	= getElementEX(nil,anchorLTRB,XYWH(25,ScrHeight-192-50-145-25,200,145),true,{nomouseevent=true,colour1=BLACKA(100)})

ENT.RESOURCES.CRATES	= getElementEX(ENT.RESOURCES,anchorLTRB,XYWH(2.5,2.5,45,45),true,{nomouseevent=true,texture='SGUI/crate.png'})
ENT.RESOURCES.OIL		= getElementEX(ENT.RESOURCES,anchorLTRB,XYWH(2.5,2.5*2+45,45,45),true,{nomouseevent=true,texture='SGUI/oil.png'})
ENT.RESOURCES.SIBERITE	= getElementEX(ENT.RESOURCES,anchorLTRB,XYWH(2.5,2.5*3+45*2,45,45),true,{nomouseevent=true,texture='SGUI/sib.png'})

ENT.RESOURCES.CRATES.LABEL   = getLabelEX(ENT.RESOURCES.CRATES,anchorNone,XYWH(45+2.5,45/2-12.5,0,25),Tahoma_16B,'',{font_colour=ENT.COL[8],font_style_outline=true,shadowtext=true,text_halign=ALIGN_LEFT,nomouseevent=true})

ENT.RESOURCES.OIL.LABEL 	 = getLabelEX(ENT.RESOURCES.OIL,anchorNone,XYWH(45+2.5,45/2-12.5,0,25),Tahoma_16B,'',{font_colour=ENT.COL[8],font_style_outline=true,shadowtext=true,text_halign=ALIGN_LEFT,nomouseevent=true})

ENT.RESOURCES.SIBERITE.LABEL = getLabelEX(ENT.RESOURCES.SIBERITE,anchorNone,XYWH(45+2.5,45/2-12.5,0,25),Tahoma_16B,'',{font_colour=ENT.COL[8],font_style_outline=true,shadowtext=true,text_halign=ALIGN_LEFT,nomouseevent=true})

-- setText(ENT.BLACK.LABEL, cards.buildings[1][1] )
setNoMouseEvent(ENT.BLACK,true) 
setNoMouseEvent(ENT.BLACK.LABEL,true)

ENT.EXPORT = {}

function detectPlayers(left,right)
	ENT.PLAYERS.POS = {left, right}
end

ENT.LABELS = {
	{   'Start by creating your warehouse: right-click to rotate the building, left-click to place it on the map.',
		'Well done! Now, place an oil drilling tower on the map.',
		'Wonderful, you may now create an oil power plant.',
		"You're almost done: choose where to put your first defensive turret.",
		'And finally, choose another location for the second defensive turret.'
	},
		{1--[[WAREHOUSE]],29--[[OIL MINE]],26--[[OIL POWER]],33--[[TURRET]],33--[[TURRET]]}
	}

function ENT.STEP(STEP,COLOUR)

if STEP == 2 then OW_CUSTOM_COMMAND(885) end

if STEP < 6 then

	setText(ENT.LABEL,ENT.LABELS[1][STEP])

	-- SHADER
	TESTTEXTURE = loadOGLTexture('SGUI/Nao/buildings/'..Preview.Textures[interfaceSide][STEP][3]..'0000.png',true,false,false,false)
	setSubCoords(MOUSEOVER,SUBCOORD(0,640-TESTTEXTURE.H,TESTTEXTURE.W,TESTTEXTURE.H))
	setWH(MOUSEOVER,TESTTEXTURE.W,TESTTEXTURE.H)


	set_Callback(gamewindow.ID,CALLBACK_MOUSECLICK,'if %b == 1 then ClickPreview(1,%b,'..interfaceSide..','..STEP..') end if %b == 0 then sub_CallPreview(1,%x,%y,'..COLOUR..','..STEP..') end') 

	set_Callback(gamewindow.ID,CALLBACK_MOUSEMOVE,'Render(%x,%y)')

	ENT.EXPORT = {interfaceSide, STEP}

	expPreview = 'SGUI/Nao/buildings/'..Preview.Textures[2][STEP][3]..'.png'
end

if STEP == 6 then
	
	cards.State = 'Game'
	
	setVisible(MOUSEOVER,false)
	setVisible(ENT.TITLE,false)
	
	set_Callback(gamewindow.ID,CALLBACK_MOUSECLICK,'') 
	set_Callback(gamewindow.ID,CALLBACK_MOUSEMOVE,'')
	
	setText(ENT.LABEL,'')
	cards.addCards(10)
	AddSingleUseTimer(0.5,'for i = 5,10 do setVisible(cards.LIST[i].ELE) setVisible(cards.LIST[i].ELE.delete) end')
	cards.ANIMTIME = 0.5 cards.ANIMMOVETIME=0.35

end
		
end

Temp = 1

function CallPreview(STATE, nation, texture, SIDE, STEP)  
-- CallPreview(1,interfaceSide,STEP+1,COLOUR)
OW_CUSTOM_COMMAND(994,1)
Temp = 1
Preview.animtime = 100

	if STATE == 10 then 
		set_Callback(gamewindow.ID,CALLBACK_MOUSECLICK,'if %b == 1 then ClickPreview(2,%b,'..nation..','..texture..') end if %b == 0 then sub_CallPreview(2,%x,%y,'..SIDE..',10) end') 
		set_Callback(gamewindow.ID,CALLBACK_MOUSEMOVE,'AttachPreview(%x,%y,'..ENT.PLAYERS.ME..')') 
				
		setVisible(Preview,true) 

		setTexture(Preview,'SGUI/Nao/buildings/b-u-'..Preview.buildings[texture+1][nation][1][1]..'0000.png')
		
		--[BTYPE][NATION+1][1][1] [BTYPE][NATION+1][1][2] [BTYPE][NATION+1][1][1] X Y STRING
		
		ENT.EXPORT = {nation, texture}
		
		setWH(Preview,Preview.buildings[texture+1][nation+1][1][1],Preview.buildings[texture+1][nation+1][1][2])
	end
	
end

function Render(a, b)
FHA = OW_FINDHEX(a,b) 
	if FHA.FOUND == true then
		HA = OW_HEXTOSCREEN(FHA.X,FHA.Y) 
		--setXY(MOUSEOVER,HA.X-OW_GETMVRXY(HA.X,HA.Y).X-getWidth(MOUSEOVER)/2,HA.Y-OW_GETMVRXY(HA.X,HA.Y).Y-getHeight(MOUSEOVER)/2)
		AddEventSlideX(MOUSEOVER.ID,HA.X-OW_GETMVRXY(HA.X,HA.Y).X-getWidth(MOUSEOVER)/2,0.1)
		setText(ENT.LABEL,'W: '..TESTTEXTURE.W..'  '..'H: '..TESTTEXTURE.H)
		AddEventSlideY(MOUSEOVER.ID,HA.Y-OW_GETMVRXY(HA.X,HA.Y).Y-getHeight(MOUSEOVER)/2,0.1)
	end
end

Preview.animtime = 100

function Preview.tick(FRAMETIME)
	if (Preview.animtime > 0) then
		Preview.animtime = Preview.animtime - FRAMETIME*200
		setText(ENT.BLACK.LABEL,Preview.animtime)
			if Preview.animtime < 0 then
				Preview.animtime = 0
			end
			if Preview.animtime > 100 then
				-- nothing
			else
				setRotation(Preview,SetRotate(1,Preview.animtime,false))
			end
	end
end

regTickCallback('Preview.tick(%frametime)')

nations = {1, 3, 2}

function sub_CallPreview(STATE,X,Y,SIDE,STEP)
	CollectHexagons(SIDE,X,Y)
	OW_CUSTOM_COMMAND(999,SIDE)
	--OW_CUSTOM_COMMAND(997,SIDE) -- create building  
	if STATE == 1 then OW_CUSTOM_COMMAND(90000,ENT.LABELS[2][STEP],STEP,nations[interfaceSide]) end
	if STATE == 2 then OW_CUSTOM_COMMAND(90000,ENT.EXPORT[2],STEP,nations[interfaceSide]) end
	if STATE == 3 then 
		OW_CUSTOM_COMMAND(90000,STEP,99,nations[interfaceSide]) 
	end
end

function HidePreview()
	setVisible(MOUSEOVER,false)
	setVisible(ENT.TITLE,false)
		
	set_Callback(gamewindow.ID,CALLBACK_MOUSECLICK,'') 
	set_Callback(gamewindow.ID,CALLBACK_MOUSEMOVE,'')
end

H= {}
function CollectHexagons(SIDE, X, Y)
FH = OW_FINDHEX(X,Y) H = OW_HEXTOSCREEN(FH.X,FH.Y) 
	if FH.FOUND == true then OW_CUSTOM_COMMAND(998,FH.X,FH.Y,SIDE) end
	PosMap = {FH.X,FH.Y}
end

function ClickPreview(STATE,BUTTON,NATION,TEXTURE)
	if BUTTON == 1 then -- RIGHT
		Temp = Temp +1
		if Temp == 7 then Temp = 1 end
			if STATE == 1 then
				TESTTEXTURE = loadOGLTexture('SGUI/Nao/buildings/'..Preview.Textures[interfaceSide][TEXTURE][3]..'000'..(Temp-1)..'.png',true,false,false,false)
				--setTexture(Preview,'SGUI/Nao/buildings/'..Preview.Textures[interfaceSide][TEXTURE][3]..'000'..(Temp-1)..'.png')
			end
			if STATE == 2 then
				--setTexture(Preview,'SGUI/Nao/buildings/b-u-'..Preview.buildings[TEXTURE+1][NATION][1][1]..'000'..(Temp-1)..'.png')
				TESTTEXTURE = loadOGLTexture('SGUI/Nao/buildings/b-u-'..Preview.Textures[TEXTURE+1][NATION][1][1]..'000'..(Temp-1)..'.png',true,false,false,false)
			end
			if STATE == 3 then
				--setTexture(Preview,'SGUI/Nao/buildings/b-u-'..Preview.buildings[TEXTURE+1][NATION][1][1]..'000'..(Temp-1)..'.png')
				TESTTEXTURE = loadOGLTexture(ENT.EXPORT[2]..(Temp-1)..'.png',true,false,false,false)
				setText(ENT.BLACK.LABEL,ENT.EXPORT[2]..(Temp-1)..'.png')
				-- 'SGUI/b'..data[interfaceSide]..Preview.buildings[cards.LIST[ID].CARD.SAILIDENT+1][1][1][1]..'0000.png
			end
		OW_CUSTOM_COMMAND(994,Temp)
	end
end

AddSingleUseTimer(1,"DisplayResources()")

function DisplayResources()
	setText(ENT.RESOURCES.CRATES.LABEL,'Crates: '..ENT.DEPOTS[ENT.PLAYERS.ME][1]) -- ENT.DEPOTS[ENT.PLAYERS.ME]
	setText(ENT.RESOURCES.OIL.LABEL,'Oil: '..ENT.DEPOTS[ENT.PLAYERS.ME][2])
	setText(ENT.RESOURCES.SIBERITE.LABEL,'Siberite: '..ENT.DEPOTS[ENT.PLAYERS.ME][3])
	AddSingleUseTimer(1,"DisplayResources()")
end

setText(ENT.RESOURCES.CRATES.LABEL,ENT.PLAYERS.ME)