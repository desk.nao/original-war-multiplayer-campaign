function FROMOW_MAP_START(MAPPATH)
	registryAchievs({'ACH_LS'})
    sync = createSync()
    sync.init()
    include('_python')
    regTickCallback('sync.tick(%frametime)')
end

function FROMOW_MAP_END()
	clearRegistryAchievs()
end

createSync = function ()
    local self = { localPos={}, lastTick= 0, temp = {}, MVR = nil, pointers = {}, me = nil }

    self.colours = {RGB(0,0,255), RGB(255,255,0), RGB(255,0,0), RGB(0,255,255), RGB(255,125,0), RGB(255,0,255), RGB(0,255,0), RGB(255,255,255) }

    self.init = function ()
        local players = MULTI_PLAYERINFO_CURRENT_PLID
        for i, v in pairs(players) do
            if v.ALIVE then
                if v.NAME == MULTI_PLAYERINFO_CURRENT_PLID[MyID].NAME then
                    self.me = v.NAME
                end
                self.pointers[v.NAME] = getElementEX(gamewindow.overlay,anchorLTRB,XYWH(math.random(200,600),math.random(200,250),32,32),true,{nomouseevent=true,texture='Pointer.png',colour1=self.colours[v.COLOUR]})
                self.pointers[v.NAME].label = getLabelEX(self.pointers[v.NAME],anchorNone,XYWH(0,-35,120,74),Tahoma_16B,v.NAME,{font_colour=self.colours[v.COLOUR],font_style_outline=true,shadowtext=true,text_halign=ALIGN_MIDDLE})
            end
        end
        setVisible(self.pointers[self.me],false)
    end

    self.tick = function (FRAMETIME)

        if (OW_GAME_TICK-self.lastTick >= 4) then
            self.lastTick = OW_GAME_TICK
            self.temp = SGUI_getmousexy()
            self.MVR = OW_GETMVRXY()
            self.localPos = {self.MVR.X + self.temp.X, self.MVR.Y + self.temp.Y}
            OW_CUSTOM_COMMAND_SGUI(999, MyID, self.localPos[1], self.localPos[2])
        end

    end

    self.updatePos = function (ID, x, y)
        local v = MULTI_PLAYERINFO_CURRENT_PLID
        if v[ID].NAME == v[MyID].NAME then
            return
        else
            AddEventSlideX(self.pointers[v[ID].NAME].ID, x, 0.05, nil)
            AddEventSlideY(self.pointers[v[ID].NAME].ID, y, 0.05, nil)
        end

    end

    return self
end

Query.dictionary = {
    ["Q1"] = {
        "Powell is more hysterical than normal. He seems to be losing his grip of the situation. Do you want to:",
        "- Ask him about the Siberite Motherlode.",
        "- Ask him about Russians.",
        "- Ask him about Legion.",
        "- Ask him about Arabs.",
        "- Ask nothing, accept the orders.",
        "- Suggest that you take over command."
    },
    ["Q1a"] = {
        "Powell is not going to tell you any more.",
        "- Ask no more, accept the orders.",
        "- Suggest that you take over command."
    },
    ["Q9"] = {
        "Roth wants you to answer him. What will you do:",
        "- Keep silent.",
        "- Tell him to go to hell.",
        "- Tell him that there is nothing you can do."
    },
    ["Q14"] = {
        "Powell's ‘friend’ from Legion offers you his services. Will you:",
        "- Accept him.",
        "- Reject him.",
        "- Have him killed."
    },
    ["Q15a"] = {
        "The Russians invented a siberite rocket. Will you:",
        "- Surrender.",
        "- Ask for five minutes to think about it.",
        "- Ignore them.",
        "- Tell them you have a rocket too!"
    }
}

dictionary = {
    ['M1'] = {
        add_main = {"Defend the cliff over Powell's base."},
        out = {"Kill general Platonov.", "Capture at least 4 bases in the area."}
    },
    ['M2'] = {
        out = {"Defend the cliff over Powell's base."},
        add_main = {"Gain control of siberite Motherlode."}
    },
    ['M3'] = {
        add_secondary = {"Capture the Alliance leader Peter Roth."}
    },
    ['M3a'] = {
        out = {"Capture the Alliance leader Peter Roth."}
    },
    ['M3b'] = {
        out = {"Capture the Alliance leader Peter Roth."}
    },
    ['Mlegion'] = {
        add_secondary = {"Destroy Legion before they manage to build the Siberite rocket."}
    },
    ['MlegionOut'] = {
        out = {"Destroy Legion before they manage to build the Siberite rocket."}
    },
    ['MlegionDel'] = {
        out = {"Destroy Legion before they manage to build the Siberite rocket."}
    },
    ['M4'] = {
        add_secondary = {"Try to cancel the Russian secret project."}
    },
    ['M4a'] = {
        out = {"Try to cancel the Russian secret project."}
    },
    ['M4b'] = {
        out = {"Try to cancel the Russian secret project."}
    },
    ['M5'] = {
        add_main = {"Kill sheikh Omar. Protect the motherlode."}
    }
}