function Init()
	--include('cards_module')

	--ChangeMainMenu()

    setText(logotext, "Who thinks outside the box\npacks the best packages")
    setY(logotext, getY(logotext) + 25)

	include("devmode")
	--include("_queries")
    include("_team_select")
    --include('_McSkirmish')
    
    include('Utils/fast2D','Utils/OpenGL')

    --AddSingleUseTimer(2,'include("_objectives")')
    --AddSingleUseTimer(3,"include('tv_lines')")
end

function ChangeMainMenu()
	sgui_delete(menu.side.grad.ID)

	setXY(logo, (LayoutWidth/2)-(getWidth(logo)/2), getY(logo)+100)

	setParent(menu.side.profilebar, nil)
	setColour1(menu.side.profilebar, BLACKA(100))

	setParent(profile_button, profilebar)

	setColour1(profile_button, BLACKA(100))
	setY(profile_button, getHeight(menu.side.profilebar)+2.5)
	--setHeight(profilebar, getHeight(profilebar)+2.5+getHeight(profile_button))

	setVisible(profile_button, false)

	setX(menu.side, (LayoutWidth/2)-(290/2))
	sgui_delete(menu.side.main.tutorial.ID)
	sgui_delete(menu.side.main.campaign.ID)
	sgui_delete(menu.side.main.skirmish.ID)
	sgui_delete(menu.side.main.credits.ID)

	setXY(menu.side, getX(menu.side), getY(logo)+getHeight(logo)+15)
	setWH(menu.side, getWidth(menu.side), 290)

	setColour1(menu.side, BLACKA(0))
	setWidth(menu.side.main, getWidth(menu.side.main)+4)

	local height = 5 + getHeight(menu.side.main.multiplayer)
	setXY(menu.side.main.multiplayer, 2, 0)
	setXY(menu.side.main.achievs, 2, height)
	setXY(menu.side.main.mods, 2, height*2)
	setXY(menu.side.main.options, 2, height*3)
	setXY(menu.side.main.quit, 2, height*4)

	setColour1(menu.side.main.multiplayer, BLACKA(200))
	setColour1(menu.side.main.achievs, BLACKA(200))
	setColour1(menu.side.main.mods, BLACKA(200))
	setColour1(menu.side.main.options, BLACKA(200))
	setColour1(menu.side.main.quit, BLACKA(200))
end

function removeFromName(tbl, value)
    for i = #tbl, 1, -1 do
        if tbl[i] == value then
            table.remove(tbl, i)
            return true
        end
    end
    return false
end

function removeFromNameAndDestroy(tbl, tbl2, value)
    for i = #tbl, 1, -1 do
        if tbl[i] == value then
            table.remove(tbl, i)
            local destroy = tbl2[i]
            sgui_delete(destroy.ID)
            table.remove(tbl2, i)
            return true
        end
    end
    return false
end

function containsValue(array, value)
    for i = 1, #array do
        if array[i] == value then
            return true
        end
    end
    return false
end

function getUnitTexture(ID)
  local r = OW_GET_UNIT_TEXTURE(ID)
  if r ~= nil then
    return r.TEXTURE
  else
    return 0
  end
end

function OnToolbarClick(ID)
         switch(ID) : caseof {
            [1]   = function (x) OW_TOOLBARBUTTON(ID); end, -- Menu
            [2]   = function (x) if getvalue(OWV_MULTIPLAYER) then display_diplomacy(); end; end,          -- Team Review
            [3]   = function (x) ShowDialog(dialog.objectives) end, -- Objectives / Diplomacy
            [4]   = function (x) dialog.options.Show(); end, -- Options
            [5]   = function (x) end, -- Help
            [6]   = function (x) showIGAchievs() end,
              }
end