teamselectCreate = function()
	local self = {aliveCounter=0,real_data={},getUnitData={},units={},unit_count=0,prof_count={["Soldier"]=0,["Engineer"]=0,["Mechanic"]=0,["Scientist"]=0}}
	self.colours = {RGB(100,100,255), RGB(255,255,100), RGB(255,100,100), RGB(100,255,255), RGB(255,125,100), RGB(255,100,255), RGB(100,255,100), RGB(255,255,255)}

	self.init = function()

		self.element = AddSkinEle_Tex(getElement(dialog.back,anchorNone,1024,768,false),'1024team_select.png')
		self.element.selected = {}

		self.element.caption = getLabelEX(self.element,anchorLTR,XYWH(11+40,30,274-80,39),Trebuchet_20,loc(TID_Other_Team_select_Title),{shadowtext=true,wordwrap=true,font_colour=RGB(200,50,50),text_halign=ALIGN_MIDDLE,nomouseevent=true,})
		self.display = getElementEX(self.element, anchorLTR, XYWH(5,345,284,277), true, {colour1 = BLACKA(200)})

		self.element.ui_hired_forced = getScrollboxEX(self.element,anchorNone,XYWH(673-318,264-176,318,176),{colour1 = BLACKA(200), c=0})
		self.element.ui_hired_forced_scrollbar = getScrollBarEX(self.element,anchorNone,XYWH(self.element.ui_hired_forced.width+getX(self.element.ui_hired_forced)-12,2+getY(self.element.ui_hired_forced),12,self.element.ui_hired_forced.height-4),'scrollbar.png',self.element.ui_hired_forced,{colour1=Scrollbar_Colour1,colour2=Scrollbar_Colour2,},{});

		self.element.ui_their_commanders = getScrollboxEX(self.element,anchorNone,XYWH(673+3,264-176,315,176),{colour1 = BLACKA(200), c=0})
		self.element.ui_their_commanders_scrollbar = getScrollBarEX(self.element,anchorNone,XYWH(self.element.ui_their_commanders.width+getX(self.element.ui_their_commanders)-12,2+getY(self.element.ui_their_commanders),12,self.element.ui_their_commanders.height-4),'scrollbar.png',self.element.ui_their_commanders,{colour1=Scrollbar_Colour1,colour2=Scrollbar_Colour2,},{});

		self.element.ui_fired = getScrollboxEX(self.element,anchorNone,XYWH(673+3,264+3,315,429),{colour1 = BLACKA(200), c=0})
		self.element.ui_fired_scrollbar = getScrollBarEX(self.element,anchorNone,XYWH(self.element.ui_fired.width+getX(self.element.ui_fired)-12,2+getY(self.element.ui_fired),12,self.element.ui_fired.height-4),'scrollbar.png',self.element.ui_fired,{colour1=Scrollbar_Colour1,colour2=Scrollbar_Colour2,},{});

		self.element.ui_hired = getScrollboxEX(self.element,anchorNone,XYWH(673-318,264+3,318,429),{colour1 = BLACKA(200), c=0})
		self.element.ui_hired_scrollbar = getScrollBarEX(self.element,anchorNone,XYWH(self.element.ui_hired.width+getX(self.element.ui_hired)-12,2+getY(self.element.ui_hired),12,self.element.ui_hired.height-4),'scrollbar.png',self.element.ui_hired,{colour1=Scrollbar_Colour1,colour2=Scrollbar_Colour2,},{});

		ChangeInterface(1)
		AddSingleUseTimer(0.1,'ChangeInterface(2)')
		ShowDialog2(self.element, true)
		self.makeTeamSelectSortButtons('dialog.team_select.doSort()')

		self.element.ok = getImageButtonEX(self.element,anchorRB,XYWH(891,716,77,27),'','','OW_CUSTOM_COMMAND_SGUI(802)',SKINTYPE_TEXTURE,{SKINTEXTURE='button-ok.png',hint=loc(TID_msg_Ok)})

		self.element.hire   = getImageButtonEX(self.element,anchorLB,XYWH(564,716,77,27),'','','',SKINTYPE_TEXTURE,{enabled=false, SKINTEXTURE='button-hire.png'})
		self.element.fire   = getImageButtonEX(self.element,anchorRB,XYWH(706,716,77,27),'','','',SKINTYPE_TEXTURE,{enabled=false, SKINTEXTURE='button-fire.png'})
		self.element.cancel = getImageButtonEX(self.element,anchorRB,XYWH(379,716,77,27),'','','OW_TEAMSELECT_CANCEL(dialog.team_select.FORMID)',SKINTYPE_TEXTURE,{SKINTEXTURE='button-cancel.png',hint=loc(TID_msg_Cancel)})
	   
	  if inMultiplayer == true then
		  local players = MULTI_PLAYERINFO_CURRENT_PLID
			for i, v in pairs(players) do
				if v.ALIVE then 
					self.aliveCounter = self.aliveCounter +1
					self.element.selected["'"..v.NAME.."'"] = getElementEX(self.element, anchorLTR, XYWH(0,0,58,72), true, {nomouseevent=true,texture='SGUI/TeamSelection/select.png',colour1=self.colours[v.COLOUR],texture_linear=true})
					self.element.selected["'"..v.NAME.."'"].label = getLabelEX(self.element.selected[v.NAME],anchorNone,XYWH(0,((self.aliveCounter-1)*12.5)-7.5,58,15),Tahoma_10,v.NAME,{scissor=true, scroll_text=true, nomouseevent=true,font_colour=self.colours[v.COLOUR],shadowtext=true,text_halign=ALIGN_MIDDLE})
				end
				if v.IsMerged then 
					self.aliveCounter = self.aliveCounter +1
				end
			end
		else
			self.element.selected = getElementEX(self.element, anchorLTR, XYWH(0,0,58,72), true, {nomouseevent=true,texture='SGUI/TeamSelection/select.png',colour1=self.colours[1],texture_linear=true})
			self.element.selected.label = getLabelEX(self.element.selected,anchorNone,XYWH(0,((1-1)*12.5)-7.5,58,15),Tahoma_10,'',{scissor=true, scroll_text=true, nomouseevent=true,font_colour=self.colours[1],shadowtext=true,text_halign=ALIGN_MIDDLE})
		end
	end

	self.makeTeamSelectSortButtons = function(CALLBACK)
		local buttonConfigs = {
		    {x = 350, y = 37.5, images = {
		        'button-change-questionmark.png',
		        'button-change-soldier.png',
		        'button-change-engineer.png',
		        'button-change-mechanic.png',
		        'button-change-scientist.png'
		    }},
		    {x = 670, y = 37.5, images = {
		        'button-change-questionmark.png',
		        'button-change-soldier.png',
		        'button-change-engineer.png',
		        'button-change-mechanic.png',
		        'button-change-scientist.png'
		    }}
		}

		for _, config in ipairs(buttonConfigs) do
		    for i, texture in ipairs(config.images) do
		        getImageButtonEX(self.element, anchorLT, XYWH(config.x + 41 * (i - 1), config.y, 41, 41), '', '', tostring(i - 1), SKINTYPE_TEXTURE, {enabled = false, SKINTEXTURE = texture, hint = ''})
		    end
		end

		self.element.changeButtons = {}
		for i = 1, 10 do
		    self.element.changeButtons[i] = getImageButtonEX(self.element, anchorRB, XYWH(20 + 41 * (i - 1), 630, 41, 41), '', '', '', SKINTYPE_TEXTURE, {SKINTEXTURE = 'button-change-questionmark.png', hint = ''})
		end
	end

	self.setUpUnit = function(Data)
		local DATA = OW_GET_UNIT_INFO(Data.UID)
		local v = getElementEX(nil, anchorLTR, XYWH(0,0,58,72), true, {CLASS=Data.class, SKILLS=Data.skills, NAME=Data.name, UID=Data.UID, isCommander=Data.isCommander, isInterlocutor=Data.isInterlocutor, isHired=Data.isHired, isChangeable=Data.isChangeable, canChangeClass=Data.canChangeClass, canIgnoreClassRestrictions=Data.canIgnoreClassRestrictions})
		v.label = getLabelEX(nil, anchorNone, XYWH(0,72,58,15), Tahoma_10, DATA["INFO"][1], {scissor=true, scroll_text=true, text_halign=ALIGN_MIDDLE,})

		setTextureFromID(v, getUnitTexture(v.UID), 58, 72, 58, 72)

		if v.isHired or v.isCommander then 
			self.prof_count[v.CLASS] = self.prof_count[v.CLASS] +1
		end

		setFontColourBoth(v.label,RGB(120,195,126))

		v.skillbars = {{},{},{},{}}
		for i = 1, 4 do
			for y = 1, 10 do
				local x = getWidth(v)+3*y
				v.skillbars[i][y] = getElementEX(v, anchorNone, XYWH(x,(6*i-1),2,5), true, {texture='SGUI/TeamSelection/Skillbar3.png'})
			end
		end

		self.showHighlightedLevels(v)

		table.insert(self.units, v.ID)
	end

	self.setUpClassInfo = function(DATA)
		self.init()
		self.classInformation = DATA
		for i = 1, DATA.PROF_COUNT do
			self.prof_count[DATA.PROFS[i].NAME] = 0
		end

		self.element.display_text = getLabelEX(self.element,anchorNone,XYWH(11+40,31,274-80,251),Tahoma_10,'',{text_halign=ALIGN_MIDDLE,wordwrap=true})
		setVisible(self.element.display_text, true)
		self.updateText(self.classInformation)
        for i = 1, 10 do
            local prof = self.element.changeButtons[i]

            if i <= DATA.PROF_COUNT then
				prof.SKINTEXTURE = self.getProfTexture(DATA.PROFS[i].ID)
				prof.CLASSID     = DATA.PROFS[i].ID
				setInterfaceTexture(prof, prof.SKINTEXTURE)
				setHint(prof, DATA.PROFS[i].NAME)
				setVisible(prof, true)
				sgui_setcallback(prof.ID, CALLBACK_MOUSECLICK, "OW_CUSTOM_COMMAND_SGUI(803,"..i..")")
			else
				setVisible(prof, false)
			end
        end
	end
	
	self.getProfTexture = function(CLASSID)
        local texture = ''
        switch(CLASSID) : caseof {
                        [class_soldier]       = function (x)
                                                        texture = 'button-change-soldier.png'
                                                end,
                        [class_engineer]      = function (x)
                                                        texture = 'button-change-engineer.png'
                                                end,
                        [class_mechanic]      = function (x)
                                                        texture = 'button-change-mechanic.png'
                                                end,
                        [class_scientist]     = function (x)
                                                        texture = 'button-change-scientist.png'
                                                end,
                        [class_sniper]        = function (x)
                                                        texture = 'button-change-sniper.png'
                                                end,
                        [class_mortarer]      = function (x)
                                                        texture = 'button-change-mortar.png'
                                                end,
                        [class_bazooker]      = function (x)
                                                        texture = 'button-change-bazooka.png'
                                                end,
        ----------------------------------------------
                        default               = function (x)
                                                        texture = 'button-change-questionmark.png'
                                                end,
		}
        return texture
	end

	self.select = function(TYPE, POSITION, PLAYER)
			set_Callback(self.element.hire.ID, CALLBACK_MOUSECLICK, 'OW_CUSTOM_COMMAND_SGUI(801,' ..POSITION..',' ..MyID..')')
			set_Callback(self.element.fire.ID, CALLBACK_MOUSECLICK, 'OW_CUSTOM_COMMAND_SGUI(801,' ..POSITION..',' ..MyID..')')

			local Selector

			if inMultiplayer == true then
				Selector = self.element.selected["'"..MULTI_PLAYERINFO_CURRENT_PLID[PLAYER].NAME.."'"]
			end

			if inMultiplayer == false then
				Selector = self.element.selected
			end

			local v = eReg:get(self.units[POSITION])

			OW_CUSTOM_COMMAND(2, v.UID)

	    self.setImageButtonEnable(self.element.hire, not v.isHired, ui.enablecol, ui.disablecol);
			self.setImageButtonEnable(self.element.fire, v.isHired, ui.enablecol, ui.disablecol);

			if v.isCommander or v.isInterlocutor then
				self.setImageButtonEnable(self.element.hire, false, ui.enablecol, ui.disablecol)
				self.setImageButtonEnable(self.element.fire, false, ui.enablecol, ui.disablecol)
			end

			self.unit_selected = POSITION

			setVisible(Selector, false)

				if TYPE == 1 then
					SGUI_setparent(Selector.ID, self.element.ui_hired.ID)
					SGUI_setparent(Selector.label.ID, Selector.ID)
				elseif TYPE == 2 then
					SGUI_setparent(Selector.ID, self.element.ui_fired.ID)
					SGUI_setparent(Selector.label.ID, Selector.ID)
				elseif TYPE == 3 then
					SGUI_setparent(Selector.ID, self.element.ui_hired_forced.ID)
					SGUI_setparent(Selector.label.ID, Selector.ID)
				elseif TYPE == 4 then
					SGUI_setparent(Selector.ID, self.element.ui_their_commanders.ID)
					SGUI_setparent(Selector.label.ID, Selector.ID)
				end

				AddSingleUseTimer(0.1,'setVisible({ID='..Selector.ID..'}, true) setXY({ID='..Selector.ID..'}, getX({ID='..v.ID..'}), getY({ID='..v.ID..'}))')

				sgui_bringtofront(Selector.ID)
				sgui_bringtofront(Selector.label.ID)


			if self.display.label == nil then
				self.updateUnitInformation(POSITION)
			end

			if PLAYER == MyID or v.NAME == getText(self.display.label) then 
				self.updateUnitInformation(POSITION)
			end
	end

	self.updateUnitInformation = function(UNIT)
		local v = eReg:get(self.units[UNIT])

		if self.display.unit == nil then
			self.display.unit = getElementEX(self.display, anchorLTR, XYWH(15,15,58*1.25,72*1.25), true, {texture_linear=true})
			self.display.label = getLabelEX(self.display.unit, anchorNone, XYWH(getWidth(self.display.unit)+7.5,0,100,15), Tahoma_10, '', {text_halign=ALIGN_LEFT,})
			self.display.label2 = getLabelEX(self.display.unit, anchorNone, XYWH(getWidth(self.display.unit)+7.5,15,100,15), Tahoma_10, '', {text_halign=ALIGN_LEFT,})

			self.display.skillrow = getElementEX(self.display.unit,anchorLTRB,XYWH(-15,getHeight(self.display.unit)+2.5,100,25),true,{colour1=BLACKA(0),})
        	self.display.paramIcons = createParamIcons(self.display,anchorTL,nil,{visible=true, x=5, y=240, labelFontColourOverride=RGB(221,224,211)})

			for i=1, 4 do
			    self.display.skillrow[i] = RewardSkills_AddSkillRow(self.display.skillrow)
			    setXYWHV(self.display.skillrow[i],0,(i-1)*33,100,33)
			    setFontName(dialog.team_select.inner.skillrow[i].exp,Tahoma_10)
			    setInterfaceTexture(self.display.skillrow[i].icon,RewardSkillIconImg[i]..RewardSkillState[1]..'.png')
			    setImageBarTexture2(self.display.skillrow[i].progress,'1024levelbarempty.png','1024levelbar.png')
			end
		end

		setTextureFromID(self.display.unit, getUnitTexture(v.UID), 58*1.25, 72*1.25, 58*1.25, 72*1.25)

		local DATA = OW_GET_UNIT_INFO(v.UID)

		if (DATA.ID ~= nil) and (DATA.ID > 0) then

			if DATA["BUDINFO"] ~= nil then
				for i = 1, #DATA["UNITSINSIDE"] do
					if v.UID == DATA["UNITSINSIDE"][i]["ID"] then
						self.real_data = DATA["UNITSINSIDE"][i]
					end
				end
			else
				self.real_data = DATA
			end

			setText(self.display.label, self.real_data["INFO"][1])
			setText(v.label, self.real_data["INFO"][1])

			self.display.paramIcons:updateLabels(self.real_data["VALUES"])

			v.NAME = self.real_data["INFO"][1]
			v.VALUES = self.real_data["VALUES"]
			v.SKILLS_EXPERIENCE = {self.real_data["SKILLSTEXT"][1][2]["VALUE"],self.real_data["SKILLSTEXT"][2][2]["VALUE"],self.real_data["SKILLSTEXT"][3][2]["VALUE"],self.real_data["SKILLSTEXT"][4][2]["VALUE"]}
			v.SKILLS_EXPERIENCE_NEXTLVL = {self.real_data["SKILLSTEXT"][1][3]["VALUE"],self.real_data["SKILLSTEXT"][2][3]["VALUE"],self.real_data["SKILLSTEXT"][3][3]["VALUE"],self.real_data["SKILLSTEXT"][4][3]["VALUE"]}
			
      for i = 1, 10 do
      	setEnabled(self.element.changeButtons[i], v.canChangeClass)
      end

      if v.CLASS == "Soldier" or v.CLASS == "Bazooker" or v.CLASS == "Mortarman" or v.CLASS == "Sniper" or v.CLASS == "Desert Warrior" then
      	setText(self.display.label2, v.CLASS .. " Level " .. v.SKILLS[1] )
      end

      if v.CLASS == "Scientist" then
      	setText(self.display.label2, v.CLASS .. " Level " .. v.SKILLS[4] )
      end

      if v.CLASS == "Engineer" then 
      	setText(self.display.label2, v.CLASS .. " Level " .. v.SKILLS[2] )
      end

      if v.CLASS == "Mechanic" then 
      	setText(self.display.label2, v.CLASS .. " Level " .. v.SKILLS[3] )
      end 

      for i = 1, 4 do
      	if v.SKILLS_EXPERIENCE_NEXTLVL[i] == nil or v.SKILLS_EXPERIENCE_NEXTLVL[i] == '' then
      		v.SKILLS_EXPERIENCE_NEXTLVL[i] = v.SKILLS_EXPERIENCE[i]
      	end

      	local currentProgress = (tonumber(v.SKILLS_EXPERIENCE[i]) / tonumber(v.SKILLS_EXPERIENCE_NEXTLVL[i]))
        setImageBarProgress(self.display.skillrow[i].progress, currentProgress) 

        setText(self.display.skillrow[i].exp,'(LVL '..v.SKILLS[i]..')')
        setText(self.display.skillrow[i].proglabel, v.SKILLS_EXPERIENCE[i] .. '/' .. v.SKILLS_EXPERIENCE_NEXTLVL[i])

        setVisible(self.display.skillrow[i].progress,v.SKILLS[i] < 10)
        setVisible(self.display.skillrow[i].proglabel,v.SKILLS[i] < 10)

        for s=1, 10 do
          if (v.SKILLS[i] >= s) then -- DATA.UNITEXP[i].LVL
						setInterfaceTexture(self.display.skillrow[i].pips[s], RewardSkillImg[3])
						setColour1(self.display.skillrow[i].pips[s], WHITE())
          else
						setTexture(self.display.skillrow[i].pips[s], '')
						setColour1(self.display.skillrow[i].pips[s], GRAY(50))
          end
            setVisible(self.display.skillrow[i].pips[s], true)
        end

      end
		end
	end

	self.updateText = function()
		local DATA = self.classInformation
		local UNITS = self.units -- eReg:get(self.units[POSITION])
		local text = ''

		if (DATA.MIN == DATA.MAX) and (self.unit_count ~= DATA.MIN) then
			text = loc1(TID_Other_Team_select_Choose_Exactly, DATA.MAX) .. '\n'
		else
			if (self.unit_count < DATA.MIN) then
				if DATA.MAX > 0 then
					text = loc2(TID_Other_Team_select_Choose_From_To, DATA.MIN, DATA.MAX) .. '\n'
				else
					text = loc2(TID_Other_Team_select_Choose_Min, DATA.MIN) .. '\n'
				end
			else
				if (self.unit_count > DATA.MAX) then
					if (DATA.MIN > 0) then
						text = loc2(TID_Other_Team_select_Choose_From_To, DATA.MIN, DATA.MAX) .. '\n'
					else
						text = loc1(TID_Other_Team_select_Choose_Max, DATA.MAX) .. '\n'
                    end
                end
            end
        end

        for i = 1, DATA.PROF_COUNT do
        	if (DATA.PROFS[i].MIN == DATA.PROFS[i].MAX) and (self.prof_count[DATA.PROFS[i].NAME] ~= DATA.PROFS[i].MIN) then
        		text = text .. loc2(TID_Other_Team_select_Exactly, DATA.PROFS[i].MIN, DATA.PROFS[i].NAME) .. '\n'
        	else
        		if (DATA.PROFS[i].MIN == DATA.PROFS[i].MAX) and (DATA.PROFS[i].MIN < 0) and (self.prof_count[DATA.PROFS[i].NAME] > 0) then
					text = text .. loc1(TID_Other_Team_select_None, DATA.PROFS[i].NAME) .. '\n'
				else
					if (DATA.PROFS[i].MIN > 0) and (self.prof_count[DATA.PROFS[i].NAME] < DATA.PROFS[i].MIN) then
						if (DATA.PROFS[i].MAX > 0) then
							text = text .. loc3(TID_Other_Team_select_Interval, DATA.PROFS[i].MIN, DATA.PROFS[i].MAX,DATA.PROFS[i].NAME) .. '\n'
						else
							text = text .. loc2(TID_Other_Team_select_At_Least, DATA.PROFS[i].MIN, DATA.PROFS[i].NAME) .. '\n'
						end
					else
						if (DATA.PROFS[i].MAX > 0) and (self.prof_count[DATA.PROFS[i].NAME] > DATA.PROFS[i].MAX) then
							if (DATA.PROFS[i].MIN > 0) then
								text = text .. loc3(TID_Other_Team_select_Interval, DATA.PROFS[i].MIN, DATA.PROFS[i].MAX, DATA.PROFS[i].NAME) .. '\n'
							else
								text = text .. loc2(TID_Other_Team_select_At_Most, DATA.PROFS[i].MAX, DATA.PROFS[i].NAME) .. '\n'
							end
						end
					end
				end
			end
		end
		setText(self.element.display_text, text)
		setEnabled(self.element.ok, (text == ''))
	end

	self.setImageButtonEnable = function(BUTTON, ENABLE, COL1, COL2)
		setEnabled(BUTTON, ENABLE)
		if (ENABLE) then
			setColour1(BUTTON, COL1)
		else
			setColour1(BUTTON, COL2)
		end
	end

	self.doHireUnit = function(POSITION, PLAYER)
			local v = eReg:get(self.units[POSITION])
			v.isHired = not v.isHired
			self.recalculate(PLAYER)
			if v.isHired then
				self.unit_count = self.unit_count+1
				OW_CUSTOM_COMMAND_SGUI(800,1,POSITION,PLAYER)
				self.prof_count[v.CLASS] = self.prof_count[v.CLASS] +1
			else
				self.unit_count = self.unit_count-1
				OW_CUSTOM_COMMAND_SGUI(800,2,POSITION,PLAYER)
				self.prof_count[v.CLASS] = self.prof_count[v.CLASS] -1
			end
			self.updateText(self.classInformation)
	end

	self.doChangeClass = function(B_CLASS)
			local DATA = self.classInformation
			B_CLASS = DATA.PROFS[B_CLASS].NAME

			local v = eReg:get(self.units[self.unit_selected])

			if B_CLASS == "Soldier" then
				OW_CUSTOM_COMMAND(1, v.UID, 1)
			end
			if B_CLASS == "Engineer" then
				OW_CUSTOM_COMMAND(1, v.UID, 2)
			end
			if B_CLASS == "Mechanic" then
				OW_CUSTOM_COMMAND(1, v.UID, 3)
			end
			if B_CLASS == "Scientist" then
				OW_CUSTOM_COMMAND(1, v.UID, 4)
			end
			if B_CLASS == "Sniper" then
				OW_CUSTOM_COMMAND(1, v.UID, 5)
			end
			if B_CLASS == "Mortarman" then
				OW_CUSTOM_COMMAND(1, v.UID, 8)
			end
			if B_CLASS == "Bazooker" then
				OW_CUSTOM_COMMAND(1, v.UID, 9)
			end
			if B_CLASS == "Desert Warrior" then
				OW_CUSTOM_COMMAND(1, v.UID, 11)
			end

			setTextureFromID(v, getUnitTexture(v.UID), 58, 72, 58, 72)

			if v.isHired or v.isCommander then
				for i = 1, #self.classInformation.PROFS do
					if self.classInformation.PROFS[i].NAME == v.CLASS then
						self.prof_count[v.CLASS] = self.prof_count[v.CLASS] -1
					end
				end
			end
			v.CLASS = B_CLASS
			if v.isHired or v.isCommander then
				for i = 1, #self.classInformation.PROFS do
					if self.classInformation.PROFS[i].NAME == v.CLASS then
						self.prof_count[v.CLASS] = self.prof_count[v.CLASS] +1
					end
				end
			end
			self.showHighlightedLevels(v)
			self.updateUnitInformation(self.unit_selected)
			self.updateText()
	end

	self.showHighlightedLevels = function(unit)
		local v = unit
		for i = 1, 4 do
			for y = 1, v.SKILLS[i] do
				setTexture(v.skillbars[i][y], 'SGUI/TeamSelection/Skillbar2.png')
			end
		end
		if v.CLASS == "Soldier" or v.CLASS == "Bazooker" or v.CLASS == "Mortarman" or v.CLASS == "Sniper" or v.CLASS == "Desert Warrior" then
			for i = 1, v.SKILLS[1] do
				setTexture(v.skillbars[1][i], 'SGUI/TeamSelection/Skillbar1.png')
			end
		end
		if v.CLASS == "Scientist" then
			for i = 1, v.SKILLS[4] do
				setTexture(v.skillbars[4][i], 'SGUI/TeamSelection/Skillbar1.png')
			end
		end
		if v.CLASS == "Mechanic" then
			for i = 1, v.SKILLS[3] do
				setTexture(v.skillbars[3][i], 'SGUI/TeamSelection/Skillbar1.png')
			end
		end
		if v.CLASS == "Engineer" then
			for i = 1, v.SKILLS[2] do
				setTexture(v.skillbars[2][i], 'SGUI/TeamSelection/Skillbar1.png')
			end
		end
	end

	self.recalculate = function(PLAYER)
		-- UID, isCommander, isInterlocutor, isHired, isChangeable, canChangeClass, canIgnoreClassRestrictions
		local user_interface = {self.element.ui_hired, self.element.ui_fired, self.element.ui_hired_forced, self.element.ui_their_commanders}

		local newPositions = {}

		for i = 1, 4 do
			user_interface[i].c = 0
		end

		for k = 1, #self.units do
			local v = eReg:get(self.units[k])
			if v.isHired then
				user_interface[1].c = user_interface[1].c +1
				newPositions = self.getNewPosition(user_interface[1].c)
				setParent(v, user_interface[1])
				setParent(v.label, user_interface[1])
				AddEventSlideX(v.ID, newPositions[1], 0.1, nil)
				AddEventSlideY(v.ID, newPositions[2], 0.1, nil)
				AddEventSlideX(v.label.ID, newPositions[1], 0.1, nil)
				AddEventSlideY(v.label.ID, newPositions[2]+72, 0.1, nil)

				set_Callback(v.ID, CALLBACK_MOUSEDBLCLICK, 'OW_CUSTOM_COMMAND_SGUI(801,' ..k..',' ..MyID..')')
				set_Callback(v.ID, CALLBACK_MOUSECLICK, 'OW_CUSTOM_COMMAND_SGUI(800,1,'..k..','..MyID..')')
			end

			if not v.isHired and not v.isInterlocutor and not v.isCommander then
				user_interface[2].c = user_interface[2].c +1
				newPositions = self.getNewPosition(user_interface[2].c)
				setParent(v, user_interface[2])
				setParent(v.label, user_interface[2])
				AddEventSlideX(v.ID, newPositions[1], 0.1, nil)
				AddEventSlideY(v.ID, newPositions[2], 0.1, nil)
				AddEventSlideX(v.label.ID, newPositions[1], 0.1, nil)
				AddEventSlideY(v.label.ID, newPositions[2]+72, 0.1, nil)

				set_Callback(v.ID, CALLBACK_MOUSEDBLCLICK, 'OW_CUSTOM_COMMAND_SGUI(801,'..k..',' ..MyID..')')
				set_Callback(v.ID, CALLBACK_MOUSECLICK, 'OW_CUSTOM_COMMAND_SGUI(800,2,'..k..','..MyID..')')

			end

			if v.isCommander then
				user_interface[3].c = user_interface[3].c +1
				newPositions = self.getNewPosition(user_interface[3].c)
				setParent(v, user_interface[3])
				setParent(v.label, user_interface[3])
				setXY(v, newPositions[1], newPositions[2])
				setXY(v.label, newPositions[1], newPositions[2]+72)
				set_Callback(v.ID, CALLBACK_MOUSECLICK, 'OW_CUSTOM_COMMAND_SGUI(800,3,'..k..','..MyID..')')
			end

			if v.isInterlocutor then
				user_interface[4].c = user_interface[4].c +1
				newPositions = self.getNewPosition(user_interface[4].c)
				setParent(v, user_interface[4])
				setParent(v.label, user_interface[4])
				setXY(v, newPositions[1], newPositions[2])
				setXY(v.label, newPositions[1], newPositions[2]+72)
				set_Callback(v.ID, CALLBACK_MOUSECLICK, 'OW_CUSTOM_COMMAND_SGUI(800,4,'..k..','..MyID..')')
			end
		end
	end

	self.getNewPosition = function(k)
		local column = (k - 1) % 3
		local row = math.floor((k - 1) / 3)
		x = 10 + column * (58 + 32 + 3 + 2)
		y = 10 + row * (20 + 72)
		return {x, y}
	end

	return self
end

TeamSelect = teamselectCreate()

function onCustomCommand3()
	if (ccSystem.DATA[2] == 800) then 
		TeamSelect.select(ccSystem.DATA[3],ccSystem.DATA[4],ccSystem.DATA[5])
	end
	if (ccSystem.DATA[2] == 801) then
		TeamSelect.doHireUnit(ccSystem.DATA[3],ccSystem.DATA[4])
	end 
	if (ccSystem.DATA[2] == 802) then
		HideDialog(TeamSelect.element)
		OW_CUSTOM_COMMAND(3)
		for i = 1, #TeamSelect.units do
			local v = eReg:get(TeamSelect.units[i])
			if v.isHired or v.isCommander then
				OW_CUSTOM_COMMAND(4, v.UID)
			end
		end
		sgui_delete(TeamSelect.element.ID)
		TeamSelect.units = {}
	end
	if (ccSystem.DATA[2] == 803) then
		TeamSelect.doChangeClass(ccSystem.DATA[3])
	end
end

ccSystem.add('onCustomCommand3();')

function FROMOW_INFOPANEL_UPDATE(DATA)
  game_info.Update(DATA)

  TeamSelect.getUnitData = DATA

  LUA_TO_DEBUGLOG(serializeTable(DATA,"FROMOW_INFOPANEL_UPDATE"))
  --[[if not wait_process_server then
    if DATA.ID > 0 and DATA.TYP == 1 then
      framework.selected = {name=DATA.INFO[1], side=framework.Colours[DATA.SIDE+1], id=DATA.ID}
      Xichted = OW_GET_UNIT_PORTRAIT_PARTS(DATA.ID)
      framework.Xichted_list = string.sub(Xichted.TYP,1,4)
      framework.Xichted_name = DATA.INFO[1]

      framework.Xichted_name_png = DATA.INFO[1]..'.png'
      for i = 1, #Xichted.PARTS do
        framework.Xichted_list = framework.Xichted_list..'|'..Xichted.PARTS[i]
      end
    end
  end--]]
end