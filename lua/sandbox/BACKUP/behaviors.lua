  commands[6] = getCheckBoxEX_UI(dynamic.commands[31],anchorLT,XYWH(5,2.5,15,15),'Neutrals join on sight',{},'behaviours(1)',{checked=false});
  dynamic['BEH'] = getImageButtonEX(dynamic.commands[33],anchorNone,XYWH(5,5,getWidth(dynamic.commands[33])-10,25),'Add Unit','',"sub_behaviours(dynamic['BEH']);",SKINTYPE_BUTTON,{font_colour_disabled=GRAY(127),V=1});

function behaviours(category)
  CHECKED = sgui_get(commands[6].ID,PROP_CHECKED)
  setVisible(dynamic['BEH'],CHECKED)
  if CHECKED == false then 
    OW_CUSTOM_COMMAND(5155,0)
  else
    OW_CUSTOM_COMMAND(5155,1)
  end
  DoInterfaceChange(interface.current);
end;

function sub_behaviours(element)
  local values,text = {false,true},{'Add Unit','Stop adding'};
  element.V = element.V +1;
  if element.V == 3 then element.V = 1; end;
  ischoosing = values[element.V];
  setText(element,text[element.V]);
end;

function addBehaviour(UID, UTEX, UNAM)
  local parent = dynamic.commands[33]
  behaviour[1] = behaviour[1] + 1
  behaviour[3] = behaviour[3] + 1
  
  if behaviour[3] == 8 then 
    behaviour[3] = 1
    behaviour[4] = behaviour[4] + 1
  end
  
  local count, y = behaviour[1], behaviour[4]
  local button = getElementEX(parent, anchorNone, XYWH(9 + 23 * (behaviour[3] - 1), 28 * (y + 1), 80/4, 100/4), true, {hint = UNAM .. '\nLeft click to remove.\nRight click to focus camera.', value = UID})
  
  behaviour[2][count] = button
  set_Callback(button.ID, CALLBACK_MOUSECLICK, "if %b == 0 then delete_behaviours(" .. button.ID .. "," .. UID .. "); end; if %b == 1 then OW_CUSTOM_COMMAND(519," .. UID .. "); end;")
  
  OW_CUSTOM_COMMAND(517, 1, UID)
  
  SGUI_settextureid(button.ID, UTEX)
end

function delete_behaviours(ELE, UID)
	for i, behaviour_instance in ipairs(behaviour[2]) do
		if behaviour_instance ~= nil and behaviour_instance.value == UID then
			sgui_delete(behaviour_instance.ID);
			table.remove(behaviour[2], i);
			OW_CUSTOM_COMMAND(517, 2, UID);
			behaviour[1] = behaviour[1] - 1;
			behaviour_sort();
			break;
		end
	end
end

function delete_behaviour_FROMSAIL(UID)
	for i = #behaviour[2],1,-1 do
		if behaviour[2][i] ~= nil and behaviour[2][i].value == UID then
		sgui_delete(behaviour[2][i].ID);
		table.remove(behaviour[2],i);
		behaviour[1] = behaviour[1]-1;
		end;
	end;
	behaviour_sort();
end;

function behaviour_sort()
  behaviour[3] = 0
  behaviour[4] = 0
  
  for i = 1, #behaviour[2] do
    local obj = behaviour[2][i]
    if obj then
      behaviour[3] = behaviour[3] + 1
      if behaviour[3] == 8 then
        behaviour[3] = 1
        behaviour[4] = behaviour[4] + 1
      end
      AddEventSlideX(obj.ID, 9 + 23 * (behaviour[3] - 1), 0.5)
      AddEventSlideY(obj.ID, 28 * (behaviour[4] + 1), 0.5)
    end
  end
end