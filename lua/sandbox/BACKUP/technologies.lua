  commands[14] = {--[[LAB2]] shortLabel_Tech(5,27.5+325,200,'Technologies Manager\n\nPick a side to edit its technological progress.',WHITE(),'You first need to click on a side to view its technological upgrades.'),
				  --[[BUT1]] shortButton_Tech(5,27.5+250,60,'Blue',RGB(175,175,175),"custom.technologies:execute(2);","Left click: add player to query pool.\nRight click: remove player from query pool."),
				  --[[BUT2]] shortButton_Tech(5+64.5,27.5+250,60,'Yellow',RGB(175,175,175),"custom.technologies:execute(3);","Left click: add player to query pool.\nRight click: remove player from query pool."),
				  --[[BUT3]] shortButton_Tech(5+128,27.5+250,60,'Red',RGB(175,175,175),"custom.technologies:execute(4);","Left click: add player to query pool.\nRight click: remove player from query pool."),
				  --[[BUT4]] shortButton_Tech(5+64.5,27.5+225,60,'Cyan',RGB(175,175,175),"custom.technologies:execute(5);","Left click: add player to query pool.\nRight click: remove player from query pool."),
				  --[[BUT5]] shortButton_Tech(5+128,27.5+225,60,'Orange',RGB(175,175,175),"custom.technologies:execute(6);","Left click: add player to query pool.\nRight click: remove player from query pool."),
				  --[[BUT6]] shortButton_Tech(5,27.5+225,60,'Purple',RGB(175,175,175),"custom.technologies:execute(7);","Left click: add player to query pool.\nRight click: remove player from query pool."),
				  --[[BUT7]] shortButton_Tech(5+64.5-32,27.5+200,60,'Green',RGB(175,175,175),"custom.technologies:execute(8);","Left click: add player to query pool.\nRight click: remove player from query pool."),
				  --[[BUT8]] shortButton_Tech(5+128-32,27.5+200,60,'Gray',RGB(175,175,175),"custom.technologies:execute(9);","Left click: add player to query pool.\nRight click: remove player from query pool.")
};

custom.technologies = {}

function custom.technologies:execute(V)

	  local sh,dy = commands['COUNT'],dynamic.commands[5];
    
	  commands[22][sh] = logLabel(2,2.5,commands.getY(sh-1),getWidth(dy)-14.25,commands['COUNT2']..' - '..dynamic.commands.all[selected_id][1],WHITE(),dynamic.commands.list[selected_id].hint,get_Callback(dynamic.commands.list[selected_id].ID, CALLBACK_MOUSECLICK), 1, 1 );
	  commands['COUNT2'] = commands['COUNT2'] +1;
	  sgui_autosizecheck(commands[22][sh].ID)
	  
	  for i = 1,72 do 
		table.insert(commands[210][special_value[14][1]],i,getTexture(commands[14][i+9]));
	  end;
	
      special_value[14][1] = V-1;

	  for i = 1,72 do 
		setTexture(commands[14][i+9],commands[210][special_value[14][1]][i]);
	  end;
	
    for i = 2,9 do set_Colour(commands[14][i].ID,PROP_FONT_COL,RGB(175,175,175)); end;
    set_Colour(commands[14][V].ID,PROP_FONT_COL,COLOURS[8]);
  
end;

