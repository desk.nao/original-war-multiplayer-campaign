cards    = {LIST={},COUNT=0,USED=0,ELE=nil,ANIMTIME=0,ANIMMOVETIME=0,CARDHEIGHT=192,CARDWIDTH=160}
cardPool = {LIST={},COUNT=0}
cards.Queue = {}
cards.Discard = {}

ENT = {}

function cards.add(DATA)
	cards.COUNT=cards.COUNT+1
    DATA.ID = cards.COUNT
	cards.LIST[cards.COUNT] = DATA
        return cards.COUNT
end

function cards.addCards(COUNT)
	cards.COUNT = 0
	cards.USED  = 0
	cards.LIST  = {}
		for i = 1,COUNT do
        	cards.add({V=false})
        end
end

function cards.findFree()
	if cards.USED < cards.COUNT then
		for i = 1,cards.COUNT do
			if (not cards.LIST[i].V) then
				return i
			end
		end
	end
	return 0
end

function cards.addCard(DATA)
	local id = cards.findFree()
		if id < 1 then
			return
		end
		
	cards.LIST[id].V = true

	local pools = { {1,25}, {1,27}, {1,27} }
		
	cards.LIST[id].CARD = cardPool.LIST[math.random(pools[interfaceSide][1],pools[interfaceSide][2])] 
	
	cards.LIST[id].animtime = cards.ANIMTIME+cards.ANIMMOVETIME
	
	if cards.LIST[id].ELE == nil then
		cards.createCard(id)
	else
		cards.updateCard(id)
    end

	setY(cards.LIST[id].ELE,cards.CARDHEIGHT)
	setY(cards.LIST[id].ELE.BACK,cards.CARDHEIGHT)

	cards.USED = cards.USED + 1
end

function cards.createOutlineLabel(PARENT,POSSIZE,FONT1,FONT2,ID)
	local ELE = getLabelEX(PARENT,anchorNone,POSSIZE,FONT1,'',{nomouseevent=true,font_colour=cards.LIST[ID].CARD.COL,shadowtext=true,text_halign=ALIGN_MIDDLE})
	ELE.o 	  = getLabelEX(PARENT,anchorNone,POSSIZE,FONT2,'',{nomouseevent=true,font_colour=cards.LIST[ID].CARD.COL,shadowtext=false,text_halign=ALIGN_MIDDLE})
	return ELE
end

function cards.setOutlineLabelCol(ELE,COL1,COL2)
	setFontColour(ELE,COL1)
	setFontColour(ELE.o,COL2)
end

function cards.createCard(ID)
	
	if ID < 13 then 
		cards.LIST[ID].ELE = getElementEX(cards.ELE,anchorLT,XYWH((ID-1)*cards.CARDWIDTH,150,cards.CARDWIDTH,cards.CARDHEIGHT),true,{callback_mouseleave="AddEventSlideY(cards.LIST["..ID.."].ELE.ID,0,0.25,'')",callback_mouseover="AddEventSlideY(cards.LIST["..ID.."].ELE.ID,-50,0.25,'') setText(ENT.BLACK.LABEL,cards.LIST["..ID.."].CARD.DESC)",callback_mouseclick='cards.onCardClick('..ID..')'})
		
		local v = cards.LIST[ID].ELE
		
		if interfaceSide == 1 then 
			cards.LIST[ID].ELE.delete = getElementEX(cards.LIST[ID].ELE,anchorNone,XYWH(0,0,cards.CARDWIDTH,cards.CARDHEIGHT),true,{texture='SGUI/Nao/cards/Skeletons/Cardfront_u_anim.png',nomouseevent=true}) 
		end
		
		if interfaceSide == 2 then 
			cards.LIST[ID].ELE.delete = getElementEX(cards.LIST[ID].ELE,anchorNone,XYWH(0,0,cards.CARDWIDTH,cards.CARDHEIGHT),true,{texture='SGUI/Nao/cards/Skeletons/Cardfront_r_anim.png',nomouseevent=true}) 
		end
		
		if interfaceSide == 3 then 
			cards.LIST[ID].ELE.delete = getElementEX(cards.LIST[ID].ELE,anchorNone,XYWH(0,0,cards.CARDWIDTH,cards.CARDHEIGHT),true,{texture='SGUI/Nao/cards/Skeletons/Cardfront_a_anim.png',nomouseevent=true}) 
		end
		
		v.title  = cards.createOutlineLabel(v,XYWH(0,15,cards.CARDWIDTH,0),'Tahoma_70_Fixed_16_2.DFF0.380.140.51','Tahoma_70_Fixed_16_2.DFF0.380.140.51',ID)
		v.attrib = getLabelEX(v,anchorNone,XYWH(20,90,cards.CARDWIDTH-40,0),Tahoma_12B,'',{nomouseevent=true,font_colour=ENT.COL[6],font_style_outline=true,shadowtext=true,text_halign=ALIGN_MIDDLE})
		v.desc   = getLabelEX(v,anchorNone,XYWH(15,118,cards.CARDWIDTH-30,55),Tahoma_12B,'',{nomouseevent=true,font_colour=ENT.COL[8],font_style_outline=true,shadowtext=true,wordwrap=true,text_halign=ALIGN_MIDDLE})
		
		local back = {'Cardback_us','Cardback_ru','Cardback_ar'}
		v.BACK   = getElementEX(cards.ELE,anchorLT,XYWH((ID-1)*160,0,cards.CARDWIDTH,cards.CARDHEIGHT),true,{nomouseevent=true,texture=back[interfaceSide]..'.png'}) 
		
		v.delete = getElementEX(cards.ELE,anchorNone,XYWH((ID-1)*160+75,4,10,9),true,{texture='delete.png',position=ID}) 
		set_Callback(v.delete.ID,CALLBACK_MOUSELEAVE,"setTexture(cards.LIST["..ID.."].ELE.delete,'delete.png')")
		set_Callback(v.delete.ID,CALLBACK_MOUSEOVER,"setTexture(cards.LIST["..ID.."].ELE.delete,'delete_over.png')")
		set_Callback(v.delete.ID,CALLBACK_MOUSECLICK,"cards.freeCard(cards.LIST["..ID.."].ELE.delete.position) OW_CUSTOM_COMMAND(889,"..ENT.PLAYERS.ME..",5)")
		
	end

	if cards.LIST[ID].CARD.SAILCATEGORY == 8 then 
	local v = cards.LIST[ID].ELE
		v.subtexture  = getElementEX(v,anchorNone,XYWH(cards.CARDWIDTH/2,100,0,0),true,{nomouseevent=true})
	end		
		
	sgui_bringtofront(cards.LIST[ID].ELE.ID)


	cards.updateCard(ID)

end

cards.buildings = { -- WIP: Setting dimensions (WILL CHANGE TO AUTO SIZER)
	
	--[[0: Depot]] 
	{ 
		{ {'depot'} }, 
		{ {166,198}, {0,0}, {0,0} }, -- AM
		{ {128,118}, {0,0}, {0,0} }, -- AR
		{ {158,216}, {0,0}, {0,0} }  -- RU
	},

	--[[1: Warehouse]]
	{ 
		{ {'warehouse'} }, 
		{ {176,220}, {0,0}, {0,0} }, -- AM
		{ {128,147}, {0,0}, {0,0} }, -- AR
		{ {164,230}, {0,0}, {0,0} }  -- RU
	},

	--[[2: Workshop]]
	{ 
		{ {'workshop'} }, 
		{ {174,202}, {0,0}, {0,0} }, -- AM
		{ {161,155}, {0,0}, {0,0} }, -- AR
		{ {172,228}, {0,0}, {0,0} }  -- RU
	},
	
	--[[3: Factory]]
	{ 
		{ {'factory'} }, 
		{ {170,198}, {0,0}, {0,0} }, -- AM
		{ {159,180}, {0,0}, {0,0} }, -- AR
		{ {174,198}, {0,0}, {0,0} }  -- RU
	},
	
	--[[4: Armoury]]
	{ 
		{ {'armoury'} }, 
		{ {131,108}, {0,0}, {0,0} }, -- AM
		{ {157,135}, {0,0}, {0,0} }, -- AR
		{ {125,107}, {0,0}, {0,0} }  -- RU
	},
	
	--[[5: Barracks]]
	{ 
		{ {'barracks'} }, 
		{ {138,152}, {0,0}, {0,0} }, -- AM
		{ {134,131}, {0,0}, {0,0} }, -- AR
		{ {124,124}, {0,0}, {0,0} }  -- RU
	},
	
	--[[6: Laboratory]]
	{ 
		{ {'basiclab'} }, 
		{ {142,172}, {0,0}, {0,0} }, -- AM
		{ {116,146}, {0,0}, {0,0} }, -- AR
		{ {122,186}, {0,0}, {0,0} }  -- RU
	},
	
	--[[7: Half Lab.]]
	{ 
		{ {'bridgeupg'} }, 
		{ {78,148}, {0,0}, {0,0} }, -- AM
		{ {69,104}, {0,0}, {0,0} }, -- AR
		{ {72,120}, {0,0}, {0,0} }  -- RU
	},
	
	--[[8: Full Lab.]]
	{ 
		{ {'bridgeupg'} }, 
		{ {78,148}, {0,0}, {0,0} }, -- AM
		{ {69,104}, {0,0}, {0,0} }, -- AR
		{ {72,120}, {0,0}, {0,0} }  -- RU
	},
	
	--[[9: Basic Lab.]]
	{ 
		{ {'basiclab'} }, 
		{ {142,172}, {0,0}, {0,0} }, -- AM
		{ {116,146}, {0,0}, {0,0} }, -- AR
		{ {122,186}, {0,0}, {0,0} }  -- RU
	},

	--[[10: Weapon L.]]
	{ 
		{ {'weaponlab'} }, 
		{ {134,208}, {0,0}, {0,0} }, -- AM
		{ {117,148}, {0,0}, {0,0} }, -- AR
		{ {138,176}, {0,0}, {0,0} }  -- RU
	},
	
	--[[11: Siberite L.]]
	{ 
		{ {'siberiumlab'} }, 
		{ {134,206}, {0,0}, {0,0} }, -- AM
		{ {123,145}, {0,0}, {0,0} }, -- AR
		{ {128,174}, {0,0}, {0,0} }  -- RU
	},
	
	--[[12: Computer L.]]
	{ 
		{ {'computerlab'} }, 
		{ {172,214}, {0,0}, {0,0} }, -- AM
		{ {0,0}, {0,0}, {0,0} }, -- AR
		{ {122,184}, {0,0}, {0,0} }  -- RU
	},
	
	--[[13: Biological L.]]
	{ 
		{ {'biologicallab'} }, 
		{ {138,190}, {0,0}, {0,0} }, -- AM
		{ {138,190}, {0,0}, {0,0} }, -- AR
		{ {138,190}, {0,0}, {0,0} }  -- RU
	},

	--[[14: Space-time L.]]
	{ 
		{ {'spacetimelab'} }, 
		{ {132,204}, {0,0}, {0,0} }, -- AM
		{ {132,204}, {0,0}, {0,0} }, -- AR
		{ {132,204}, {0,0}, {0,0} }  -- RU
	},

	--[[15: Opto-electronics]]
	{ 
		{ {'optolab'} }, 
		{ {146,196}, {0,0}, {0,0} }, -- AM
		{ {127,147}, {0,0}, {0,0} }, -- AR
		{ {0,0}, {0,0}, {0,0} }  -- RU
	},
	
	--[[16: Ext. Tracks]]
	{ 
		{ {'trackext'} }, 
		{ {112,136}, {0,0}, {0,0} }, -- AM
		{ {117,84}, {0,0}, {0,0} }, -- AR
		{ {112,134}, {0,0}, {0,0} }  -- RU
	},
	
	--[[17: Ext. Guns]]
	{ 
		{ {'gunext'} }, 
		{ {130,124}, {0,0}, {0,0} }, -- AM
		{ {115,102}, {0,0}, {0,0} }, -- AR
		{ {118,104}, {0,0}, {0,0} }  -- RU
	},
	
	--[[18: Ext. Rockets]]
	{ 
		{ {'rocketext'} }, 
		{ {114,122}, {0,0}, {0,0} }, -- AM
		{ {110,87}, {0,0}, {0,0} }, -- AR
		{ {110,100}, {0,0}, {0,0} }  -- RU
	},

	--[[19: Ext. Civilian]]
	{ 
		{ {'non-combatext'} }, 
		{ {112,112}, {0,0}, {0,0} }, -- AM
		{ {115,95}, {0,0}, {0,0} }, -- AR
		{ {104,94}, {0,0}, {0,0} }  -- RU
	},
	
	--[[20: Ext. Radar]]
	{ 
		{ {'radarext'} }, 
		{ {110,136}, {0,0}, {0,0} }, -- AM
		{ {101,79}, {0,0}, {0,0} }, -- AR
		{ {110,102}, {0,0}, {0,0} }  -- RU
	},
	
	--[[21: Ext. Siberite]]
	{ 
		{ {'siberiumext'} }, 
		{ {114,112}, {0,0}, {0,0} }, -- AM
		{ {108,94}, {0,0}, {0,0} }, -- AR
		{ {116,112}, {0,0}, {0,0} }  -- RU
	},
	
	--[[22: Ext. Radio]]
	{ 
		{ {'radioext'} }, 
		{ {114,118}, {0,0}, {0,0} }, -- AM
		{ {114,70}, {0,0}, {0,0} }, -- AR
		{ {0,0}, {0,0}, {0,0} }  -- RU
	},
	
	--[[23: Ext. Stitch]]
	{ 
		{ {'solarext'} }, 
		{ {112,112}, {0,0}, {0,0} }, -- AM
		{ {113,91}, {0,0}, {0,0} }, -- AR
		{ {0,0}, {0,0}, {0,0} }  -- RU
	},

	--[[24: Ext. Computer]]
	{ 
		{ {'computerext'} }, 
		{ {110,150}, {0,0}, {0,0} }, -- AM
		{ {0,0}, {0,0}, {0,0} }, -- AR
		{ {112,102}, {0,0}, {0,0} }  -- RU
	},

	--[[25: Ext. Laser]]
	{ 
		{ {'laserext'} }, 
		{ {114,130}, {0,0}, {0,0} }, -- AM
		{ {0,0}, {0,0}, {0,0} }, -- AR
		{ {0,0}, {0,0}, {0,0} }  -- RU
	},

	--[[26: Oil Power]]
	{ 
		{ {'oilpower'} }, 
		{ {84,146}, {0,0}, {0,0} }, -- AM
		{ {98,103}, {0,0}, {0,0} }, -- AR
		{ {82,102}, {0,0}, {0,0} }  -- RU
	},

	--[[27: Solar Power]]
	{ 
		{ {'solarpower'} }, 
		{ {76,78}, {0,0}, {0,0} }, -- AM
		{ {81,92}, {0,0}, {0,0} }, -- AR
		{ {0,0}, {0,0}, {0,0} }  -- RU
	},
	
	--[[28: Siberite Power]]
	{ 
		{ {'siberiumpower'} }, 
		{ {72,150}, {0,0}, {0,0} }, -- AM
		{ {79,93}, {0,0}, {0,0} }, -- AR
		{ {92,114}, {0,0}, {0,0} }  -- RU
	},
	
	--[[29: Oil Mine]]
	{ 
		{ {'oilmine'} }, 
		{ {84,82}, {0,0}, {0,0} }, -- AM
		{ {79,93}, {40,156}, {24,-11} }, -- AR
		{ {88,122}, {0,0}, {0,0} }  -- RU
	},

	--[[30: Siberite Mine]]
	{ 
		{ {'siberiummine'} }, 
		{ {84,74}, {0,0}, {0,0} }, -- AM
		{ {0,0}, {0,0}, {0,0} }, -- AR
		{ {84,76}, {0,0}, {0,0} }  -- RU
	},
	
	--[[31: Breastworks]]
	{ 
		{ {'breastworks'} }, 
		{ {94,110}, {0,0}, {0,0} }, -- AM
		{ {74,86}, {0,0}, {0,0} }, -- AR
		{ {86,108}, {0,0}, {0,0} }  -- RU
	},
	
	--[[32: Bunker]]
	{ 
		{ {'bunker'} }, 
		{ {88,76}, {0,0}, {0,0} }, -- AM
		{ {82,66}, {0,0}, {0,0} }, -- AR
		{ {86,70}, {0,0}, {0,0} }  -- RU
	},
	
	--[[33: Turret]]
	{ 
		{ {'automaticturret'} }, 
		{ {80,76}, {0,0}, {0,0} }, -- AM
		{ {0,0}, {0,0}, {0,0} }, -- AR
		{ {72,60}, {0,0}, {0,0} }  -- RU
	},

	--[[34: Teleport]]
	{ 
		{ {'teleport'} }, 
		{ {138,154}, {0,0}, {0,0} }, -- AM
		{ {138,154}, {0,0}, {0,0} }, -- AR
		{ {138,154}, {0,0}, {0,0} }  -- RU
	},

	--[[35: Fort]]
	{ 
		{ {'bunker-s'} }, 
		{ {84,120}, {0,0}, {0,0} }, -- AM
		{ {84,120}, {0,0}, {0,0} }, -- AR
		{ {84,120}, {0,0}, {0,0} }  -- RU
	},

	--[[36: Control Tower]]
	{ 
		{ {'ct'} }, 
		{ {142,194}, {0,0}, {0,0} }, -- AM
		{ {142,194}, {0,0}, {0,0} }, -- AR
		{ {142,194}, {0,0}, {0,0} }  -- RU
	},

	--[[37: Behemoth]]
	{ 
		{ {'behemoth'} }, 
		{ {148,142}, {0,0}, {0,0} }, -- AM
		{ {148,142}, {0,0}, {0,0} }, -- AR
		{ {148,142}, {0,0}, {0,0} }  -- RU
	},

	--[[38: Eon]]
	{ 
		{ {'eon'} }, 
		{ {48,128}, {0,0}, {0,0} }, -- AM
		{ {48,128}, {0,0}, {0,0} }, -- AR
		{ {48,128}, {0,0}, {0,0} }  -- RU
	},

	--[[39: Alien Tower]]
	{ 
		{ {'alien'} }, 
		{ {200,270}, {0,0}, {0,0} }, -- AM
		{ {200,270}, {0,0}, {0,0} }, -- AR
		{ {200,270}, {0,0}, {0,0} }  -- RU
	}
					
}

cards.Nations = {'u','r','a'}

function cards.updateCard(ID)
	setTexture(cards.LIST[ID].ELE,cards.LIST[ID].CARD.TEXTURE)

	if cards.LIST[ID].CARD.SAILCATEGORY == 8 then
		setTexture(cards.LIST[ID].ELE.subtexture,cards.LIST[ID].CARD.SUBTEXTURE)
		setWH(cards.LIST[ID].ELE.subtexture,cards.LIST[ID].CARD.SUBTW,cards.LIST[ID].CARD.SUBTH)
		setX(cards.LIST[ID].ELE.subtexture,getX(cards.LIST[ID].ELE.subtexture)-cards.LIST[ID].CARD.SUBTW/2)
	end
		
	setRotation(cards.LIST[ID].ELE,SetRotate(1,0,true))
	setRotation(cards.LIST[ID].ELE.BACK,SetRotate(1,0,false))
	setVisible(cards.LIST[ID].ELE,true)
	setVisible(cards.LIST[ID].ELE.BACK,true)

	setText(cards.LIST[ID].ELE.title,cards.LIST[ID].CARD.TITLE)
	setText(cards.LIST[ID].ELE.title.o,cards.LIST[ID].CARD.TITLE)

	setText(cards.LIST[ID].ELE.attrib,cards.LIST[ID].CARD.ATTRIB)
	setText(cards.LIST[ID].ELE.desc,cards.LIST[ID].CARD.DESC)	

	if cards.LIST[ID].CARD.SAILCATEGORY == 4 then
	local v = cards.LIST[ID].ELE
		v.xicht  = getElementEX(v,anchorNone,XYWH(15,166-80-40,50,62),true,{texture=cards.LIST[ID].CARD.XICHT,nomouseevent=true})
	end

	if cards.LIST[ID].CARD.SAILCATEGORY == 3 then
		local v = cards.LIST[ID].ELE -- setVisible( cards.LIST[ID].ELE.building, true)

		local dimensions = {15,22,14}
		
		local _w,_h = scaleWH(cards.buildings[cards.LIST[ID].CARD.SAILIDENT+1][cards.LIST[ID].CARD.NATION+1][1][1], cards.buildings[cards.LIST[ID].CARD.SAILIDENT+1][cards.LIST[ID].CARD.NATION+1][1][2],160,(105-dimensions[cards.LIST[ID].CARD.NATION]))

		local _wA, _hA = cards.buildings[cards.LIST[ID].CARD.SAILIDENT+1][cards.LIST[ID].CARD.NATION+1][2][1], cards.buildings[cards.LIST[ID].CARD.SAILIDENT+1][cards.LIST[ID].CARD.NATION+1][2][2]
		local _l  	   = cards.buildings[cards.LIST[ID].CARD.SAILIDENT+1][1][1][1]
		local _n 	   = cards.Nations[interfaceSide]
																				-- dimensions[cards.LIST[ID].CARD.NATION] + 
		v.building     = getElementEX(v,anchorNone,XYWH(cards.CARDWIDTH/2-(_w/2),dimensions[cards.LIST[ID].CARD.NATION]+(105-dimensions[cards.LIST[ID].CARD.NATION])/2-_h/2,_w,_h),true,{texture='SGUI/b-'.._n..'-'.._l..'0000.png',nomouseevent=true})
		v.building.top = getElementEX(v.building,anchorNone,XYWH(24,-11,_wA,_hA),true,{texture='SGUI/a-'.._n..'-'.._l..'0000.png',nomouseevent=true})
	end
	
	if cards.LIST[ID].CARD.NATION == 1 then
	
		if cards.LIST[ID].CARD.SAILCATEGORY == 4 then -- Heroes
		
			local v = cards.LIST[ID].ELE
			v.effect_3 = getElementEX(v,anchorNone,XYWH(14,-11,32,35),true,{texture='SGUI/lasseg_end.png',nomouseevent=true})
			v.effect_3b = getElementEX(v,anchorNone,XYWH(113,-11,32,35),true,{texture='SGUI/lasseg_end.png',nomouseevent=true})
			v.effect_6 = getElementEX(v,anchorNone,XYWH(3,105,19,57),true,{texture='SGUI/a-u-trackext_1.png',nomouseevent=true})
			v.effect_7 = getElementEX(v,anchorNone,XYWH(138,105,19,57),true,{texture='SGUI/a-u-trackext_1.png',nomouseevent=true})
		end
		
		if cards.LIST[ID].CARD.SAILCATEGORY == 3 then -- Buildings
		
			local v = cards.LIST[ID].ELE
			v.effect_1 = getElementEX(v,anchorNone,XYWH(108,79,44,48),true,{texture='SGUI/lasseg_end_green.png',nomouseevent=true})
			v.effect_2 = getElementEX(v,anchorNone,XYWH(108,79,44,48),true,{texture='SGUI/lasseg_end_green.png',nomouseevent=true})
			
		end
		
		if cards.LIST[ID].CARD.SAILCATEGORY == 5 then -- Humans
			local v = cards.LIST[ID].ELE
			v.effect_4 = getElementEX(v,anchorNone,XYWH(147,15,20,48),true,{texture='SGUI/US_LaserExt_0d.png',nomouseevent=true})
			v.effect_5 = getElementEX(v,anchorNone,XYWH(-6,15,20,48),true,{texture='SGUI/US_LaserExt_2d.png',nomouseevent=true})
		end	

	end 

	if cards.LIST[ID].CARD.NATION == 3 then
	
		if cards.LIST[ID].CARD.SAILCATEGORY == 5 then -- Humans
			local v = cards.LIST[ID].ELE
			v.effect_4 = getElementEX(v,anchorNone,XYWH(3,41,58,95),true,{texture='SGUI/RU_SiberExt_4_20000.png',nomouseevent=true})
		end	
	
		if cards.LIST[ID].CARD.SAILCATEGORY == 3 then -- Buildings
			local v = cards.LIST[ID].ELE
			
			v.effect_1 = getElementEX(v,anchorNone,XYWH(3,41,58,95),true,{texture='SGUI/RU_SiberExt_4_20000.png',nomouseevent=true})
			v.effect_2 = getElementEX(v,anchorNone,XYWH(18,57,27,86),true,{texture='SGUI/Kour_siber.png',nomouseevent=true})
			v.effect_6 = getElementEX(v,anchorNone,XYWH(18,169,22,106),true,{texture='SGUI/a-r-trackext_1.png',nomouseevent=true})
			v.effect_7 = getElementEX(v,anchorNone,XYWH(116,169,22,106),true,{texture='SGUI/a-r-trackext_1.png',nomouseevent=true})
		end
		
		if cards.LIST[ID].CARD.SAILCATEGORY == 4 then -- Heroes
			local v = cards.LIST[ID].ELE
			v.effect_3 = getElementEX(v,anchorNone,XYWH(121,-14,52,100),true,{texture='SGUI/RU_SiberExt_1d_2.png',nomouseevent=true})
		end
		
	end
	
	if cards.LIST[ID].CARD.NATION == 2 then
	
		if cards.LIST[ID].CARD.SAILCATEGORY == 5 then -- Humans
			local v = cards.LIST[ID].ELE
			v.effect_4 = getElementEX(v,anchorNone,XYWH(139,74,26,43),true,{texture='SGUI/n-a-radar.png',nomouseevent=true})
		end	
	
		if cards.LIST[ID].CARD.SAILCATEGORY == 3 then -- Buildings
			local v = cards.LIST[ID].ELE
			
			v.effect_1 = getElementEX(v,anchorNone,XYWH(-3,90,36,56),true,{texture='SGUI/a-a-solarpower.png',nomouseevent=true})
		end
		
		if cards.LIST[ID].CARD.SAILCATEGORY == 4 then -- Heroes
			local v = cards.LIST[ID].ELE
			v.effect_3 = getElementEX(v,anchorNone,XYWH(139,29,16,24),true,{texture='SGUI/c-a-hovercraft-rem.png',nomouseevent=true})
			v.effect_2 = getElementEX(v,anchorNone,XYWH(-1,22,29,72),true,{texture='SGUI/n-a-control.png',nomouseevent=true})
		end
		
	end
	
	sgui_bringtofront(cards.LIST[ID].ELE.delete.ID)
	
	cards.onUpdateCard(ID)
end

function scaleWH(WIDTH,HEIGHT,TARGET_WIDTH,TARGET_HEIGHT)
  if (WIDTH <= TARGET_WIDTH) and (HEIGHT <= TARGET_HEIGHT) then
    return WIDTH, HEIGHT
  end

  if WIDTH > HEIGHT then
    return TARGET_WIDTH,math.floor(HEIGHT*TARGET_WIDTH/WIDTH)
  else
    return math.floor(WIDTH*TARGET_HEIGHT/HEIGHT),TARGET_HEIGHT
  end
end

function cards.setRotation(ID)
	local a = (cards.ANIMTIME-cards.LIST[ID].animtime)/cards.ANIMTIME*180
  	setRotation(cards.LIST[ID].ELE,SetRotate(1,a,true))
	setRotation(cards.LIST[ID].ELE.BACK,SetRotate(1,a,false))
	setY(cards.LIST[ID].ELE,0)
	setY(cards.LIST[ID].ELE.BACK,0)
end

function cards.setMovement(ID)
	local y = (cards.LIST[ID].animtime-cards.ANIMTIME)/cards.ANIMMOVETIME*cards.CARDHEIGHT
        setY(cards.LIST[ID].ELE,y)
        setY(cards.LIST[ID].ELE.BACK,y)
end

function cards.animate(ID,FRAMETIME)
	if (cards.LIST[ID].V) and (cards.LIST[ID].animtime > 0) then
		cards.LIST[ID].animtime = cards.LIST[ID].animtime - FRAMETIME
			if cards.LIST[ID].animtime < 0 then
				cards.LIST[ID].animtime = 0
			end
			if cards.LIST[ID].animtime > cards.ANIMTIME then
				cards.setMovement(ID)
			else
				cards.setRotation(ID)
			end
	end
end

function cards.freeCard(ID)

	if cards.LIST[ID].CARD.SAILCATEGORY == 4 then
		sgui_delete(cards.LIST[ID].ELE.xicht.ID)
	end
	
	if cards.LIST[ID].CARD.SAILCATEGORY == 3 then
		sgui_delete(cards.LIST[ID].ELE.building.ID)
	end
		
	if cards.LIST[ID].CARD.NATION == 1 then
		if cards.LIST[ID].CARD.SAILCATEGORY == 3 then
			sgui_delete(cards.LIST[ID].ELE.effect_1.ID)
			sgui_delete(cards.LIST[ID].ELE.effect_2.ID)
		end
		if cards.LIST[ID].CARD.SAILCATEGORY == 4 then
			sgui_delete(cards.LIST[ID].ELE.effect_3.ID)
			sgui_delete(cards.LIST[ID].ELE.effect_3b.ID)
			sgui_delete(cards.LIST[ID].ELE.effect_6.ID)
			sgui_delete(cards.LIST[ID].ELE.effect_7.ID)
		end
		if cards.LIST[ID].CARD.SAILCATEGORY == 5 then
			sgui_delete(cards.LIST[ID].ELE.effect_4.ID)
			sgui_delete(cards.LIST[ID].ELE.effect_5.ID)
		end
	end
		
	if cards.LIST[ID].CARD.NATION == 3 then
	
		if cards.LIST[ID].CARD.SAILCATEGORY == 3 then
			sgui_delete(cards.LIST[ID].ELE.effect_1.ID)
			sgui_delete(cards.LIST[ID].ELE.effect_2.ID)
			sgui_delete(cards.LIST[ID].ELE.effect_6.ID)
			sgui_delete(cards.LIST[ID].ELE.effect_7.ID)
		end
	
		if cards.LIST[ID].CARD.SAILCATEGORY == 4 then
			sgui_delete(cards.LIST[ID].ELE.effect_3.ID)
		end
		
		if cards.LIST[ID].CARD.SAILCATEGORY == 5 then
			sgui_delete(cards.LIST[ID].ELE.effect_4.ID)
		end
	
	end
	
	if cards.LIST[ID].CARD.NATION == 2 then
	
		if cards.LIST[ID].CARD.SAILCATEGORY == 3 then
			sgui_delete(cards.LIST[ID].ELE.effect_1.ID)
		end
	
		if cards.LIST[ID].CARD.SAILCATEGORY == 4 then
			sgui_delete(cards.LIST[ID].ELE.effect_3.ID)
			sgui_delete(cards.LIST[ID].ELE.effect_2.ID)
		end
		
		if cards.LIST[ID].CARD.SAILCATEGORY == 5 then
			sgui_delete(cards.LIST[ID].ELE.effect_4.ID)
		end
	
	end

	if not cards.LIST[ID].V then
        return
    end

    cards.LIST[ID].V = false
	setVisible(cards.LIST[ID].ELE,false)
	setVisible(cards.LIST[ID].ELE.BACK,false)

	cards.USED = cards.USED - 1
end

function cards.tick(FRAMETIME)
	if cards.USED > 0 then
        for i=1,cards.COUNT do
            cards.animate(i,FRAMETIME)
        end
    end
		

	if (cards.USED < cards.COUNT) then
        cards.addCard(cardPool.randomCard(),0) 
	end			

end

function cardPool.add(DATA)
	cardPool.COUNT=cardPool.COUNT+1
	cardPool.LIST[cardPool.COUNT] = DATA
        return cardPool.COUNT
end

function cardPool.randomCard()
	return cardPool.LIST[math.random(1,cardPool.COUNT)]
end

regTickCallback('cards.tick(%frametime)')

-----------------------------------------------------

cards.ELE = getElementEX(nil,anchorNone,XYWH(0,ScrHeight-cards.CARDHEIGHT,ScrWidth,cards.CARDHEIGHT),true,{nomouseeventthis=true,colour1=BLACKA(0),scissor=false})

function cards.onCardClick(ID)
	
	OW_CUSTOM_COMMAND(999,ENT.PLAYERS.ME)

	if cards.LIST[ID].animtime == 0 then
		local PRICE = ENT.DEPOTS[ENT.PLAYERS.ME][1]
		local card = cards.LIST[ID].CARD
		
		if cards.State == 'Game' and PRICE >= card.PRICE then 
			OW_CUSTOM_COMMAND(999, MULTI_PLAYERINFO_CURRENT_PLID[MyID].COLOUR)
			switch (card.SAILCATEGORY) : caseof {
				[3] = function (x)
					cards.addQueue(card.SAILIDENT, card.NATION)
					cards.renderPreview(interfaceSide, card.SAILIDENT)
					OW_CUSTOM_COMMAND(8, 1, 3, 0, 0)
					cards.freeCard(ID)
					end
				}
			if card.SAILCATEGORY ~= 3 then
				OW_CUSTOM_COMMAND(card.SAILCATEGORY, card.SAILIDENT, card.PRICE, 0, card.NATION)
				cards.freeCard(ID)
			end
		end
	end
end

function cards.renderPreview(side, ident)
  local data = {'-u-', '-r-', '-a-'}
  local texture = 'SGUI/b'..data[side]..Preview.buildings[ident+1][1][1][1]..'000'..(Temp-1)..'.png'

  TESTTEXTURE = loadOGLTexture(texture, true, false, false, false)
  setSubCoords(MOUSEOVER, SUBCOORD(0, 640-TESTTEXTURE.H, TESTTEXTURE.W, TESTTEXTURE.H))
  setWH(MOUSEOVER, TESTTEXTURE.W, TESTTEXTURE.H)
  setVisible(MOUSEOVER, true)

  set_Callback(gamewindow.ID,CALLBACK_MOUSECLICK,'if %b == 1 then ClickPreview(3,%b,'..side..',"'..texture..'") end if %b == 0 then sub_CallPreview(3,%x,%y,'..ENT.PLAYERS.ME..','..ident..') end') 

  set_Callback(gamewindow.ID,CALLBACK_MOUSEMOVE,'Render(%x,%y)')

  ENT.EXPORT = {side, string.sub(texture, 0, -6)}
end

function cards.onUpdateCard(ID)
	setFontColour(cards.LIST[ID].ELE.title,cards.LIST[ID].CARD.COL)
	setFontColour(cards.LIST[ID].ELE.title.o,cards.LIST[ID].CARD.COL)
end

function cards.addQueue(DATA,NATION)
	cards.Queue = {ENT.PLAYERS.ME,DATA,NATION}
end

-- Directories
cards.dir = {
['HUMANS'] = 'SGUI/Nao/cards/Humans/',
['Russian' ] = 'SGUI/Nao/cards/Skeletons/Cardfront_r.png',
['American'] = 'SGUI/Nao/cards/Skeletons/Cardfront_u.png',
['Arabian' ] = 'SGUI/Nao/cards/Skeletons/Cardfront_a.png'
}

local ENT = {COL = SIDE_COLOURS}

cardPool.data = {
	
	{ 
		-- Animals
		{2,1,0,'x0 crates',loc(TID_CARDS_Apeman),0,ENT.COL[8],0,0,''},
		{2,2,0,'x0 crates',loc(TID_CARDS_ExtremeApeman),0,ENT.COL[8],0,0,''},
		{2,3,0,'x0 crates',loc(TID_CARDS_Kamikaze),0,ENT.COL[8],0,0,''},
		{2,4,0,'x0 crates',loc(TID_CARDS_Machairodus),0,ENT.COL[8],0,0,''},
		
		-- Resources
		{6,1,10,'x10 crates',loc(TID_CARDS_Crates),0,ENT.COL[8],0,0,''},
		
		-- Buildings
		{3,31,20,'x20 crates',loc(TID_CARDS_Breastworks),0,ENT.COL[1],15,1,''},
		{3,29,20,'x20 crates',loc(TID_CARDS_Derrick),0,ENT.COL[1],15,1,''},
		{3,30,20,'x20 crates',loc(TID_CARDS_AlaskiteMine),0,ENT.COL[1],15,1,''},
		{3,6,30,'x30 crates',loc(TID_CARDS_Laboratory),0,ENT.COL[1],15,1,''},
		{3,2,30,'x30 crates',loc(TID_CARDS_Workshop),0,ENT.COL[1],15,1,''},
		{3,26,25,'x25 crates',loc(TID_CARDS_CombustionPowerPlant),0,ENT.COL[1],15,1,''},
		{3,4,45,'x45 crates',loc(TID_CARDS_Armoury),0,ENT.COL[1],19,1,''},
		{3,5,25,'x25 crates',loc(TID_CARDS_Barracks),0,ENT.COL[1],15,1,''},
		
		-- Humans
		{5,1,0,'x0 crates',loc(TID_CARDS_Soldier),0,ENT.COL[1],0,1,'American Soldier'},
		{5,2,10,'x10 crates',loc(TID_CARDS_Engineer),0,ENT.COL[1],0,1,'American Engineer'},
		
		-- Heroes
		{4,99,10,'x10 crates',loc(TID_CARDS_YourLobbyHero),cards.dir['HUMANS']..'dev/LobbyHeroXicht.png',ENT.COL[1],0,1,'Your Lobby Hero'},
		{4,100,20,'x20 crates',loc(TID_CARDS_ArthurPowell),cards.dir['HUMANS']..'dev/ArthurPowellXicht.png',ENT.COL[1],0,1,'Arthur Powell'},
		{4,101,20,'x20 crates',loc(TID_CARDS_HughStevens),cards.dir['HUMANS']..'dev/HuckStevensXicht.png',ENT.COL[1],0,1,'Hugh Stevens'},
		{4,102,15,'x15 crates',loc(TID_CARDS_PeterRoth),cards.dir['HUMANS']..'dev/PeterRothXicht.png',ENT.COL[1],0,1,'Peter Roth'},
		{4,103,18,'x18 crates',loc(TID_CARDS_RonHarrison),cards.dir['HUMANS']..'dev/RonHarrisonXicht.png',ENT.COL[1],0,1,'Ron Harrison'},
		{4,104,60,'x60 crates',loc(TID_CARDS_JohnMacmillan),cards.dir['HUMANS']..'dev/JohnMacmillanXicht.png',ENT.COL[1],0,1,'John Macmillan'},
		{4,105,24,'x24 crates',loc(TID_CARDS_FrankForsyth),cards.dir['HUMANS']..'dev/FrankForsythXicht.png',ENT.COL[1],0,1,'Frank Forsyth'},
		{4,106,9,'x9 crates',loc(TID_CARDS_JeffBrown),cards.dir['HUMANS']..'dev/JeffBrownXicht.png',ENT.COL[1],0,1,'Jeff Brown'},
		{4,120,0,'x0 crates',loc(TID_CARDS_TimGladstone),cards.dir['HUMANS']..'dev/TimGladstoneXicht.png',ENT.COL[1],0,1,'Tim Gladstone'},
		{4,116,18,'x18 crates',loc(TID_CARDS_LisaLawson),cards.dir['HUMANS']..'dev/LisaLawsonXicht.png',ENT.COL[1],0,1,'Lisa Lawson'}
		--{4,2001,75,'x75 crates','VOID',0,ENT.COL[1],0,1,'VOID'}
		--{4,2002,75,'x75 crates','VOID',0,ENT.COL[1],0,1,'VOID'}
		
	}, -- AM 1-25

	{ 
		-- Animals
		{2,1,0,'x0 crates',loc(TID_CARDS_Apeman),0,ENT.COL[8],0,0,''},
		{2,2,0,'x0 crates',loc(TID_CARDS_ExtremeApeman),0,ENT.COL[8],0,0,''},
		{2,3,0,'x0 crates',loc(TID_CARDS_Kamikaze),0,ENT.COL[8],0,0,''},
		{2,4,0,'x0 crates',loc(TID_CARDS_Machairodus),0,ENT.COL[8],0,0,''},
		
		-- Resources
		{6,1,10,'x10 crates',loc(TID_CARDS_Crates),0,ENT.COL[8],0,0,''},

		-- Buildings
		{3,31,20,'x20 crates',loc(TID_CARDS_Breastworks),0,ENT.COL[3],15,3,''},
		{3,29,20,'x20 crates',loc(TID_CARDS_Derrick),0,ENT.COL[3],15,3,''},
		{3,30,20,'x20 crates',loc(TID_CARDS_AlaskiteMine),0,ENT.COL[3],15,3,''},
		{3,6,30,'x30 crates',loc(TID_CARDS_Laboratory),0,ENT.COL[3],15,3,''},
		{3,2,30,'x30 crates',loc(TID_CARDS_Workshop),0,ENT.COL[3],15,3,''},
		{3,26,25,'x25 crates',loc(TID_CARDS_CombustionPowerPlant),0,ENT.COL[3],15,3,''},
		{3,4,45,'x45 crates',loc(TID_CARDS_Armoury),0,ENT.COL[3],19,3,''},
		{3,5,25,'x25 crates',loc(TID_CARDS_Barracks),0,ENT.COL[3],15,3,''},
		{3,37,0,'x0 crates',loc(TID_CARDS_Behemoth),0,ENT.COL[3],15,3,''},
		
		-- Humans
		{5,1,0,'x0 crates',loc(TID_CARDS_Soldier),0,ENT.COL[3],0,3,'Russian Soldier'},
		{5,2,10,'x10 crates',loc(TID_CARDS_Engineer),0,ENT.COL[3],0,3,'Russian Engineer'},

		-- Heroes
		{4,99,10,'x10 crates',loc(TID_CARDS_YourLobbyHero),cards.dir['HUMANS']..'dev/LobbyHeroXicht.png',ENT.COL[3],0,3,'Your Lobby Hero'},
		{4,300,60,'x60 crates',loc(TID_CARDS_BurlakGorky),cards.dir['HUMANS']..'dev/BurlakGorkyXicht.png',ENT.COL[3],0,3,'Burlak Gorky'},
		{4,301,0,'x00 crates',loc(TID_CARDS_MajorPlatonov),cards.dir['HUMANS']..'dev/MajorPlatonovXicht.png',ENT.COL[3],0,3,'Major Platonov'},
		{4,302,18,'x18 crates',loc(TID_CARDS_SergeyPopov),cards.dir['HUMANS']..'dev/SergeyPopovXicht.png',ENT.COL[3],0,3,'Sergey I. Popov'},
		{4,303,36,'x36 crates',loc(TID_CARDS_MarshalYashin),cards.dir['HUMANS']..'dev/MarshalYashinXicht.png',ENT.COL[3],0,3,'Marshal Yashin'},
		{4,304,30,'x30 crates',loc(TID_CARDS_IvanKurin),cards.dir['HUMANS']..'dev/IvanKurinXicht.png',ENT.COL[3],0,3,'Ivan Kurin'},
		{4,314,0,'x0 crates',loc(TID_CARDS_Grishko),cards.dir['HUMANS']..'dev/GrishkoXicht.png',ENT.COL[3],0,3,'Grishko'},
		{4,305,12,'x12 crates',loc(TID_CARDS_Belkov),cards.dir['HUMANS']..'dev/BelkovXicht.png',ENT.COL[3],0,3,'P. M. Belkov'},
		{4,329,24,'x24 crates',loc(TID_CARDS_Petrovova),cards.dir['HUMANS']..'dev/PetrovovaXicht.png',ENT.COL[3],0,3,'Petrovova'},
		{4,310,75,'x75 crates',loc(TID_CARDS_Eisenstein),cards.dir['HUMANS']..'dev/EisensteinXicht.png',ENT.COL[3],0,3,'Eisenstein'},
		{4,335,75,'x75 crates',loc(TID_CARDS_Tsarytsyn),cards.dir['HUMANS']..'dev/TsarytsynXicht.png',ENT.COL[3],0,3,'Tsarytsyn'}
		
	}, -- RU 27TOTAL 26-42
	
	{ 
		-- Animals
		{2,1,0,'x0 crates',loc(TID_CARDS_Apeman),0,ENT.COL[8],0,0,''},
		{2,2,0,'x0 crates',loc(TID_CARDS_ExtremeApeman),0,ENT.COL[8],0,0,''},
		{2,3,0,'x0 crates',loc(TID_CARDS_Kamikaze),0,ENT.COL[8],0,0,''},
		{2,4,0,'x0 crates',loc(TID_CARDS_Machairodus),0,ENT.COL[8],0,0,''},
		
		-- Resources
		{6,1,10,'x10 crates',loc(TID_CARDS_Crates),0,ENT.COL[8],0,0,''},
		
		-- Buildings
		{3,31,20,'x20 crates',loc(TID_CARDS_Breastworks),0,ENT.COL[2],15,2,''},
		{3,29,20,'x20 crates',loc(TID_CARDS_Derrick),0,ENT.COL[2],15,2,''},
		{3,30,20,'x20 crates',loc(TID_CARDS_AlaskiteMine),0,ENT.COL[2],15,2,''},
		{3,6,30,'x30 crates',loc(TID_CARDS_Laboratory),0,ENT.COL[2],15,2,''},
		{3,2,30,'x30 crates',loc(TID_CARDS_Workshop),0,ENT.COL[2],15,2,''},
		{3,26,25,'x25 crates',loc(TID_CARDS_CombustionPowerPlant),0,ENT.COL[2],15,2,''},
		{3,4,45,'x45 crates',loc(TID_CARDS_Armoury),0,ENT.COL[2],19,2,''},
		{3,5,25,'x25 crates',loc(TID_CARDS_Barracks),0,ENT.COL[2],15,2,''},
		
		-- Humans
		{5,1,0,'x0 crates',loc(TID_CARDS_Soldier),0,ENT.COL[2],0,2,'Arabian Soldier'},
		{5,2,10,'x10 crates',loc(TID_CARDS_Engineer),0,ENT.COL[2],0,2,'Arabian Engineer'},
		{5,11,0,'x0 crates',loc(TID_CARDS_Sheikh),0,ENT.COL[2],0,2,'Arabian Sheikh'},

		-- Heroes
		{4,99,10,'x10 crates',loc(TID_CARDS_YourLobbyHero),cards.dir['HUMANS']..'dev/LobbyHeroXicht.png',ENT.COL[2],0,2,'Your Lobby Hero'},
		{4,200,30,'x30 crates',loc(TID_CARDS_AbdulShariff),cards.dir['HUMANS']..'dev/AbdulShariffXicht.png',ENT.COL[2],0,2,'Abdul Shariff'},
		{4,202,30,'x30 crates',loc(TID_CARDS_DietrichGensher),cards.dir['HUMANS']..'dev/DietrichGensherXicht.png',ENT.COL[2],0,2,'Dietrich Gensher'},
		{4,203,30,'x30 crates',loc(TID_CARDS_RobertFarmer),cards.dir['HUMANS']..'dev/RobertFarmerXicht.png',ENT.COL[2],0,2,'Robert Farmer'},
		{4,204,48,'x48 crates',loc(TID_CARDS_HeikeSteyer),cards.dir['HUMANS']..'dev/HeikeSteyerXicht.png',ENT.COL[2],0,2,'Heike Steyer'},
		{4,205,24,'x24 crates',loc(TID_CARDS_KurtSchmidt),cards.dir['HUMANS']..'dev/KurtSchmidtXicht.png',ENT.COL[2],0,2,'Kurt Schmidt'},
		{4,206,15,'x18 crates',loc(TID_CARDS_SheikhOmar),cards.dir['HUMANS']..'dev/SheikhOmarXicht.png',ENT.COL[2],0,2,'Sheikh Omar'},
		{4,207,25,'x25 crates',loc(TID_CARDS_Sheherezade),cards.dir['HUMANS']..'dev/SheherezadeXicht.png',ENT.COL[2],0,2,'Sheherezade'},
		{4,208,24,'x24 crates',loc(TID_CARDS_RaulXavier),cards.dir['HUMANS']..'dev/RaulXavierXicht.png',ENT.COL[2],0,2,'Raul Xavier'},
		{4,2001,75,'x75 crates','VOID',0,ENT.COL[2],0,2,'VOID'},
		{4,2002,75,'x75 crates2','VOID',0,ENT.COL[2],0,2,'VOID'}
		
	} -- AR TOTAL27 43-69
	
}
	
--[[SAILCATEGORY]]
--[[SAILIDENT]]
--[[PRICE]]
--[[ATTRIB]]
--[[DESC]]
--[[XICHT]]
--[[COL]]
--[[PREVIEW]]
--[[NATION]]
--[[TITLE]]

function InitCards()

	for i = 1,#cardPool.data[interfaceSide] do 
		local D = cardPool.data
		local S = interfaceSide
		local T = {'American','Russian','Arabian'}
		cardPool.add({SAILCATEGORY=D[S][i][1],SAILIDENT=D[S][i][2],PRICE=D[S][i][3],TEXTURE=cards.dir[T[interfaceSide]],ATTRIB=D[S][i][4],XICHT=D[S][i][6],DESC=D[S][i][5],COL=D[S][i][7],PREVIEW=D[S][i][8],NATION=D[S][i][9],TITLE=D[S][i][10]})
	end

end

function InitCards()
  local data = cardPool.data[interfaceSide]
  local T = {'American','Russian','Arabian'}
  local texture = cards.dir[T[interfaceSide]]

  for _, card in ipairs(data) do
    cardPool.add({
      SAILCATEGORY  = card[1],
      SAILIDENT 	= card[2],
      PRICE 		= card[3],
      TEXTURE 		= texture,
      ATTRIB 		= card[4],
      XICHT 		= card[6],
      DESC 			= card[5],
      COL 			= card[7],
      PREVIEW 		= card[8],
	  NATION 		= card[9],
	  TITLE 		= card[10]
	})
  end
end

function StartIt(Who)
	-- Core management
	OW_CUSTOM_COMMAND(999,ENT.PLAYERS.ME) 
	OW_CUSTOM_COMMAND(997,SIDE)
end