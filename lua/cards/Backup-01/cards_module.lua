cards    = {LIST={}, COUNT=0, USED=0, ELE=nil, ANIMTIME=0, ANIMMOVETIME=0, CARDHEIGHT=192, CARDWIDTH=160, Queue={}}
cardPool = {LIST={}, COUNT=0}

function cards.add(DATA)
	cards.COUNT=cards.COUNT+1
    DATA.ID = cards.COUNT
	cards.LIST[cards.COUNT] = DATA
    return cards.COUNT
end

function cards.addCards(COUNT)
	cards.COUNT = 0
	cards.USED  = 0
	cards.LIST  = {}
	for i = 1,COUNT do
		cards.add({V=false})
	end
end

function cards.findFree()
	if cards.USED < cards.COUNT then
		for i = 1,cards.COUNT do
			if (not cards.LIST[i].V) then
				return i
			end
		end
	end
	return 0
end

function cards.addCard(DATA)
	local id = cards.findFree()
	if id < 1 then
		return
	end
		
	cards.LIST[id].V = true
	
	if cards.State == 'Game' then
		
		if interfaceSide == 1 then
			cards.LIST[id].CARD = cardPool.LIST[math.random(16,42)] 
		end
				
		if interfaceSide == 3 then
			cards.LIST[id].CARD = cardPool.LIST[math.random(43,69)] 
		end
			
		if interfaceSide == 2 then
			cards.LIST[id].CARD = cardPool.LIST[math.random(70,96)] 
		end	

	end
		
    cards.LIST[id].animtime = cards.ANIMTIME+cards.ANIMMOVETIME
    
    if cards.LIST[id].ELE == nil then
        cards.createCard(id)
    else
        cards.updateCard(id)
    end

    setY(cards.LIST[id].ELE,cards.CARDHEIGHT)
    setY(cards.LIST[id].ELE.BACK,cards.CARDHEIGHT)

    cards.USED = cards.USED + 1
end

function cards.createOutlineLabel(PARENT, POSSIZE, FONT1, FONT2, ID)
	local 	ELE = getLabelEX(PARENT,anchorNone,POSSIZE,FONT1,'',{font_colour=cards.LIST[ID].CARD.COL,shadowtext=true,text_halign=ALIGN_MIDDLE})
			ELE.o = getLabelEX(PARENT,anchorNone,POSSIZE,FONT2,'',{font_colour=cards.LIST[ID].CARD.COL,shadowtext=false,text_halign=ALIGN_MIDDLE})
	return  ELE
end

function cards.setOutlineLabelCol(ELE, COL1, COL2)
	setFontColour(ELE, COL1)
    setFontColour(ELE.o, COL2)
end

function cards.createCard(ID)
	if ID < 13 then 
		cards.LIST[ID].ELE 		  = getElementEX(cards.ELE,anchorLT,XYWH((ID-1)*cards.CARDWIDTH,150,cards.CARDWIDTH,cards.CARDHEIGHT),true,{})
		cards.LIST[ID].ELE.xicht  = getElementEX(cards.LIST[ID].ELE,anchorNone,XYWH(15,166-80-40,50,62),true,{})
		cards.LIST[ID].ELE.title  = cards.createOutlineLabel(cards.LIST[ID].ELE,XYWH(0,15,cards.CARDWIDTH,0),'Tahoma_70_Fixed_16_2.DFF0.380.140.51','Tahoma_70_Fixed_16_2.DFF0.380.140.51',ID)
		cards.LIST[ID].ELE.attrib = getLabelEX(cards.LIST[ID].ELE,anchorNone,XYWH(20,90,cards.CARDWIDTH-40,0),Tahoma_12B,'',{font_colour=purple,font_style_outline=true,shadowtext=true,text_halign=ALIGN_MIDDLE})
		cards.LIST[ID].ELE.desc   = getLabelEX(cards.LIST[ID].ELE,anchorNone,XYWH(15,118,cards.CARDWIDTH-30,55),Tahoma_12B,'',{font_colour=description,font_style_outline=true,shadowtext=true,wordwrap=true,text_halign=ALIGN_MIDDLE})
		cards.LIST[ID].ELE.BACK   = getElementEX(cards.ELE,anchorLT,XYWH((ID-1)*160,0,cards.CARDWIDTH,cards.CARDHEIGHT),true,{texture='cardtest1.png',callback_mouseleave="AddEventSlideY(cards.LIST["..ID.."].ELE.ID,0,0.25,'')",callback_mouseover="AddEventSlideY(cards.LIST["..ID.."].ELE.ID,-50,0.25,'') setText(datainfo,cards.LIST["..ID.."].CARD.DESC)",callback_mouseclick='cards.onCardClick('..ID..')'}) 
	end
    cards.updateCard(ID)
end

discard = {}
for i = 1, 4 do
	table.insert(discard, getImageButtonEX(nil, anchorNone, XYWH(cards.CARDWIDTH*(i-1), ScrHeight-cards.CARDHEIGHT-25, cards.CARDWIDTH, 25), 'Discard', '', 'cards.freeCard('..i..')', SKINTYPE_BUTTON, {colour2=BLACKA(350), font_colour_disabled=GRAY(127)}))
end

function cards.updateCard(ID)
	setTexture(cards.LIST[ID].ELE,cards.LIST[ID].CARD.TEXTURE) 
	setTexture(cards.LIST[ID].ELE.xicht,cards.LIST[ID].CARD.XICHT)
	if cards.LIST[ID].CARD.SAILCATEGORY == 8 then
		setTexture(cards.LIST[ID].ELE.subtexture,cards.LIST[ID].CARD.SUBTEXTURE)
		setWH(cards.LIST[ID].ELE.subtexture,cards.LIST[ID].CARD.SUBTW,cards.LIST[ID].CARD.SUBTH)
		setX(cards.LIST[ID].ELE.subtexture,getX(cards.LIST[ID].ELE.subtexture)-cards.LIST[ID].CARD.SUBTW/2)
	end
	setRotation(cards.LIST[ID].ELE,SetRotate(1,0,true))
	setRotation(cards.LIST[ID].ELE.BACK,SetRotate(1,0,false))
	setVisible(cards.LIST[ID].ELE,true)
	setVisible(cards.LIST[ID].ELE.BACK,true)
	setText(cards.LIST[ID].ELE.title,cards.LIST[ID].CARD.TITLE)
	setText(cards.LIST[ID].ELE.title.o,cards.LIST[ID].CARD.TITLE)
	setText(cards.LIST[ID].ELE.attrib,cards.LIST[ID].CARD.ATTRIB)
	setText(cards.LIST[ID].ELE.desc,cards.LIST[ID].CARD.DESC)
	cards.onUpdateCard(ID)
end

function cards.setRotation(ID)
	local a = (cards.ANIMTIME-cards.LIST[ID].animtime)/cards.ANIMTIME*180
  	setRotation(cards.LIST[ID].ELE,SetRotate(1,a,true))
	setRotation(cards.LIST[ID].ELE.BACK,SetRotate(1,a,false))
	setY(cards.LIST[ID].ELE,0)
	setY(cards.LIST[ID].ELE.BACK,0)
end

function cards.setMovement(ID)
	local y = (cards.LIST[ID].animtime-cards.ANIMTIME)/cards.ANIMMOVETIME*cards.CARDHEIGHT
	setY(cards.LIST[ID].ELE,y)
	setY(cards.LIST[ID].ELE.BACK,y)
end

function cards.animate(ID, FRAMETIME)
	if (cards.LIST[ID].V) and (cards.LIST[ID].animtime > 0) then
		cards.LIST[ID].animtime = cards.LIST[ID].animtime - FRAMETIME
		if cards.LIST[ID].animtime < 0 then
			cards.LIST[ID].animtime = 0
		end
		if cards.LIST[ID].animtime > cards.ANIMTIME then
			cards.setMovement(ID)
		else
			cards.setRotation(ID)
		end
	end
end

function cards.freeCard(ID)
	if not cards.LIST[ID].V then
		return
	end
	cards.LIST[ID].V = false
	setVisible(cards.LIST[ID].ELE,false)
	setVisible(cards.LIST[ID].ELE.BACK,false) 
	cards.USED = cards.USED - 1 
end

function cards.tick(FRAMETIME)
	if cards.USED > 0 then
		for i= 1, cards.COUNT do
			cards.animate(i, FRAMETIME)
		end
	end
	if (cards.USED < cards.COUNT) then
		cards.addCard(cardPool.randomCard(), 0) 
	end
end

function cardPool.add(DATA)
	cardPool.COUNT = cardPool.COUNT +1
	cardPool.LIST[cardPool.COUNT] = DATA
	return cardPool.COUNT
end

function cardPool.randomCard()
	return cardPool.LIST[math.random(1, cardPool.COUNT)]
end

regTickCallback('cards.tick(%frametime)')

-----------------------------------------------------

cards.ELE = getElementEX(nil, anchorNone, XYWH(0, ScrHeight-cards.CARDHEIGHT, 0, 0), true, {colour1=BLACKA(0), scissor=false})

function cards.onCardClick(ID)
	OW_CUSTOM_COMMAND(999, MULTI_PLAYERINFO_CURRENT_PLID[MyID].COLOUR)
	if cards.LIST[ID].animtime == 0 then
		if cards.State == 'Game' then			
			if cards.LIST[ID].CARD.SAILCATEGORY == 3 then 
				cards.addQueue(cards.LIST[ID].CARD.SAILIDENT, cards.LIST[ID].CARD.NATION)
				CallPreview(1, interfaceSide, 1, MULTI_PLAYERINFO_CURRENT_PLID[MyID].COLOUR)
				OW_CUSTOM_COMMAND(8, 1, 3, 0, 0)
			end
			OW_CUSTOM_COMMAND(cards.LIST[ID].CARD.SAILCATEGORY, cards.LIST[ID].CARD.SAILIDENT, cards.LIST[ID].CARD.PRICE, cards.LIST[ID].CARD.VEHICLEPART, cards.LIST[ID].CARD.NATION)				
			cards.freeCard(ID)
		end
	end
end

function cards.onUpdateCard(ID)
	if cards.State == 'Game' then 
		setFontColour(cards.LIST[ID].ELE.title, cards.LIST[ID].CARD.COL)
		setFontColour(cards.LIST[ID].ELE.title.o, cards.LIST[ID].CARD.COL)
	end
end

function cards.addQueue(DATA,NATION)
	cards.Queue = {MULTI_PLAYERINFO_CURRENT_PLID[MyID].COLOUR, DATA, NATION}
end

include('translations')

include('var')

include('Module/Library/Starter/_us')
include('Module/Library/Starter/_ar')
include('Module/Library/Starter/_ru')

include('Module/Library/Deck/_us')
include('Module/Library/Deck/_ar')
include('Module/Library/Deck/_ru')

include('Module/Library/Power/_powers')

include('Module/Library/Component/_us')
include('Module/Library/Component/_ar')
include('Module/Library/Component/_ru')