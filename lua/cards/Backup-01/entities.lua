ENT = {DEV="NAO"};

ENT.COL = {
	RGB(0,0,255), 		-- Blue
	RGB(255,255,0), 	-- Yellow
	RGB(255,0,0), 		-- Red
	RGB(0,255,255), 	-- Cyan
	RGB(255,125,0), 	-- Orange
	RGB(255,0,255), 	-- Purple
	RGB(0,255,0),	 	-- Green
	RGB(255,255,255) }; -- White
	
ENT.PLAYERS = {};

ENT.PLAYERS.POS = {};

ENT.DEPOTS = {
	{0,0,0,1}, -- Crates, Oil, Alaskite, Side
	{0,0,0,2}  -- Crates, Oil, Alaskite, Side
};

ENT.CONSOLE = 1; -- (Not Multiplayer)