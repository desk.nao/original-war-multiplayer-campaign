include('Utils/fast2D','Utils/OpenGL');
MONITOR_FREQ = 60;

function FROMSGUI_INITEND()
	Monitors = GET_MONITOR_LIST();
	
	for i=1,#Monitors do
		if Monitors[i].IsPrimary then
			MONITOR_FREQ = Monitors[i].Freq;
			break;
		end;
	end;
end;

VSYNC_MODE = false;

--| MISC | --

TVLINES_GLSL                 = loadGLSL('tvlines');
TVLINES_GLSL.TIME            = TVLINES_GLSL:getLocation('time');
TESTTEXTURE = loadOGLTexture('images/b-a-armoury0004.png',true,false,false,false);

function do_Init()
	local w,h = 640,640;

	FBO = fboclass.make(w,h,true,false,0,false);
	
	MOUSEOVER = getElementEX(nil,anchorLT,XYWH(0,0,TESTTEXTURE.W,TESTTEXTURE.H),true,{nomouseevent=true,subtexture=true,subcoords=SUBCOORD(0,640-TESTTEXTURE.H,TESTTEXTURE.W,TESTTEXTURE.H)});	
	
	SGUI_settextureid(MOUSEOVER.ID,FBO:getTextureID(),640,640,640,640);
	
	timer:single(0.0001,"SGUI_register_tick_callback('rendertest(%frametime)');");
end;

timer:single(1,'do_Init()');

THETIME = 0;
FRAME_PROG = 0;

function rendertest(FRAMETIME)	
	FRAME_PROG = FRAME_PROG + FRAMETIME;
	
	local needupdate = false;
	
	while FRAME_PROG >= 1/30 do				

		needupdate = true;
		THETIME = THETIME + 1/30;
	
		FRAME_PROG = FRAME_PROG - 1/30;
		if FRAME_PROG > 1/2 then
			FRAME_PROG = 0;
		end;
	end;
	
	if (not needupdate) then
		return;
	end;

	FBO:setClearColour(0,0,0,0,false);
	
	FBO:doBegin();
		OGL_BEGIN();
			OGL_GLENABLE(GL_BLEND);
			OGL_QUAD_BEGIN2D(FBO.W,FBO.H);
			
			TVLINES_GLSL:use();
			TVLINES_GLSL:setMatrixs();
			TVLINES_GLSL:setUniform1F(TVLINES_GLSL.TIME, THETIME);
									
			OGL_TEXTURE_BINDID(TESTTEXTURE:getTextureID());
			OGL_QUAD_RENDER(0,0,TESTTEXTURE.W,TESTTEXTURE.H,0,0,1,1); 
			
			OGL_QUAD_END2D();
		OGL_END();
	FBO:doEnd();
end;