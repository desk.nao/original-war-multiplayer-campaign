-- Colours
blue = RGB(100,149,237)
red =  RGB(240,128,128)
yellow = RGB(255,255,0)
description = RGB(210,180,140)
purple = RGB(230,230,250)

-- Directories
EMPTY = 'SGUI/Nao/cards/Skeletons/EmptyXicht.png'
ANIMALS = 'SGUI/Nao/cards/Animals/'
BUILDINGS = 'SGUI/Nao/cards/Buildings/'
VEHICLES = 'SGUI/Nao/cards/Vehicles/'
AMERICAN = 'skeleton_vehicles_american.png'
ARABIAN = 'skeleton_vehicles_arabian.png'
RUSSIAN = 'skeleton_vehicles_russian.png'
HUMANS = 'SGUI/Nao/cards/Humans/'
POWERS = 'SGUI/Nao/cards/Powers/'
RESOURCES = 'SGUI/Nao/cards/Resources/'
SKELETONS = 'SGUI/Nao/cards/Skeletons/Cardfront.png'
Start = 'SGUI/Nao/cards/Starting/'