function makeDebugSailWindow()
	if debugsailwindow == nil then
		debugsailwindow = AddElement({
			type=TYPE_TEXTBOX,
			anchor={top=true,bottom=true,left=true,right=true},
			x=10,
			y=10,
			width=ScrWidth-20-12-1,
			height=500,
			colour1=IRCBackCol,
			font_name=Tahoma_13,
			wordwrap=true,
			bevel_colour1=GRAYA(14,200),
			bevel_colour2=GRAYA(14,200),
			text='',
		});

		debugsailwindow.scrollV = AddElement({
			type=TYPE_SCROLLBAR,
			anchor={top=true,bottom=true,left=false,right=true},
			x=debugsailwindow.x+debugsailwindow.width+1,
			y=debugsailwindow.y,
			width=12,
			height=debugsailwindow.height,
			colour1=Scrollbar_Colour1,
			colour2=Scrollbar_Colour2,
			texture2="scrollbar.png",
		});

		sgui_set(debugsailwindow.ID,PROP_SCROLLBAR,debugsailwindow.scrollV.ID);
        end;
end;

function toggleDebugSailWindow()
	if debugsailwindow ~= nil then
		setVisible(debugsailwindow,not getVisible(debugsailwindow));
		setVisible(debugsailwindow.scrollV,not getVisible(debugsailwindow.scrollV));
	end;
end;

function DebugSailWindow_BuildCommon(TITLE,DATA)
	local c;

	c = '['..TITLE..']'..CHAR13;

	for i=1,DATA.COUNT do
		if DATA.ITEMS[i] ~= nil then
				c = c..DATA.ITEMS[i].NAME..': '..DATA.ITEMS[i].VALUE;
		end;
		c = c..CHAR13;
	end;

	return c..CHAR13;
end;

function DebugSailWindow_BuildEvents(TITLE,DATA)
	local c;

	c = '['..TITLE..']'..CHAR13;

	for i=1,DATA.COUNT do
		if DATA.ITEMS[i] ~= nil then
			c = c..(i)..': '..DATA.ITEMS[i];
		else
			c = c..(i)..': NIL';
		end;
		c = c..CHAR13;
	end;

	return c..CHAR13;
end;

function DebugSailWindow_BuildArtifacts(TITLE,DATA)
	local c;

	c = '['..TITLE..']'..CHAR13;

	for i=1,DATA.MAXSIDES do
		if DATA.ITEMS[i][j] ~= nil then
			c = c..(i)..' - STATE:'..DATA.ITEMS[i][j].STATE..' LAB:'..DATA.ITEMS[i][j].LAB;
		else
			c = c..(i)..': NIL';
		end;
		c = c..CHAR13;
	end;

	return c..CHAR13;
end;

function FROMOW_DEBUG_SAIL(DATA)
	makeDebugSailWindow();

	setText(debugsailwindow,'');

	local all = '';

	all = all..DebugSailWindow_BuildCommon('EXPORTS',DATA.EXPORTS)..DebugSailWindow_BuildCommon('LOCAL',DATA.LOCAL)..DebugSailWindow_BuildCommon('VARS',DATA.VARS);
	all = all..DebugSailWindow_BuildEvents('TRIGGERS',DATA.TRIGGERS)..DebugSailWindow_BuildEvents('EVENT',DATA.EVENT)..DebugSailWindow_BuildArtifacts('ARTIFACT',DATA.ARTIFACT);

	setText(debugsailwindow,all);
end;


OW_DEBUG_SAIL();
