function Init()
	include('cards_module')
  	MONITOR_FREQ = 60
end

function FROMSGUI_INITEND()
  Monitors = GET_MONITOR_LIST()

  for i = 1, #Monitors do
    if Monitors[i].IsPrimary then
      MONITOR_FREQ = Monitors[i].Freq
      break
    end
  end

  VSYNC_MODE = false

  include('Utils/fast2D','Utils/OpenGL','shad')
end