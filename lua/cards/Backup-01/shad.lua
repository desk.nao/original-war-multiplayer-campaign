OGL_SET_VSYNC(VSYNC_MODE)
SET_FPS_LIMIT(0)

--| FPS Counter | --

fps = getLabelEX(nil,anchorLTR,XYWH(0,0,LayoutWidth,20),Tahoma_10B,'',{text_halign=ALIGN_LEFT,special=true,font_style_outline=true,shadowtext=false,font_colour=RGB(250,250,250),visible=false})

function FROMAPP_FPS(FPS, AFPS)
	setText(fps,AFPS)
end

testlabel = getLabelEX(nil,anchorLTR,XYWH(0,40,LayoutWidth,20),Tahoma_10B,'',{text_halign=ALIGN_LEFT,special=true,font_style_outline=true,shadowtext=false,font_colour=RGB(250,250,250)})

--| MISC | --

setColour1({ID=0},RGB(0,0,0)) -- Set Desktop Colour

TVLINES_GLSL                 = loadGLSL('tvlines')
TVLINES_GLSL.TIME            = TVLINES_GLSL:getLocation('time')
TESTTEXTURE = loadOGLTexture('SGUI/buildings/b-r-warehouse0000.png',true,false,false,false)


function do_Init()
	local w,h = 640,640

	getLabelEX(nil,anchorTR,XYWH(w-32,0,32,32),Tahoma_20,'X',{special=true,nomouseevent=false,callback_mouseclick='sgui_debug(1)',text_halign=ALIGN_MIDDLE,font_colour=RGB(255,255,255)})
	getLabelEX(nil,anchorTR,XYWH(w-64,0,32,32),Tahoma_20,'R',{special=true,nomouseevent=false,callback_mouseclick='sgui_debug(0)',text_halign=ALIGN_MIDDLE,font_colour=RGB(255,255,255)})
	getLabelEX(nil,anchorTR,XYWH(w-96,0,32,32),Tahoma_20,'V',{special=true,nomouseevent=false,callback_mouseclick='VSYNC_MODE = not VSYNC_MODE OGL_SET_VSYNC(VSYNC_MODE)',text_halign=ALIGN_MIDDLE,font_colour=RGB(255,255,255)})
	
	FBO = fboclass.make(w,h,true,false,0,false)
	
	setVisible(fps,true)	
	
	MOUSEOVER = getElementEX({ID=1},anchorLT,XYWH(0,0,TESTTEXTURE.W,TESTTEXTURE.H),true,{subtexture=true,subcoords=SUBCOORD(0,640-TESTTEXTURE.H,TESTTEXTURE.W,TESTTEXTURE.H)})	
	SGUI_settextureid(MOUSEOVER.ID,FBO:getTextureID(),640,640,640,640)
	
	timer:single(0.0001,"SGUI_register_tick_callback('rendertest(%frametime)')")
end

timer:single(1,'do_Init()')

THETIME = 0
FRAME_PROG = 0

function rendertest(FRAMETIME)	
	FRAME_PROG = FRAME_PROG + FRAMETIME
	
	local needupdate = false
	
	while FRAME_PROG >= 1/30 do				

		needupdate = true
		THETIME = THETIME + 1/30
	
		FRAME_PROG = FRAME_PROG - 1/30
		if FRAME_PROG > 1/2 then
			FRAME_PROG = 0
		end
	end
	
	if (not needupdate) then
		return
	end

	FBO:setClearColour(0,0,0,0,false)
	
	FBO:doBegin()
		OGL_BEGIN()
			OGL_GLENABLE(GL_BLEND)
			OGL_QUAD_BEGIN2D(FBO.W,FBO.H)
			
			TVLINES_GLSL:use()
			TVLINES_GLSL:setMatrixs()
			TVLINES_GLSL:setUniform1F(TVLINES_GLSL.TIME, THETIME)
									
			OGL_TEXTURE_BINDID(TESTTEXTURE:getTextureID())
			OGL_QUAD_RENDER(0,0,TESTTEXTURE.W,TESTTEXTURE.H,0,0,1,1) 
			
			OGL_QUAD_END2D()
		OGL_END()
	FBO:doEnd()
end