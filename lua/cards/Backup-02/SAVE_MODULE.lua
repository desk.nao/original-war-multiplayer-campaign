--OW_MULTI_GET_PLAYERINFO
--OW_MULTI_GET_STATS

Character = 1;
AmountSaved = 0;

--[[We need:
Humans       (X, Y, Side, Nation, Gender, Class, Health Points, Direction, Tasks)
Animals      (X, Y, Side, Type, Health Points, Directions, Tasks)
Buildings    (X, Y, Side, Nation, Type, Health Points, Direction, Storage for Depots, Tasks, Level)
Vehicles     (X, Y, Side, Nation, Type, Health Points, Direction, Oil or Solar percentage, Tasks)
Environments (X, Y, Identifier, Number)

Destroyed Environments (X, Y) for each--]]

function SAVE_POINTS(SECTION, NAME, V2, V3, V4, V5, V6, V7)
	OW_SETTING_WRITE('UNITS_TSK1','Unit#'..NAME,'M'); 
	OW_SETTING_WRITE('UNITS_TSK2','Unit#'..NAME,V2);
	OW_SETTING_WRITE('UNITS_TSK3','Unit#'..NAME,V3);
	OW_SETTING_WRITE('UNITS_TSK4','Unit#'..NAME,V4);
	OW_SETTING_WRITE('UNITS_TSK5','Unit#'..NAME,V5);
	OW_SETTING_WRITE('UNITS_TSK6','Unit#'..NAME,V6);
	OW_SETTING_WRITE('UNITS_TSK7','Unit#'..NAME,V7);
	OW_SETTING_WRITE('UNITS_ALLT','Unit#'..NAME,'M,'..V2..','..V3..','..V4..','..V5..','..V6..','..V7);
end;

-- for units:     x, y, side, nation, gender, class, lives, direction, soldier, engineer, mechanic, scientistic, speed, armour, inside, tasks
-- for buildings: x, y, side, nation, type, storage, lives, direction, level, sub1, sub2, tasks

function SAVE_TECHNOLOGY(SECTION, NAME, VALUE)
OW_SETTING_WRITE('TECHNOLOGIES','Side#'..SECTION..'.Technology#'..NAME,VALUE);
end;

CurrentColour = 0;
function SAVE(SECTION, NAME, VALUE)

	if SECTION == 110 then OW_SETTING_WRITE('BUILDINGS_AMOT','Buildings.with.units',VALUE); end; 
	if SECTION == 111 then OW_SETTING_WRITE('VEHICLES_AMOT','Vehicles.with.units',VALUE); end;
	
	if SECTION == 112 then OW_SETTING_WRITE('MAP_SAVED','Player#'..NAME..'.active',VALUE); end;
		
	if SECTION == 100 then CurrentColour = CurrentColour+1; OW_SETTING_WRITE('MAP_SAVED','Player#'..NAME..'.name',Players[CurrentColour].Name); end;
	if SECTION == 101 then OW_SETTING_WRITE('MAP_SAVED','Player#'..NAME..'.side',VALUE); end;
	if SECTION == 101 then OW_SETTING_WRITE('MAP_SAVED','Player#'..NAME..'.colour',VALUE); end;
	if SECTION == 102 then OW_SETTING_WRITE('MAP_SAVED','Player#'..NAME..'.team',VALUE); end;
	
	if SECTION == 103 then 
		OW_SETTING_WRITE('MAP_SAVED','Player#'..NAME..'.nation',3);
		OW_SETTING_WRITE('MAP_SAVED','Player#'..NAME..'.nation',2);
	end;
	
	if SECTION == 104 then OW_SETTING_WRITE('MAP_SAVED','Player#'..NAME..'.position',VALUE); end;
	if SECTION == 105 then OW_SETTING_WRITE('MAP_SAVED','Map.gametype',VALUE); end;	
	OW_SETTING_WRITE('MAP_SAVED','Map.name',MultiDef.MapName.MAP);
	if SECTION == 107 then OW_SETTING_WRITE('MAP_SAVED','AmountPlayers',VALUE); end;	
	if SECTION == 108 then OW_SETTING_WRITE('MAP_SAVED','MyIdentifier',VALUE); end;	
	
	if SECTION == 201 then OW_SETTING_WRITE('BUILDINGS_POSX','Building.with.unit#'..NAME,VALUE); end;
	if SECTION == 202 then OW_SETTING_WRITE('BUILDINGS_POSY','Building.with.unit#'..NAME,VALUE); end;
	if SECTION == 203 then OW_SETTING_WRITE('BUILDINGS_SIDE','Building.with.unit#'..NAME,VALUE); end; 
	if SECTION == 204 then OW_SETTING_WRITE('BUILDINGS_NATI','Building.with.unit#'..NAME,VALUE); end;
	if SECTION == 205 then OW_SETTING_WRITE('BUILDINGS_TYPE','Building.with.unit#'..NAME,VALUE); end; 
	if SECTION == 206 then OW_SETTING_WRITE('BUILDINGS_HPOI','Building.with.unit#'..NAME,VALUE); end;
	if SECTION == 207 then OW_SETTING_WRITE('BUILDINGS_DIRE','Building.with.unit#'..NAME,VALUE); end;
	if SECTION == 208 then OW_SETTING_WRITE('BUILDINGS_STOR','Building.with.unit#'..NAME,VALUE); end;
	if SECTION == 209 then OW_SETTING_WRITE('BUILDINGS_TASK','Building.with.unit#'..NAME,VALUE); end;
	if SECTION == 250 then OW_SETTING_WRITE('BUILDINGS_LEVE','Building.with.unit#'..NAME,VALUE); end;
	if SECTION == 255 then OW_SETTING_WRITE('BUILDINGS_SUB1','Building.with.unit#'..NAME,VALUE); end;
	if SECTION == 256 then OW_SETTING_WRITE('BUILDINGS_SUB2','Building.with.unit#'..NAME,VALUE); end;
	
	if SECTION == 227 then OW_SETTING_WRITE('VEHICLES_POSX','Vehicle.with.unit#'..NAME,VALUE); end; 
	if SECTION == 228 then OW_SETTING_WRITE('VEHICLES_POSY','Vehicle.with.unit#'..NAME,VALUE); end; 
	if SECTION == 229 then OW_SETTING_WRITE('VEHICLES_SIDE','Vehicle.with.unit#'..NAME,VALUE); end; 
	if SECTION == 230 then OW_SETTING_WRITE('VEHICLES_NATI','Vehicle.with.unit#'..NAME,VALUE); end; 
	if SECTION == 231 then OW_SETTING_WRITE('VEHICLES_CRAT','Vehicle.with.unit#'..NAME,VALUE); end; 
	if SECTION == 252 then OW_SETTING_WRITE('VEHICLES_OILB','Vehicle.with.unit#'..NAME,VALUE); end; 
	if SECTION == 253 then OW_SETTING_WRITE('VEHICLES_ALAS','Vehicle.with.unit#'..NAME,VALUE); end; 
	if SECTION == 232 then OW_SETTING_WRITE('VEHICLES_HPOI','Vehicle.with.unit#'..NAME,VALUE); end; 
	if SECTION == 233 then OW_SETTING_WRITE('VEHICLES_DIRE','Vehicle.with.unit#'..NAME,VALUE); end; 
	if SECTION == 234 then OW_SETTING_WRITE('VEHICLES_FUEL','Vehicle.with.unit#'..NAME,VALUE); end; 
	if SECTION == 235 then OW_SETTING_WRITE('VEHICLES_TASK','Vehicle.with.unit#'..NAME,VALUE); end; 
	if SECTION == 236 then OW_SETTING_WRITE('VEHICLES_CHAS','Vehicle.with.unit#'..NAME,VALUE); end; 
	if SECTION == 237 then OW_SETTING_WRITE('VEHICLES_ENGI','Vehicle.with.unit#'..NAME,VALUE); end; 
	if SECTION == 238 then OW_SETTING_WRITE('VEHICLES_CONT','Vehicle.with.unit#'..NAME,VALUE); end; 
	if SECTION == 239 then OW_SETTING_WRITE('VEHICLES_WEAP','Vehicle.with.unit#'..NAME,VALUE); end; 	

	if SECTION == 0 then OW_SETTING_WRITE('BUILDINGS_AMOT','AmountBuildings',VALUE); end;
	
	if SECTION == 1 then OW_SETTING_WRITE('BUILDINGS_POSX','Building#'..NAME,VALUE); end;
	if SECTION == 2 then OW_SETTING_WRITE('BUILDINGS_POSY','Building#'..NAME,VALUE); end;
	if SECTION == 3 then OW_SETTING_WRITE('BUILDINGS_SIDE','Building#'..NAME,VALUE); end; 
	if SECTION == 4 then OW_SETTING_WRITE('BUILDINGS_NATI','Building#'..NAME,VALUE); end;
	if SECTION == 5 then OW_SETTING_WRITE('BUILDINGS_TYPE','Building#'..NAME,VALUE); end; 
	if SECTION == 6 then OW_SETTING_WRITE('BUILDINGS_HPOI','Building#'..NAME,VALUE); end;
	if SECTION == 7 then OW_SETTING_WRITE('BUILDINGS_DIRE','Building#'..NAME,VALUE); end;
	if SECTION == 8 then OW_SETTING_WRITE('BUILDINGS_STOR','Building#'..NAME,VALUE); end;
	if SECTION == 9 then OW_SETTING_WRITE('BUILDINGS_TASK','Building#'..NAME,VALUE); end;
	if SECTION == 50 then OW_SETTING_WRITE('BUILDINGS_LEVE','Building#'..NAME,VALUE); end;
	if SECTION == 55 then OW_SETTING_WRITE('BUILDINGS_SUB1','Building#'..NAME,VALUE); end;
	if SECTION == 56 then OW_SETTING_WRITE('BUILDINGS_SUB2','Building#'..NAME,VALUE); end;
	
	if SECTION == 10 then OW_SETTING_WRITE('UNITS_AMOT','AmountUnits',VALUE); end; 
	
	if SECTION == 11 then OW_SETTING_WRITE('UNITS_POSX','Unit#'..NAME,VALUE); end; 
	if SECTION == 12 then OW_SETTING_WRITE('UNITS_POSY','Unit#'..NAME,VALUE); end; 
	if SECTION == 13 then OW_SETTING_WRITE('UNITS_SIDE','Unit#'..NAME,VALUE); end; 
	if SECTION == 14 then OW_SETTING_WRITE('UNITS_NATI','Unit#'..NAME,VALUE); end; 
	if SECTION == 15 then OW_SETTING_WRITE('UNITS_GEND','Unit#'..NAME,VALUE); end; 
	if SECTION == 16 then OW_SETTING_WRITE('UNITS_CLAS','Unit#'..NAME,VALUE); end; 
	if SECTION == 17 then OW_SETTING_WRITE('UNITS_HPOI','Unit#'..NAME,VALUE); end; 
	if SECTION == 18 then OW_SETTING_WRITE('UNITS_DIRE','Unit#'..NAME,VALUE); end; 
	if SECTION == 20 then OW_SETTING_WRITE('UNITS_SOLD','Unit#'..NAME,VALUE); end;
	if SECTION == 21 then OW_SETTING_WRITE('UNITS_ENGI','Unit#'..NAME,VALUE); end; 
	if SECTION == 22 then OW_SETTING_WRITE('UNITS_MECH','Unit#'..NAME,VALUE); end; 
	if SECTION == 23 then OW_SETTING_WRITE('UNITS_SCIE','Unit#'..NAME,VALUE); end; 	
	if SECTION == 24 then OW_SETTING_WRITE('UNITS_SPEE','Unit#'..NAME,VALUE); end; 
	if SECTION == 25 then OW_SETTING_WRITE('UNITS_ARMO','Unit#'..NAME,VALUE); end; 
	if SECTION == 53 then OW_SETTING_WRITE('UNITS_INSI','Unit#'..NAME,VALUE); end; 
	
	if SECTION == 26 then OW_SETTING_WRITE('ANIMALS_AMOT','AmountAnimals',VALUE); end; 
	
	if SECTION == 27 then OW_SETTING_WRITE('ANIMALS_POSX','Animal#'..NAME,VALUE); end; 
	if SECTION == 28 then OW_SETTING_WRITE('ANIMALS_POSY','Animal#'..NAME,VALUE); end; 
	if SECTION == 29 then OW_SETTING_WRITE('ANIMALS_SIDE','Animal#'..NAME,VALUE); end; 
	if SECTION == 30 then OW_SETTING_WRITE('ANIMALS_TYPE','Animal#'..NAME,VALUE); end; 
	if SECTION == 31 then OW_SETTING_WRITE('ANIMALS_HPOI','Animal#'..NAME,VALUE); end; 
	if SECTION == 32 then OW_SETTING_WRITE('ANIMALS_DIRE','Animal#'..NAME,VALUE); end; 
	if SECTION == 33 then OW_SETTING_WRITE('ANIMALS_TASK','Animal#'..NAME,VALUE); end; 
	
	if SECTION == 26 then OW_SETTING_WRITE('VEHICLES_AMOT','AmountVehicles',VALUE); end; 
	
	if SECTION == 27 then OW_SETTING_WRITE('VEHICLES_POSX','Vehicle#'..NAME,VALUE); end; 
	if SECTION == 28 then OW_SETTING_WRITE('VEHICLES_POSY','Vehicle#'..NAME,VALUE); end; 
	if SECTION == 29 then OW_SETTING_WRITE('VEHICLES_SIDE','Vehicle#'..NAME,VALUE); end; 
	if SECTION == 30 then OW_SETTING_WRITE('VEHICLES_NATI','Vehicle#'..NAME,VALUE); end; 
	if SECTION == 31 then OW_SETTING_WRITE('VEHICLES_CRAT','Vehicle#'..NAME,VALUE); end; 
	if SECTION == 52 then OW_SETTING_WRITE('VEHICLES_OILB','Vehicle#'..NAME,VALUE); end; 
	if SECTION == 53 then OW_SETTING_WRITE('VEHICLES_ALAS','Vehicle#'..NAME,VALUE); end; 
	if SECTION == 32 then OW_SETTING_WRITE('VEHICLES_HPOI','Vehicle#'..NAME,VALUE); end; 
	if SECTION == 33 then OW_SETTING_WRITE('VEHICLES_DIRE','Vehicle#'..NAME,VALUE); end; 
	if SECTION == 34 then OW_SETTING_WRITE('VEHICLES_FUEL','Vehicle#'..NAME,VALUE); end; 
	if SECTION == 35 then OW_SETTING_WRITE('VEHICLES_TASK','Vehicle#'..NAME,VALUE); end; 
	if SECTION == 36 then OW_SETTING_WRITE('VEHICLES_CHAS','Vehicle#'..NAME,VALUE); end; 
	if SECTION == 37 then OW_SETTING_WRITE('VEHICLES_ENGI','Vehicle#'..NAME,VALUE); end; 
	if SECTION == 38 then OW_SETTING_WRITE('VEHICLES_CONT','Vehicle#'..NAME,VALUE); end; 
	if SECTION == 39 then OW_SETTING_WRITE('VEHICLES_WEAP','Vehicle#'..NAME,VALUE); end; 	
	
	if SECTION == 40 then OW_SETTING_WRITE('ENVIRONMENTS_AMOT','AmountEnvironments',VALUE); end; 
	
	if SECTION == 41 then OW_SETTING_WRITE('ENVIRONMENTS_POSX','Environment#'..NAME,VALUE); end;
	if SECTION == 42 then OW_SETTING_WRITE('ENVIRONMENTS_POSY','Environment#'..NAME,VALUE); end;
	if SECTION == 43 then OW_SETTING_WRITE('ENVIRONMENTS_IDEN','Environment#'..NAME,VALUE); end;
	if SECTION == 44 then OW_SETTING_WRITE('ENVIRONMENTS_NUMB','Environment#'..NAME,VALUE); end;
	
	if SECTION == 45 then OW_SETTING_WRITE('RESOURCES_AMOT','AmountResources',VALUE); end; 
	
	if SECTION == 46 then OW_SETTING_WRITE('RESOURCES_POSX','Resource#'..NAME,VALUE); end;
	if SECTION == 47 then OW_SETTING_WRITE('RESOURCES_POSY','Resource#'..NAME,VALUE); end;
	if SECTION == 48 then OW_SETTING_WRITE('RESOURCES_TYPE','Resource#'..NAME,VALUE); end;
	if SECTION == 49 then OW_SETTING_WRITE('RESOURCES_NUMB','Resource#'..NAME,VALUE); end;

end;

function AMOUNT_SAVED(AMOUNT)
AmountSaved = AMOUNT;
end;

function game.chat.edit.onkeypress(key)

	if (key == 13) then

		if string.sub(getText(game.chat.edit),1) == 'save game' then
			OW_CUSTOM_COMMAND(1,0,0,0,0);
			OW_CUSTOM_COMMAND(2,0,0,0,0);
			game.chat.close(); setText(game.chat.edit,'');
		return;	end;

		if string.sub(getText(game.chat.edit),1) == 'load game' then
		
		OW_CUSTOM_COMMAND(99);
		
		for SideChecker = 1,8 do 
			if OW_SETTING_READ_STRING("MAP_SAVED",'Player#'..SideChecker..'.active') == 1 then
				for TechnologyID = 1,71 do 
					OW_CUSTOM_COMMAND(3,TechnologyID,SideChecker,OW_SETTING_READ_STRING('TECHNOLOGIES','Side#'..SideChecker..'.Technology#'..TechnologyID));
				end;
			end;
		end;

				for i = 1,OW_SETTING_READ_STRING("BUILDINGS_AMOT",'Buildings.with.units') do
				
					DATAB1 = OW_SETTING_READ_STRING("BUILDINGS_POSX",'Building.with.unit#'..i);
					DATAB2 = OW_SETTING_READ_STRING("BUILDINGS_POSY",'Building.with.unit#'..i);
					DATAB3 = OW_SETTING_READ_STRING("BUILDINGS_SIDE",'Building.with.unit#'..i);
					DATAB4 = OW_SETTING_READ_STRING("BUILDINGS_NATI",'Building.with.unit#'..i);
					DATAB5 = OW_SETTING_READ_STRING("BUILDINGS_TYPE",'Building.with.unit#'..i);
					DATAB6 = OW_SETTING_READ_STRING("BUILDINGS_HPOI",'Building.with.unit#'..i);
					DATAB7 = OW_SETTING_READ_STRING("BUILDINGS_DIRE",'Building.with.unit#'..i);
					DATAB8 = OW_SETTING_READ_STRING("BUILDINGS_TASK",'Building.with.unit#'..i);
					DATAB9 = OW_SETTING_READ_STRING("BUILDINGS_STOR",'Building.with.unit#'..i);
					DATAB10 = OW_SETTING_READ_STRING("BUILDINGS_LEVE",'Building.with.unit#'..i);
					DATAB11 = OW_SETTING_READ_STRING("BUILDINGS_SUB1",'Building.with.unit#'..i);
					DATAB12 = OW_SETTING_READ_STRING("BUILDINGS_SUB2",'Building.with.unit#'..i);

					OW_CUSTOM_COMMAND(11,DATAB3,DATAB4,DATAB5,DATAB7);		
					OW_CUSTOM_COMMAND(13,DATAB11,DATAB12,DATAB5);		
					OW_CUSTOM_COMMAND(12,DATAB6,DATAB10,DATAB1,DATAB2);

				end;
		
				for i = 1,OW_SETTING_READ_STRING("BUILDINGS_AMOT",'AmountBuildings') do
				
					DATAB1 = OW_SETTING_READ_STRING("BUILDINGS_POSX",'Building#'..i);
					DATAB2 = OW_SETTING_READ_STRING("BUILDINGS_POSY",'Building#'..i);
					DATAB3 = OW_SETTING_READ_STRING("BUILDINGS_SIDE",'Building#'..i);
					DATAB4 = OW_SETTING_READ_STRING("BUILDINGS_NATI",'Building#'..i);
					DATAB5 = OW_SETTING_READ_STRING("BUILDINGS_TYPE",'Building#'..i);
					DATAB6 = OW_SETTING_READ_STRING("BUILDINGS_HPOI",'Building#'..i);
					DATAB7 = OW_SETTING_READ_STRING("BUILDINGS_DIRE",'Building#'..i);
					DATAB8 = OW_SETTING_READ_STRING("BUILDINGS_TASK",'Building#'..i);
					DATAB9 = OW_SETTING_READ_STRING("BUILDINGS_STOR",'Building#'..i);
					DATAB10 = OW_SETTING_READ_STRING("BUILDINGS_LEVE",'Building#'..i);
					DATAB11 = OW_SETTING_READ_STRING("BUILDINGS_SUB1",'Building#'..i);
					DATAB12 = OW_SETTING_READ_STRING("BUILDINGS_SUB2",'Building#'..i);

					OW_CUSTOM_COMMAND(11,DATAB3,DATAB4,DATAB5,DATAB7);		
					OW_CUSTOM_COMMAND(13,DATAB11,DATAB12,DATAB5);		
					OW_CUSTOM_COMMAND(12,DATAB6,DATAB10,DATAB1,DATAB2);

				end;
	
				--[[for i = 1, OW_SETTING_READ_STRING("VEHICLES_AMOT",'Vehicles.with.units') do
				
					DATAV1 = OW_SETTING_READ_STRING("VEHICLES_POSX",'Vehicle#'..i);
					DATAV2 = OW_SETTING_READ_STRING("VEHICLES_POSY",'Vehicle#'..i);
					DATAV3 = OW_SETTING_READ_STRING("VEHICLES_SIDE",'Vehicle#'..i);
					DATAV4 = OW_SETTING_READ_STRING("VEHICLES_NATI",'Vehicle#'..i);
					DATAV5 = OW_SETTING_READ_STRING("VEHICLES_CRAT",'Vehicle#'..i);
					DATAV6 = OW_SETTING_READ_STRING("VEHICLES_OILB",'Vehicle#'..i);
					DATAV7 = OW_SETTING_READ_STRING("VEHICLES_ALAS",'Vehicle#'..i);
					DATAV8 = OW_SETTING_READ_STRING("VEHICLES_HPOI",'Vehicle#'..i);
					DATAV9 = OW_SETTING_READ_STRING("VEHICLES_DIRE",'Vehicle#'..i);
					DATAV10 = OW_SETTING_READ_STRING("VEHICLES_FUEL",'Vehicle#'..i);
					DATAV11 = OW_SETTING_READ_STRING("VEHICLES_TASK",'Vehicle#'..i);
					DATAV12 = OW_SETTING_READ_STRING("VEHICLES_CHAS",'Vehicle#'..i);
					DATAV13 = OW_SETTING_READ_STRING("VEHICLES_ENGI",'Vehicle#'..i);					
					DATAV14 = OW_SETTING_READ_STRING("VEHICLES_CONT",'Vehicle#'..i);
					DATAV15 = OW_SETTING_READ_STRING("VEHICLES_WEAP",'Vehicle#'..i);					

					OW_CUSTOM_COMMAND(31,DATAV3,DATAV4,DATAV9,DATAV15);		
					OW_CUSTOM_COMMAND(32,DATAV13,DATAV12,DATAV14,DATAV10);
					OW_CUSTOM_COMMAND(34,DATAV1,DATAV2,DATAV8);
					OW_CUSTOM_COMMAND(33,DATAV5,DATAV6,DATAV7);
					
				end;--]]
	
				for i = 1,OW_SETTING_READ_STRING("VEHICLES_AMOT",'AmountVehicles') do
				
					DATAV1 = OW_SETTING_READ_STRING("VEHICLES_POSX",'Vehicle#'..i);
					DATAV2 = OW_SETTING_READ_STRING("VEHICLES_POSY",'Vehicle#'..i);
					DATAV3 = OW_SETTING_READ_STRING("VEHICLES_SIDE",'Vehicle#'..i);
					DATAV4 = OW_SETTING_READ_STRING("VEHICLES_NATI",'Vehicle#'..i);
					DATAV5 = OW_SETTING_READ_STRING("VEHICLES_CRAT",'Vehicle#'..i);
					DATAV6 = OW_SETTING_READ_STRING("VEHICLES_OILB",'Vehicle#'..i);
					DATAV7 = OW_SETTING_READ_STRING("VEHICLES_ALAS",'Vehicle#'..i);
					DATAV8 = OW_SETTING_READ_STRING("VEHICLES_HPOI",'Vehicle#'..i);
					DATAV9 = OW_SETTING_READ_STRING("VEHICLES_DIRE",'Vehicle#'..i);
					DATAV10 = OW_SETTING_READ_STRING("VEHICLES_FUEL",'Vehicle#'..i);
					DATAV11 = OW_SETTING_READ_STRING("VEHICLES_TASK",'Vehicle#'..i);
					DATAV12 = OW_SETTING_READ_STRING("VEHICLES_CHAS",'Vehicle#'..i);
					DATAV13 = OW_SETTING_READ_STRING("VEHICLES_ENGI",'Vehicle#'..i);					
					DATAV14 = OW_SETTING_READ_STRING("VEHICLES_CONT",'Vehicle#'..i);
					DATAV15 = OW_SETTING_READ_STRING("VEHICLES_WEAP",'Vehicle#'..i);					

					OW_CUSTOM_COMMAND(31,DATAV3,DATAV4,DATAV9,DATAV15);		
					OW_CUSTOM_COMMAND(32,DATAV13,DATAV12,DATAV14,DATAV10);
					OW_CUSTOM_COMMAND(34,DATAV1,DATAV2,DATAV8);
					OW_CUSTOM_COMMAND(33,DATAV5,DATAV6,DATAV7);
					
				end;
				
				for i = 1,OW_SETTING_READ_STRING("RESOURCES_AMOT",'AmountResources') do
				
					DATAR1 = OW_SETTING_READ_STRING("RESOURCES_POSX",'Resource#'..i);
					DATAR2 = OW_SETTING_READ_STRING("RESOURCES_POSY",'Resource#'..i);
					DATAR3 = OW_SETTING_READ_STRING("RESOURCES_TYPE",'Resource#'..i);
					DATAR4 = OW_SETTING_READ_STRING("RESOURCES_NUMB",'Resource#'..i);

					OW_CUSTOM_COMMAND(31,DATAR1,DATAR2,DATAR3,DATAR4);		
		
				end;

				AddSingleUseTimer(1,"Load_Units();");

				end;
	
			game.chat.close(); setText(game.chat.edit,'');
		return;	end;			
end;

function Load_Units()

				for i = 1,OW_SETTING_READ_STRING("UNITS_AMOT",'AmountUnits') do
				
					--if not OW_SETTING_READ_STRING("UNITS_INSI",'Unit#'..i) == 0 then 
					DATAU1 = OW_SETTING_READ_STRING("UNITS_POSX",'Unit#'..i);
					DATAU2 = OW_SETTING_READ_STRING("UNITS_POSY",'Unit#'..i);
					DATAU3 = OW_SETTING_READ_STRING("UNITS_SIDE",'Unit#'..i);
					DATAU4 = OW_SETTING_READ_STRING("UNITS_NATI",'Unit#'..i);
					DATAU5 = OW_SETTING_READ_STRING("UNITS_GEND",'Unit#'..i);
					DATAU6 = OW_SETTING_READ_STRING("UNITS_CLAS",'Unit#'..i);
					DATAU7 = OW_SETTING_READ_STRING("UNITS_HPOI",'Unit#'..i);
					DATAU8 = OW_SETTING_READ_STRING("UNITS_DIRE",'Unit#'..i);
					DATAU9 = OW_SETTING_READ_STRING("UNITS_TASK",'Unit#'..i);
					DATAU10 = OW_SETTING_READ_STRING("UNITS_SOLD",'Unit#'..i);
					DATAU11 = OW_SETTING_READ_STRING("UNITS_ENGI",'Unit#'..i);
					DATAU12 = OW_SETTING_READ_STRING("UNITS_MECH",'Unit#'..i);
					DATAU13 = OW_SETTING_READ_STRING("UNITS_SCIE",'Unit#'..i);
					DATAU14 = OW_SETTING_READ_STRING("UNITS_SPEE",'Unit#'..i);
					DATAU15 = OW_SETTING_READ_STRING("UNITS_ARMO",'Unit#'..i);
					
					DATAU16 = OW_SETTING_READ_STRING("UNITS_TSK2",'Unit#'..i);
					DATAU17 = OW_SETTING_READ_STRING("UNITS_TSK3",'Unit#'..i);
					DATAU18 = OW_SETTING_READ_STRING("UNITS_TSK4",'Unit#'..i);
					DATAU19 = OW_SETTING_READ_STRING("UNITS_TSK5",'Unit#'..i);
					DATAU20 = OW_SETTING_READ_STRING("UNITS_TSK6",'Unit#'..i);
					DATAU21 = OW_SETTING_READ_STRING("UNITS_TSK7",'Unit#'..i);
					
					OW_CUSTOM_COMMAND(21,DATAU10,DATAU11,DATAU12,DATAU13);		
					OW_CUSTOM_COMMAND(22,DATAU3,DATAU14,DATAU15,DATAU4);
					OW_CUSTOM_COMMAND(23,DATAU5,DATAU8,DATAU6);
					OW_CUSTOM_COMMAND(24,DATAU1,DATAU2,DATAU7,DATAU3);
					OW_CUSTOM_COMMAND(25,DATAU16,DATAU17,DATAU18,DATAU19);
					OW_CUSTOM_COMMAND(26,DATAU20,DATAU21);
					--end;
					end;
				end;
