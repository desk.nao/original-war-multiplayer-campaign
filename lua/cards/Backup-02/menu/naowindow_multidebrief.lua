-----------------------------------------------------------------------------
---  Orig. File : /lua/window_multidebrief.lua
---  Version    : 8
---
---  Summary    : Multiplayer Debriefing
---  
---  Created    : Petr 'Sali' Salak, Freya Group
------------------------------------------------------------------------------
DATADEBRIEF = {};
md_totalTime = 0;
function FROMOW_MULTIPLAYER_DEBRIEF(DATA)
--[[
DATA BREAKDOWN
        WIN Boolean
		EOM Interger
		SYNCLOST Boolean
		COLOURS : Array [1..8] of
				NAME : String
				NATION : Integer
				TEAM : Integer
				PLAYERCOUNT : Integer
				PLAYERS : Array [1..12] of Integer
		MULTI_WITH_SCORE : Boolean
		MULTI_SCORE :  array [1..8] of Integer
		DEBRIEF 1..9 of
               COUNT Integer														-- Note, not player sides will have 0 (no data exist)
               TIME integer (In minutes)
               ITEMS 1..COUNT of
                                   HUMANS Integer
                                   VEHICLES Integer
                                   BUILDINGS Integer
                                   HUMANSSTRENGTH Integer
                                   VEHICLESSTRENGTH Integer
                                   BUILDINGSSTRENGTH Integer
                                   CRATES Integer
                                   OIL Integer
                                   SIBERITE Integer
                                   COUNT Integer
                                   ITEMS [1..COUNT] of  -- Events
                                                      TYPE Integer
                                                      SIDE Integer

Events
'ev_vehicle_constructed', 'ev_building_built',									1	2
     'ev_vehicle_captured', 'ev_building_captured',                    			3	4			 // moji strane bylo neco zajmuto (kym)
     'ev_capture_vehicle', 'ev_capture_building',                      			5	6			 // moje strana neco zajmula (ci)
     'ev_human_killed', 'ev_vehicle_destroyed', 'ev_building_destroyed',		7	8	9		  // moje jednotka byla znicena (kym)
     'ev_kill_human', 'ev_destroy_vehicle', 'ev_destroy_building',        		10	11	12		// moje strana znicila jednotku (ci)
     'ev_ape_tamed', 'ev_technology_invented'									13	14
--]]

	DATADEBRIEF = DATA;
	setVisible(Multi_Window,false);
	setVisible(Multi_Room,false);
	setVisible(ChangeMapWindow,false);
	AddSingleUseTimer(1,'backToMenu();');
	local hours = 0;
	local minutes = 0;
	md_totalTime = DATA.DEBRIEF[1].TIME;
	for i=2, 9 do
		if md_totalTime  < DATA.DEBRIEF[i].TIME then
			md_totalTime  = DATA.DEBRIEF[i].TIME;
		end;
	end;
	
	if md_totalTime < 60 then
		minutes = md_totalTime;
		hours = 0;
	else
		hours = div(md_totalTime,60);
		minutes = mod(md_totalTime,60);
	end;
	
	if minutes < 10 then
		setText(Multi_Debrief.Time,loc(TID_InGame_Total_time) .. ' ' .. hours..':0'..minutes);
	else
		setText(Multi_Debrief.Time,loc(TID_InGame_Total_time) .. ' ' .. hours..':'..minutes);
	end;
	
	--setText(Multi_Debrief.WinLose,tostring(DATA.EOM));
	

	changeInterfaceAndWinLost(DATA.SYNCLOST, DATA.WIN);
	
	setText(Multi_Debrief.MapTitle, MultiDef.MapName.MAPLOC);
	setText(Multi_Debrief.GameTypeTitle,MultiDef.MapName.GAMETYPELOC);

	setVisible(profilebar,false);
	showMPDebrief(DATA);
end;

-- Jména                 Stav (892, 893, 900)   Celkový čas (703)
-- Kombinovaná síla (743)   -  Ztráty protivníků (751)
-- Lidi(744)                -  Vlastní ztráty (752)
-- Vozidla (745)            -  Bedny ve skladech (748)
-- Budovy (746)             -  Barelů ve skladech (749)
-- Technologie (747)        -  Krystaly ve skladech (750)

-- Skóre (860), Celkem (857), Postaveno a vytvořeno (858), Ztraceno a zničeno (859), OK (754)

function leaveDebrief()
	setVisible(profilebar,true);
	setVisible(Multi_Debrief,false);
--	setVisible(Multi_Debrief.Suma,false);
--	OW_IRC_DESTROY();
	backToMenu_Multiplayer();
	
--	setEnabled(Multi_Debrief.butsuma,true);
--	setEnabled(Multi_Debrief.butcreated,true);
--	setEnabled(Multi_Debrief.butdestroyed,true);
	setEnabled(Multi_Debrief.Menu.Score,true);
	
end;

local score = {count=0;};
md_sides = {};
function resetScore()
	score = {people={count=0, max = 0, [1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {}, },
			vehicles={count=0, max = 0, [1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {}, },
			buildings={count=0, max = 0, [1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {}, },
			totalStrength={count=0, max = 0, [1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {}, },
			humansStrength={count=0, max = 0, [1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {}, },
			vehiclesStrength={count=0, max = 0, [1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {}, },
			buildingsStrength={count=0, max = 0, [1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {}, },
			crates={count=0, max = 0, [1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {}, },
			oil={count=0, max = 0, [1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {}, },
			siberite={count=0, max = 0, [1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {}, },
			technologies = {count=0, max = 0, [1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {}, },
			destroyed = {count=0, max = 0, [1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {}, },
			lost = {count=0, max = 0, [1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {}, },
			events = { techs ={
							[1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {},
						},
						bc_built ={
							[1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {},
						},
						bc_create ={
							[1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {},
						},
						bc_tamed ={
							[1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {},
						},
						des_humans ={
							[1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {},
						},
						des_veh ={
							[1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {},
						},
						des_buil ={
							[1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {},
						},
						lost_humans ={
							[1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {},
						},
						lost_veh ={
							[1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {},
						},
						lost_buil ={
							[1] = {}, [2] = {}, [3] = {}, [4] = {}, [5] = {}, [6] = {}, [7] = {}, [8] = {}, [9] = {},
						},
			},
			count=0,
			};
	md_sides.alive = {};
end;

	sideCheckBoxSuma ={};
local notScoreScreen = true;
function showMPDebrief(DATA)
	setVisible(Multi_Debrief,true);
	--DoInterfaceChange(interface.amer);
	--if not interface.current then
	--interface.current = interface.amer;   -- for test
	--end;
	--setTexture(Multi_Debrief.Suma, interface.current.debrief.suma.bg);
	--setTexture(Multi_Debrief.Score, interface.current.debrief.score.bg);
	--setVisible(Multi_Debrief.Suma, true);
	--setEnabled(Multi_Debrief.butdestroyed,true);
	--setEnabled(Multi_Debrief.butcreated,true);

	if not DATA.MULTI_WITH_SCORE then
	--	setEnabled(Multi_Debrief.butsuma,false);
	--	setVisible(Multi_Debrief.butscore,false);
		setEnabled(Multi_Debrief.Menu.Score,false);
		setVisible(Multi_Debrief.Menu.Score,false);
		MD_showList(2);
	--	setVisible(Multi_Debrief.Suma, true);
	--	setVisible(Multi_Debrief.Score, false);
		--createScore(seriazableMultiScore(DATA.MULTI_SCORE));
	else
	--	setVisible(Multi_Debrief.butscore,true);
		setEnabled(Multi_Debrief.Menu.Score,false);
		setVisible(Multi_Debrief.Menu.Score,true);
		MD_showList(1);
	--	setEnabled(Multi_Debrief.butscore,true);
	--	setVisible(Multi_Debrief.Score, true);
	--	setVisible(Multi_Debrief.Suma, false);
		
		createScore(seriazableMultiScore(DATA.MULTI_SCORE));
	end;
	
	--local countData = 0;
	local techs;
	local kills;
	local losses;
	
	resetScore();
	if DATA.DEBRIEF then				---- Serializabling datas
		for i= 1, 9 do-------------------------------------- will be better, but not now
			countData = 0;
					----------------------------------------------------------- Humans
			if DATA.DEBRIEF[i].COUNT > 0 then
				md_sides.alive[i] = true;
			else
				md_sides.alive[i] = false;
			end;
			for d=1, DATA.DEBRIEF[i].COUNT do
				local val = DATA.DEBRIEF[i].ITEMS[d].HUMANS;
				
				if val then
					if val > score.people.max then
						score.people.max = val;
					end;
					
					score.people[i][d] = val;
				end;

						----- -------------------------------------------------- Vehicles
				local val = DATA.DEBRIEF[i].ITEMS[d].VEHICLES;
				
				if val then
					if val > score.vehicles.max then
						score.vehicles.max = val;
					end;
					
					score.vehicles[i][d] = val;
				end;

			---------------------------------------------------------------------- Buildings

				local val = DATA.DEBRIEF[i].ITEMS[d].BUILDINGS;
				
				if val then
					if val > score.buildings.max then
						score.buildings.max = val;
					end;
					
					score.buildings[i][d] = val;
				end;

			-----------------------------------------------------------------------crates
				local val = DATA.DEBRIEF[i].ITEMS[d].CRATES;
				
				if val then
					if val > score.crates.max then
						score.crates.max = val;
					end;
					
					score.crates[i][d] = val;
				end;

			------------------------------------------------------------------------OIL
				local val = DATA.DEBRIEF[i].ITEMS[d].OIL;
				
				if val then
					if val > score.oil.max then
						score.oil.max = val;
					end;
					
					score.oil[i][d] = val;
				end;

			--------------------------------------------------------------------- siberite
				local val = DATA.DEBRIEF[i].ITEMS[d].SIBERITE;
				
				if val then
					if val > score.siberite.max then
						score.siberite.max = val;
					end;
					
					score.siberite[i][d] = val;
				end;

			-------------------------------------------------------------------- Total Strenght
				local val = DATA.DEBRIEF[i].ITEMS[d].HUMANSSTRENGTH + DATA.DEBRIEF[i].ITEMS[d].VEHICLESSTRENGTH + DATA.DEBRIEF[i].ITEMS[d].BUILDINGSSTRENGTH;

			--	if val then
					if val > score.totalStrength.max then
						score.totalStrength.max = val;
--						score.humansStrength.max = val;
--						score.vehiclesStrength.max = val;
--						score.buildingsStrength.max = val;
					end;
					
					if DATA.DEBRIEF[i].ITEMS[d].HUMANSSTRENGTH > score.humansStrength.max then
						score.humansStrength.max = DATA.DEBRIEF[i].ITEMS[d].HUMANSSTRENGTH;
						score.vehiclesStrength.max = DATA.DEBRIEF[i].ITEMS[d].HUMANSSTRENGTH;
						score.buildingsStrength.max = DATA.DEBRIEF[i].ITEMS[d].HUMANSSTRENGTH;
					end;
					
					if DATA.DEBRIEF[i].ITEMS[d].VEHICLESSTRENGTH > score.vehiclesStrength.max then
						score.humansStrength.max = DATA.DEBRIEF[i].ITEMS[d].VEHICLESSTRENGTH;
						score.vehiclesStrength.max = DATA.DEBRIEF[i].ITEMS[d].VEHICLESSTRENGTH;
						score.buildingsStrength.max = DATA.DEBRIEF[i].ITEMS[d].VEHICLESSTRENGTH;
					end;
					
					if DATA.DEBRIEF[i].ITEMS[d].BUILDINGSSTRENGTH > score.buildingsStrength.max then
						score.humansStrength.max = DATA.DEBRIEF[i].ITEMS[d].BUILDINGSSTRENGTH;
						score.vehiclesStrength.max = DATA.DEBRIEF[i].ITEMS[d].BUILDINGSSTRENGTH;
						score.buildingsStrength.max = DATA.DEBRIEF[i].ITEMS[d].BUILDINGSSTRENGTH;
					end;

					
					score.totalStrength[i][d] = val;
					score.humansStrength[i][d] = DATA.DEBRIEF[i].ITEMS[d].HUMANSSTRENGTH;
					score.vehiclesStrength[i][d] = DATA.DEBRIEF[i].ITEMS[d].VEHICLESSTRENGTH;
					score.buildingsStrength[i][d] = DATA.DEBRIEF[i].ITEMS[d].BUILDINGSSTRENGTH;
			--	end;


				--------------------------------------------------------------------- Serializable Events
			
				local eventCount = DATA.DEBRIEF[i].ITEMS[d].COUNT
				if d > 1 then
					techs = score.technologies[i][d-1];
					kills = score.destroyed[i][d-1];
					losses = score.lost[i][d-1];
				else
					techs = 0;
					kills = 0;
					losses = 0;
				end;
				
				for e = 1, eventCount do
					local val = DATA.DEBRIEF[i].ITEMS[d].ITEMS[e];
					
					if  evGroups.technologies[val.TYPE]then 
						techs = techs +1;
					elseif evGroups.lost[val.TYPE] or evGroups.captured[val.TYPE] then
						losses = losses +1;
					elseif evGroups.destroyed[val.TYPE] or evGroups.capture[val.TYPE] then
						kills = kills + 1;
					end;
					
				end;
				
				score.technologies[i][d] = techs;
				score.destroyed[i][d] = kills;
				score.lost[i][d] = losses;
			
				if techs > score.technologies.max then
					score.technologies.max = techs;
				end;
				
				if kills > score.destroyed.max then
					score.destroyed.max = kills;
				end;
				
				if losses > score.lost.max then
					score.lost.max = losses;
				end;
				
			end;
			
			if score.count < DATA.DEBRIEF[i].COUNT then
				score.count = DATA.DEBRIEF[i].COUNT;
			end;
			
		end;
		-------------------------------------------------------------------------------- Draw graphs

		drawGraph (Multi_Debrief.Graphs.Strenght.Graph_TS,score.totalStrength);
		drawGraph (Multi_Debrief.Graphs.Strenght.Graph_HS,score.humansStrength);
		drawGraph (Multi_Debrief.Graphs.Strenght.Graph_VS,score.vehiclesStrength);
		drawGraph (Multi_Debrief.Graphs.Strenght.Graph_BS,score.buildingsStrength);
		drawGraph (Multi_Debrief.Graphs.Units.Humans,score.people);
		drawGraph (Multi_Debrief.Graphs.Units.Veicles,score.vehicles);
		drawGraph (Multi_Debrief.Graphs.Units.Buildings,score.buildings);
		drawGraph (Multi_Debrief.Graphs.Techs.Graph,score.technologies);
		drawGraph (Multi_Debrief.Graphs.LD.Destroyed ,score.destroyed);
		drawGraph (Multi_Debrief.Graphs.LD.Lost,score.lost);
		drawGraph (Multi_Debrief.Graphs.Income.Crates,score.crates);
		drawGraph (Multi_Debrief.Graphs.Income.Oil,score.oil);
		drawGraph (Multi_Debrief.Graphs.Income.Siberite,score.siberite);


		local c =0;
		local sides = {};
		for s=1 , 9 do
			if md_sides.alive[s] == true then
				md_sides.alive[s] = true;
				c=c+1;
				sides[c] = s;
				
				for i= 1, DATA.DEBRIEF[s].COUNT do
					score.events.techs[s][i] = {};
					score.events.bc_built[s][i] = {};
					score.events.bc_create[s][i] = {};
					score.events.bc_tamed[s][i] = {};
					score.events.des_humans[s][i] = {};
					score.events.des_veh[s][i] = {};
					score.events.des_buil[s][i] = {};
					score.events.lost_humans[s][i] = {};
					score.events.lost_veh[s][i] = {};
					score.events.lost_buil[s][i] = {};
					local counts = {
						techs = 0, 
						bc_built = 0, 
						bc_create = 0, 
						bc_tamed = 0, 
						des_humans = 0, 
						des_veh = 0 , 
						des_buil = 0, 
						lost_humans = 0 , 
						lost_veh = 0, 
						lost_buil = 0
					};
					
					for d=1, DATA.DEBRIEF[s].ITEMS[i].COUNT do
						local event = DATA.DEBRIEF[s].ITEMS[i].ITEMS[d];

						if evGroups.technologies[event.TYPE] then
							score.events.techs[s][i][counts.techs+1]={};
							score.events.techs[s][i][counts.techs+1].type = event.TYPE;
							score.events.techs[s][i][counts.techs+1].side = s;
							counts.techs = counts.techs + 1;
							
						elseif evGroups.createdBul[event.TYPE]  or evGroups.captureBul[event.TYPE] then
							score.events.bc_built[s][i][counts.bc_built+1]={};
							score.events.bc_built[s][i][counts.bc_built+1].type = event.TYPE;
							if evGroups.captureBul[event.TYPE] then
								score.events.bc_built[s][i][counts.bc_built+1].side = event.SIDE;
							else
								score.events.bc_built[s][i][counts.bc_built+1].side = s;
							end;
							counts.bc_built = counts.bc_built + 1;
							
						elseif evGroups.createdVeh[event.TYPE]  or evGroups.captureVeh[event.TYPE]  then
							score.events.bc_create[s][i][counts.bc_create+1]={};
							score.events.bc_create[s][i][counts.bc_create+1].type = event.TYPE;
							if evGroups.captureVeh[event.TYPE] then
								score.events.bc_create[s][i][counts.bc_create+1].side = event.SIDE;
							else
								score.events.bc_create[s][i][counts.bc_create+1].side = s;
							end;
							counts.bc_create = counts.bc_create + 1;

						elseif evGroups.tamed[event.TYPE] then
							score.events.bc_tamed[s][i][counts.bc_tamed+1]={};
							score.events.bc_tamed[s][i][counts.bc_tamed+1].type = event.TYPE;
							score.events.bc_tamed[s][i][counts.bc_tamed+1].side = event.SIDE;
							counts.bc_tamed = counts.bc_tamed + 1;

						elseif evGroups.killed[event.TYPE] then
							score.events.des_humans[s][i][counts.des_humans+1]={};
							score.events.des_humans[s][i][counts.des_humans+1].type = event.TYPE;
							score.events.des_humans[s][i][counts.des_humans+1].side = event.SIDE;
							counts.des_humans = counts.des_humans + 1;

						elseif evGroups.desVeh[event.TYPE]  then
							score.events.des_veh[s][i][counts.des_veh+1]={};
							score.events.des_veh[s][i][counts.des_veh+1].type = event.TYPE;
							score.events.des_veh[s][i][counts.des_veh+1].side = event.SIDE;
							counts.des_veh = counts.des_veh + 1;

						elseif evGroups.desBuil[event.TYPE] then
							score.events.des_buil[s][i][counts.des_buil+1]={};
							score.events.des_buil[s][i][counts.des_buil+1].type = event.TYPE;
							score.events.des_buil[s][i][counts.des_buil+1].side = event.SIDE;
							counts.des_buil = counts.des_buil + 1;

						elseif evGroups.lostHumans[event.TYPE] then
							score.events.lost_humans[s][i][counts.lost_humans+1]={};
							score.events.lost_humans[s][i][counts.lost_humans+1].type = event.TYPE;
							score.events.lost_humans[s][i][counts.lost_humans+1].side = event.SIDE;
							counts.lost_humans = counts.lost_humans + 1;

						elseif evGroups.lostVeh[event.TYPE] or evGroups.capturedVeh[event.TYPE] then
							score.events.lost_veh[s][i][counts.lost_veh+1]={};
							score.events.lost_veh[s][i][counts.lost_veh+1].type = event.TYPE;
							score.events.lost_veh[s][i][counts.lost_veh+1].side = event.SIDE;
							counts.lost_veh = counts.lost_veh + 1;

						elseif evGroups.lostBuil[event.TYPE] or evGroups.capturedBuil[event.TYPE] then
							score.events.lost_buil[s][i][counts.lost_buil+1]={};
							score.events.lost_buil[s][i][counts.lost_buil+1].type = event.TYPE;
							score.events.lost_buil[s][i][counts.lost_buil+1].side = event.SIDE;
							counts.lost_buil = counts.lost_buil + 1;

						end;
					end;
					score.events.techs[s][i].count = counts.techs;
					score.events.bc_built[s][i].count = counts.bc_built;
					score.events.bc_create[s][i].count = counts.bc_create;
					score.events.bc_tamed[s][i].count = counts.bc_tamed;
					score.events.des_humans[s][i].count = counts.des_humans;
					score.events.des_veh[s][i].count = counts.des_veh;
					score.events.des_buil[s][i].count = counts.des_buil;
					score.events.lost_humans[s][i].count = counts.lost_humans;
					score.events.lost_veh[s][i].count = counts.lost_veh;
					score.events.lost_buil[s][i].count = counts.lost_buil;
				end;
				score.events.techs[s].count = DATA.DEBRIEF[s].COUNT;
				score.events.bc_built[s].count = DATA.DEBRIEF[s].COUNT;
				score.events.bc_create[s].count = DATA.DEBRIEF[s].COUNT;
				score.events.bc_tamed[s].count = DATA.DEBRIEF[s].COUNT;
				score.events.des_humans[s].count = DATA.DEBRIEF[s].COUNT;
				score.events.des_veh[s].count = DATA.DEBRIEF[s].COUNT;
				score.events.des_buil[s].count = DATA.DEBRIEF[s].COUNT;
				score.events.lost_humans[s].count = DATA.DEBRIEF[s].COUNT;
				score.events.lost_veh[s].count = DATA.DEBRIEF[s].COUNT;
				score.events.lost_buil[s].count = DATA.DEBRIEF[s].COUNT;
			end;
		end;
		
	MD_CreateEvetsSpaces(Multi_Debrief.Graphs.Techs.Inverted,c,sides);
	MD_CreateEvetsSpaces(Multi_Debrief.Graphs.BC.Built,c,sides);
	MD_CreateEvetsSpaces(Multi_Debrief.Graphs.BC.Create,c,sides);
	MD_CreateEvetsSpaces(Multi_Debrief.Graphs.BC.Tamed,c,sides);
	MD_CreateEvetsSpaces(Multi_Debrief.Graphs.Destroyed.Humans,c,sides);
	MD_CreateEvetsSpaces(Multi_Debrief.Graphs.Destroyed.Vehicles,c,sides);
	MD_CreateEvetsSpaces(Multi_Debrief.Graphs.Destroyed.Buildings,c,sides);
	MD_CreateEvetsSpaces(Multi_Debrief.Graphs.Lost.Humans,c,sides);
	MD_CreateEvetsSpaces(Multi_Debrief.Graphs.Lost.Vehicles,c,sides);
	MD_CreateEvetsSpaces(Multi_Debrief.Graphs.Lost.Buildings,c,sides);
	for i=1 , 9 do
		if md_sides.alive[i] == true then
			MD_AddEvents(Multi_Debrief.Graphs.Techs.Inverted.Space[i],score.events.techs[i]);
			MD_AddEvents(Multi_Debrief.Graphs.BC.Built.Space[i],score.events.bc_built[i]);
			MD_AddEvents(Multi_Debrief.Graphs.BC.Create.Space[i],score.events.bc_create[i]);
			MD_AddEvents(Multi_Debrief.Graphs.BC.Tamed.Space[i],score.events.bc_tamed[i]);
			MD_AddEvents(Multi_Debrief.Graphs.Destroyed.Humans.Space[i],score.events.des_humans[i]);
			MD_AddEvents(Multi_Debrief.Graphs.Destroyed.Vehicles.Space[i],score.events.des_veh[i]);
			MD_AddEvents(Multi_Debrief.Graphs.Destroyed.Buildings.Space[i],score.events.des_buil[i]);
			MD_AddEvents(Multi_Debrief.Graphs.Lost.Humans.Space[i],score.events.lost_humans[i]);
			MD_AddEvents(Multi_Debrief.Graphs.Lost.Vehicles.Space[i],score.events.lost_veh[i]);
			MD_AddEvents(Multi_Debrief.Graphs.Lost.Buildings.Space[i],score.events.lost_buil[i]);
		end;
	end;
--		buildEventCrafts
--[[
Multi_Debrief.Graphs.Techs.Inverted
Multi_Debrief.Graphs.BC.Built 
Multi_Debrief.Graphs.BC.Create
Multi_Debrief.Graphs.BC.Tamed
Multi_Debrief.Graphs.Destroyed.Humans
Multi_Debrief.Graphs.Destroyed.Vehicles
Multi_Debrief.Graphs.Destroyed.Buildings
Multi_Debrief.Graphs.Lost.Humans
Multi_Debrief.Graphs.Lost.Vehicles
Multi_Debrief.Graphs.Lost.Buildings

MD_CreateEvetsSpaces(Multi_Debrief.Graphs.BC.Built.Space,8,{2,3,4,5,6,7,8,9});
]]--
	end;
--[[
	for side=1, 9 do
		if md_sides.alive[side] then
			setTexture(sideCheckBoxSuma[side].Checker, interface.current.debrief.md_sidesBox);
			setVisible(sideCheckBoxSuma[side].Checker,true);
			if side ~= 1 then 
				setHint(sideCheckBoxSuma[side].Checker, DATA.COLOURS[side-1].NAME);
			end;
		else
			setVisible(sideCheckBoxSuma[side].Checker,false);
		end;
	end;
--]]

	sgui_deletechildren(Multi_Debrief.Show.Players.ID);
	local pos = 0;
	for side = 1, 9 do
		local name = loc(TID_Debrief_Nature);
		if side > 1 then
			name = DATA.COLOURS[side-1].NAME;
		end;
		if md_sides.alive[side] then
			sideCheckBoxSuma[side] = makeCustomSideChechboxElement(
											Multi_Debrief.Show.Players,
											name,
											side,
											true,
											7,--(150+50*mc_w)+16*(side-1),
											20+7+19*(pos),--Multi_Debrief.height-((16+24)*mc_h),
											'changeVisibleGraphs('..side..');');
											
			pos = pos+1;
		end;
	end;
	

end;

local textures = {count=0};


function drawGraph (unit,data)
	local space = unit.Space2;
	local graph = unit.Space;
	sgui_deletechildren(graph.ID);
	sgui_deletechildren(space.ID);
	local fillTrans = OW_SETTING_READ_NUMBER('DEBRIEF', 'TRANS_FILL', 15);
	
	local gw = getWidth(graph)-1;
	local gh = getHeight(graph)-1;


	local max = 1;
	if data.max >1 then
		max = data.max;
	end;
	local v= gh / max;
	local lastVal=0;
	local s = gw / score.count;
	for side = 1, 9 do
	
		local c = SIDE_COLOURS[side];
		--c = RGBA(c.red,c.green,c.blue, 150);
		if md_sides.alive[side] then
			local tex = CTAPI_newTexture(gw+1,gh+1);
			graph[side] =  AddElement({
				type=TYPE_ELEMENT,
				parent=graph,
				anchor=anchorLT,
				visible=true,
				x=0,
				y=0,
				width=gw+1,
				height=gh+1,
				colour1=WHITEA(255),
			});
			
			for i = 1, score.count do
				local val =data[side][i];
				
				if fillTrans > 0 then
					CTAPI_drawBox(tex,(i-1) * s+1,0,s+1,(val * v),RGBA(c.red, c.green, c.blue, fillTrans));
				end;
				if i == 1 or val == lastVal then
					--[[
					AddElement({
						type=TYPE_ELEMENT,
						parent=graph,
						anchor=anchorLT,
						visible=true,
						x=(i-1) * s,
						y=gh-(val * v),
						width=1,
						height=1,
						colour1=c,
					});
					--]]
					CTAPI_drawBox(tex,(i-1) * s+1,(val * v),1,1,c);
					if s > 1 then
						--[[
						AddElement({
							type=TYPE_ELEMENT,
							parent=graph,
							anchor=anchorLT,
							visible=true,
							x=(i-1) * s,
							y=gh-(val * v),
							width=s,
							height=1,
							colour1=c,
						});
						--]]
						CTAPI_drawBox(tex,((i-1) * s)+2,(val * v),s,1,c);
					end;

				elseif val > lastVal then
					--[[
					AddElement({
						type=TYPE_ELEMENT,
						parent=graph,
						anchor=anchorLT,
						visible=true,
						x=(i-1) * s,
						y=gh-(val * v),
						width=1,
						height=(val-lastVal)*v,
						colour1=c,
					});
					--]]
					CTAPI_drawBox(tex,(i-1) * s+1,(val * v)-(val-lastVal)*v,1,(val-lastVal)*v+1,c);
					if s > 1 then
						--[[
						AddElement({
							type=TYPE_ELEMENT,
							parent=graph,
							anchor=anchorLT,
							visible=true,
							x=(i-1) * s,
							y=gh-(val * v),
							width=s,
							height=1,
							colour1=c,
						});
						--]]
						CTAPI_drawBox(tex,((i-1) * s)+1,(val * v),s+1,1,c);
					end;
				else
					--[[
					AddElement({
						type=TYPE_ELEMENT,
						parent=graph,
						anchor=anchorLT,
						visible=true,
						x=(i-1) * s,
						y=gh-(val * v) - (lastVal-val)*v,
						width=1,
						height=(lastVal-val)*v,
						colour1=c,
					});
					--]]
					CTAPI_drawBox(tex,(i-1) * s+1,(val * v),1,(lastVal-val)*v+1,c);
					if s > 1 then
						--[[
						AddElement({
							type=TYPE_ELEMENT,
							parent=graph,
							anchor=anchorLT,
							visible=true,
							x=(i-1) * s,
							y=gh-(val * v),
							width=s,
							height=1,
							colour1=c,
						});
						--]]
						CTAPI_drawBox(tex,((i-1) * s)+2,(val * v),s,1,c);
					end;
				end;
				lastVal = val;
			end;
			lastVal = 0;
			
			CTAPI_updateTexture(tex);
			CTAPI_finishTexture(tex);
			CTAPI_setElementTexture(tex,graph[side]);
			--setColour1(graph[side],WHITEA(255));
			--CTAPI_freeTexture(tex);
			textures.count=textures.count+1;
			textures[textures.count]= tex;
		end;
	end;
	local scoreString = '';
	for i = 1, score.count do
		scoreString = '';
		for side = 1, 9 do
			if md_sides.alive[side] then
				scoreString = scoreString .. '[' .. tostring(side) .. '] = ' .. tostring(data[side][i]) .. ',';
			else
				scoreString = scoreString .. '[' .. tostring(side) .. '] = ' .. tostring(0) .. ',';
			end;
		end;
		
			local InfoLabel = getElementEX(
					space,
					anchorTR,
					XYWH(
						(i-1) * s+1,
						0,
						s,
						gh
					),
					true,
					{
						colour1=WHITEA(0),
					}
					);
				set_Callback(InfoLabel.ID,CALLBACK_MOUSEOVER, 'set_MultiGraphsInfo( { '.. scoreString .. 'time = '.. i .. '});  setColour1ID('..InfoLabel.ID..','..colourToString(RGBA(60,60,60,60)) ..');' );
				set_Callback(InfoLabel.ID,CALLBACK_MOUSELEAVE,'hide_MultiGraphsinfo(); ' .. 
									'setColour1ID('..InfoLabel.ID..','..colourToString(RGBA(0,0,0,0)) ..');' );				
	end;
	
	local timesItems = {
			unit.Time1,
			unit.Time2,
			unit.Time3
	};

	local fullTime = md_totalTime;
	local timeCount =0;
	local mp = {};
	if MPDeb_aspect == '_c' then
		mp = {0.33,0.66}; --multiplicator
		timeCount = 2;
	else
		mp = {0.25,0.50,0.75}; --multiplicator
		timeCount = 3;
	end;

	for i= 1, timeCount do
		local time = fullTime * mp[i];
		time = div(time,1);

		if time < 60 then
			minutes = time;
			hours = 0;
		else
			hours = div(time,60);
			minutes = mod(time,60);
		end;

		if minutes < 10 then
			setText(timesItems[i],hours..':0'..minutes);
		else
			setText(timesItems[i],Time,hours..':'..minutes);
		end;
	end;
	
	setText(unit.PointHalf,max/2);
	setText(unit.PointFull,max);
end;

function set_MultiGraphsInfo(score)
	for side=1, 9 do
		if sideCheckBoxSuma[side] then
			setText(sideCheckBoxSuma[side].Score, tostring(score[side]));
			setVisible(sideCheckBoxSuma[side].Score,true);
		end;
	end;
	setText(Multi_Debrief.Show.Time,getTime(score.time));
end;

function hide_MultiGraphsinfo()
	for side=1, 9 do
		if sideCheckBoxSuma[side] then
			setText(sideCheckBoxSuma[side].Score, '0');
			setVisible(sideCheckBoxSuma[side].Score,false);
		end;
	end;
	setText(Multi_Debrief.Show.Time,'');
end;

-------------------------------------------------------------------------------------
-------------------------------- Interface ------------------------------------------
-------------------------------------------------------------------------------------
 MPDeb_aspect = ScrWidth / ScrHeight;
if MPDeb_aspect <= 1.5 then
	MPDeb_aspect = '_c';
else MPDeb_aspect = '_w';
end;

local mc_w = ScrWidth / 1024;
local mc_h = ScrHeight / 768;



Multi_Debrief = getElementEX(
	menu,
	anchorLTRB,
	XYWH(
		0,
		0,
		LayoutWidth,
		LayoutHeight
	),
	false,
	{
		colour1=WHITEA(255),
		--texture= '/SGUI/' .. 'Amer' .. '/deb_multi_lose'.. '_w'..'.png';
	}
);

Multi_Debrief.Background = getElementEX(
	Multi_Debrief,
	anchorLTRB,
	XYWH(
		0,
		0,
		LayoutWidth,
		LayoutHeight
	),
	true,
	{
		colour1=WHITEA(0),
		--texture= '/SGUI/' .. 'Amer' .. '/deb_multi_lose'.. '_w'..'.png';
		nomouseevent=true,
	}
);
-------------- HEAD ------------------------------

Multi_Debrief.Head = getElementEX(
	Multi_Debrief,
	anchorLT,
	XYWH(
		76,
		11,
		ScrWidth-2*76,
		60
	),
	true,
	{
		colour1=BLACKA(200),
		edges=true,
		--edges_colour1=SIDE_COLOURS[2],
	}
);

Multi_Debrief.MapTitle  = getLabelEX (
	Multi_Debrief.Head,
	anchorLT,
	XYWH(
		15*mc_w,
		4,
		300*mc_w,
		30
	),
	Tahoma_20B,
	loc(TID_InGame_NoName),
	{
		font_colour=WHITEA(255),--WHITEA(255),
		nomouseevent=true,
		text_halign=ALIGN_LEFT,
		shadowtext=true,

	}
);

Multi_Debrief.GameTypeTitle = getLabelEX (
	Multi_Debrief.Head,
	anchorLT,
	XYWH(
		15*mc_w,
		24,
		300*mc_w,
		30
	),
	Tahoma_20B,
	loc(TID_InGame_NoName),
	{
		font_colour=WHITEA(255),--WHITEA(255),
		nomouseevent=true,
		text_halign=ALIGN_LEFT,
		shadowtext=true,

	}
);

Multi_Debrief.WinLose = getLabelEX (
	Multi_Debrief.Head,
	anchorLT,
	XYWH(
		getWidth(Multi_Debrief.Head)/2-(150*mc_w),
		20,
		300*mc_w,
		30
	),
	Tahoma_20B,
	loc(TID_Multi_Loss),
	{
		font_colour=WHITEA(255),--WHITEA(255),
		nomouseevent=true,
		text_halign=ALIGN_MIDDLE,
		shadowtext=true,

	}
);


Multi_Debrief.Time = getLabelEX (
	Multi_Debrief.Head,
	anchorLT,
	XYWH(
		getWidth(Multi_Debrief.Head)-(200*mc_w + 15*mc_w),
		20,
		200*mc_w,
		30
	),
	Tahoma_20B,
	loc(TID_InGame_Total_time) .. ' 0:00', 
	{
		font_colour=WHITEA(255),
		nomouseevent=true,
		text_halign=ALIGN_RIGHT,
		shadowtext=true,

	}
);

--------------------------------------------------------------------- Left --------------------------------------------------------------------
---------------------------- Show ---------------------------
Multi_Debrief.Show = getElementEX(
	Multi_Debrief,
	anchorLT,
	XYWH(
		28,--15*mc_w,
		102,
		170,--*mc_w,
		200
	),
	true,
	{
		colour1=BLACKA(200),
		edges=true,
		--edges_colour1=SIDE_COLOURS[2],
	}
);

Multi_Debrief.Show.Players = getElementEX(
	Multi_Debrief.Show,
	anchorLT,
	XYWH(
		0,
		0,
		Multi_Debrief.Show.width,--*mc_w,
		Multi_Debrief.Show.height
	),
	true,
	{
		colour1=BLACKA(0),
	}
);

Multi_Debrief.Show.TimeL = getLabelEX(
	Multi_Debrief.Show,
	anchorLT,
	XYWH(
		7,
		5,
		100,
		20
	),
	nil,
	loc_format(TID_Time,{''}),
	{
		font_colour=WHITEA(255),--WHITEA(255),
		nomouseevent=true,
		text_halign=ALIGN_LEFT,
		shadowtext=true,
	}
);

Multi_Debrief.Show.Time = getLabelEX(
	Multi_Debrief.Show,
	anchorLT,
	XYWH(
		Multi_Debrief.Show.width-67,
		5,
		60,
		20
	),
	nil,
	'',
	{
		font_colour=WHITEA(255),--WHITEA(255),
		nomouseevent=true,
		text_halign=ALIGN_RIGHT,
		shadowtext=true,
	}
);

------------------------------- Manu -------------------------------
	Multi_Debrief.Menu = getElementEX(
	Multi_Debrief,
	anchorLT,
	XYWH(
		28,--15*mc_w,
		102+10+200,
		getWidth(Multi_Debrief.Show),
		LayoutHeight*mc_h-102-10-200-20
	),
	true,
	{
		colour1=BLACKA(200),
		edges=true,
		--edges_colour1=SIDE_COLOURS[2],
	}
);

------ Buttons ------
--TID_msg_Ok, TID_MultiHint_Debrief_Births, TID_MultiHint_Debrief_Kills, TID_MultiHint_Debrief_Total, TID_MultiHint_Debrief_Score
Multi_Debrief.Menu.Score = AddDialogButton(DBT_NORMAL,{
	type=TYPE_IMAGEBUTTON,
	parent=Multi_Debrief.Menu,
	anchor=anchorLT,
	x=getWidth(Multi_Debrief.Menu)/2-78,--*mc_w,
	y=10,
	width=156,--*mc_w,
	height=24,
	text=loc(860),
	font_colour_disabled=GRAY(127),
	callback_mouseclick='MD_showList(1)',
    hint='',
});

Multi_Debrief.Menu.Units = AddDialogButton(DBT_NORMAL,{
	type=TYPE_IMAGEBUTTON,
	parent=Multi_Debrief.Menu,
	anchor=anchorLT,
	x=getWidth(Multi_Debrief.Menu)/2-78,--*mc_w,
	y=10+28,
	width=156,--*mc_w,
	height=24,
	text=loc(TID_Debrief_Units),
	font_colour_disabled=GRAY(127),
	callback_mouseclick='MD_showList(2)',
    hint='',
});

Multi_Debrief.Menu.Income = AddDialogButton(DBT_NORMAL,{
	type=TYPE_IMAGEBUTTON,
	parent=Multi_Debrief.Menu,
	anchor=anchorLT,
	x=getWidth(Multi_Debrief.Menu)/2-78,--*mc_w,
	y=10+28*2,
	width=156,--*mc_w,
	height=24,
	text=loc(TID_Debrief_Income),
	font_colour_disabled=GRAY(127),
	callback_mouseclick='MD_showList(3)',
    hint='',
});

Multi_Debrief.Menu.Strenght = AddDialogButton(DBT_NORMAL,{
	type=TYPE_IMAGEBUTTON,
	parent=Multi_Debrief.Menu,
	anchor=anchorLT,
	x=getWidth(Multi_Debrief.Menu)/2-78,--*mc_w,
	y=10+28*3,
	width=156,--*mc_w,
	height=24,
	text=loc(743),
	font_colour_disabled=GRAY(127),
	callback_mouseclick='MD_showList(4)',
    hint='',
});

Multi_Debrief.Menu.LostKill = AddDialogButton(DBT_NORMAL,{
	type=TYPE_IMAGEBUTTON,
	parent=Multi_Debrief.Menu,
	anchor=anchorLT,
	x=getWidth(Multi_Debrief.Menu)/2-78,--*mc_w,
	y=10+28*4,
	width=156,--*mc_w,
	height=24,
	text=loc(859),
	font_colour_disabled=GRAY(127),
	callback_mouseclick='MD_showList(5)',
    hint='',
});

Multi_Debrief.Menu.Created = AddDialogButton(DBT_NORMAL,{
	type=TYPE_IMAGEBUTTON,
	parent=Multi_Debrief.Menu,
	anchor=anchorLT,
	x=getWidth(Multi_Debrief.Menu)/2-78,--*mc_w,
	y=10+28*5,
	width=156,--*mc_w,
	height=24,
	text=loc(858),
	font_colour_disabled=GRAY(127),
	callback_mouseclick='MD_showList(6)',
    hint='',
});

Multi_Debrief.Menu.Techs = AddDialogButton(DBT_NORMAL,{
	type=TYPE_IMAGEBUTTON,
	parent=Multi_Debrief.Menu,
	anchor=anchorLT,
	x=getWidth(Multi_Debrief.Menu)/2-78,--*mc_w,
	y=10+28*6,
	width=156,--*mc_w,
	height=24,
	text=loc(747),
	font_colour_disabled=GRAY(127),
	callback_mouseclick='MD_showList(7)',
    hint='',
});

Multi_Debrief.Menu.Kills = AddDialogButton(DBT_NORMAL,{
	type=TYPE_IMAGEBUTTON,
	parent=Multi_Debrief.Menu,
	anchor=anchorLT,
	x=getWidth(Multi_Debrief.Menu)/2-78,--*mc_w,
	y=10+28*7,
	width=156,--*mc_w,
	height=24,
	text=loc(751),
	font_colour_disabled=GRAY(127),
	callback_mouseclick='MD_showList(8)',
    hint='',
});

Multi_Debrief.Menu.Lost = AddDialogButton(DBT_NORMAL,{
	type=TYPE_IMAGEBUTTON,
	parent=Multi_Debrief.Menu,
	anchor=anchorLT,
	x=getWidth(Multi_Debrief.Menu)/2-78,--*mc_w,
	y=10+28*8,
	width=156,--*mc_w,
	height=24,
	text=loc(752),
	font_colour_disabled=GRAY(127),
	callback_mouseclick='MD_showList(9)',
    hint='',
});

Multi_Debrief.Menu.OK = AddDialogButton(DBT_NORMAL,{
	type=TYPE_IMAGEBUTTON,
	parent=Multi_Debrief.Menu,
	anchor=anchorLB,
	x=getWidth(Multi_Debrief.Menu)/2-78,--*mc_w,
	y=(LayoutHeight*mc_h-60-22-20-10-200-20)-34,
	width=156,--*mc_w,
	height=24,
	text=loc(TID_msg_Ok),
	font_colour_disabled=GRAY(127),
	callback_mouseclick='leaveDebrief();',
    hint='',
});


--[[ =getButtonEX(
	Multi_Debrief.Menu,
	anchorLT,
	XYWH(
		(150*mc_w)/2-75,
		10,
		150,
		24
	),
	SKINTYPE_BUTTON,
	'',
	'',
	'leaveDebrief();',
	{
		text=loc(TID_msg_Ok),
		font_colour_disabled=GRAY(127),
	}
); --]]
-- Inter

-----------------------------------------------------------------------------------------------------  Graphs  --------------------------------------------------------------
Multi_Debrief.Graphs = getElementEX(
	Multi_Debrief,
	anchorLT,
	XYWH(
		28 + getWidth(Multi_Debrief.Menu) +20,
		60+22+20,
		LayoutWidth*mc_w-28-170-50,
		LayoutHeight*mc_h-60-22-20-20
	),
	true,
	{
		colour1=BLACKA(200),
		edges=true,
		--edges_colour1=SIDE_COLOURS[2],
	}
);

Multi_Debrief.Foreground = getElementEX(
	Multi_Debrief,
	anchorLTRB,
	XYWH(
		0,
		0,
		LayoutWidth,
		LayoutHeight
	),
	true,
	{
		colour1=WHITEA(0),
		--texture= '/SGUI/' .. 'Amer' .. '/deb_multi_lose'.. '_w'..'.png';
		nomouseevent = true,
	}
);


function MD_CreatList()
	local el = getElementEX(
		Multi_Debrief.Graphs,
		anchorLT,
		XYWH(
			0,
			0,
			getWidth(Multi_Debrief.Graphs),
			getHeight(Multi_Debrief.Graphs)
		),
		true,
		{
			colour1=BLACKA(0),
			--edges=true,
		}
	);
	
	return el;
end;

--- Univerzal Graph ---
function MD_CreateGraph(parent,x,y,w,h,offset,name)
	local graph = getElementEX(
		parent,
		anchorLT,
		XYWH(
			x,
			y,
			w,
			h
		),
		true,
		{
			colour1=BLACKA(220),
		}
	);

	graph.Name = getLabelEX(
		graph,
		anchorLT,
		XYWH(
		
		),
		Tahoma_14 ,
		name,
		{
			font_colour=WHITEA(255),--WHITEA(255),
			nomouseevent=true,
			text_halign=ALIGN_LEFT,
			font_name=Tahoma_14 ,
			shadowtext=true,
		}
	);

	graph.VLine =  getElementEX(
		graph,
		anchorLT,
		XYWH(
			20+offset,
			30,
			1,
			graph.height - 50+10
		),
		true,
		{
			colour1=RGB(0,150,0)
		}
	);


	graph.HLine = getElementEX(
		graph,
		anchorLT,
		XYWH(
			graph.VLine.x-10,
			25+graph.VLine.height-10,
			graph.width-graph.VLine.x,
			1
		),
		true,
		{
			colour1=RGB(0,150,0)
		}
	);

	graph.HLine2 = getElementEX(
		graph,
		anchorLT,
		XYWH(
			graph.HLine.x,
			25+graph.VLine.height/2-10,
			graph.HLine.width,
			1
		),
		true,
		{
			colour1=RGB(0,150,0,100)
		}
	);

	graph.Zero = getLabelEX(
		graph,
		anchorLT,
		XYWH(
			graph.VLine.x + 5,
			graph.VLine.y+graph.VLine.height - 10 ,
			0,
			0
		),
		Tahoma_10,
		'0',
		{
			colour1=BLACKA(0),
			font_colour=WHITEA(200),
			--scissor=true,
			text_halign=ALIGN_RIGHT,
		}
	);

	graph.PointHalf = getLabelEX(
		graph,
		anchorLT,
		XYWH(
			graph.VLine.x + 5,
			graph.VLine.y+(graph.VLine.height)/2 - 10 ,
			0,
			0
		),
		Tahoma_10,
		'600',
		{
			colour1=BLACKA(0),
			font_colour=WHITEA(200),
			--scissor=true,
			text_halign=ALIGN_RIGHT,
		}
	);

	graph.PointFull = getLabelEX(
		graph,
		anchorLT,
		XYWH(
			graph.VLine.x + 5,
			graph.VLine.y ,
			0,
			0
		),
		Tahoma_10,
		'1200',
		{
			colour1=BLACKA(0),
			font_colour=WHITEA(200),
			--scissor=true,
			text_halign=ALIGN_RIGHT,
		}
	);

	if MPDeb_aspect == '_c' then

		graph.VLine2 =  AddElement({
			type=TYPE_ELEMENT,
			parent=graph,
			anchor=anchorLT,
			visible=true,
			x=graph.HLine.x + (graph.HLine.width-10) *0.33,
			y=25,
			width=1,
			height=graph.height - 50+5,
			colour1=RGBA(0,150,0,100),
		});

		graph.VLine3 =  AddElement({
			type=TYPE_ELEMENT,
			parent=graph,
			anchor=anchorLT,
			visible=true,
			x=graph.HLine.x + (graph.HLine.width-10) *0.66,
			y=25,
			width=1,
			height=graph.height - 50+5,
			colour1=RGBA(0,150,0,100),
		});

		graph.Time1 = getLabelEX(
			graph,
			anchorLT,
			XYWH(
				graph.HLine.x + (graph.HLine.width-10) *0.33 ,
				graph.VLine.y+graph.VLine.height - 10 +5,
				0,
				0
			),
			Tahoma_10,
			'0:45',
			{
				colour1=BLACKA(0),
				font_colour=WHITEA(200),
				--scissor=true,
				text_halign=ALIGN_MIDDLE,
			}
		);

		graph.Time2 = getLabelEX(
			graph,
			anchorLT,
			XYWH(
				graph.HLine.x + (graph.HLine.width-10) *0.66 ,
				graph.VLine.y+graph.VLine.height - 10 +5,
				0,
				0
			),
			Tahoma_10,
			'1:30',
			{
				colour1=BLACKA(0),
				font_colour=WHITEA(200),
				--scissor=true,
				text_halign=ALIGN_MIDDLE,
			}
		);
	else
		graph.VLine2 =  AddElement({
			type=TYPE_ELEMENT,
			parent=graph,
			anchor=anchorLT,
			visible=true,
			x=graph.HLine.x + (graph.HLine.width-10) *0.25,
			y=25,
			width=1,
			height=graph.height - 50+5,
			colour1=RGBA(0,150,0,100),
		});

		graph.VLine3 =  AddElement({
			type=TYPE_ELEMENT,
			parent=graph,
			anchor=anchorLT,
			visible=true,
			x=graph.HLine.x + (graph.HLine.width-10) *0.50,
			y=25,
			width=1,
			height=graph.height - 50+5,
			colour1=RGBA(0,150,0,100),
		});

		graph.VLine4 =  AddElement({
			type=TYPE_ELEMENT,
			parent=graph,
			anchor=anchorLT,
			visible=true,
			x=graph.HLine.x + (graph.HLine.width-10) *0.75,
			y=25,
			width=1,
			height=graph.height - 50+5,
			colour1=RGBA(0,150,0,100),
		});

		graph.Time1 = getLabelEX(
			graph,
			anchorLT,
			XYWH(
				graph.HLine.x + (graph.HLine.width-10) *0.25 ,
				graph.VLine.y+graph.VLine.height - 10 +5,
				0,
				0
			),
			Tahoma_10,
			'0:30',
			{
				colour1=BLACKA(0),
				font_colour=WHITEA(200),
				--scissor=true,
				text_halign=ALIGN_MIDDLE,
			}
		);

		graph.Time2 = getLabelEX(
			graph,
			anchorLT,
			XYWH(
				graph.HLine.x + (graph.HLine.width-10) *0.50 ,
				graph.VLine.y+graph.VLine.height - 10 +5,
				0,
				0
			),
			Tahoma_10,
			'1:00',
			{
				colour1=BLACKA(0),
				font_colour=WHITEA(200),
				--scissor=true,
				text_halign=ALIGN_MIDDLE,
			}
		);

		graph.Time3 = getLabelEX(
			graph,
			anchorLT,
			XYWH(
				graph.HLine.x + (graph.HLine.width-10) *0.75 ,
				graph.VLine.y+graph.VLine.height - 10 +5,
				0,
				0
			),
			Tahoma_10,
			'1:30',
			{
				colour1=BLACKA(0),
				font_colour=WHITEA(200),
				--scissor=true,
				text_halign=ALIGN_MIDDLE,
			}
		);
	end;

	graph.Space = getElementEX(
		graph,
		anchorLT,
		XYWH(
			graph.VLine.x,
			25,
			graph.HLine.width-10,
			graph.VLine.height-10
		),
		true,
		{
			colour1=WHITEA(0),
		}
	);

	graph.Space2 = getElementEX(
		graph,
		anchorLT,
		XYWH(
			graph.VLine.x,
			25,
			graph.HLine.width-10,
			graph.VLine.height-10
		),
		true,
		{
			colour1=WHITEA(0),
		}
	);
	
	return graph;
end;


function MD_CraeteEventsGraphs (parent, x, y, w, h, name)
	local el =  AddElement({
		type=TYPE_ELEMENT,
		parent=parent,
		anchor=anchorLT,
		visible=true,
		x=x,
		y=y,
		width=w,
		height=h,
		colour1=BLACKA(200),
	});

	el.Name = AddElement ({
		type=TYPE_LABEL,
		parent=el,
		anchor=anchorLT,
		x=5,
		y=5,
		width=460,
		height=20,
		text=name,
		font_colour=WHITEA(255),--WHITEA(255),
		nomouseevent=true,
		text_halign=ALIGN_LEFT,
		font_name=Tahoma_14 ,
		shadowtext=true,
	});

	el.VLine =  AddElement({
		type=TYPE_ELEMENT,
		parent=el,
		anchor=anchorLT,
		visible=true,
		x=20,
		y=25,
		width=1,
		height=el.height - 44,
		colour1=RGB(0,150,0),
	});


	el.HLine =  AddElement({
		type=TYPE_ELEMENT,
		parent=el,
		anchor=anchorLT,
		visible=true,
		x=10,
		y=25+el.VLine.height,
		width=el.width-20-10-20,
		height=1,
		colour1=RGBA(0,150,0,255),
	});
	
	el.Space = getElementEX(
		el,
		anchorLT,
		XYWH(
			el.VLine.x,
			25,
			el.HLine.width-20,
			el.VLine.height
		),
		true,
		{
			colour1=WHITEA(0),
		}
	);

	el.VLine2 =  AddElement({
		type=TYPE_ELEMENT,
		parent=el,
		anchor=anchorLT,
		visible=true,
		x=20+el.HLine.width-20,
		y=25,
		width=1,
		height=el.height - 44,
		colour1=RGB(0,150,0),
	});

	el.HLine2 =  AddElement({
		type=TYPE_ELEMENT,
		parent=el,
		anchor=anchorLT,
		visible=true,
		x=10,
		y=25,
		width=el.HLine.width,
		height=1,
		colour1=RGBA(0,150,0,255),
	});

	if MPDeb_aspect == '_c' then

		el.VLine2 =  AddElement({
			type=TYPE_ELEMENT,
			parent=el,
			anchor=anchorLT,
			visible=true,
			x=20 + (el.HLine.width) *0.33,
			y=26,
			width=1,
			height=el.height - 50+5,
			colour1=RGBA(0,150,0,100),
		});

		el.VLine3 =  AddElement({
			type=TYPE_ELEMENT,
			parent=el,
			anchor=anchorLT,
			visible=true,
			x=20 + (el.HLine.width) *0.66,
			y=26,
			width=1,
			height=el.height - 50+5,
			colour1=RGBA(0,150,0,100),
		});

		el.Time1 = getLabelEX(
			el,
			anchorLT,
			XYWH(
				10+el.HLine.x + (el.HLine.width) *0.33 ,
				el.VLine.y+el.VLine.height ,
				0,
				0
			),
			Tahoma_10,
			'0:45',
			{
				colour1=BLACKA(0),
				font_colour=WHITEA(200),
				--scissor=true,
				text_halign=ALIGN_MIDDLE,
			}
		);

		el.Time2 = getLabelEX(
			el,
			anchorLT,
			XYWH(
				10+el.HLine.x + (el.HLine.width) *0.66 ,
				el.VLine.y+el.VLine.height ,
				0,
				0
			),
			Tahoma_10,
			'1:30',
			{
				colour1=BLACKA(0),
				font_colour=WHITEA(200),
				--scissor=true,
				text_halign=ALIGN_MIDDLE,
			}
		);
	else
		el.VLine2 =  AddElement({
			type=TYPE_ELEMENT,
			parent=el,
			anchor=anchorLT,
			visible=true,
			x=20 + (el.HLine.width) *0.25,
			y=26,
			width=1,
			height=el.height - 50,
			colour1=RGBA(0,150,0,100),
		});

		el.VLine3 =  AddElement({
			type=TYPE_ELEMENT,
			parent=el,
			anchor=anchorLT,
			visible=true,
			x=20 + (el.HLine.width) *0.50,
			y=26,
			width=1,
			height=el.height - 50,
			colour1=RGBA(0,150,0,100),
		});

		el.VLine4 =  AddElement({
			type=TYPE_ELEMENT,
			parent=el,
			anchor=anchorLT,
			visible=true,
			x=20 + (el.HLine.width) *0.75,
			y=26,
			width=1,
			height=el.height - 50,
			colour1=RGBA(0,150,0,100),
		});

		el.Time1 = getLabelEX(
			el,
			anchorLT,
			XYWH(
				12+el.HLine.x + (el.HLine.width) *0.25 ,
				el.VLine.y+el.VLine.height ,
				0,
				0
			),
			Tahoma_10,
			'0:30',
			{
				colour1=BLACKA(0),
				font_colour=WHITEA(200),
				--scissor=true,
				text_halign=ALIGN_MIDDLE,
			}
		);

		el.Time2 = getLabelEX(
			el,
			anchorLT,
			XYWH(
				12+el.HLine.x + (el.HLine.width) *0.50 ,
				el.VLine.y+el.VLine.height ,
				0,
				0
			),
			Tahoma_10,
			'1:00',
			{
				colour1=BLACKA(0),
				font_colour=WHITEA(200),
				--scissor=true,
				text_halign=ALIGN_MIDDLE,
			}
		);

		el.Time3 = getLabelEX(
			el,
			anchorLT,
			XYWH(
				12+el.HLine.x + (el.HLine.width) *0.75 ,
				el.VLine.y+el.VLine.height ,
				0,
				0
			),
			Tahoma_10,
			'1:30',
			{
				colour1=BLACKA(0),
				font_colour=WHITEA(200),
				--scissor=true,
				text_halign=ALIGN_MIDDLE,
			}
		);
	end;
	
	return el;
end;

function MD_CreateEvetsSpaces(parent,numberofside,sides)
	local space = parent.Space;
	
	sgui_deletechildren(space.ID);
	local heightSpace = space.height/numberofside;

	for i =1 , numberofside do
		space[sides[i]] = getElementEX(
			space,
			anchorLT,
			XYWH(
				0,
				heightSpace*(i-1)+1,
				space.width,
				heightSpace-1
			),
			true,
			{
				colour1 = RGBA(SIDE_COLOURS[sides[i]].red,SIDE_COLOURS[sides[i]].green,SIDE_COLOURS[sides[i]].blue,20),
			}
		);
		space[sides[i]].Suma = getLabelEX(
			space,
			anchorLT,
			XYWH(
				space[sides[i]].x+space[sides[i]].width+10+10 ,
				space[sides[i]].y+space[sides[i]].height/2-5 ,
				0,
				0
			),
			Tahoma_10,
			'27',
			{
				colour1=BLACKA(0),
				font_colour=WHITEA(200),
				--scissor=true,
				text_halign=ALIGN_MIDDLE,
			}
		);
		
		if i < numberofside then
			space[sides[i]].Hline = getElementEX(
				space,
				anchorLT,
				XYWH(
					0,
					heightSpace*(i),
					space.width+5,
					1
				),
				true,
				{
					colour1=RGBA(0,150,0,100),
				}
			);
		end;
	end;

	
	local timesItems = {
			parent.Time1,
			parent.Time2,
			parent.Time3
	};

	local fullTime = md_totalTime;
	local timeCount =0;
	local mp = {};
	if MPDeb_aspect == '_c' then
		mp = {0.33,0.66}; --multiplicator
		timeCount = 2;
	else
		mp = {0.25,0.50,0.75}; --multiplicator
		timeCount = 3;
	end;

	for i= 1, timeCount do
		local time = fullTime * mp[i];
		time = div(time,1);

		if time < 60 then
			minutes = time;
			hours = 0;
		else
			hours = div(time,60);
			minutes = mod(time,60);
		end;

		if minutes < 10 then
			setText(timesItems[i],hours..':0'..minutes);
		else
			setText(timesItems[i],Time,hours..':'..minutes);
		end;
	end;
end;

function MD_AddEvents(space,data)
--[[
	DATA BREAKDOWN
		time [1 .. data.count]
			events  [1 .. data.count.count]
				type  - int  type events
				side  - int  side colour
--]]

		local count = 0;
		for i=1, data.count do
			for e=1, data[i].count do
				local type = data[i][e].type;
				local hs = getHeight(space)-evIcons[type].h;
				local ws = getWidth(space);
				local hp = data[i].count;
				local ye = e;
				
				if getHeight(space) <= hp*evIcons[type].h then
					ye = e-1;
				else
					hp = hp+1;
				end;
				
				local y = hs / (hp);
				local x = (ws -10)/( data.count-1);

				local event = getElementEX(
							space,
							anchorLT,
							XYWH(
								(i-1)*x-(evIcons[type].w/2) +5,
								ye*y,
								evIcons[type].w,--11,
								evIcons[type].h--12
							),
							true,
							{
								colour1=SIDE_COLOURS[data[i][e].side],
								texture='/SGUI/Amer/deb_icons.png',
								subtexture=true,
								subcoords=evIcons[type],
								hint = loc_format(TID_Time, { getTime(i)}) .. CHAR13 .. evHints[type],
							}
						);


				count = count + 1;
			end;
		end;

		setText (space.Suma, count);
	
end;
------------------------ Score -----------------------------------------
Multi_Debrief.Graphs.Score =  MD_CreatList();

Multi_Debrief.Graphs.Score.NamesArea = AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Debrief.Graphs.Score,
	anchor=anchorLT,
	visible=true,
	x=Multi_Debrief.Graphs.Score.width/2 + (100*mc_w)- (353*mc_w),
	y=50*mc_h,
	width=353*mc_w,
	height=229*mc_h,
	--colour1=WHITEA(0),
	 colour1=BLACKA(153),   --test
});

Multi_Debrief.Graphs.Score.PosArea = AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Debrief.Graphs.Score,
	anchor=anchorLT,
	visible=true,
	x=Multi_Debrief.Graphs.Score.width/2 + (100*mc_w) - (438*mc_w) ,
	y=50*mc_h,
	width=82*mc_w,
	height=229*mc_h,
	--colour1=WHITEA(0),
	 colour1=BLACKA(153),   --test
});

Multi_Debrief.Graphs.Score.ScoreArea = AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Debrief.Graphs.Score,
	anchor=anchorLT,
	visible=true,
	x=Multi_Debrief.Graphs.Score.width/2 + (100*mc_w) + (3*mc_w) ,
	y=50*mc_h,
	width=82*mc_w,
	height=229*mc_h,
	--colour1=WHITEA(0),
	 colour1=BLACKA(153),   --test
});

Multi_Debrief.Graphs.Score.ScoreGraph = MD_CreateGraph(
				Multi_Debrief.Graphs.Score,
				5,
				5+((Multi_Debrief.Graphs.Score.height-5*3)/2+5),
				Multi_Debrief.Graphs.Score.width-10,
				(Multi_Debrief.Graphs.Score.height-5*3)/2,
				10,
				''
	);

setVisible(Multi_Debrief.Graphs.Score.ScoreGraph,false);
------------------------ Units -----------------------------------------
Multi_Debrief.Graphs.Units =  MD_CreatList();

Multi_Debrief.Graphs.Units.Humans = MD_CreateGraph(
				Multi_Debrief.Graphs.Units,
				5,
				5,
				Multi_Debrief.Graphs.Units.width-10,
				((Multi_Debrief.Graphs.Units.height-5*4)/3)*1,
				10,
				loc(744)
	);

Multi_Debrief.Graphs.Units.Veicles = MD_CreateGraph(
				Multi_Debrief.Graphs.Units,
				5,
				5+((Multi_Debrief.Graphs.Units.height-5*4)/3+5),
				Multi_Debrief.Graphs.Units.width-10,
				(Multi_Debrief.Graphs.Units.height-5*4)/3,
				10,
				loc(745)
	);

	Multi_Debrief.Graphs.Units.Buildings = MD_CreateGraph(
				Multi_Debrief.Graphs.Units,
				5,
				5+((Multi_Debrief.Graphs.Units.height-5*4)/3+5)*2,
				Multi_Debrief.Graphs.Units.width-10,
				(Multi_Debrief.Graphs.Units.height-5*4)/3,
				10,
				loc(746)
	);

------------------------ Income -----------------------------------------
Multi_Debrief.Graphs.Income =  MD_CreatList();

Multi_Debrief.Graphs.Income.Crates = MD_CreateGraph(
				Multi_Debrief.Graphs.Income,
				5,
				5,
				Multi_Debrief.Graphs.Income.width-10,
				((Multi_Debrief.Graphs.Income.height-5*4)/3)*1,
				10,
				loc(748)
	);

Multi_Debrief.Graphs.Income.Oil = MD_CreateGraph(
				Multi_Debrief.Graphs.Income,
				5,
				5+((Multi_Debrief.Graphs.Income.height-5*4)/3+5),
				Multi_Debrief.Graphs.Income.width-10,
				(Multi_Debrief.Graphs.Income.height-5*4)/3,
				10,
				loc(749)
	);

	Multi_Debrief.Graphs.Income.Siberite = MD_CreateGraph(
				Multi_Debrief.Graphs.Income,
				5,
				5+((Multi_Debrief.Graphs.Income.height-5*4)/3+5)*2,
				Multi_Debrief.Graphs.Income.width-10,
				(Multi_Debrief.Graphs.Income.height-5*4)/3,
				10,
				loc(750)
	);
	
------------------------ Strenght --------------------------------------

Multi_Debrief.Graphs.Strenght =  MD_CreatList();
--- Combined Strenght ---
Multi_Debrief.Graphs.Strenght.Graph_TS = MD_CreateGraph(
				Multi_Debrief.Graphs.Strenght,
				5,
				5,
				Multi_Debrief.Graphs.Strenght.width-10,
				((Multi_Debrief.Graphs.Strenght.height-5*6)/5)*2+5,
				10,
				loc(743)
	);

Multi_Debrief.Graphs.Strenght.Graph_HS = MD_CreateGraph(
				Multi_Debrief.Graphs.Strenght,
				5,
				5+((Multi_Debrief.Graphs.Strenght.height-5*6)/5+5)*2,
				Multi_Debrief.Graphs.Strenght.width-10,
				(Multi_Debrief.Graphs.Strenght.height-5*6)/5,
				10,
				loc(TID_Debrief_HumansStrength)
	);

Multi_Debrief.Graphs.Strenght.Graph_VS = MD_CreateGraph(
				Multi_Debrief.Graphs.Strenght,
				5,
				5+((Multi_Debrief.Graphs.Strenght.height-5*6)/5+5)*3,
				Multi_Debrief.Graphs.Strenght.width-10,
				(Multi_Debrief.Graphs.Strenght.height-5*6)/5,
				10,
				loc(TID_Debrief_VehiclesStrenght)
	);

Multi_Debrief.Graphs.Strenght.Graph_BS = MD_CreateGraph(
				Multi_Debrief.Graphs.Strenght,
				5,
				5+((Multi_Debrief.Graphs.Strenght.height-5*6)/5+5)*4,
				Multi_Debrief.Graphs.Strenght.width-10,
				(Multi_Debrief.Graphs.Strenght.height-5*6)/5,
				10,
				loc(TID_Debrief_BuildingsStrenght)
	);

------------------------ Lost and Destroyed -----------------------------------------
Multi_Debrief.Graphs.LD =  MD_CreatList();

Multi_Debrief.Graphs.LD.Destroyed = MD_CreateGraph(
				Multi_Debrief.Graphs.LD,
				5,
				5,
				Multi_Debrief.Graphs.LD.width-10,
				((Multi_Debrief.Graphs.LD.height-5*3)/2)*1,
				10,
				loc(751)
	);

Multi_Debrief.Graphs.LD.Lost = MD_CreateGraph(
				Multi_Debrief.Graphs.LD,
				5,
				5+((Multi_Debrief.Graphs.LD.height-5*3)/2+5),
				Multi_Debrief.Graphs.LD.width-10,
				(Multi_Debrief.Graphs.LD.height-5*3)/2,
				10,
				loc(752)
	);
------------------------ Built and Create --------------------------------------

Multi_Debrief.Graphs.BC =  MD_CreatList();
--- Built --
Multi_Debrief.Graphs.BC.Built = MD_CraeteEventsGraphs (
		Multi_Debrief.Graphs.BC,
		5, 
		5+((Multi_Debrief.Graphs.BC.height-5*4)/3+5)*0, 
		Multi_Debrief.Graphs.BC.width-10, 
		(Multi_Debrief.Graphs.BC.height-5*4)/3,
		loc(TID_Debrief_Built)
	);
--- Create --
Multi_Debrief.Graphs.BC.Create = MD_CraeteEventsGraphs (
		Multi_Debrief.Graphs.BC,
		5, 
		5+((Multi_Debrief.Graphs.BC.height-5*4)/3+5)*1, 
		Multi_Debrief.Graphs.BC.width-10, 
		(Multi_Debrief.Graphs.BC.height-5*4)/3,
		loc(TID_Debrief_Constructed)
	);
--- Tamed --
Multi_Debrief.Graphs.BC.Tamed = MD_CraeteEventsGraphs (
		Multi_Debrief.Graphs.BC,
		5, 
		5+((Multi_Debrief.Graphs.BC.height-5*4)/3+5)*2, 
		Multi_Debrief.Graphs.BC.width-10, 
		(Multi_Debrief.Graphs.BC.height-5*4)/3,
		loc(TID_Debrief_Tamed)
	);
	
------------------ Technologies -----------------------------------------
Multi_Debrief.Graphs.Techs =  MD_CreatList();

Multi_Debrief.Graphs.Techs.Inverted = MD_CraeteEventsGraphs (
		Multi_Debrief.Graphs.Techs,
		5, 
		5, 
		Multi_Debrief.Graphs.Techs.width-10, 
		(Multi_Debrief.Graphs.Techs.height-5*4)/3,
		loc(747)
	);

	--- Graph --
Multi_Debrief.Graphs.Techs.Graph = MD_CreateGraph (
		Multi_Debrief.Graphs.Techs,
		5, 
		5+((Multi_Debrief.Graphs.Techs.height-5*4)/3+5), 
		Multi_Debrief.Graphs.Techs.width-10, 
		(Multi_Debrief.Graphs.Techs.height-5*4)/3,
		10,
		loc(747)
	);
	
------------------------ Lost --------------------------------------

Multi_Debrief.Graphs.Lost =  MD_CreatList();
--- Humans --
Multi_Debrief.Graphs.Lost.Humans = MD_CraeteEventsGraphs (
		Multi_Debrief.Graphs.Lost,
		5, 
		5+((Multi_Debrief.Graphs.Lost.height-5*4)/3+5)*0, 
		Multi_Debrief.Graphs.Lost.width-10, 
		(Multi_Debrief.Graphs.Lost.height-5*4)/3,
		loc(744)
	);
--- Vehicles --
Multi_Debrief.Graphs.Lost.Vehicles = MD_CraeteEventsGraphs (
		Multi_Debrief.Graphs.Lost,
		5, 
		5+((Multi_Debrief.Graphs.Lost.height-5*4)/3+5)*1, 
		Multi_Debrief.Graphs.Lost.width-10, 
		(Multi_Debrief.Graphs.Lost.height-5*4)/3,
		loc(745)
	);
--- Buildings --
Multi_Debrief.Graphs.Lost.Buildings = MD_CraeteEventsGraphs (
		Multi_Debrief.Graphs.Lost,
		5, 
		5+((Multi_Debrief.Graphs.Lost.height-5*4)/3+5)*2, 
		Multi_Debrief.Graphs.Lost.width-10, 
		(Multi_Debrief.Graphs.Lost.height-5*4)/3,
		loc(746)
	);

------------------------ Destroyed --------------------------------------

Multi_Debrief.Graphs.Destroyed =  MD_CreatList();
--- Humans --
Multi_Debrief.Graphs.Destroyed.Humans = MD_CraeteEventsGraphs (
		Multi_Debrief.Graphs.Destroyed,
		5, 
		5+((Multi_Debrief.Graphs.Destroyed.height-5*4)/3+5)*0, 
		Multi_Debrief.Graphs.Destroyed.width-10, 
		(Multi_Debrief.Graphs.Destroyed.height-5*4)/3,
		loc(744)
	);
--- Vehicles --
Multi_Debrief.Graphs.Destroyed.Vehicles = MD_CraeteEventsGraphs (
		Multi_Debrief.Graphs.Destroyed,
		5, 
		5+((Multi_Debrief.Graphs.Destroyed.height-5*4)/3+5)*1, 
		Multi_Debrief.Graphs.Destroyed.width-10, 
		(Multi_Debrief.Graphs.Destroyed.height-5*4)/3,
		loc(745)
	);
--- Buildings --
Multi_Debrief.Graphs.Destroyed.Buildings = MD_CraeteEventsGraphs (
		Multi_Debrief.Graphs.Destroyed,
		5, 
		5+((Multi_Debrief.Graphs.Destroyed.height-5*4)/3+5)*2, 
		Multi_Debrief.Graphs.Destroyed.width-10, 
		(Multi_Debrief.Graphs.Destroyed.height-5*4)/3,
		loc(746)
	);
-------------------------------------------------------------------------------------------------

function MD_showList(id)

	setVisible(Multi_Debrief.Graphs.Strenght,false);
	setVisible(Multi_Debrief.Graphs.Units,false);
	setVisible(Multi_Debrief.Graphs.Techs,false);
	setVisible(Multi_Debrief.Graphs.LD,false);
	setVisible(Multi_Debrief.Graphs.Income,false);
	setVisible(Multi_Debrief.Graphs.Score,false);
	setVisible(Multi_Debrief.Graphs.BC,false);
	setVisible(Multi_Debrief.Graphs.Destroyed,false);
	setVisible(Multi_Debrief.Graphs.Lost,false);
	if DATADEBRIEF.MULTI_WITH_SCORE then
		setEnabled(Multi_Debrief.Menu.Score , true);
	end;
	setEnabled( Multi_Debrief.Menu.Units, true);
	setEnabled(Multi_Debrief.Menu.Income , true);
	setEnabled(Multi_Debrief.Menu.Strenght , true);
	setEnabled(Multi_Debrief.Menu.LostKill , true);
	setEnabled(Multi_Debrief.Menu.Created , true);
	setEnabled(Multi_Debrief.Menu.Techs , true);
	setEnabled(Multi_Debrief.Menu.Kills , true);
	setEnabled(Multi_Debrief.Menu.Lost  , true);
	
	switch(id) : caseof {

		[1]   = function (x) setVisible(Multi_Debrief.Graphs.Score,true); 
							setEnabled(Multi_Debrief.Menu.Score,false); 
							--setFontColour(Multi_Debrief.Menu.Score, RGB(0,255,0));  
				end,
		[2]   = function (x) setVisible(Multi_Debrief.Graphs.Units,true); 
							setEnabled(Multi_Debrief.Menu.Units,false); 
							--setFontColour(Multi_Debrief.Menu.Units, RGB(0,255,0));
				end,
		[3]   = function (x) setVisible(Multi_Debrief.Graphs.Income,true);  
							setEnabled(Multi_Debrief.Menu.Income,false); 
							--setFontColour(Multi_Debrief.Menu.Income, RGB(0,255,0));
				end,
		[4]   = function (x) setVisible(Multi_Debrief.Graphs.Strenght,true);  
							setEnabled(Multi_Debrief.Menu.Strenght,false); 
							--setFontColour(Multi_Debrief.Menu.Strenght, RGB(0,255,0));
				end,
		[5]   = function (x) setVisible(Multi_Debrief.Graphs.LD,true);  
							setEnabled(Multi_Debrief.Menu.LostKill,false); 
							--setFontColour(Multi_Debrief.Menu.LostKill, RGB(0,255,0));
				end,
		[6]   = function (x) setVisible(Multi_Debrief.Graphs.BC,true);  
							setEnabled(Multi_Debrief.Menu.Created,false); 
							--setFontColour(Multi_Debrief.Menu.Created, RGB(0,255,0));
				end,
		[7]   = function (x) setVisible(Multi_Debrief.Graphs.Techs,true);  
							setEnabled(Multi_Debrief.Menu.Techs,false); 
							--setFontColour(Multi_Debrief.Menu.Techs, RGB(0,255,0));
				end,
		[8]   = function (x) setVisible(Multi_Debrief.Graphs.Destroyed,true);  
							setEnabled(Multi_Debrief.Menu.Kills,false); 
							--setFontColour(Multi_Debrief.Menu.Kills, RGB(0,255,0));
				end,
		[9]   = function (x) setVisible(Multi_Debrief.Graphs.Lost,true);  
							setEnabled(Multi_Debrief.Menu.Lost ,false); 
							--setFontColour(Multi_Debrief.Menu.Lost, RGB(0,255,0));
				end;

	}
	
end;

function makeCustomSideChechboxElement(parent,name,side,checked,x,y,callback)
	local cb = {};
	cb.Checked = checked;
	cb.Side = side;
	local vs;



	if cb.Checked then
		sb = SUBCOORD(0,16,16,16);
	else
		sb = SUBCOORD(0,0,16,16);
	end;

	cb.Checker = getElementEX(
		parent,
		anchorLT,
		XYWH(
			x+2,
			y,
			18,--Multi_Room.GameParameters.width-10,
			18
		),
		true,
		{
			hint = '',
			nomouseevent = false,
			visible = true,
			texture=interface.current.debrief.sideBox,
			subtexture=true,
			subcoords=sb,
			colour1=SIDE_COLOURS[side],
			callback_mouseclick = callback,
		}
	);
	
	cb.Name = getLabelEX (
		parent,
		anchorLT,
		XYWH(
			x+20,
			y-1,
			100,
			18
		),
		nil,
		name,
		{
			font_colour=SIDE_COLOURS[side],
			callback_mouseclick = callback,
			scissor=true,
			shadowtext=true,
		}
	);
	
	cb.Score = getLabelEX (
		parent,
		anchorLT,
		XYWH(
			parent.width-85,
			y-2,
			78,
			16
		),
		nil,
		0,
		{
			font_colour=SIDE_COLOURS[side],
			callback_mouseclick = callback,
			text_halign=ALIGN_RIGHT,
			visible=false,
			shadowtext=true,
			
		}
	);
	set_Callback(cb.Checker.ID,CALLBACK_MOUSEOVER,'moveToFront('..side..');');
	set_Callback(cb.Name.ID,CALLBACK_MOUSEOVER,'moveToFront('..side..');');

	return cb;
end;

function moveToFront(side)
		bringToFront(Multi_Debrief.Graphs.Strenght.Graph_TS.Space[side]);
		bringToFront(Multi_Debrief.Graphs.Strenght.Graph_HS.Space[side]);
		bringToFront(Multi_Debrief.Graphs.Strenght.Graph_VS.Space[side]);
		bringToFront(Multi_Debrief.Graphs.Strenght.Graph_BS.Space[side]);
		bringToFront(Multi_Debrief.Graphs.Units.Humans.Space[side]);
		bringToFront(Multi_Debrief.Graphs.Units.Veicles.Space[side]);
		bringToFront(Multi_Debrief.Graphs.Units.Buildings.Space[side]);
		bringToFront(Multi_Debrief.Graphs.Techs.Graph.Space[side]);
		bringToFront(Multi_Debrief.Graphs.LD.Destroyed.Space[side]);
		bringToFront(Multi_Debrief.Graphs.LD.Lost.Space[side]);
		bringToFront(Multi_Debrief.Graphs.Income.Crates.Space[side]);
		bringToFront(Multi_Debrief.Graphs.Income.Oil.Space[side]);
		bringToFront(Multi_Debrief.Graphs.Income.Siberite.Space[side]);
--		bringToFront(Multi_Debrief.Graphs.Score.ScoreGraph.Space[side]);

end;

function changeVisibleGraphs(side)
	local cb = sideCheckBoxSuma[side];
	
	if cb.Checked then
		setVisible(Multi_Debrief.Graphs.Strenght.Graph_TS.Space[side],false);
		setVisible(Multi_Debrief.Graphs.Strenght.Graph_HS.Space[side],false);
		setVisible(Multi_Debrief.Graphs.Strenght.Graph_VS.Space[side],false);
		setVisible(Multi_Debrief.Graphs.Strenght.Graph_BS.Space[side],false);
		setVisible(Multi_Debrief.Graphs.Units.Humans.Space[side],false);
		setVisible(Multi_Debrief.Graphs.Units.Veicles.Space[side],false);
		setVisible(Multi_Debrief.Graphs.Units.Buildings.Space[side],false);
		setVisible(Multi_Debrief.Graphs.Techs.Graph.Space[side],false);
		setVisible(Multi_Debrief.Graphs.LD.Destroyed.Space[side],false);
		setVisible(Multi_Debrief.Graphs.LD.Lost.Space[side],false);
		setVisible(Multi_Debrief.Graphs.Income.Crates.Space[side],false);
		setVisible(Multi_Debrief.Graphs.Income.Oil.Space[side],false);
		setVisible(Multi_Debrief.Graphs.Income.Siberite.Space[side],false);
--		setVisible(Multi_Debrief.Graphs.Score.ScoreGraph.Space[side],false);
		cb.Checked = false;
		setSubCoords(cb.Checker,SUBCOORD(0,0,16,16));
	else 
		setVisible(Multi_Debrief.Graphs.Strenght.Graph_TS.Space[side],true);
		setVisible(Multi_Debrief.Graphs.Strenght.Graph_HS.Space[side],true);
		setVisible(Multi_Debrief.Graphs.Strenght.Graph_VS.Space[side],true);
		setVisible(Multi_Debrief.Graphs.Strenght.Graph_BS.Space[side],true);
		setVisible(Multi_Debrief.Graphs.Units.Humans.Space[side],true);
		setVisible(Multi_Debrief.Graphs.Units.Veicles.Space[side],true);
		setVisible(Multi_Debrief.Graphs.Units.Buildings.Space[side],true);
		setVisible(Multi_Debrief.Graphs.Techs.Graph.Space[side],true);
		setVisible(Multi_Debrief.Graphs.LD.Destroyed.Space[side],true);
		setVisible(Multi_Debrief.Graphs.LD.Lost.Space[side],true);
		setVisible(Multi_Debrief.Graphs.Income.Crates.Space[side],true);
		setVisible(Multi_Debrief.Graphs.Income.Oil.Space[side],true);
		setVisible(Multi_Debrief.Graphs.Income.Siberite.Space[side],true);
--		setVisible(Multi_Debrief.Graphs.Score.ScoreGraph.Space[side],true);
		cb.Checked = true;
		setSubCoords(cb.Checker,SUBCOORD(0,16,16,16));
	end;

end;


function ShowDebrief(id) 
	
		setVisible(Multi_Debrief.Suma, true);
		setVisible(Multi_Debrief.Score, false);

		switch(id) : caseof {
			[1]   = function (x) 
						setVisible(Multi_Debrief.Score, true);
						setVisible(Multi_Debrief.Suma, false);
					end,
			[2]   = function (x) 
						setVisible(Multi_Debrief.Suma, true);
						setVisible(Multi_Debrief.Score, false);
					end,

		}
end;

-----------------------------------------------------------------------------------------------------  SCORE  --------------------------------------------------------------
if ScrHeight < 1000 then
	sFont = Tahoma_13B;
	sbFont = Tahoma_16B;
	sFontSpace = 13;
	sbFontSpace = 16;
else
	sFont = Tahoma_16B;
	sbFont = Tahoma_20B;
	sFontSpace = 16;
	sbFontSpace = 20;
end;

function createScore(multiScore)
--[[
BREAKDOWN
	multiScore 
		count	interger
		items[1 - count]
			position string
			name string
			side interger (colour)
			score interger
			bigFont bool
]]--
	sgui_deletechildren(Multi_Debrief.Graphs.Score.NamesArea.ID);
	sgui_deletechildren(Multi_Debrief.Graphs.Score.PosArea.ID);
	sgui_deletechildren(Multi_Debrief.Graphs.Score.ScoreArea.ID);
	local yPos = 1;
	for i=1, multiScore.count do
		local font;
		local usedSpace;
		if multiScore.items[i].bigFont then
			font = sbFont;
			usedSpace = sbFontSpace;
		else
			font = sFont;
			usedSpace = sFontSpace;
		end;
		
		AddElement ( {	
			type=TYPE_LABEL,
			parent=Multi_Debrief.Graphs.Score.PosArea,
			anchor={top=true,bottom=false,left=true,right=false},
			x=0,
			y=yPos,
			text=multiScore.items[i].position, 
			width=getWidth(Multi_Debrief.Graphs.Score.PosArea),
			nomouseevent=true,
			font_colour = SIDE_COLOURS[multiScore.items[i].side],
			text_halign=ALIGN_MIDDLE,
			font_name=font ,
		});
		
		AddElement ( {	
			type=TYPE_LABEL,
			parent=Multi_Debrief.Graphs.Score.NamesArea,
			anchor={top=true,bottom=false,left=true,right=false},
			x=0,
			y=yPos,
			text=multiScore.items[i].name, 
			width=getWidth(Multi_Debrief.Graphs.Score.NamesArea),
			nomouseevent=true,
			font_colour = SIDE_COLOURS[multiScore.items[i].side],
			text_halign=ALIGN_MIDDLE,
			font_name=font ,
		});
		
		AddElement ( {	
			type=TYPE_LABEL,
			parent=Multi_Debrief.Graphs.Score.ScoreArea,
			anchor={top=true,bottom=false,left=true,right=false},
			x=0,
			y=yPos,
			text=multiScore.items[i].score, 
			width=getWidth(Multi_Debrief.Graphs.Score.ScoreArea),
			nomouseevent=true,
			font_colour = SIDE_COLOURS[multiScore.items[i].side],
			text_halign=ALIGN_MIDDLE,
			font_name=font,
		});
		
		yPos = yPos + usedSpace;
	end;
	

end;

function seriazableMultiScore(SCORE)
	local sc = copytable(SCORE);
	
	sc = getKeysSortedByValue(sc, function(a, b) return a > b end);
	local lasts=nil;
	local lastp=0;
	local lastpp=1;
	local newScore = {};
	for _, key in ipairs(sc) do
		if SCORE[key] ~= lasts and DATADEBRIEF.DEBRIEF[key+1].COUNT > 0  then
			lastpp = 1;
			lastp = lastp+1;
			newScore[lastp] = {};
			newScore[lastp][lastpp] = key;
			newScore[lastp].count = 1;
		elseif DATADEBRIEF.DEBRIEF[key+1].COUNT > 0 then
			lastpp= lastpp+1;
			if not newScore[lastp] then
				newScore[lastp] = {};
			end;
			--lastp = lastp -1;
			newScore[lastp][lastpp] = key;
			newScore[lastp].count = lastpp;
			--lastp = lastp+1;
		end;
		lasts = SCORE[key];
	end;
	newScore.count = lastp;
	-- example result newScore = {{1,4,7,count=3},{3,count=1},{6,8,count=2}};
	local pos='';
	local side= 0;
	local name = 'test';
	local score = 0;
	local bigFont = true;
	local lastPos=0;
	local multiScore = {items={},count=0};
	for i=1, newScore.count do
		for s=1, newScore[i].count do
			lastPos = lastPos+1;
			
			if i == 1 then
				bigFont = true;
			else
				bigFont = false;
			end;
			
			if newScore[i].count > 1 then
				if s == 1 then
					pos = tostring(lastPos);--..'.- '.. tostring(lastPos+newScore[i].count-1) .. '.' ;
				else
					pos = '';
				end;
				name = DATADEBRIEF.COLOURS[newScore[i][s]].NAME;
				side = newScore[i][s]+1;
				score = SCORE[newScore[i][s]];
			else
				
				pos = tostring(lastPos)..'.';
				name = DATADEBRIEF.COLOURS[newScore[i][s]].NAME;
				side = newScore[i][s]+1;
				score = SCORE[newScore[i][s]];
				
				
			end;
			multiScore.items[lastPos] = {position = pos, name = name, side = side, bigFont = bigFont, score = score};
		end;
	end;
	multiScore.count = lastPos;
	
	return multiScore;
end;

function getKeysSortedByValue(tbl, sortFunction)
  local keys = {}
  for key in pairs(tbl) do
    table.insert(keys, key)
  end

  table.sort(keys, function(a, b)
    return sortFunction(tbl[a], tbl[b])
  end)

  return keys
end;
------------------------------------------------------------------------------------- ETC ---------------------------------------------------------
function changeInterfaceAndWinLost(SYNCLOST, WIN)
	sgui_deletechildren(Multi_Debrief.Background.ID);
	sgui_deletechildren(Multi_Debrief.Foreground.ID);
	
	if SYNCLOST then
		setText(Multi_Debrief.WinLose,loc(TID_MultiMsg_Sync_Out));
		if interface.current.side == interface.amer.side then
			setColour1(Multi_Debrief,BLACKA(255));
			BuildAmericanDebrief(false);
		elseif interface.current.side == interface.rus.side then
			setColour1(Multi_Debrief,BLACKA(255));
			BuildRussianDebrief(false);
		else
			setColour1(Multi_Debrief,WHITEA(255));
			setTexture(Multi_Debrief, interface.current.debrief.lose);
		end;
	elseif WIN then
		setText(Multi_Debrief.WinLose,loc(TID_Multi_Victory));
		if interface.current.side == interface.amer.side then
			setColour1(Multi_Debrief,BLACKA(255));
			BuildAmericanDebrief(true);
		elseif interface.current.side == interface.rus.side then
			setColour1(Multi_Debrief,BLACKA(255));
			BuildRussianDebrief(true);
		else
			setColour1(Multi_Debrief,WHITEA(255));
			setTexture(Multi_Debrief, interface.current.debrief.win);
		end;
	else
		setText(Multi_Debrief.WinLose,loc(TID_Multi_Loss)); 
		if interface.current.side == interface.amer.side then
			setColour1(Multi_Debrief,BLACKA(255));
			BuildAmericanDebrief(false);
		elseif interface.current.side == interface.rus.side then
			setColour1(Multi_Debrief,BLACKA(255));
			BuildRussianDebrief(false);
		else
			setColour1(Multi_Debrief,WHITEA(255));
			setTexture(Multi_Debrief, interface.current.debrief.lose);
		end;
	end;
end;


------------------------------------------- Build Nation Debrief ---------------------------------
-----------------------------------------------American ------------------------------------------
function BuildAmericanDebrief(won)
	local rClip = "/SGUI/Amer/debrief/right_clip.png";
	local lClip = "/SGUI/Amer/debrief/left_clip.png";
	local dClip = "/SGUI/Amer/debrief/doubleclip.png";
	local rTClip = "/SGUI/Amer/debrief/right_title_clip.png";
	local lTClip = "/SGUI/Amer/debrief/left_title_clip.png";
	local rod = "/SGUI/Amer/debrief/rod.png";
	local rod_AC = "/SGUI/Amer/debrief/rod_afterclip.png";
	local corner_BL = "/SGUI/Amer/debrief/corner_BL.png";
	local corner_BR = "/SGUI/Amer/debrief/corner_BR.png";
	local corner_TL = "/SGUI/Amer/debrief/corner_TL.png";
	local corner_TR = "/SGUI/Amer/debrief/corner_TR.png";
	local SC_BL = "/SGUI/Amer/debrief/SC_BL.png";
	local SC_BR = "/SGUI/Amer/debrief/SC_BR.png";
	local SC_TL = "/SGUI/Amer/debrief/SC_TL.png";
	local SC_TR = "/SGUI/Amer/debrief/SC_TR.png";
	local SF_B = "/SGUI/Amer/debrief/SF_B.png";
	local SF_L = "/SGUI/Amer/debrief/SF_L.png";
	local SF_R = "/SGUI/Amer/debrief/SF_R.png";
	local SF_T = "/SGUI/Amer/debrief/SF_T.png";
	local connector = "/SGUI/Amer/debrief/connector.png";
	local wire = "/SGUI/Amer/debrief/wire.png";
	local score_BG = "/SGUI/Amer/debrief/Score_background.png"
	
	local BG = Multi_Debrief.Background;
	local FG = Multi_Debrief.Foreground;
	
	local DebriefElements = {};
	DebriefElements.wire = getElementEX(
		FG,
		anchorLT,
		XYWH(
			0,
			ScrHeight-8,
			ScrWidth,
			5
		),
		true,
		{
			tile=true,
			texture = wire;
		}
	);
	
	DebriefElements.lRod = getElementEX(
		FG,
		acnhorLT,
		XYWH(
			0,
			0,
			23,
			ScrHeight
		),
		true,
		{
			tile=true,
			texture = rod,
		}
	);
	
	DebriefElements.rRod = getElementEX(
		FG,
		acnhorLT,
		XYWH(
			ScrWidth-23,
			0,
			23,
			ScrHeight
		),
		true,
		{
			tile=true,
			texture = rod,
		}
	);

	DebriefElements.cBL = getElementEX(
		FG,
		anchorLT,
		XYWH(
			0,
			ScrHeight-43,
			28,
			43
		),
		true,
		{
			texture = corner_BL,
		}
	);

	DebriefElements.cBR = getElementEX(
		FG,
		anchorLT,
		XYWH(
			ScrWidth-28,
			ScrHeight-44,
			28,
			43
		),
		true,
		{
			texture = corner_BR,
		}
	);

	DebriefElements.cTL = getElementEX(
		FG,
		anchorLT,
		XYWH(
			0,
			0,
			28,
			40
		),
		true,
		{
			texture = corner_TL,
		}
	);

	DebriefElements.cTR = getElementEX(
		FG,
		anchorLT,
		XYWH(
			ScrWidth-28,
			0,
			28,
			40
		),
		true,
		{
			texture = corner_TR,
		}
	);
	DebriefElements.connectors = {}
	DebriefElements.connectors[1] = getElementEX(
		FG,
		anchorLT,
		XYWH(
			96,
			ScrHeight-15,
			33,
			14
		),
		true,
		{
			texture = connector,
		}
	);

	local bCount = div((getWidth(Multi_Debrief.Graphs)-180), 200) +1;
	
	if bCount < 2 then
		bCount = 2;
	end;

	for i=0, bCount do
		DebriefElements.connectors[i+2] = getElementEX(
			FG,
			anchorLT,
			XYWH(
				200 + (getWidth(Multi_Debrief.Graphs)-180)/bCount*i +60,
				ScrHeight-15,
				33,
				14
			),
			true,
			{
				texture = connector,
			}
		);
	end;
	DebriefElements.TitleFrame = BuildAmerFrame(74,11,ScrWidth-2*76,65);
	DebriefElements.ShowFrame = BuildAmerFrame(getX(Multi_Debrief.Show),getY(Multi_Debrief.Show),getWidth(Multi_Debrief.Show),getHeight(Multi_Debrief.Show)-10);
	DebriefElements.MenuFrame = BuildAmerFrame(getX(Multi_Debrief.Menu),getY(Multi_Debrief.Menu),getWidth(Multi_Debrief.Menu),getHeight(Multi_Debrief.Menu));
	DebriefElements.GraphsFrame = BuildAmerFrame(getX(Multi_Debrief.Graphs),getY(Multi_Debrief.Graphs),getWidth(Multi_Debrief.Graphs),getHeight(Multi_Debrief.Graphs) );

	DebriefElements.TitleLClip = getElementEX(
		FG,
		anchorLT,
		XYWH(
			0,
			40,
			85,
			21
		),
		true,
		{
			texture = lTClip;
		}
	);

	DebriefElements.TitleRClip = getElementEX(
		FG,
		anchorLT,
		XYWH(
			ScrWidth-87,
			40,
			88,
			21
		),
		true,
		{
			texture = rTClip;
		}
	);

	DebriefElements.ShowLClip1 = getElementEX(
		FG,
		anchorLT,
		XYWH(
			0,
			DebriefElements.ShowFrame.y+30,
			40,
			22
		),
		true,
		{
			texture = lClip;
		}
	);

	DebriefElements.ShowLClip2 = getElementEX(
		FG,
		anchorLT,
		XYWH(
			0,
			DebriefElements.ShowFrame.y+DebriefElements.ShowFrame.h-30-20,
			40,
			22
		),
		true,
		{
			texture = lClip;
		}
	);
	
	DebriefElements.MenuLClip1 = getElementEX(
		FG,
		anchorLT,
		XYWH(
			0,
			DebriefElements.MenuFrame.y+30,
			40,
			22
		),
		true,
		{
			texture = lClip;
		}
	);

	DebriefElements.MenuLClip2 = getElementEX(
		FG,
		anchorLT,
		XYWH(
			0,
			DebriefElements.MenuFrame.y+DebriefElements.MenuFrame.h-30-20,
			40,
			22
		),
		true,
		{
			texture = lClip;
		}
	);
	
	if DebriefElements.MenuFrame.h >= 400 then
	
		DebriefElements.MenuLClip3 = getElementEX(
			FG,
			anchorLT,
			XYWH(
				0,
				DebriefElements.MenuFrame.y+DebriefElements.MenuFrame.h/2-11-20,
				40,
				22
			),
			true,
			{
				texture = lClip;
			}
		);
	end;

	DebriefElements.MidleClip1 = getElementEX(
		FG,
		anchorLT,
		XYWH(
			DebriefElements.ShowFrame.x+DebriefElements.ShowFrame.w-10,
			DebriefElements.ShowFrame.y+DebriefElements.ShowFrame.h/2-11-20,
			40,
			50
		),
		true,
		{
			texture = dClip;
		}
	);
	
	DebriefElements.MidleClip2 = getElementEX(
		FG,
		anchorLT,
		XYWH(
			DebriefElements.MenuFrame.x+DebriefElements.MenuFrame.w-10,
			DebriefElements.MenuFrame.y+DebriefElements.MenuFrame.h/3-11-20,
			40,
			50
		),
		true,
		{
			texture = dClip;
		}
	);
	
	DebriefElements.MidleClip3 = getElementEX(
		FG,
		anchorLT,
		XYWH(
			DebriefElements.MenuFrame.x+DebriefElements.MenuFrame.w-10,
			DebriefElements.MenuFrame.y+DebriefElements.MenuFrame.h/3*2-11-20,
			40,
			50
		),
		true,
		{
			texture = dClip;
		}
	);
	
	local rCount = div((DebriefElements.GraphsFrame.h-90),200 )+1;
	
	if rCount < 2 then
		rCount = 2;
	end;
	DebriefElements.RightClip = {};
	for i=0, rCount do 
		DebriefElements.RightClip[i+1] = getElementEX(
		FG,
		anchorLT,
		XYWH(
			ScrWidth-41,
			DebriefElements.GraphsFrame.y+(DebriefElements.GraphsFrame.h-90)/rCount*i +30,
			42,
			22
		),
		true,
		{
			texture = rClip;
		}
	);
	end;

end;

function BuildAmerFrame(x,y,w,h)
	local frame =	{x=x,y=y,w=w,h=h};
	local FG = Multi_Debrief.Foreground;
	local SC_BL = "/SGUI/Amer/debrief/SC_BL.png";
	local SC_BR = "/SGUI/Amer/debrief/SC_BR.png";
	local SC_TL = "/SGUI/Amer/debrief/SC_TL.png";
	local SC_TR = "/SGUI/Amer/debrief/SC_TR.png";
	local SF_B = "/SGUI/Amer/debrief/SF_B.png";
	local SF_L = "/SGUI/Amer/debrief/SF_L.png";
	local SF_R = "/SGUI/Amer/debrief/SF_R.png";
	local SF_T = "/SGUI/Amer/debrief/SF_T.png";
	
	
	
	frame.T = getElementEX(
		FG,
		acnhorLT,
		XYWH(
			x-7+26,
			y-7,
			w-26-7,
			14
		
		),
		true,
		{
			texture = SF_T,
			tile = true
		}
	);

	frame.L = getElementEX(
		FG,
		acnhorLT,
		XYWH(
			x-7,
			y-7+26,
			15,
			h-26-11
		
		),
		true,
		{
			texture = SF_L,
			tile = true
		}
	);

	frame.R = getElementEX(
		FG,
		acnhorLT,
		XYWH(
			x+w-6,
			y-7+26,
			15,
			h-26-11
		
		),
		true,
		{
			texture = SF_R,
			tile = true
		}
	);

	
	frame.B = getElementEX(
		FG,
		acnhorLT,
		XYWH(
			x-7+26,
			y+h-9,
			w-26-7,
			14
		
		),
		true,
		{
			texture = SF_B,
			tile = true
		}
	);
	
	frame.cLT = getElementEX(
		FG,
		acnhorLT,
		XYWH(
			x-7,
			y-7,
			26,
			26
		
		),
		true,
		{
			texture = SC_TL,
		}
	);
	
	frame.cLB = getElementEX(
		FG,
		acnhorLT,
		XYWH(
			x-7,
			y+h-21,
			26,
			26
		
		),
		true,
		{
			texture = SC_BL,
		}
	);
	
		frame.cRT = getElementEX(
		FG,
		acnhorLT,
		XYWH(
			x+w-17,
			y-7,
			26,
			26
		
		),
		true,
		{
			texture = SC_TR,
		}
	);
	
		frame.cRB = getElementEX(
		FG,
		acnhorLT,
		XYWH(
			x+w-17,
			y+h-21,
			26,
			26
		
		),
		true,
		{
			texture = SC_BR,
		}
	);

	return frame;
end;


--------------------------------------------- Russian Debrief -----------------------------------
function BuildRussianDebrief(won)
	local LTC = "/SGUI/Rus/debrief/LTC.png";
	local TB = "/SGUI/Rus/debrief/TB.png";
	local TBRez = "/SGUI/Rus/debrief/TBRez.png";
	local RTC = "/SGUI/Rus/debrief/RTC.png";
	local MB = "/SGUI/Rus/debrief/MB.png";
	local MBRez = "/SGUI/Rus/debrief/MBRez.png";
	local MVB = "/SGUI/Rus/debrief/MVB.png";
	local MVBRez = "/SGUI/Rus/debrief/MVBRez.png";
	local Part = "/SGUI/Rus/debrief/Part.png";
	local LB = "/SGUI/Rus/debrief/LB.png";
	local RB = "/SGUI/Rus/debrief/RB.png";
	local LBC = "/SGUI/Rus/debrief/LBC.png";
	local BB = "/SGUI/Rus/debrief/BB.png";
	local BBRez = "/SGUI/Rus/debrief/BBRez.png";
	local RBC = "/SGUI/Rus/debrief/RBC.png";


	local BG = Multi_Debrief.Background;
	local FG = Multi_Debrief.Foreground;
	
	local DebriefElements = {};


	DebriefElements.TB = getElementEX(
		FG,
		anchorLT,
		XYWH(
			0,
			-17,
			ScrWidth,
			36
		),
		true,
		{
			tile=true,
			texture = TB;
		}
	);
	
	DebriefElements.TBRez = getElementEX(
		FG,
		anchorLT,
		XYWH(
			ScrWidth/3*2-200,
			-17,
			328,
			38
		),
		true,
		{
		---	tile=true,
			texture = TBRez;
		}
	);
	
	DebriefElements.MVB = getElementEX(
		FG,
		anchorLT,
		XYWH(
			198,
			102,
			28,
			ScrHeight-102
		),
		true,
		{
			tile=true,
			texture = MVB;
		}
	);

	DebriefElements.MVBRez = getElementEX(
		FG,
		anchorLT,
		XYWH(
			198,
			(ScrHeight-102)/3+102+70,
			28,
			395
		),
		true,
		{
			texture = MVBRez,
		}
	);


	DebriefElements.Part = getElementEX(
		FG,
		anchorLT,
		XYWH(
			198-191+28,
			102+68,
			191,
			255
		),
		true,
		{
			texture = Part,
		}
	);

	DebriefElements.MB = getElementEX(
		FG,
		anchorLT,
		XYWH(
			0,
			62,
			ScrWidth,
			41
		),
		true,
		{
			tile=true,
			texture = MB;
		}
	);
	
	DebriefElements.MBRez = getElementEX(
		FG,
		anchorLT,
		XYWH(
			ScrWidth/3,
			62,
			884,
			38
		),
		true,
		{
		---	tile=true,
			texture = MBRez;
		}
	);
	
	DebriefElements.LB = getElementEX(
		FG,
		anchorLT,
		XYWH(
			0,
			0,
			34,
			ScrHeight
		),
		true,
		{
			tile=true,
			texture = LB,
		}
	);

	DebriefElements.LBRez = getElementEX(
		FG,
		anchorLT,
		XYWH(
			5,
			ScrHeight/3-70,
			28,
			395
		),
		true,
		{
			colour1 = WHITEA(150),
			texture = MVBRez,
		}
	);


	DebriefElements.RB = getElementEX(
		FG,
		anchorLT,
		XYWH(
			ScrWidth-39,
			0,
			39,
			ScrHeight
		),
		true,
		{
			tile=true,
			texture = RB,
		}
	);

	DebriefElements.BB = getElementEX(
		FG,
		anchorLT,
		XYWH(
			0,
			ScrHeight-23,
			ScrWidth,
			23
		),
		true,
		{
			tile=true,
			texture = BB,
		}
	);


	DebriefElements.BBRez = getElementEX(
		FG,
		anchorLT,
		XYWH(
			ScrWidth/3-50,
			ScrHeight-23,
			328,
			38
		),
		true,
		{
		---	tile=true,
			texture = TBRez;
		}
	);


	DebriefElements.LTC = getElementEX(
		FG,
		anchorLT,
		XYWH(
			-1,
			-17,
			417,
			282
		),
		true,
		{
			texture = LTC;
		}
	);
	
	DebriefElements.RTC = getElementEX(
		FG,
		anchorLT,
		XYWH(
			ScrWidth-197,
			-17,
			197,
			281
		),
		true,
		{
			texture = RTC;
		}
	);

	DebriefElements.LBC = getElementEX(
		FG,
		anchorLT,
		XYWH(
			0,
			ScrHeight-205,
			133,
			205
		),
		true,
		{
			texture = LBC;
		}
	);
	
	DebriefElements.RBC = getElementEX(
		FG,
		anchorLT,
		XYWH(
			ScrWidth-157,
			ScrHeight-237,
			157,
			237
		),
		true,
		{
			texture = RBC;
		}
	);

	return DebriefElements;
end;

