-----------------------------------------------------------------------------
---  Orig. File : /lua/multiroom_changmap.lua
---  Version    : 9
---
---  Summary    : Change and view maps Management.
---  
---  Created    : Petr 'Sali' Salak, Freya Group
------------------------------------------------------------------------------
--[[
mapLockedByAchiev = {
--	["ALIEN BASE"] = 'ACH_RAD',
--	["BABYLON+KILL 'EM ALL"] = 'ACH_PAC',
--	["BABYLON+MASTER OF THE ISLAND"] = 'ACH_RAD',
};

ChangeMapWindow = AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Window,
	anchor={top=true,bottom=true,left=true,right=true},
	visible=false,
	x=0,
	y=0,
	width=ScrWidth,
	height=ScrHeight,
	colour1=BLACKA(153),
});

ChangeMapWindow.ChangeMap= AddElement({
	type=TYPE_ELEMENT,
	parent=ChangeMapWindow,
	anchor={top=true,bottom=false,left=true,right=false},
	x=ofset_rw.x,
	y=ofset_rw.y,
	width=MpScrW,
	height=MpScrH,
	colour1=BLACKA(0),
	--colour1=WHITEA(153),
});

ChangeMapWindow.ChangeMap.BackGround= AddElement({
	type=TYPE_ELEMENT,
	parent=ChangeMapWindow.ChangeMap,
	anchor={top=true,bottom=true,left=true,right=true},
	x=30,
	y=30,
	width=ChangeMapWindow.ChangeMap.width-60,
	height=ChangeMapWindow.ChangeMap.height-60,
	--colour1=BLACKA(0),
	colour1=WHITEA(60),
});

local scale = ChangeMapWindow.ChangeMap.width/(1920-2*ofset_rw.x)

if ChangeMapWindow.ChangeMap.height/2 < 330*scale then
	scale = scale*((1/(330*scale))*(ChangeMapWindow.ChangeMap.height/2-80))
end;
------------------------------  Top ----------------------------------------
ChangeMapWindow.ChangeMap.MapName = AddElement({
	type=TYPE_LABEL,
	parent=ChangeMapWindow.ChangeMap,
	anchor={top=true,bottom=false,left=true,right=true},
	x=40,
	y=40,
	width=(500*scale-30),
	height=30,
	colour1={red=0,green=0,blue=0,alpha=0},
	text='',
	nomouseevent=true,
	wordwrap=false,
	font_colour=WHITEA(255),
	font_name=Tahoma_20B ,
	scissor=true
});

ChangeMapWindow.ChangeMap.MapImage = AddElement({
	type=TYPE_ELEMENT,
	parent=ChangeMapWindow.ChangeMap,
	anchor=anchorLT,
	x=40,
	y=80,
	width=500*scale,
	height=330*scale,
	texture= 'skirmish_unknown.png',
	texture_fallback='skirmish_unknown.png',
});

ChangeMapWindow.ChangeMap.GTName = AddElement({
	type=TYPE_LABEL,
	parent=ChangeMapWindow.ChangeMap,
	anchor={top=true,bottom=false,left=true,right=true},
	x=40+500*scale+20,
	y=40,
	width=((ChangeMapWindow.ChangeMap.width/3)-30),
	height=30,
	colour1={red=0,green=0,blue=0,alpha=0},
	text='',
	nomouseevent=true,
	wordwrap=false,
	font_colour=WHITEA(255),
	font_name=Tahoma_20B ,
	scissor=true
});


ChangeMapWindow.RulesBack= AddElement({
	type=TYPE_ELEMENT,
	parent=ChangeMapWindow.ChangeMap,
	anchor={top=true,bottom=true,left=true,right=true},
	x=40+500*scale+20,
	y=40+40,
	width=((ChangeMapWindow.ChangeMap.width/3)-30-13),
	height=330*scale,
	colour1={red=50,green=50,blue=50,alpha=180},
	bevel=true,
});

ChangeMapWindow.Rules_scrollbox_scrollV = AddElement({
	type=TYPE_SCROLLBAR,
	parent=ChangeMapWindow.ChangeMap,
	anchor={top=true,bottom=true,left=false,right=true},
	x=ChangeMapWindow.RulesBack.x+ChangeMapWindow.RulesBack.width+1,
	y=ChangeMapWindow.RulesBack.y+1,
	width=12,
	height=ChangeMapWindow.RulesBack.height-2,
	colour1=Scrollbar_Colour1,
	colour2=Scrollbar_Colour2,
	texture2="scrollbar.png",
});

ChangeMapWindow.ChangeMap.Rules = AddElement({
	type=TYPE_TEXTBOX,
	parent=ChangeMapWindow.ChangeMap,
	x=ChangeMapWindow.RulesBack.x+5,
	y=ChangeMapWindow.RulesBack.y+5,
	width=ChangeMapWindow.RulesBack.width-10,--Multi_Room.GameParameters.width-10,
    height=ChangeMapWindow.RulesBack.height-10,
    text='';
	nomouseevent=true,
	wordwrap=true,
	text_halign=ALIGN_LEFT,
	font_name=Tahoma_14 ,
	font_colour=WHITEA(255),
});

sgui_set(ChangeMapWindow.ChangeMap.Rules.ID,PROP_SCROLLBAR,ChangeMapWindow.Rules_scrollbox_scrollV.ID);  


ChangeMapWindow.ChangeMap.Chat = AddElement({
	type=TYPE_ELEMENT,
	parent=ChangeMapWindow.ChangeMap,
	anchor={top=true,bottom=true,left=true,right=true},
	x=40+((ChangeMapWindow.ChangeMap.width/3)-30+500*scale+20),
	y=40+40,
	width=ChangeMapWindow.ChangeMap.width-(40+((ChangeMapWindow.ChangeMap.width/3)+40+500*scale+40)-45),
	height=330*scale,
	colour1=BLACKA(153),
});

ChangeMapWindow.ChangeMap.Chat.Room = AddElement({
	type=TYPE_ELEMENT,
	parent=ChangeMapWindow.ChangeMap.Chat,
	anchor=anchorLTRB,
	x=0,
	y=0,
	width=294,
	height=340,
	colour1=BLACKA(0),
});

ChangeMapWindow.ChangeMap.Chat.BG = AddElement ({
	type=TYPE_ELEMENT,
	parent=ChangeMapWindow.ChangeMap,
	anchor=anchorLT,
	x=ChangeMapWindow.ChangeMap.Chat.x + 0,
	y=ChangeMapWindow.ChangeMap.Chat.y -25,
	width=169,
	height=25,
	colour1=BLACKA(153),
});

ChangeMapWindow.ChangeMap.Chat.Button = AddElement ({
	type=TYPE_LABEL,
	parent=ChangeMapWindow.ChangeMap,
	anchor=anchorLT,
	x=ChangeMapWindow.ChangeMap.Chat.x + 0,
	y=ChangeMapWindow.ChangeMap.Chat.y - 22,
	width=169,
	height=20,
	colour1=BLACKA(0),
	text=loc(TID_Multi_button_chat),--'Chat',
	wordwrap=false,
	font_name=Tahoma_13B ,
	font_colour=WHITEA(255),
	text_halign=ALIGN_MIDDLE,
	--callback_mouseclick='showRoomChat();',
	--texture='SGUI/Alien/Icons/chatico.png',

});

ChangeMapWindow.ChangeMap.Chat.Line = AddElement({
	type=TYPE_ELEMENT,
	parent=ChangeMapWindow.ChangeMap.Chat.Room,
   	anchor=anchorLT,
	x=169,
	y=1,
	width=ChangeMapWindow.ChangeMap.Chat.width-169,
	height=2,
	colour1=WHITEA(220),
});

-- duplicate text from multi room chat box
ChangeMapWindow.ChangeMap.Chat.TextBox = AddElement({
	type=TYPE_TEXTBOX,
	parent=ChangeMapWindow.ChangeMap.Chat.Room,
	anchor={top=false,bottom=true,left=true,right=true},
	x=2,
	y=2,
	width=ChangeMapWindow.ChangeMap.Chat.width-4-13,
	height=ChangeMapWindow.ChangeMap.Chat.height-4-20,
	colour1=BLACKA(200);
	text='',
	font_name=Tahoma_12B ,
	wordwrap=true;
});

ChangeMapWindow.ChangeMap.Chat.TEXTBOX_scrollV = AddElement({
	type=TYPE_SCROLLBAR,
	parent=ChangeMapWindow.ChangeMap.Chat.Room,
	anchor={top=true,bottom=true,left=false,right=true},
	x=ChangeMapWindow.ChangeMap.Chat.TextBox.x+ChangeMapWindow.ChangeMap.Chat.TextBox.width+1,
	y=ChangeMapWindow.ChangeMap.Chat.TextBox.y,
	width=12,
	height=ChangeMapWindow.ChangeMap.Chat.TextBox.height,
	colour1=Scrollbar_Colour1,
	colour2=Scrollbar_Colour2,
	texture2="scrollbar.png",
});

sgui_set(ChangeMapWindow.ChangeMap.Chat.TextBox.ID,PROP_SCROLLBAR,ChangeMapWindow.ChangeMap.Chat.TEXTBOX_scrollV.ID);


ChangeMapWindow.ChangeMap.Chat.TextBoxEdit = AddElement({
	type=TYPE_EDIT,
	parent=ChangeMapWindow.ChangeMap.Chat.Room,
	anchor={top=false,bottom=true,left=true,right=true},
	x=2,
	y=elementBottom(ChangeMapWindow.ChangeMap.Chat.TextBox)+2,
	width=ChangeMapWindow.ChangeMap.Chat.width-4,
	height=20,
	colour1=BLACKA(200);
	font_name=Tahoma_12B ,
	text='',
	callback_keyup='ChangeMapWindow.ChangeMap.Chat.TextBoxEdit_keyup(%k);',
});

function ChangeMapWindow.ChangeMap.Chat.TextBoxEdit_keyup(key)
	if key == 13 then
		OW_MULTI_SENDALLCHATMSG( getText(ChangeMapWindow.ChangeMap.Chat.TextBoxEdit), getHexColour(SIDE_COLOURS[Players[MyID].Colour+1])); 
		--OW_MULTI_SENDALLCHATMSG(getHexColour(SIDE_COLOURS[Players[MyID].Colour+1])..getText(ChangeMapWindow.ChangeMap.Chat.TextBoxEdit) .. getHexColour(nil));
		setText(ChangeMapWindow.ChangeMap.Chat.TextBoxEdit,'');
	--	syncChat();
	end;
end;

-------------------------- Middle -----------------------------
local scaleV = (ChangeMapWindow.ChangeMap.height-2*ofset_rw.y-ChangeMapWindow.ChangeMap.MapImage.height-120-70-60)/3 /145;

if ChangeMapWindow.ChangeMap.width < (4*200*scaleV+40) then
	scaleV = scaleV*((1/(ChangeMapWindow.ChangeMap.width/2-80))*(200*scaleV))
end;

ChangeMapWindow.ChangeMap.selectionMap = AddElement({
	type=TYPE_LABEL,
	parent=ChangeMapWindow.ChangeMap,
	anchor={top=true,bottom=false,left=true,right=true},
	x=40,
	y=40+330*scale + 40,
	width=((ChangeMapWindow.ChangeMap.width/3)-30),
	height=30,
	colour1={red=0,green=0,blue=0,alpha=0},
	text=loc(TID_Multi_label_MapName); 
	nomouseevent=true,
	wordwrap=true,
	font_colour=WHITEA(255),
	font_name=Tahoma_20B ,
});

ChangeMapWindow.multiplayer_map_scrollbox = AddElement({
	type=TYPE_SCROLLBOX,
	parent=ChangeMapWindow.ChangeMap,
	anchor={top=true,bottom=true,left=true,right=true},
	x=40,
	y=40+330*scale + 40+30,
	width=(ChangeMapWindow.ChangeMap.width)-80,
	height=145*scaleV*2,--ChangeMapWindow.ChangeMap.height-(40+330*scale + 20) -145*scale-3*70,
	colour1={red=0,green=0,blue=0,alpha=180},
	bevel=true,
});

ChangeMapWindow.multiplayer_map_scrollbox_scrollV = AddElement({
	type=TYPE_SCROLLBAR,
	parent=ChangeMapWindow.ChangeMap,
	anchor={top=true,bottom=true,left=false,right=true},
	x=ChangeMapWindow.multiplayer_map_scrollbox.x,
	y=ChangeMapWindow.multiplayer_map_scrollbox.y+ChangeMapWindow.multiplayer_map_scrollbox.height+1,
	width=ChangeMapWindow.multiplayer_map_scrollbox.width,
	height=12,
	colour1=Scrollbar_Colour1,
	colour2=Scrollbar_Colour2,
	texture2="scrollbar.png",
	vertical=false,
});

sgui_set(ChangeMapWindow.multiplayer_map_scrollbox.ID,PROP_SCROLLBAR2,ChangeMapWindow.multiplayer_map_scrollbox_scrollV.ID);

-------------------------- Bottom -----------------------------
ChangeMapWindow.ChangeMap.selectionMap = AddElement({
	type=TYPE_LABEL,
	parent=ChangeMapWindow.ChangeMap,
	anchor={top=true,bottom=false,left=true,right=true},
	x=40,
	y=40+330*scale + 40 + ChangeMapWindow.multiplayer_map_scrollbox.height+45,
	width=((ChangeMapWindow.ChangeMap.width/3)-30),
	height=30,
	colour1={red=0,green=0,blue=0,alpha=0},
	text=loc(TID_Multi_label_GameName); 
	nomouseevent=true,
	wordwrap=true,
	font_colour=WHITEA(255),
	font_name=Tahoma_20B ,
});

ChangeMapWindow.multiplayer_gametype_scrollbox = AddElement({
	type=TYPE_SCROLLBOX,
	parent=ChangeMapWindow.ChangeMap,
	anchor={top=true,bottom=true,left=true,right=true},
	x=40,
	y=40+330*scale + 40 + ChangeMapWindow.multiplayer_map_scrollbox.height+40+40,
	width=(ChangeMapWindow.ChangeMap.width)-80,
	height=145*scaleV,
	colour1={red=0,green=0,blue=0,alpha=180},
	bevel=true,
});

ChangeMapWindow.multiplayer_gametype_scrollbox_scrollV = AddElement({
	type=TYPE_SCROLLBAR,
	parent=ChangeMapWindow.ChangeMap,
	anchor={top=true,bottom=true,left=false,right=true},
	x=ChangeMapWindow.multiplayer_gametype_scrollbox.x,
	y=ChangeMapWindow.multiplayer_gametype_scrollbox.y+ChangeMapWindow.multiplayer_gametype_scrollbox.height+1,
	width=ChangeMapWindow.multiplayer_gametype_scrollbox.width,
	height=12,
	colour1=Scrollbar_Colour1,
	colour2=Scrollbar_Colour2,
	texture2="scrollbar.png",
	vertical=false,
});

sgui_set(ChangeMapWindow.multiplayer_gametype_scrollbox.ID,PROP_SCROLLBAR2,ChangeMapWindow.multiplayer_gametype_scrollbox_scrollV.ID);

-------------------------- Buttons ----------------------------
ChangeMapWindow.ButtonOK = makeGradButton({
	parent=ChangeMapWindow.ChangeMap,
	anchor=anchorLT,
	x=ChangeMapWindow.ChangeMap.width-300,
	y=ChangeMapWindow.ChangeMap.height-40-30,
	width=180,
	height=30,
	text=loc(TID_Main_Menu_Options_OK),--'OK', 
	font_name=Tahoma_12B ,
	text_case=CASE_UPPER,
	--disabled=true,
	callback_mouseclick='hideChangeMap(true);',
},GradButton_Green);

ChangeMapWindow.ButtonStorno = makeGradButton({
	parent=ChangeMapWindow.ChangeMap,
	anchor=anchorLT,
	x=ChangeMapWindow.ChangeMap.width-300-180-20,
	y=ChangeMapWindow.ChangeMap.height-40-30,
	width=180,
	height=30,
	text=loc(TID_Multi_button_BackButton),--'Storno', 
	font_name=Tahoma_12B ,
	text_case=CASE_UPPER,
	--disabled=true,
	callback_mouseclick='hideChangeMap(false);',
},GradButton_Red);

------------------------------ functions -----------------------------------
MPMapsList={};

function MPmaps_listbox_add(INFO,Index)
	--local scale = getWidth(ChangeMapWindow.multiplayer_map_scrollbox) / 600;
	local scale = getHeight(ChangeMapWindow.multiplayer_map_scrollbox) / 290;
	MPMapsList[Index] = INFO;

	local rowcolour1={red=0,green=0,blue=0,alpha=127};
	local rowcolour2={red=50,green=50,blue=50,alpha=127};
	local colourhover={red=150,green=200,blue=150,alpha=255};

	local fcolourover={red=255,green=255,blue=255,alpha=255};
	local fcolourleave={red=150,green=200,blue=150,alpha=255};

--	if odd(Index) then
--		colour=rowcolour1;
--	else
--		colour=rowcolour2;
--	end;

	local Row = AddElement({
		type=TYPE_ELEMENT,
		parent=ChangeMapWindow.multiplayer_map_scrollbox,
		anchor={top=true,bottom=false,left=true,right=true},
		x=(Index-1)*(200*scale)+1,
		y=0,
		width=200*scale, -- Since we get this at runtime after the resolution has changed, we need to get the current width
		height=ChangeMapWindow.multiplayer_map_scrollbox.height,
		colour1=WHITEA(0), --colour,
		callback_mouseclick='multiplayer_map_select('..Index..')',
--		texture='%missions%/\_multiplayer/'..INFO.NAME..'/mp_mapbar.png',
		--texture_fallback='SGUI/Alien/mp_unknow.png'
		--texture_fallback='gray.png',
	});
	
	Row.bar = getElementEX(
			Row,
		anchorLT,
		XYWH(
			0,
			0,
			200*scale,
			(160)*scale
		),
		true,
		{
			texture='%missions%/\_multiplayer/'..INFO.NAME..'/mp_mapbar.png',
			texture_fallback='gray.png',
			nomouseevent=true
		}
	);


	Row.mark = getElementEX(
		Row,
		anchorLT,
		XYWH(
			Row.width-30*scale-1,
			Row.height/2-15*scale,
			30*scale,
			30*scale
		),
		true,
		{
			texture='%missions%/\_multiplayer/'..INFO.NAME..'/mark.png',
			texture_fallback='empty.png',
			nomouseevent=true
		}
	);

	Row.mark2 = getElementEX(
		Row,
		anchorLT,
		XYWH(
			Row.width-60*scale-2,
			Row.height/2-15*scale,
			30*scale,
			30*scale
		),
		true,
		{
			texture='%missions%/\_multiplayer/'..INFO.NAME..'/mark2.png',
			texture_fallback='empty.png',
			nomouseevent=true
		}
	);
	
	--[[AddElement({
		type=TYPE_ELEMENT,
		parent=Row,
		anchor={top=true,bottom=false,left=true,right=false},
		x=0,
		y=0,
		width=250,
		height=165,
		colour1={red=255,green=255,blue=255,alpha=200},
		texture='%missions%/_multiplayer/'..INFO.NAME..'/mappic.png',
		texture_fallback='skirmish_unknown.png',
		nomouseevent=true,
	});--]]
	Row.map = getElementEX(
		Row,
		anchorLT,
		XYWH(
			0,
			(290-132)*scale,
			200*scale,
			132*scale
		),
		true,
		{
			texture='%missions%/\_multiplayer/'..INFO.NAME..'/mappic.png',
			texture_fallback='skirmish_unknown.png',
			nomouseevent=true
		}
	);
	
	local Label = AddElement({
		type=TYPE_LABEL,
		parent=Row,
		anchor={top=true,bottom=false,left=true,right=true},
		x=0,
		y=10*scale,
		width=Row.width,
		height=135*scale,
		colour1={red=0,green=0,blue=0,alpha=0},
		text=trim(INFO.NAMELOC),
		nomouseevent=true,
		font_name='Tahoma_70_Fixed_16_2.DFF;'..0.60*scale..';0.14;0.5;1',--Tahoma_14B,
		--wordwrap=true,
		font_colour={red=150,green=200,blue=150,alpha=255},
		shadowtext=true,
		outline=true,
		wordwrap=true,
		text_halign=ALIGN_MIDDLE,
		text_valign=ALIGN_TOP,
	});


	set_Callback(Row.ID,CALLBACK_MOUSEOVER,'setColour1ID('..Row.bar.ID..','..colourToString(colourhover)..');setFontColourID('..Label.ID..','..colourToString(fcolourover)..');setColour1ID('..Row.map.ID..','..colourToString(colourhover)..')');
	set_Callback(Row.ID,CALLBACK_MOUSELEAVE,'setColour1ID('..Row.bar.ID..','..colourToString(WHITEA(255))..');setFontColourID('..Label.ID..','..colourToString(fcolourleave)..');setColour1ID('..Row.map.ID..','..colourToString(WHITEA(255))..')');
	
	local name = string.upper(INFO.NAME);
	if mapLockedByAchiev[name] then
		local achieved = checkAchieved(mapLockedByAchiev[name]);
		if not achieved == true and achievements[mapLockedByAchiev[name]] then
			setFontColourID(Label.ID,RGBA(200,50,50,255))
			set_Callback(Row.ID, CALLBACK_MOUSECLICK, '');
			setColour1ID(Row.bar.ID,RGBA(200,150,150,255));
			setColour1ID(Row.map.ID,RGBA(200,150,150,255));
			setHint(Row, loc_format(TID_LockedByAchiev, {achievements[mapLockedByAchiev[name]][1]}));
			set_Callback(Row.ID,CALLBACK_MOUSEOVER,'setColour1ID('..Row.bar.ID..','..colourToString(RGBA(100,50,50,255))..');setColour1ID('..Row.map.ID..','..colourToString(RGBA(100,50,50,255))..');setFontColourID('..Label.ID..','..colourToString(fcolourover)..');');
			set_Callback(Row.ID,CALLBACK_MOUSELEAVE,'setColour1ID('..Row.bar.ID..','..colourToString(RGBA(200,150,150,255))..');setColour1ID('..Row.map.ID..','..colourToString(RGBA(200,150,150,255))..');setFontColourID('..Label.ID..','..colourToString(RGBA(200,50,50,255))..');');
		end;
	end;
end;

MPGTList={};

function MPGT_listbox_add(INFO,Index)
	--local scale = getWidth(ChangeMapWindow.multiplayer_gametype_scrollbox) / 600;
	local scale = getHeight(ChangeMapWindow.multiplayer_gametype_scrollbox)/145;
	MPGTList[Index] = INFO;

	local rowcolour1={red=0,green=0,blue=0,alpha=127};
	local rowcolour2={red=50,green=50,blue=50,alpha=27};
	local colourhover={red=150,green=200,blue=150,alpha=255};

	local fcolourover={red=255,green=255,blue=255,alpha=255};
	local fcolourleave={red=150,green=200,blue=150,alpha=255};

	local colour = WHITEA(255);
--	if odd(Index) then
--		colour=rowcolour1;
--	else
--		colour=rowcolour2;
--	end;

	local Row = AddElement({
		type=TYPE_ELEMENT,
		parent=ChangeMapWindow.multiplayer_gametype_scrollbox,
		anchor={top=true,bottom=false,left=true,right=true},
		x=(Index-1)*200*scale+1,
		y=0,
		width=200*scale, -- Since we get this at runtime after the resolution has changed, we need to get the current width
		height=145*scale,
		colour1=colour,
		callback_mouseclick='multiplayer_gt_select('..Index..')',
		texture='SGUI/Alien/GameTypes/'.. INFO.NAME.. '.png',
		--texture_fallback='SGUI/Alien/mpgt_unknow.png'
		texture_fallback='gray.png',
	});

	Row.mark = getElementEX(
		Row,
		anchorLT,
		XYWH(
			Row.width-30*scale-1,
			Row.height/2-15*scale,
			30*scale,
			30*scale
		),
		true,
		{
			texture='%missions%/\_multiplayer/'..changedMap.NAME..'/'.. INFO.NAME.. '_mark.png',
			texture_fallback='empty.png'
		}
	);
	
	local Label = AddElement({
		type=TYPE_LABEL,
		parent=Row,
		anchor={top=true,bottom=false,left=true,right=true},
		x=0,
		y=10*scale,
		width=Row.width,
		height=135*scale,
		colour1={red=0,green=0,blue=0,alpha=0},
		text=trim(INFO.NAMELOC),
		nomouseevent=true,
		font_name='Tahoma_70_Fixed_16_2.DFF;'..0.50*scale..';0.14;0.5;1',--Tahoma_14B,
		--wordwrap=true,
		font_colour={red=150,green=200,blue=150,alpha=255},
		shadowtext=true,
		outline=true,
		wordwrap=true,
		text_halign=ALIGN_MIDDLE,
		text_valign=ALIGN_TOP,
	});

	set_Callback(Row.ID,CALLBACK_MOUSEOVER,'setColour1ID('..Row.ID..','..colourToString(colourhover)..');setFontColourID('..Label.ID..','..colourToString(fcolourover)..');');
	set_Callback(Row.ID,CALLBACK_MOUSELEAVE,'setColour1ID('..Row.ID..','..colourToString(colour)..');setFontColourID('..Label.ID..','..colourToString(fcolourleave)..');');
	
	local name = string.upper(changedMap.NAME .. "+" .. INFO.NAME);
	if mapLockedByAchiev[name] then
		local achieved = checkAchieved(mapLockedByAchiev[name]);
		if not achieved == true and achievements[mapLockedByAchiev[name]] then
			setFontColourID(Label.ID,RGBA(200,50,50,255))
			set_Callback(Row.ID, CALLBACK_MOUSECLICK, '');
			setColour1ID(Row.ID,RGBA(200,150,150,255));
			setHint(Row, loc_format(TID_LockedByAchiev, {achievements[mapLockedByAchiev[name]][1]}));
			set_Callback(Row.ID,CALLBACK_MOUSEOVER,'setColour1ID('..Row.ID..','..colourToString(RGBA(100,50,50,255))..');setFontColourID('..Label.ID..','..colourToString(fcolourover)..');');
			set_Callback(Row.ID,CALLBACK_MOUSELEAVE,'setColour1ID('..Row.ID..','..colourToString(RGBA(200,150,150,255))..');setFontColourID('..Label.ID..','..colourToString(RGBA(200,50,50,255))..');');
		end;
	end;
end;


	changedMap =  {NAME='',NAMELOC=''};
	changedGT =  {NAME='',NAMELOC=''};
	cache_rules = {};
	ifWantSelectMapScreen = false;
	
function multiplayer_map_select(index)
ifWantSelectMapScreen=true;
	local map= MPMapsList[index];


	setTexture(ChangeMapWindow.ChangeMap.MapImage,'%missions%/\_multiplayer/'..map.NAME..'/mappic.png');
	setTextureFallback(ChangeMapWindow.ChangeMap.MapImage,'skirmish_unknown.png');
	setText(ChangeMapWindow.ChangeMap.MapName,map.NAMELOC);
	changedMap = map;
	ToOW_getGTList(map.NAME);
	--ToOW_Host_setMap(mapName,'');
	multiplayer_gt_select(1)
ifWantSelectMapScreen=false;
end;
--[[
function setInitInformationForCM(map)
	setTexture(ChangeMapWindow.ChangeMap.MapImage,'%missions%/\_multiplayer/'..map.MAP..'/mappic.tga');
	setTextureFallback(ChangeMapWindow.ChangeMap.MapImage,'skirmish_unknown.png')
	setText(ChangeMapWindow.ChangeMap.MapName,map.MAPLOC);
	setText(ChangeMapWindow.ChangeMap.GTName,map.GAMETYPELOC);

	changedMap ={NAME=map.MAP,NAMELOC=map.MAPLOC};
	changedGT = {NAME=map.GAMETYPE,NAMELOC=map.GAMETYPELOC};
	
	--callRefreshRules(changedMap.NAME,map.GAMETYPE);
	set_ChangeMap_setDesc(Multi_Room.MapInfo.Rules.text);
end; --]]

function multiplayer_gt_select(index)
	ifWantSelectMapScreen=true;
	local gt = MPGTList[index];
	setText(ChangeMapWindow.ChangeMap.GTName,gt.NAMELOC);

	changedGT = gt;
	callRefreshRules(changedMap.NAME,changedGT.NAME);
	ifWantSelectMapScreen=false;
end;

function callRefreshRules(map,gt)
	if cache_rules[map..gt] ~= nil then 
		set_ChangeMap_setDesc(cache_rules[map..gt]);
	else 
		ToOW_getMapInfo(map,gt);
	end;
end;

function set_ChangeMap_setDesc(desc)
	setText(ChangeMapWindow.ChangeMap.Rules,desc);
end;

function loadLoadedMap(map,gt)
	ifWantSelectMapScreen=true;
	setTexture(ChangeMapWindow.ChangeMap.MapImage,'%missions%/\_multiplayer/'..map.NAME..'/mappic.png');
	setTextureFallback(ChangeMapWindow.ChangeMap.MapImage,'skirmish_unknown.png')
	setText(ChangeMapWindow.ChangeMap.MapName,map.NAMELOC);
	ToOW_getGTList(map.NAME);
	setText(ChangeMapWindow.ChangeMap.GTName,gt.NAMELOC);
	callRefreshRules(map.NAME,gt.NAME);
	ifWantSelectMapScreen=false;
end;

function showChangeMap(forbidden)
	setVisible(ChangeMapWindow,true);
	--OW_ROOM_SETUP(ChangeMapWindow.ChangeMap.Chat.TextBox.ID,Multi_Room.Panel.Status.ID,Multi_Room.TeamList.ID);
	--ifWantSelectMapScreen = true;
	changedMap ={NAME=MultiDef.MapName.MAP,NAMELOC=MultiDef.MapName.MAPLOC};
	changedGT = {NAME=MultiDef.MapName.GAMETYPE,NAMELOC=MultiDef.MapName.GAMETYPELOC};
	loadLoadedMap(changedMap,changedGT);
	
	if forbidden then
		setVisible(ChangeMapWindow.ButtonStorno,false);
	else
		setVisible(ChangeMapWindow.ButtonStorno,true);
	end;
	
	if ifIsServer then
		setVisible(ChangeMapWindow.ButtonOK,true);
	else
		setVisible(ChangeMapWindow.ButtonOK,false);
	end;
end;

function hideChangeMap(bool)

	if bool then
--		local toCheck = false;
		
--		if mapLockedByAchiev[string.upper(changedMap.NAME)] then
--			toCheck = mapLockedByAchiev[string.upper(changedMap.NAME)];
--		elseif mapLockedByAchiev[string.upper(changedMap.NAME .. "+".. changedGT.NAME)] then
--			toCheck = mapLockedByAchiev[string.upper(changedMap.NAME .. "+".. changedGT.NAME)];
--		end;
		
--		if toCheck then
--			local achieved = checkAchieved(mapLockedByAchiev[toCheck]);			-- Caussing crash
--			if achieved == true or not achievements[mapLockedByAchiev[toCheck]] then
--				toCheck = true;
--			else
--				toCheck = false;
--			end;
--		else
--			toCheck = true;
--		end;
--		if toCheck == true then
			ToOW_Host_setMap(changedMap.NAME,changedGT.NAME);
			setVisible(ChangeMapWindow,false);
--		end;
	else 
		setTexture(ChangeMapWindow.ChangeMap.MapImage,'%missions%/\_multiplayer/'..changedMap.NAME..'/mappic.png');
		setTextureFallback(ChangeMapWindow.ChangeMap.MapImage,'skirmish_unknown.png')
		setText(ChangeMapWindow.ChangeMap.MapName,changedGT.NAME);
		setVisible(ChangeMapWindow,false);
	end;
	--OW_ROOM_SETUP(Multi_Room.Chat.TextBox.ID,Multi_Room.Panel.Status.ID,Multi_Room.TeamList.ID);

	--ifWantSelectMapScreen=false;
	
	--ToOW_getCurrentMapInfo();
end;

	taskOnGT = false;
	GTScrollElement = {};
	
function ChangeGameType()
	taskOnGT = true;
	
	sgui_deletechildren(Multi_Window.Clone.ID);
	
	setVisible(Multi_Window.Clone,true);
	local GTScrollBox = AddElement({
		type=TYPE_SCROLLBOX,
		parent=Multi_Window.Clone,
		anchor={top=true,bottom=false,left=true,right=false},
		x=SGUI_getabsx(Multi_Room.GameParameters.GameActions.TypeChange.ID)+Multi_Room.GameParameters.GameActions.TypeChange.width+2,
		y=SGUI_getabsy(Multi_Room.GameParameters.GameActions.TypeChange.ID),
		width=(ChangeMapWindow.ChangeMap.width/3)-45,
		height=ChangeMapWindow.ChangeMap.height-40-40-40-40,
		colour1={red=0,green=0,blue=0,alpha=180},
		bevel=true,
	});

	local GTScrollBoxV = AddElement({
		type=TYPE_SCROLLBAR,
		parent=Multi_Window.Clone,
		anchor={top=true,bottom=false,left=false,right=true},
		x=GTScrollBox.x+GTScrollBox.width+1,
		y=GTScrollBox.y,
		width=12,
		height=GTScrollBox.height-2,
		colour1=Scrollbar_Colour1,
		colour2=Scrollbar_Colour2,
		texture2="scrollbar.png",
	});
	
	sgui_set(GTScrollBox.ID,PROP_SCROLLBAR,GTScrollBoxV.ID);
	GTScrollElement = GTScrollBox;
	
	set_Callback(GTScrollElement.ID,CALLBACK_MOUSECLICK,'setVisibleID('..Multi_Window.Clone.ID..',false);');
	
	ToOW_getGTList(MultiDef.MapName.MAP);
	
	taskOnGT = false;
end;

function MPGT_listbox_add2(INFO,Index)
	MPGTList[Index] = INFO;

	local rowcolour1={red=0,green=0,blue=0,alpha=127};
	local rowcolour2={red=50,green=50,blue=50,alpha=27};
	local colourhover={red=150,green=200,blue=150,alpha=200};

	local fcolourover={red=255,green=255,blue=255,alpha=255};
	local fcolourleave={red=150,green=200,blue=150,alpha=255};

	if odd(Index) then
		colour=rowcolour1;
	else
		colour=rowcolour2;
	end;

	local Row = AddElement({
		type=TYPE_ELEMENT,
		parent=GTScrollElement,
		anchor={top=true,bottom=false,left=true,right=true},
		x=0,
		y=(Index-1)*30,
		width=getWidth(GTScrollElement), -- Since we get this at runtime after the resolution has changed, we need to get the current width
		height=30,
		colour1=colour,
		callback_mouseclick='multiplayer_gt_load('..Index..')',
	});

	local Label = AddElement({
		type=TYPE_LABEL,
		parent=Row,
		anchor={top=true,bottom=false,left=true,right=true},
		x=0,
		y=0,
		width=Row.width,
		height=30,
		colour1={red=0,green=0,blue=0,alpha=0},
		text=INFO.NAMELOC,
		nomouseevent=true,
		--wordwrap=true,
		font_colour={red=150,green=200,blue=150,alpha=255},
	});

	set_Callback(Row.ID,CALLBACK_MOUSEOVER,'setColour1ID('..Row.ID..','..colourToString(colourhover)..');setFontColourID('..Label.ID..','..colourToString(fcolourover)..');');
	set_Callback(Row.ID,CALLBACK_MOUSELEAVE,'setColour1ID('..Row.ID..','..colourToString(colour)..');setFontColourID('..Label.ID..','..colourToString(fcolourleave)..');');
	
	local name = string.upper(MultiDef.MapName.MAP .. "+" .. INFO.NAME);
	if mapLockedByAchiev[name] then
		local achieved = checkAchieved(mapLockedByAchiev[name]);
		if not achieved == true and achievements[mapLockedByAchiev[name]] then
			setFontColourID(Label.ID,RGBA(100,50,50,255))
			set_Callback(Row.ID, CALLBACK_MOUSECLICK, '');
			setHint(Row, loc_format(TID_LockedByAchiev, {achievements[mapLockedByAchiev[name]][1]}));
			set_Callback(Row.ID,CALLBACK_MOUSEOVER,'setColour1ID('..Row.ID..','..colourToString(RGBA(100,50,50,255))..');setFontColourID('..Label.ID..','..colourToString(fcolourover)..');');
			set_Callback(Row.ID,CALLBACK_MOUSELEAVE,'setColour1ID('..Row.ID..','..colourToString(colour)..');setFontColourID('..Label.ID..','..colourToString(RGBA(100,50,50,255))..');');
		end;
	end;
end;

function multiplayer_gt_load(Index)
	ToOW_Host_setMap(MultiDef.MapName.MAP,MPGTList[Index].NAME);
	setVisible(Multi_Window.Clone,false);
end;--]]