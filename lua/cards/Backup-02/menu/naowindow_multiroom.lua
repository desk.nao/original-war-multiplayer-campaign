-----------------------------------------------------------------------------
---  Orig. File : /lua/window_multiroom.lua
---  Version    : 54
---
---  Summary    : Multiplayer Room.
---
---  Created    : Petr 'Sali' Salak, Freya Group
---  Modified   : Stuart 'Stucuk' Carey, OW Support
------------------------------------------------------------------------------

----- MP Global value -----
	op_TechLimit=56;-- before not exit
	op_LockGame=57;--27;

	op_RndPosition=58;--28;  -- before random teams
	op_RndColour=59;--29;
	op_RndNation=60;--30;  -- before allower of merge sides
	op_LockTeams=55;-- before not exit
	op_Ranked=51;-- before not exit

	SHVFMP = { --Stupid Help Variable For Map Params  -  Because LUA does not know if in array
		[op_TechLimit] = true;
		[op_LockGame] = true;
		[op_RndPosition] = true;
		[op_RndColour] = true;
		[op_RndNation] = true;
		[op_LockTeams] = true;
		[op_Ranked] = true;
	};

	ifIsServer= false;
	ifIsClient= false;

	MultiDef = {};
	MultiDef.MapName = {};
	MultiDef.MapParams = {};
	MultiDef.MapParamCount = 0;
	MultiDef.MapList = {};
	MultiDef.MapListCount = 0;
	MultiDef.GameTypeList = {};
	MultiDef.GameTypeListCount = 0;
	MultiDef.MultiMap = {};
	MultiDef.TeamDef = {};
	MultiDef.SideDef = {};
	MultiDef.MultiMap.POSCOORS = nil;
	MultiDef.MultiMap.UNITED = false;
	MultiDef.MultiMap.MODIFEDNATIONS = {};
	MultiDef.MultiMap.Restrictions = {};
	Players = {};
	MyID = 0;
	APositions = {};
	AColours = {};
	firstPlayer = nil;
	teamGame = false;
	RestrictionLock = false;
	LockedTeams = false;
	include('Menu/multiroom_additionalMapInfo');
----- MAIN -----
ofset_rw = {x=0, y=0};
if ScrWidth > 3000 then
	MpScrW = 3000;
	ofset_rw.x = Int((ScrWidth-3000)/2);
elseif ScrWidth >= 2450 then
	ofset_rw.x = Int((ScrWidth-2450)/8);
	if ofset_rw.y > 30 then
		ofset_rw.y = 30;
	end;
	MpScrW = ScrWidth-(ofset_rw.x*2);
else
	MpScrW = ScrWidth;
end;

if ScrHeight > 1300 then
	ofset_rw.y = Int((ScrHeight - 1300)/8);
	if ofset_rw.y > 60 then
		ofset_rw.y = 60;
	end;
    MpScrH = (ScrHeight - (2*ofset_rw.y));
else
	MpScrH = ScrHeight;
end;

Multi_Window = AddElement({
	type=TYPE_ELEMENT,
	parent=menu,
	anchor={top=true,bottom=true,left=true,right=true},
	visible=false,
	x=0,
	y=0,
	width=ScrWidth,
	height=ScrHeight,
	colour1=BLACKA(0),
	-- colour1=BLACKA(153),   --test
});

Multi_Room = AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Window,
	anchor={top=true,bottom=true,left=true,right=true},
	visible=false,
	x=ofset_rw.x,
	y=ofset_rw.y,
	width=MpScrW,
	height=MpScrH,
	colour1=BLACKA(0),
	-- colour1=BLACKA(153),   --test
});

Multi_Window.Clone = AddElement({						---- For creating MP listboxes, warning: must be deleted children before visible
	type=TYPE_ELEMENT,
	parent=Multi_Window,
	anchor={top=true,bottom=true,left=true,right=true},
	visible=false,
	x=0,
	y=0,
	width=ScrWidth,
	height=ScrHeight,
	colour1=BLACKA(10),
	callback_mouseclick='setVisibleID(%id,false);';
	-- colour1=BLACKA(153),   --test
});
-----  Head -----
Multi_Room.Panel = AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Room,
	anchor=anchorLT,
	x=0,
	y=0,
	width=MpScrW-profilebar.width-10,
	height=70,
    colour1=BLACKA(0),
});

Multi_Room.Panel.BG = AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Room.Panel,
	anchor=anchorLT,
	x=200,
	y=5,
	width=Multi_Room.Panel.width-200,
	height=40,
	colour1=BLACKA(153),
});

include('Menu/multiroom_changemap');

Multi_Room.Panel.StartGame = makeGradButton({
	parent=Multi_Room.Panel,
	anchor=anchorLT,
	x=10,
	y=5,
	width=180,
	height=30,
	text=loc(TID_Multi_button_Start_Game),
	font_name=Tahoma_12B ,
	text_case=CASE_UPPER,
	--disabled=true,
	callback_mouseclick='startMPgame();',
},GradButton_Green);

Multi_Room.Panel.LeaveGame = makeGradButton({
	parent=Multi_Room.Panel,
	anchor=anchorLT,
	x=10,
	y=5+30+5,
	width=180,
	height=30,
	text=loc(TID_Multi_button_Leave_game),
	font_name=Tahoma_12B ,
	text_case=CASE_UPPER,
	--disabled=true,
	callback_mouseclick='leaveGameRoom();',
},GradButton_Red);

Multi_Room.Panel.MapTitle = AddElement ({
	type=TYPE_LABEL,
	parent=Multi_Room.Panel,
	anchor=anchorLT,
	x=200+10,
	y=10,
	width=Multi_Room.Panel.width-50-210-20,
	height=30,
	text=loc(TID_InGame_NoName) .. '-' .. loc(TID_InGame_NoName), --Game name + Game Type
	font_colour=WHITEA(255),--WHITEA(255),
	nomouseevent=true,
	text_halign=ALIGN_LEFT,
	font_name=Trebuchet_20 ,
	shadowtext=true,
});

Multi_Room.Panel.PlayersCount = AddElement ({
	type=TYPE_LABEL,
	parent=Multi_Room.Panel,
	anchor=anchorLT,
	x=Multi_Room.Panel.width-50,
	y=10,
	width=30,
	height=30,
	text= '0/0', -- Players / Max Players
	font_colour=WHITEA(255),--WHITEA(255),
	nomouseevent=true,
	text_halign=ALIGN_RIGHT,
	font_name=Trebuchet_20 ,
	shadowtext=true,
});

----- GameParameters -----
Multi_Room.GameParameters = AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Room,
	anchor=anchorLT,
	x=10,
	y=Multi_Room.Panel.height + 30,
	width=700,
	height=340,
	colour1=BLACKA(0),

});
					    -- MapPic --
Multi_Room.MapBox = AddElement ({
	type=TYPE_ELEMENT,
	parent=Multi_Room.GameParameters,
	anchor=anchorLT,
	x=0,
	y=0,
	width=510,
	height=340,
	colour1=BLACKA(153),

});

Multi_Room.MapBox.MapImage = AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Room.MapBox,
	anchor=anchorLT,
	x=5,
	y=5,
	width=500,
	height=330,
	texture= 'skirmish_unknown.png',
	texture_fallback='skirmish_unknown.png',
});

if MpScrW <= 2450 then
	Multi_Room.MapButBG = AddElement ({
		type=TYPE_ELEMENT,
		parent=Multi_Room,
		anchor=anchorLT,
		x=10,
		y=Multi_Room.Panel.height + 30-25,
		width=169,
		height=25,
		colour1=BLACKA(153),
	});

	Multi_Room.MapBut = AddElement ({
		type=TYPE_LABEL,
		parent=Multi_Room,
		anchor=anchorLT,
		x=10,
		y=Multi_Room.Panel.height+30-22,
		width=169,
		height=20,
		colour1=BLACKA(0),
		text=loc(TID_Multi_button_map), --'Map & Positions',
		wordwrap=false,
		font_name=Tahoma_13B ,
		font_colour=WHITEA(255),--WHITEA(255),
		text_halign=ALIGN_MIDDLE,
		callback_mouseclick='showMap();',
	});
end;

Multi_Room.MapBox.Line = AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Room.MapBox,
   	anchor=anchorLT,
	x=169,
	y=1,
	width=Multi_Room.MapBox.width-169,
	height=2,
	colour1=WHITEA(220),
});

						  --Etc --

Multi_Room.GameParameters.GameActions = AddElement ({
	type=TYPE_ELEMENT,
	parent=Multi_Room.GameParameters,
	anchor=anchorLT,
	x=510+5,
	y=0,
	width=700-510-5,
	height=340,
	colour1=BLACKA(153),

});

Multi_Room.GameParameters.GameActions.MapChange = makeGradButton({
	parent=Multi_Room.GameParameters.GameActions,
	anchor=anchorLT,
	x=5,
	y=5,
	width=175,--Multi_Room.GameParameters.width-10,
	height=30,
	text=loc(TID_Multi_button_changeMap),--'Change Map',
	font_name=Tahoma_12B ,
	text_case=CASE_UPPER,
	--disabled=true,
	callback_mouseclick='showChangeMap(false);',
	font_colour=RGB(220,220,220),
},GradButton_BlueStu);

Multi_Room.GameParameters.GameActions.TypeChange = makeGradButton({
	parent=Multi_Room.GameParameters.GameActions,
	anchor=anchorLT,
	x=5,
	y=5+30+5,
	width=175,--Multi_Room.GameParameters.width-10,
	height=30,
	text=loc(TID_Multi_button_changeGT),--'Change GameType',
	font_name=Tahoma_12B ,
	text_case=CASE_UPPER,
	--disabled=true,
	callback_mouseclick='ChangeGameType();',
	font_colour=RGB(220,220,220),
},GradButton_BlueStu);

Multi_Room.GameParameters.GameActions.LoadGame = makeGradButton({
	parent=Multi_Room.GameParameters.GameActions,
	anchor=anchorLT,
	x=5+32.5,
	y=5+30+5+240,
	width=110,--Multi_Room.GameParameters.width-10,
	height=40,
	text='SAVED GAME\n> PRESETS <',--'Change GameType',
	font_name=Tahoma_12B ,
	text_case=CASE_UPPER,
	--disabled=true,
	callback_mouseclick='CheckSavedGame()',
	font_colour=RGB(220,220,220),
},GradButton_BlueStu);

function CheckSavedGame()

MyIdentifier = OW_SETTING_READ_NUMBER("MAP_SAVED",'MyIdentifier');

ToOW_Host_setMap(OW_SETTING_READ_STRING("MAP_SAVED",'Map.name'),MultiDef.GameTypeList[OW_SETTING_READ_NUMBER("MAP_SAVED",'Map.gametype')-1].NAME);
AddSingleUseTimer(0.1,"ToOW_Host_setMap(OW_SETTING_READ_STRING(\"MAP_SAVED\",\'Map.name\'),MultiDef.GameTypeList[OW_SETTING_READ_NUMBER(\"MAP_SAVED\",\'Map.gametype\')-1].NAME)");

	--if OW_SETTING_READ_STRING("MAP_SAVED",'Player#'..MyIdentifier..'.name') == 'Nao' then 
		-- Colours
		AddSingleUseTimer(0.1,"ToOW_setMyColour(OW_SETTING_READ_NUMBER(\"MAP_SAVED\",\'Player#\'..MyIdentifier..\'.colour\'));");
		AddSingleUseTimer(0.1,"ToOW_setMySide(OW_SETTING_READ_NUMBER(\"MAP_SAVED\",\'Player#\'..MyIdentifier..\'.side\'));");		
		-- Nations
		if OW_SETTING_READ_NUMBER("MAP_SAVED",'Player#'..MyIdentifier..'.nation') == 2 then AddSingleUseTimer(0.3,"ToOW_setMyNation(3);"); end;
		if OW_SETTING_READ_NUMBER("MAP_SAVED",'Player#'..MyIdentifier..'.nation') == 3 then AddSingleUseTimer(0.3,"ToOW_setMyNation(2);"); end;
		if OW_SETTING_READ_NUMBER("MAP_SAVED",'Player#'..MyIdentifier..'.nation') == 1 then AddSingleUseTimer(0.3,"ToOW_setMyNation(1);"); end;
		
		AddSingleUseTimer(0.1,"OW_MULTIROOM_SET_MYTEAMANDPOS(OW_SETTING_READ_NUMBER(\"MAP_SAVED\",\'Player#\'..MyIdentifier..\'.team\'),OW_SETTING_READ_NUMBER(\"MAP_SAVED\",\'Player#\'..MyIdentifier..\'.position\'));");
	--end;
		for SideChecker = 1,8 do 
			if OW_SETTING_READ_STRING("MAP_SAVED",'Player#'..SideChecker..'.active') == 1 then
				if string.sub(OW_SETTING_READ_STRING("MAP_SAVED",'Player#'..SideChecker..'.name'),1,4) == 'COMP' then
				OW_MULTIROOM_HOST_ADDCOMPUTER(OW_SETTING_READ_NUMBER("MAP_SAVED",'Player#'..SideChecker..'.team'));
				end;
			end;
		end;
	
end;

Multi_Room.GameParameters.GameActions.Checkers = AddElement({
	type=TYPE_Element,
	parent=Multi_Room.GameParameters.GameActions,
	x=5,
	y=5+2*(30+5),
	width=175,
	height=7*(20+5);
	colour1=WHITEA(0),
});

AddElement({
	type=TYPE_LABEL,
	parent=Multi_Room.GameParameters.GameActions.Checkers,
	x=0,
	y=25,
	width=175,
	text=loc(TID_Multi_Random),
	font_colour=WHITEA(255),
});

function makeCustomChechboxElement(parent,name,hint,checked,lockable,locked,x,y,w,callback)
	local cb = {};
	cb.Lockable = lockable;
	cb.Locked = locked;
	cb.Checked = checked;

	local x2 = x;
	local vis = false;
	local fcolor = WHITEA(255);

	cb.bg = AddElement ({
		type=TYPE_ELEMENT,
		parent=parent,
		anchor=anchorLT,
		x=x,
		y=y,
		width=w,
		height=20,
		colour1=WHITEA(0),

	});
	x = 0;
	x2 = 0;
	y = 0;

	if cb.Lockable then
		x2 = x2+20;

		if cb.Locked then
			vis = true;
			fcolor = RGBA(200,200,200,255);
		end;
			cb.Locker = AddElement ({
				type=TYPE_ELEMENT,
				parent=cb.bg,
				anchor=anchorLT,
				x=x,
				y=y,
				width=20,--Multi_Room.GameParameters.width-10,
				height=20,
				colour1=WHITEA(255),
				texture = 'SGUI/Alien/multiplayer/player_lock.png',
				hint = loc(TID_Multi_Hint_OptionLock),
				visible = vis;
			});
	end;

	cb.Checkerbg = AddElement ({
		type=TYPE_ELEMENT,
		parent=cb.bg,
		anchor=anchorLT,
		x=x2,
		y=y,
		width=20,--Multi_Room.GameParameters.width-10,
		height=20,
		colour1=BLACKA(150),
		hint = hint;
		callback_mouseclick = callback .. ');';
	});

	if cb.Checked then
		vis = true;
	else
		vis = false;
	end;

	cb.Checker = AddElement ({
		type=TYPE_ELEMENT,
		parent=cb.bg,
		anchor=anchorLT,
		x=x2+3,
		y=y+3,
		width=14,--Multi_Room.GameParameters.width-10,
		height=14,
		colour1=WHITEA(150),
		hint = hint,
		nomouseevent = true,
		visible = vis,
	});

	if callback == 'ChangeTechLimit(' then
		cb.Query = AddElement ({
			type=TYPE_ELEMENT,
			parent=cb.bg,
			anchor=anchorLT,
			x=x2+3,
			y=y+3,
			width=14,--Multi_Room.GameParameters.width-10,
			height=14,
			colour1=WHITEA(150),
			hint = 'This map not support it, so we can`t know it.',
			--nomouseevent = true,
			visible = false,
			texture='SGUI/Alien/multiplayer/NationR.png',
		});
	end;

	cb.Name = AddElement({
		type=TYPE_LABEL,
		parent=cb.bg,
		x=x2+22,
		y=y,
		height = 20,
		width=w-x2-22,
		text=name,
		font_colour=fcolor,
		hint = hint,
		font_name=Tahoma_13 ,
		callback_mouseclick = callback .. ');';
		wordwrap = true,
	});

	return cb;
end;

Multi_Room.GameParameters.GameActions.LockRoom = makeCustomChechboxElement(
	Multi_Room.GameParameters.GameActions.Checkers,
	loc(TID_Multi_checkbox_Lock_game),
	loc(TID_Multi_Lock_game_Hint),
	false,
	false,
	false,
	0,0,175,
	'ChangeLock(');

Multi_Room.GameParameters.GameActions.Colours = makeCustomChechboxElement(
	Multi_Room.GameParameters.GameActions.Checkers,				-- Parent
	loc(TID_Multi_Random_Side_Colors2),							-- Name
	loc(TID_Multi_Random_Side_Colors_Hint),						-- Hint
	false,														-- Checked
	true,														-- Lockable
	false,														-- Locked
	10,25*2,165,													-- X,Y,W
	'ChangeRandColours(' );										-- Callback

Multi_Room.GameParameters.GameActions.Nats = makeCustomChechboxElement(
	Multi_Room.GameParameters.GameActions.Checkers,
	loc(TID_Multi_Random_Side_Nations),
	loc(TID_Multi_Random_Side_Nations_Hint),
	true,
	true,
	false,
	10,25*3,165,
	'ChangeRandNats(' );

Multi_Room.GameParameters.GameActions.Poss = makeCustomChechboxElement(
	Multi_Room.GameParameters.GameActions.Checkers,
	loc(TID_Multi_Random_Side_Positions2),
	loc(TID_Multi_Random_Side_Positions_Hint),
	false,
	true,
	false,
	10,25*4,165,
	'ChangeRandPoss(' );

Multi_Room.GameParameters.GameActions.TechLimit = makeCustomChechboxElement(
	Multi_Room.GameParameters.GameActions.Checkers,
	loc(TID_Multi_Checkbox_TechLimits),
	loc(TID_Multi_Checkbox_TechLimits_Hint),
	true,
	true,
	true,
	0,(25*5)+5,175,
	'ChangeTechLimit(' );

Multi_Room.GameParameters.GameActions.LockTeams = makeCustomChechboxElement(
	Multi_Room.GameParameters.GameActions.Checkers,
	loc(TID_Multi_LockTeam),
	loc(TID_Multi_LockTeam_Hint),
	false,
	true,
	false,
	0,(25*6)+5,175,
	'ChangeLockTeams(' );

--[[

Multi_Room.GameParameters.GameActions.LockRoom=makeCheckBox(merge({
	type=TYPE_CHECKBOX,
	parent=Multi_Room.GameParameters.GameActions,
	x=10,
	y=5+2*(30+5),
	width=20,
	height=20,
	callback_checked='setMapParam_checkbox('..op_LockGame..',%value)',
	checked=false,
	hint = loc(TID_Multi_Lock_game_Hint),
},checkbox_merge),loc(TID_Multi_checkbox_Lock_game));


Multi_Room.GameParameters.GameActions.Colours=makeCheckBox(merge({
	type=TYPE_CHECKBOX,
	parent=Multi_Room.GameParameters.GameActions,
	x=30,
	y=5+2*(30+5)+2*(20+5),
	width=20,
	height=20,
	callback_checked='setMapParam_checkbox('..op_RndColour..',%value)',
	checked=false,
	hint = loc(TID_Multi_Random_Side_Colors_Hint),
},checkbox_merge),loc(TID_Multi_Random_Side_Colors2));--'Colours');

Multi_Room.GameParameters.GameActions.Nats=makeCheckBox(merge({
	type=TYPE_CHECKBOX,
	parent=Multi_Room.GameParameters.GameActions,
	x=30,
	y=5+2*(30+5)+3*(20+5),
	width=20,
	height=20,
	callback_checked='setMapParam_checkbox(1,%value)',
	checked=false,
	disabled=true,
	--nomouseevent=true,
	hint=loc(TID_Multi_Random_Side_Nations_Hint),--After checked sides (playrers) can't select their nations.",
},checkbox_merge),loc(TID_Multi_Random_Side_Nations)); --'Nations');

Multi_Room.GameParameters.GameActions.Poss=makeCheckBox(merge({
	type=TYPE_CHECKBOX,
	parent=Multi_Room.GameParameters.GameActions,
	x=30,
	y=5+2*(30+5)+4*(20+5),
	width=20,
	height=20,
	callback_checked='setMapParam_checkbox('.. op_RndPositions..',%value)',
	checked=false,
	hint=loc(TID_Multi_Random_Side_Positions_Hint);
},checkbox_merge),loc(TID_Multi_Random_Side_Positions2) ); -- Psitions

Multi_Room.GameParameters.GameActions.Techs=makeCheckBox(merge({
	type=TYPE_CHECKBOX,
	parent=Multi_Room.GameParameters.GameActions,
	x=10,
	y=5+2*(30+5)+5*(20+5),
	width=20,
	height=20,
	callback_checked='setMapParam_checkbox('.. op_MultiSides..',%value)',
	checked=true,
	hint=loc(TID_Multi_Checkbox_TechLimits_Hint),--'Limits by Technological Upgrades, like amount of special soldiers based on tier of basic upgrade. (unchecked = unlimited)   Note: Map can ignore this option.',
},checkbox_merge),loc(TID_Multi_Checkbox_TechLimits));--'Technological Limits');
--]]



Multi_Room.Panel.StatusBG = AddElement ({
	type=TYPE_ELEMENT,
	parent=Multi_Room.Panel,
	anchor=anchorLT,
	x=Multi_Room.Panel.BG.x,
	y=Multi_Room.Panel.BG.y+Multi_Room.Panel.BG.height+5,
	width=Multi_Room.Panel.BG.width,--Multi_Room.GameParameters.width-10,
	height=20,
	colour1=BLACKA(200),
});

Multi_Room.Panel.Status = AddElement({
	type=TYPE_LABEL,
	parent=Multi_Room.Panel.StatusBG,
	x=10,
	y=0,
	width=Multi_Room.Panel.StatusBG.width-20,
	height=Multi_Room.Panel.StatusBG.height,
    text='';
	nomouseevent=true,
	wordwrap=true,
	text_halign=ALIGN_LEFT,
	font_name=Tahoma_12B ,
	font_colour=WHITEA(255),
});

----- Map Info -----
Multi_Room.MapInfo = AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Room,
	anchor=anchorLT,
	x=10+700+10,
	y=Multi_Room.Panel.height + 30,
	width=510,
	height=340,
	colour1=BLACKA(153),

});
--[[
Multi_Room.MapInfo.Sscrollbox = AddElement({
	type=TYPE_SCROLLBOX,
	parent=Multi_Room.MapInfo,
	anchor=anchorLT,
	x=5,
	y=5,
	width=Multi_Room.MapInfo.width-10-13,
	height=Multi_Room.MapInfo.height-20,
	colour1=BLACKA(0),
	bevel=true,
});--]]

Multi_Room.MapInfo.Sscrollbox_scrollV = AddElement({
	type=TYPE_SCROLLBAR,
	parent=Multi_Room.MapInfo,
	anchor={top=true,bottom=false,left=false,right=false},
	x=Multi_Room.MapInfo.width-10-13+5+1,
	y=5,
	width=12,
	height=Multi_Room.MapInfo.height-20,
	colour1=Scrollbar_Colour1,
	colour2=Scrollbar_Colour2,
	texture2="scrollbar.png",
});

Multi_Room.MapInfo.Rules = AddElement({
	type=TYPE_TEXTBOX,
	parent=Multi_Room.MapInfo,
	x=5,
	y=5,
	--width=Multi_Room.MapInfo.Sscrollbox.width-23,--Multi_Room.GameParameters.width-10,
    --height=Multi_Room.MapInfo.Sscrollbox.height*2,
	width=Multi_Room.MapInfo.width-10-13,
	height=Multi_Room.MapInfo.height-20,
	--width=0,
	--heigh=0,
	--automaxwidth=Multi_Room.MapInfo.Sscrollbox.width-23,
    text='';
	nomouseevent=true,
	wordwrap=true,
	text_halign=ALIGN_LEFT,
	font_name=Tahoma_13,
	font_colour=WHITEA(255),
});

--sgui_set(Multi_Room.MapInfo.Sscrollbox.ID,PROP_SCROLLBAR,Multi_Room.MapInfo.Sscrollbox_scrollV.ID);
sgui_set(Multi_Room.MapInfo.Rules.ID,PROP_SCROLLBAR,Multi_Room.MapInfo.Sscrollbox_scrollV.ID);
----  Game Chat ----

Multi_Room.Chat = AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Room,
	anchor=anchorLT,
	x=10+700+10,
	y=Multi_Room.Panel.height + 30,
	width=294,
	height=340,
	colour1=BLACKA(153),

});

Multi_Room.Chat.Room = AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Room.Chat,
	anchor=anchorLTRB,
	x=0,
	y=0,
	width=294,
	height=340,
	colour1=BLACKA(0),
});

Multi_Room.Chat.BG = AddElement ({
	type=TYPE_ELEMENT,
	parent=Multi_Room,
	anchor=anchorLT,
	x=Multi_Room.Chat.x + 0,
	y=Multi_Room.Chat.y -25,
	width=169,
	height=25,
	colour1=BLACKA(153),
});

Multi_Room.Chat.Button = AddElement ({
	type=TYPE_LABEL,
	parent=Multi_Room,
	anchor=anchorLT,
	x=Multi_Room.Chat.x + 0,
	y=Multi_Room.Chat.y - 22,
	width=169,
	height=20,
	colour1=BLACKA(0),
	text=loc(TID_Multi_button_chat),--'Chat',
	wordwrap=false,
	font_name=Tahoma_13B ,
	font_colour=WHITEA(255),
	text_halign=ALIGN_MIDDLE,
	callback_mouseclick='showRoomChat();',
	--texture='SGUI/Alien/Icons/chatico.png',

});

Multi_Room.Chat.Line = AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Room.Chat.Room,
   	anchor=anchorLT,
	x=169,
	y=1,
	width=Multi_Room.Chat.width-169,
	height=2,
	colour1=WHITEA(220),
});

Multi_Room.Chat.TextBox = AddElement({
	type=TYPE_TEXTBOX,
	parent=Multi_Room.Chat.Room,
	anchor={top=false,bottom=true,left=true,right=true},
	x=2,
	y=2,
	width=Multi_Room.Chat.width-4-13,
	height=Multi_Room.Chat.height-4-20,
	colour1=BLACKA(200);
	text='',
	font_name=Tahoma_12B ,
	wordwrap=true;
	callback_itemadded='setText({ID='..ChangeMapWindow.ChangeMap.Chat.TextBox.ID..'},%data);',
});

Multi_Room.Chat.TEXTBOX_scrollV = AddElement({
	type=TYPE_SCROLLBAR,
	parent=Multi_Room.Chat.Room,
	anchor={top=true,bottom=true,left=false,right=true},
	x=Multi_Room.Chat.TextBox.x+Multi_Room.Chat.TextBox.width+1,
	y=Multi_Room.Chat.TextBox.y,
	width=12,
	height=Multi_Room.Chat.TextBox.height,
	colour1=Scrollbar_Colour1,
	colour2=Scrollbar_Colour2,
	texture2="scrollbar.png",
});

sgui_set(Multi_Room.Chat.TextBox.ID,PROP_SCROLLBAR,Multi_Room.Chat.TEXTBOX_scrollV.ID);

Multi_Room.Chat.TextBoxEdit = AddElement({
	type=TYPE_EDIT,
	parent=Multi_Room.Chat.Room,
	anchor={top=false,bottom=true,left=true,right=true},
	x=2,
	y=elementBottom(Multi_Room.Chat.TextBox)+2,
	width=Multi_Room.Chat.width-4,
	height=20,
	colour1=BLACKA(200);
	font_name=Tahoma_12B ,
	text='',
	callback_keyup='Multi_Room.Chat.TextBoxEdit_keyup(%k);',
	wordwrap=true;
});

function Multi_Room.Chat.TextBoxEdit_keyup(key)
	if key == 13 then
		OW_MULTI_SENDALLCHATMSG( getText(Multi_Room.Chat.TextBoxEdit), getHexColour(SIDE_COLOURS[Players[MyID].Colour+1]));  -- not colouring, and Idk why
		--OW_MULTI_SENDALLCHATMSG(getHexColour(SIDE_COLOURS[Players[MyID].Colour+1])..getText(ChangeMapWindow.ChangeMap.Chat.TextBoxEdit) .. getHexColour(nil));
		setText(Multi_Room.Chat.TextBoxEdit,'');
	--	syncChat();
	end;
end;

Multi_Room.Chat.IRC = AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Room.Chat,
	anchor=anchorLTRB,
	x=0,
	y=0,
	width=294,
	height=340,
	colour1=BLACKA(0),
	visible=false;
});

Multi_Room.Chat.IRC.BG = AddElement ({
	type=TYPE_ELEMENT,
	parent=Multi_Room,
	anchor=anchorLT,
	x=Multi_Room.Chat.x + 0,
	y=Multi_Room.Chat.y -25,
	width=169,
	height=25,
	colour1=BLACKA(153),
});

Multi_Room.Chat.IRC.Button = AddElement ({
	type=TYPE_LABEL,
	parent=Multi_Room,
	anchor=anchorLT,
	x=Multi_Room.Chat.x + 0,
	y=Multi_Room.Chat.y - 22,
	width=169,
	height=20,
	colour1=BLACKA(0),
	text=loc(TID_Multi_button_lobbyChat),--'Chat',
	wordwrap=false,
	font_name=Tahoma_13B ,
	font_colour=RGBA(150,150,150,255),
	text_halign=ALIGN_MIDDLE,
	callback_mouseclick='showLobbyChat();',
	--texture='SGUI/Alien/Icons/chatico.png',

});

Multi_Room.Chat.IRC.Line = AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Room.Chat.IRC,
   	anchor=anchorLT,
	x=0,
	y=1,
	width=170,
	height=2,
	colour1=WHITEA(220),
});

Multi_Room.Chat.IRC.Line2 = AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Room.Chat.IRC,
   	anchor=anchorLT,
	x=(169*2)+1,
	y=1,
	width=Multi_Room.Chat.width-(169*2)+1,
	height=2,
	colour1=WHITEA(220),
});

Multi_Room.Chat.IRC.TextBox = AddElement({
	type=TYPE_TEXTBOX,
	parent=Multi_Room.Chat.IRC,
	anchor={top=false,bottom=true,left=true,right=true},
	x=5,
	y=5,
	width=Multi_Room.Chat.IRC.width*0.60-10-13-13,
	height=Multi_Room.Chat.IRC.height-30-10,
	colour1=BLACKA(200);
	text='',
	font_name=Tahoma_12B ,
	callback_itemadded='sentToMPROom(%data);',
	wordwrap=true;
});

Multi_Room.Chat.IRC.LISTBOX = AddElement({
	type=TYPE_CUSTOMLISTBOX,
	parent=Multi_Room.Chat.IRC,
	anchor={top=true,bottom=true,left=false,right=true},
	x=Multi_Room.Chat.IRC.width-Multi_Room.Chat.IRC.width*0.40-5-13,
	y=5,
	width=Multi_Room.Chat.IRC.width*0.40,
	height=Multi_Room.Chat.IRC.height-30-10,
	colour1=IRCBackCol,
	font_name=Tahoma_14,
	itemheight=36,
	callback_itemadded='irc_name_added(1,%id,%rowid,%index,%data);',
	callback_itemupdated='irc_name_updated(1,%rowid,%index,%data);',
--	callback_itemdeleted='irc_name_deleted(%index);',
	callback_itemselected='setColour1ID(%rowid,BLACKA(200));setBevelID(%rowid,true);setGradientID(%rowid,true);',
	callback_itemunselected='setColour1ID(%rowid,BLACKA(0));setBevelID(%rowid,false);setGradientID(%rowid,false);',
	bevel=true,
	bevel_colour1=GRAYA(14,200),
	bevel_colour2=GRAYA(14,200),
	--edges=true,
});

Multi_Room.Chat.IRC.TEXTBOX_scrollV = AddElement({
	type=TYPE_SCROLLBAR,
	parent=Multi_Room.Chat.IRC,
	anchor={top=true,bottom=true,left=false,right=true},
	x=Multi_Room.Chat.IRC.TextBox.x+Multi_Room.Chat.IRC.TextBox.width+1,
	y=Multi_Room.Chat.IRC.TextBox.y,
	width=12,
	height=Multi_Room.Chat.IRC.TextBox.height,
	colour1=Scrollbar_Colour1,
	colour2=Scrollbar_Colour2,
	texture2="scrollbar.png",
});

Multi_Room.Chat.IRC.LISTBOX_scrollV = AddElement({
	type=TYPE_SCROLLBAR,
	parent=Multi_Room.Chat.IRC,
	anchor={top=true,bottom=true,left=false,right=true},
	x=Multi_Room.Chat.IRC.LISTBOX.x+Multi_Room.Chat.IRC.LISTBOX.width+1,
	y=Multi_Room.Chat.IRC.LISTBOX.y,
	width=12,
	height=Multi_Room.Chat.IRC.LISTBOX.height,
	colour1=Scrollbar_Colour1,
	colour2=Scrollbar_Colour2,
	texture2="scrollbar.png",
});

sgui_set(Multi_Room.Chat.IRC.TextBox.ID,PROP_SCROLLBAR,Multi_Room.Chat.IRC.TEXTBOX_scrollV.ID);
sgui_set(Multi_Room.Chat.IRC.LISTBOX.ID,PROP_SCROLLBAR,Multi_Room.Chat.IRC.LISTBOX_scrollV.ID);

Multi_Room.Chat.IRC.TextBoxEdit = AddElement({
	type=TYPE_EDIT,
	parent=Multi_Room.Chat.IRC,
	anchor={top=false,bottom=true,left=true,right=true},
	x=10,
	y=Multi_Room.Chat.IRC.height-10-18,
	width=Multi_Room.Chat.IRC.width-20,
	height=20,
	colour1=BLACKA(200);
	font_name=Tahoma_12B ,
	text='',
	callback_keyup='Multi_Room.Chat.IRC.TextBoxEdit_keyup(%k);',
});

function getselectedname2()
	if getIndex(Multi_Room.Chat.IRC.LISTBOX) <= 0 then
		return '';
	else
		return getircname(ircnamerows[1][getIndex(Multi_Room.Chat.IRC.LISTBOX)]);
	end;
end;

function Multi_Room.Chat.IRC.TextBoxEdit_keyup(key)
	if key == 13 then
		OW_IRC_SENDTEXT(getText(Multi_Room.Chat.IRC.TextBoxEdit),getselectedname2());
		setText(Multi_Room.Chat.IRC.TextBoxEdit,'');
	end;
end;


---- Restrictions ----
Multi_Room.Restriction = AddElement ({
	type=TYPE_ELEMENT,
	parent=Multi_Room,
	anchor=anchorLT,
	x=10,
	y=Multi_Room.Panel.height+30,
	width=510,
	height=340,
	colour1=BLACKA(170),
	visible=false;
});

function makeTechLevelsButton()
	Multi_Room.Rests.TechLevels_Button = makeGradButton({
			parent=Multi_Room.Restriction,
			anchor=anchorLT,
			x=Multi_Room.Restriction.width-180,
			y=5,
			width=180,
			height=30,
			text=loc(TID_Multi_TechLevel),
			font_name=Tahoma_12B ,
			text_case=CASE_UPPER,
			--disabled=true,
			callback_mouseclick='showTechLevels();',
			font_colour=RGB(220,220,220),
		},GradButton_Grey_Light);
end;

if MpScrW <= 2450 then

	Multi_Room.RestsBG = AddElement ({
		type=TYPE_ELEMENT,
		parent=Multi_Room,
		anchor=anchorLT,
		x=10+170,--+510 -170,
		y=Multi_Room.Panel.height+5,
		width=170,
		height=25,
		colour1=BLACKA(153),
	});

	Multi_Room.Rests = AddElement ({
		type=TYPE_LABEL,
		parent=Multi_Room,
		anchor=anchorLT,
		x=10+176,--510 -176+5,
		y=Multi_Room.Panel.height+5+3,
		width=170,
		height=20,
		colour1=BLACKA(0),
		text=loc(TID_Multi_button_rest)..' (0)',--loc(TID_Multi_button_Rest),
		wordwrap=false,
		font_name=Tahoma_13B ,
		font_colour=RGBA(150,150,150,255),
		text_halign=ALIGN_MIDDLE,
		callback_mouseclick='showRest();',
	});

	Multi_Room.Rests.Locker = AddElement ({
		type=TYPE_ELEMENT,
		parent=Multi_Room.Rests,
		anchor=anchorLT,
		x=2,
		y=2,
		width=16,--Multi_Room.GameParameters.width-10,
		height=16,
		colour1=WHITEA(255),
		texture = 'SGUI/Alien/multiplayer/player_lock.png',
		hint = loc(TID_Multi_Hint_OptionLock),
		visible = true;
	});

	makeTechLevelsButton();
end;

include('Menu/multiroom_restrictions');

---- Players Area -----
AddElement ({
	type=TYPE_ELEMENT,
	parent=Multi_Room,
	anchor=anchorLT,
	x=10,
	y=Multi_Room.Panel.height + 30 + 340 + 30-25,
	width=169,
	height=25,
	colour1=BLACKA(153),
});

Multi_Room.PlayersRow= AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Room,
	anchor=anchorLT,
	x=10,
	y=Multi_Room.Panel.height + 30 + 340 + 30,
	width=700+60,
	height=340,
	colour1=BLACKA(153),

});

-- Nezn�m� ��el
AddElement ({
	type=TYPE_ELEMENT,
	parent=Multi_Room.PlayersRow,
	anchor=anchorLT,
    x=0,
    y=-25,
	width=169,
	height=25,
	colour1=BLACKA(153),
});

Multi_Room.PlayersRow.Line = AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Room.PlayersRow,
   	anchor=anchorLTR,
	x=169,
	y=1,
	width=Multi_Room.PlayersRow.width-169,
	height=2,
	colour1=WHITEA(220),
});

include('Menu/multiroom_players');
---- settings Area ----

Multi_Room.SettingsRow= AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Room,
	anchor=anchorLT,
	x=10+710+60,
	y=Multi_Room.Panel.height + 30 + 340 + 30,
	width=700,
	height=340,
	colour1=BLACKA(153),

});

Multi_Room.SettingsRow.Line = AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Room.SettingsRow,
   	anchor=anchorLTR,
	x=169,
	y=1,
	width=Multi_Room.SettingsRow.width-169,
	height=2,
	colour1=WHITEA(220),
});

include('Menu/multiroom_settings');

---------------------- Ranked Game Area ---------------------------------------

Multi_Room.RankedRow = AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Room,
	anchor=anchorLT,
	x=10+710+60,
	y=Multi_Room.Panel.height + 30 + 340 + 30,
	width=700,
	height=340,
	colour1=BLACKA(153),
	visible=false,

});

Multi_Room.RankedRow.Line = AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Room.RankedRow,
   	anchor=anchorLT,
	x=0,
	y=1,
	width=169,
	height=2,
	colour1=WHITEA(220),
});

Multi_Room.RankedRow.Line2 = AddElement({
	type=TYPE_ELEMENT,
	parent=Multi_Room.RankedRow,
   	anchor=anchorLTR,
	x=169+170,
	y=1,
	width=Multi_Room.RankedRow.width-169-170,
	height=2,
	colour1=WHITEA(220),
});

local rankCheckX = (ChangeMapWindow.ChangeMap.width/3)+20+5;
if 500 > (ChangeMapWindow.ChangeMap.width/3) then
	rankCheckX = 40;
end;

Multi_Room.RankedRow.CheckBox = makeCustomChechboxElement(
	--Multi_Room.RankedRow,
	ChangeMapWindow.ChangeMap,
	loc(TID_Multi_label_CB_GameType),
	'',--loc(TID_Multi_Lock_game_Hint),
	false,
	false,
	false,
--	10,15,500,
	rankCheckX,ChangeMapWindow.ChangeMap.height-40-30+5,500,
	'ChangeRanked(');

Multi_Room.RankedRow.LoginRow = getElementEX(
	Multi_Room.RankedRow,
	anchorLT,
	XYWH(
		10,
		40,
		220,
		6*25+5
	),
	false,
	{
		colour1=WHITEA(10),
	}
);

Multi_Room.RankedRow.LoginRow.State = getLabelEX(
	Multi_Room.RankedRow.LoginRow,
	anchorLT,
	XYWH(
		5,
		5,
		290,
		20
	),
	Tahoma_13B,
	loc(TID_Multi_CB_Status) .. ' '..loc(TID_Multi_CB_NotLoggedIn),
	{
		font_colour=WHITEA(255),
	}
);

Multi_Room.RankedRow.LoginRow.NameLabel = getLabelEX(
	Multi_Room.RankedRow.LoginRow,
	anchorLT,
	XYWH(
		5,
		25,
		290,
		20
	),
	Tahoma_13,
	loc(TID_Multi_CB_Username),
	{
		font_colour=WHITEA(255),
	}
);

Multi_Room.RankedRow.LoginRow.PasswordLabel = getLabelEX(
	Multi_Room.RankedRow.LoginRow,
	anchorLT,
	XYWH(
		5,
		25+25+25,
		290,
		20
	),
	Tahoma_13,
	loc(TID_Multi_CB_Password),
	{
		font_colour=WHITEA(255),
	}
);

Multi_Room.RankedRow.LoginRow.Name = AddElement({
	type=TYPE_EDIT,
	parent=Multi_Room.RankedRow.LoginRow,
	anchor={top=true,bottom=true,left=true,right=true},
	x=15,
	y=25+25,
	width=200,
	height=25,
	colour1=BLACKA(200),
	font_name=Tahoma_12B ,
	wordwrap=false,
	visible=true,
	bevel=true;
	bevel_colour1=GRAYA(14,200),
	bevel_colour2=GRAYA(14,200),
	text='',
	callback_keyup='Multi_Room.RankedRow.LoginRow.Name_keyup(%k);',
	edges=true,
});

Multi_Room.RankedRow.LoginRow.Password = AddElement({
	type=TYPE_EDIT,
	parent=Multi_Room.RankedRow.LoginRow,
	anchor={top=true,bottom=true,left=true,right=true},
	x=15,
	y=4*25,
	width=200,
	height=25,
	colour1=BLACKA(200),
	font_name=Tahoma_12B ,
	wordwrap=false,
	visible=true,
	bevel=true;
	bevel_colour1=GRAYA(14,200),
	bevel_colour2=GRAYA(14,200),
	text='',
	callback_keyup='Multi_Room.RankedRow.LoginRow.Password_keyup(%k);',
	edges=true,
});

function Multi_Room.RankedRow.LoginRow.Name_keyup(key)

	OW_SETTING_WRITE("MP","RANKED_NAME",getText(Multi_Room.RankedRow.LoginRow.Name));
	if key == 13 then
		loginToRanked();
	end;
end;

function Multi_Room.RankedRow.LoginRow.Password_keyup(key)
	if key == 13 then
		loginToRanked();
	end;
end;

Multi_Room.RankedRow.LoginRow.LoginBut = makeGradButton({
	parent=Multi_Room.RankedRow.LoginRow,
	anchor=anchorLT,
	x=5,
	y=5*25+5,
	width=210,
	height=20,
	text=loc(TID_Multi_CB_Login),--'Change Map',
	font_name=Tahoma_12B ,
	text_case=CASE_UPPER,
	--disabled=true,
	callback_mouseclick='loginToRanked();',
	font_colour=RGB(220,220,220),
},GradButton_Green);

function loginToRanked()
	ToOW_RankedLogin(getText(Multi_Room.RankedRow.LoginRow.Name),getText(Multi_Room.RankedRow.LoginRow.Password));
end;
---------------------- Steam - Profile Bar ------------------------------------
----- Steam Bar -------
Multi_Room.SteamBar = {};
Multi_Room.SteamBar.Invite = makeGradButton({
	parent=profilebar,
	anchor=anchorLT,
	x=64+5,
	y=28,
	width=225,
	height=30,
	text=loc(TID_Multi_button_steamInvite),	--Invite your Steam friends'
	font_name=Tahoma_12B ,
	text_case=CASE_UPPER,
	visible=false,
	callback_mouseclick='ToOW_SteamInvite();',
},GradButton_Blue);


---- profile Bar -----
Multi_Room.Profile = {};
Multi_Room.Profile.MPName = AddElement({
	type=TYPE_EDIT,
	parent=profilebar,
	anchor={top=true,bottom=true,left=true,right=true},
	x=5+70,
	y=35,
	width=180,
	height=25,
	colour1=BLACKA(200),
	font_name=Tahoma_12B ,
	wordwrap=false,
	visible=false,
	bevel=true;
	bevel_colour1=GRAYA(14,200),
	bevel_colour2=GRAYA(14,200),
	text=getvalue(OWV_USERNAME),
	callback_keyup='Multi_Room.Profile.MPName_keyup(%k);',
	--edges=true,
});

function Multi_Room.Profile.MPName_keyup(key)
	ToOW_setMyName( getText(Multi_Room.Profile.MPName));
	OW_SETTING_WRITE("MP","NAME",getText(Multi_Room.Profile.MPName));
end;

-------------- Only Server -------------
--[[	      --      !!!!!!	It's not good idea how block changing options, becouse client can not see hints	  !!!!!!!!!
client_aperture = {
	AddElement ({
		type=TYPE_ELEMENT,
		parent=Multi_Room.Restriction,
		anchor=anchorLTRB,
		x=0,
		y=20,
		width=Multi_Room.Restriction.width,
		height=Multi_Room.Restriction.height-20,
		colour1=BLACKA(0),

	});

	AddElement ({
	type=TYPE_ELEMENT,
	parent=Multi_Room.GameParameters.GameActions,
	anchor=anchorLTRB,
	x=0,
	y=70,
	width=Multi_Room.GameParameters.GameActions.width,
	height=Multi_Room.GameParameters.GameActions.height-70,
	colour1=BLACKA(0),

    });

	AddElement ({
		type=TYPE_ELEMENT,
		parent=Multi_Room.SettingsRow,
		anchor=anchorLTRB,
		x=0,
		y=0,
		width=Multi_Room.SettingsRow.width,
		height=Multi_Room.SettingsRow.height,
		colour1=BLACKA(0),

	});

}
--]]

--function screenResReorganization()
-------------------------------- Screen Reorganization --------------------------------
--- Change names size for lowest resolutions
  if MpScrW < 1360 then
    --Multi_Room.Panel.MapTitle.font_name = Tahoma_14 ;

	--SetFont(Multi_Room.Panel.MapTitle, Tahoma_14 );
	set_Property(Multi_Room.Panel.MapTitle.ID,PROP_FONT_NAME,Tahoma_13);
  end;

--- Reorganization  top line

    --Multi_Room.MapInfo.width = 1400;
  spaceWidth = MpScrW -730;

  if MpScrW >= 2960 then
	Multi_Room.MapBut = AddElement ({
		type=TYPE_LABEL,
		parent=Multi_Room.GameParameters,
		anchor=anchorLT,
		x=0,
		y=-25,
		width=169,
		height=25,
		colour1=BLACKA(153),
		text=loc(TID_Multi_button_map), --'Map & Positions',
		wordwrap=false,
		font_name=Tahoma_13B ,
		font_colour=WHITEA(255),--WHITEA(255),
		text_halign=ALIGN_MIDDLE,
		--callback_mouseclick='showMap();',
	});

    AddElement ({
		type=TYPE_ELEMENT,
		parent=Multi_Room.MapInfo,
		anchor=anchorLT,
		x=0,
		y=-25,
		width=169,
		height=25,
		colour1=BLACKA(153),
	});

    AddElement({
		type=TYPE_LABEL,
		parent=Multi_Room.MapInfo,
		anchor=anchorLT,
		x=0,
		y=-22,
		width=169,
		height=20,
		colour1=BLACKA(0),
		text=loc(TID_Multi_button_MapDesc),
		wordwrap=false,
		font_name=Tahoma_13B ,
		font_colour=WHITEA(255),
		text_halign=ALIGN_MIDDLE,
		--callback_mousedown='showDes();',
	});

	AddElement({
		type=TYPE_ELEMENT,
		parent=Multi_Room.MapInfo,
    	anchor=anchorLT,
		x=170,
		y=1,
		width=510-169,
		height=2,
		colour1=WHITEA(220),
    });

	Multi_Room.Rests = AddElement ({
		type=TYPE_LABEL,
		parent=Multi_Room,
		anchor=anchorLT,
		x=720;-- -176,
		y=Multi_Room.Panel.height+5,
		width=169,
		height=25,
		colour1=BLACKA(153),
		text=loc(TID_Multi_button_rest)..' (0)', --'Restrictions (0)',
		wordwrap=false,
		font_name=Tahoma_13B ,
		font_colour=RGBA(255,255,255,255),
		text_halign=ALIGN_MIDDLE,
		--callback_mouseclick='showRest();',
	});

	Multi_Room.Rests.Locker = AddElement ({
		type=TYPE_ELEMENT,
		parent=Multi_Room.Rests,
		anchor=anchorLT,
		x=2,
		y=2,
		width=16,--Multi_Room.GameParameters.width-10,
		height=16,
		colour1=WHITEA(255),
		texture = 'SGUI/Alien/multiplayer/player_lock.png',
		hint = loc(TID_Multi_Hint_OptionLock),
		visible = true;
	});

	AddElement({
		type=TYPE_ELEMENT,
		parent=Multi_Room.Restriction,
		anchor=anchorLTR,
		x=169,
		y=1,
		width=510-170,
		height=2,
		colour1=WHITEA(220),
	});

	Multi_Room.Restriction.b_but = AddElement({
		type=TYPE_LABEL,
		parent=Multi_Room.Restriction,
		anchor=anchorLT,
		x=5,
		y=5,
		width=130,
		height=20,
		colour1=BLACKA(0),
		text=loc(TID_Multi_button_restBuildings), --'Buildings',
		wordwrap=false,
		font_name=Tahoma_13B ,
		font_colour=WHITEA(255),
		--callback_mousedown='showBuildings();',
	});

	Multi_Room.Restriction.t_but = AddElement({
		type=TYPE_LABEL,
		parent=Multi_Room.Restriction,
		anchor=anchorLT,
		x=510,
		y=5,
		width=130,
		height=20,
		colour1=BLACKA(0),
		text=loc(TID_Multi_button_restTechs), --'Technologies',
		wordwrap=false,
		font_name=Tahoma_13B ,
		font_colour=WHITEA(255),
		--callback_mousedown='showTechs();',
	});

	makeTechLevelsButton();

	setXYV(Multi_Room.Restriction,720,Multi_Room.Panel.height + 30);
	setWHV(Multi_Room.Restriction,1020,Multi_Room.Restriction.height);
	setColour1(Multi_Room.Restriction,BLACKA(153));
	setVisible(Multi_Room.Restriction,true);
	setVisible(Multi_Room.Restriction.t_row,true);
	setXYV(Multi_Room.Restriction.t_row,510,Multi_Room.Restriction.t_row.y);
    setXYV(Multi_Room.MapInfo,720+1030,Multi_Room.Panel.height + 30);
    setXYV(Multi_Room.Chat,720+1030+520,Multi_Room.Panel.height + 30);
    setWHV(Multi_Room.Chat,spaceWidth-520-1030,340);
	setWHV(Multi_Room.Chat.Line,spaceWidth-520-1030-169,2);

	Multi_Room.Restriction.Line = AddElement({
		type=TYPE_ELEMENT,
		parent=Multi_Room.Restriction,
		anchor=anchorLT,
		x=509,
		y=20,
		width=2,
		height=300,
		colour1=WHITEA(220),
	});

  elseif MpScrW >= 2450 then
	Multi_Room.MapBut = AddElement ({
		type=TYPE_LABEL,
		parent=Multi_Room.GameParameters,
		anchor=anchorLT,
		x=0,
		y=-25,
		width=169,
		height=25,
		colour1=BLACKA(153),
		text=loc(TID_Multi_button_map), --'Map & Positions',
		wordwrap=false,
		font_name=Tahoma_13B ,
		font_colour=WHITEA(255),--WHITEA(255),
		text_halign=ALIGN_MIDDLE,
		--callback_mouseclick='showMap();',
	});

    AddElement ({
		type=TYPE_ELEMENT,
		parent=Multi_Room.MapInfo,
		anchor=anchorLT,
		x=0,
		y=-25,
		width=169,
		height=25,
		colour1=BLACKA(153),
	});

    AddElement({
		type=TYPE_LABEL,
		parent=Multi_Room.MapInfo,
		anchor=anchorLT,
		x=0,
		y=-22,
		width=169,
		height=20,
		colour1=BLACKA(0),
		text=loc(TID_Multi_button_MapDesc),
		wordwrap=false,
		font_name=Tahoma_13B ,
		font_colour=WHITEA(255),
		text_halign=ALIGN_MIDDLE,
		--callback_mousedown='showDes();',
	});

	AddElement({
		type=TYPE_ELEMENT,
		parent=Multi_Room.MapInfo,
    		anchor=anchorLT,
		x=170,
		y=1,
		width=510-169,
		height=2,
		colour1=WHITEA(220),
    });

	Multi_Room.Rests = AddElement ({
		type=TYPE_LABEL,
		parent=Multi_Room,
		anchor=anchorLT,
		x=720;-- -176,
		y=Multi_Room.Panel.height+5,
		width=169,
		height=25,
		colour1=BLACKA(153),
		text=loc(TID_Multi_button_rest) .. ' (0)', --'Restrictions (0)',
		wordwrap=false,
		font_name=Tahoma_13B ,
		font_colour=RGBA(255,255,255,255),
		text_halign=ALIGN_MIDDLE,
		--callback_mouseclick='showRest();',

	});

	Multi_Room.Rests.Locker = AddElement ({
		type=TYPE_ELEMENT,
		parent=Multi_Room.Rests,
		anchor=anchorLT,
		x=2,
		y=2,
		width=16,--Multi_Room.GameParameters.width-10,
		height=16,
		colour1=WHITEA(255),
		texture = 'SGUI/Alien/multiplayer/player_lock.png',
		hint = loc(TID_Multi_Hint_OptionLock),
		visible = true;
	});


	AddElement({
		type=TYPE_ELEMENT,
		parent=Multi_Room.Restriction,
		anchor=anchorLT,
		x=169,
		y=1,
		width=510-170,
		height=2,
		colour1=WHITEA(220),
	});

	makeTechLevelsButton();

	setXYV(Multi_Room.Restriction,720,Multi_Room.Panel.height + 30);
	setColour1(Multi_Room.Restriction,BLACKA(153));
	setVisible(Multi_Room.Restriction,true);
	setXYV(Multi_Room.MapInfo,720+520,Multi_Room.Panel.height + 30);
	setXYV(Multi_Room.Chat,720+520+520,Multi_Room.Panel.height + 30);
	setWHV(Multi_Room.Chat,spaceWidth-520-520,340);
	setWHV(Multi_Room.Chat.Line,spaceWidth-520-520-169,2);
	showBuildings();
  elseif MpScrW >= 1920 then

    AddElement ({
		type=TYPE_ELEMENT,
		parent=Multi_Room.MapInfo,
		anchor=anchorLT,
		x=0,
		y=-25,
		width=169,
		height=25,
		colour1=BLACKA(153),
	});
--[[
	AddElement ({
		type=TYPE_ELEMENT,
		parent=Multi_Room.MapInfo,
		anchor=anchorLT,
		x=3,
		y=-19,
		width=40,
		height=18,
		texture='SGUI/Alien/Icons/desico.png',

	});
--]]
    AddElement({
		type=TYPE_LABEL,
		parent=Multi_Room.MapInfo,
		anchor=anchorLT,
		x=0,
		y=-22,
		width=169,
		height=20,
		colour1=BLACKA(0),
		text=loc(TID_Multi_button_MapDesc),
		wordwrap=false,
		font_name=Tahoma_13B ,
		font_colour=WHITEA(255),
		text_halign=ALIGN_MIDDLE,
		--callback_mousedown='showDes();',
	});

	AddElement({
		type=TYPE_ELEMENT,
		parent=Multi_Room.MapInfo,
    	anchor=anchorLT,
		x=170,
		y=1,
		width=510-169,
		height=2,
		colour1=WHITEA(220),
    });

	AddElement({
		type=TYPE_ELEMENT,
		parent=Multi_Room.Restriction,
		anchor=anchorLT,
		x=0,
		y=1,
		width=170,
		height=2,
		colour1=WHITEA(220),
	});

	AddElement({
		type=TYPE_ELEMENT,
		parent=Multi_Room.Restriction,
		anchor=anchorLT,
		x=2*170,
		y=1,
		width=170,
		height=2,
		colour1=WHITEA(220),
	});

    setXYV(Multi_Room.Chat,720+520,Multi_Room.Panel.height + 30);
    setWHV(Multi_Room.Chat,spaceWidth-520,340);
	setWHV(Multi_Room.Chat.Line,spaceWidth-520-169,2);
	setVisible(Multi_Room.Restriction.t_row,false);
	
  else
    
   setXYV(Multi_Room.MapInfo,10,Multi_Room.Panel.height + 30);
   setColour1(Multi_Room.MapInfo,BLACKA(170));

   setVisible(Multi_Room.MapInfo,false);


   AddElement ({
	    type=TYPE_ELEMENT,
		parent=Multi_Room,
		anchor=anchorLT,
		x=10+170,
		y=Multi_Room.Panel.height+30-25,
		width=169,
		height=25,
		colour1=BLACKA(153),
	});

	desBut = AddElement ({
		type=TYPE_LABEL,
		parent=Multi_Room,
		anchor=anchorLT,
		x=10+170,
		y=Multi_Room.Panel.height+30-22,
		width=169,
		height=20,
		text=loc(TID_Multi_button_MapDesc),
		wordwrap=false,
		font_name=Tahoma_13B ,
		text_halign=ALIGN_MIDDLE,
		callback_mouseclick='showDes();',
		font_colour = RGBA(150,150,150,255),

	});

	AddElement({
		type=TYPE_ELEMENT,
		parent=Multi_Room.MapInfo,
    	anchor=anchorLT,
		x=0,
		y=1,
		width=170+1,
		height=2,
		colour1=WHITEA(220),
    });

	AddElement({
		type=TYPE_ELEMENT,
		parent=Multi_Room.MapInfo,
    	anchor=anchorLT,
		x=170+169,
		y=1,
		width=171,
		height=2,
		colour1=WHITEA(220),
    });

	setX(Multi_Room.Rests,10+170+1+170);
	setX(Multi_Room.RestsBG,10+170+170);


	AddElement({
		type=TYPE_ELEMENT,
		parent=Multi_Room.Restriction,
    	anchor=anchorLT,
		x=0,
		y=1,
		width=170+169+1,
		height=2,
		colour1=WHITEA(220),
    });

    setXYV(Multi_Room.Chat,720,Multi_Room.Panel.height + 30);
    setWHV(Multi_Room.Chat,spaceWidth,340);
    setWHV(Multi_Room.Chat.Line,spaceWidth-169,2);
	showBuildings();
  end;

  -- Reorganization bottom line and chat buttons
  if MpScrW >= 1360 then

	Multi_Room.SettingsButBG = AddElement ({
		type=TYPE_ELEMENT,
		parent=Multi_Room,
		anchor=anchorLT,
		x=10+710+60,
		y=Multi_Room.Panel.height + 30 + 340 + 30-25,
		width=169,
		height=25,
		colour1=BLACKA(153),
	});

	setBut = AddElement ({
		type=TYPE_LABEL,
		parent=Multi_Room.SettingsButBG,
		anchor=anchorLT,
		x=0,
		y=2,
		width=169,
		height=20,
		colour1=BLACKA(0),
		text=loc(TID_Multi_button_MapOpt),
		wordwrap=false,
		font_name=Tahoma_13B ,
		font_colour=WHITEA(255),
		text_halign=ALIGN_MIDDLE,
		callback_mouseclick='showSets();',
	});

	
	Multi_Room.RankedButBG = AddElement ({
		type=TYPE_ELEMENT,
		parent=Multi_Room,
		anchor=anchorLT,
		x=10+710+60+170,
		y=Multi_Room.Panel.height + 30 + 340 + 30-25,
		width=169,
		height=25,
		colour1=BLACKA(153),
	});
	
	rankBut = AddElement ({
		type=TYPE_LABEL,
		parent=Multi_Room.RankedButBG,
		anchor=anchorLT,
		x=0,
		y=2,
		width=169,
		height=20,
		colour1=BLACKA(0),
		text=loc(TID_Multi_button_CB),
		wordwrap=false,
		font_name=Tahoma_13B ,
		font_colour=WHITEA(150),
		text_halign=ALIGN_MIDDLE,
		callback_mouseclick='showRank();',
	});

	AddElement ({
		type=TYPE_LABEL,
		parent=Multi_Room.PlayersRow,
		anchor=anchorLT,
		x=0,
		y=-22,
		width=169,
		height=20,
		colour1=BLACKA(0),
		text=loc(TID_Multi_button_Players), --"Players' Settings",
		wordwrap=false,
		font_name=Tahoma_13B ,
		font_colour=WHITEA(255),
		text_halign=ALIGN_MIDDLE,
	--	callback_mouseclick='showPlayers();',
	});
	
	local SettingsWidth = spaceWidth-60;
	if SettingsWidth <= 1130 then

		setWHV(Multi_Room.SettingsRow,SettingsWidth,MpScrH - (Multi_Room.Panel.height + 30 +60 +340));
		setWHV(Multi_Room.RankedRow,SettingsWidth,MpScrH - (Multi_Room.Panel.height + 30 +60 +340));
		--SetWHV(Multi_Room.SettingsRow.Line,spaceWidth-169-60,2);
		setWHV(Multi_Room.PlayersRow,Multi_Room.PlayersRow.width,MpScrH - (Multi_Room.Panel.height + 30 +60 +340));
		--SetWHV(multiplayer_options_scrollbox,spaceWidth-20-13,MpScrH - (Multi_Room.Panel.height + 30 +60 +340)-20);
	else
		local xoffset = (MpScrW - 1130-730- 60)/2;
		setXYWHV(Multi_Room.SettingsRow,Multi_Room.SettingsRow.x+xoffset,Multi_Room.SettingsRow.y,1130,MpScrH - (Multi_Room.Panel.height + 30 +60 +340));
		
		setXYWHV(Multi_Room.RankedRow,Multi_Room.RankedRow.x+xoffset,Multi_Room.RankedRow.y,1130,MpScrH - (Multi_Room.Panel.height + 30 +60 +340));
		
		setXYWHV(Multi_Room.PlayersRow,Multi_Room.PlayersRow.x+xoffset,Multi_Room.PlayersRow.y,Multi_Room.PlayersRow.width,MpScrH - (Multi_Room.Panel.height + 30 +60 +340));
	end;
		--setWidth(Multi_Room.RankedRow.Line2,getWidth(Multi_Room.RankedRow)-169-170);
		setXYV(Multi_Room.Chat.BG,getX(Multi_Room.Chat),getY(Multi_Room.Chat) - 25);
		setXYV(Multi_Room.Chat.Button,getX(Multi_Room.Chat),getY(Multi_Room.Chat) - 23);
		setXYV(Multi_Room.Chat.IRC.BG,getX(Multi_Room.Chat)+170,getY(Multi_Room.Chat) - 25);
		setXYV(Multi_Room.Chat.IRC.Button,getX(Multi_Room.Chat)+170,getY(Multi_Room.Chat) - 23);
		setXYWHV(Multi_Room.Chat.IRC.Line2,(170*2)+1,1,getWidth(Multi_Room.Chat)-(170*2)+1,2);
  else

  	AddElement ({
		type=TYPE_ELEMENT,
		parent=Multi_Room,
		anchor=anchorLT,
		x=Multi_Room.PlayersRow.x+170,
		y=Multi_Room.PlayersRow.y-25,
		width=169,
		height=25,
		colour1=BLACKA(153),
	});

	setBut = AddElement ({
		type=TYPE_LABEL,
		parent=Multi_Room,
		anchor=anchorLT,
		x=Multi_Room.PlayersRow.x+170,
		y=Multi_Room.PlayersRow.y-22,
		width=169,
		height=20,
		colour1=BLACKA(0),
		text=loc(TID_Multi_button_MapOpt),
		wordwrap=false,
		font_name=Tahoma_13B ,
		font_colour=WHITEA(255),
		text_halign=ALIGN_MIDDLE,
		callback_mouseclick='showSets();',
	});

	playersBut = AddElement ({
		type=TYPE_LABEL,
		parent=Multi_Room,
		anchor=anchorLT,
		x=Multi_Room.PlayersRow.x,
		y=Multi_Room.PlayersRow.y-22,
		width=169,
		height=20,
		colour1=BLACKA(0),
		text=loc(TID_Multi_button_Players), --"Players' Settings",
		wordwrap=false,
		font_name=Tahoma_13B ,
		font_colour=WHITEA(255),
		text_halign=ALIGN_MIDDLE,
		callback_mouseclick='showPlayers();',
	});

	AddElement({
		type=TYPE_ELEMENT,
		parent=Multi_Room.SettingsRow,
    	anchor=anchorLT,
		x=0,
		y=1,
		width=170,
		height=2,
		colour1=WHITEA(220),
    });

	Multi_Room.RankedButBG = AddElement ({
		type=TYPE_ELEMENT,
		parent=Multi_Room,
		anchor=anchorLT,
		x=Multi_Room.PlayersRow.x+170+170,
		y=Multi_Room.Panel.height + 30 + 340 + 30-25,
		width=169,
		height=25,
		colour1=BLACKA(153),
	});
	
	rankBut = AddElement ({
		type=TYPE_LABEL,
		parent=Multi_Room.RankedButBG,
		anchor=anchorLT,
		x=0,
		y=2,
		width=169,
		height=20,
		colour1=BLACKA(0),
		text=loc(TID_Multi_button_CB),
		wordwrap=false,
		font_name=Tahoma_13B ,
		font_colour=WHITEA(220),
		text_halign=ALIGN_MIDDLE,
		callback_mouseclick='showRank();',
	});
	
    setXYV(Multi_Room.SettingsRow,Multi_Room.PlayersRow.x,Multi_Room.PlayersRow.y);
    --SetWHV(Multi_Room.SettingsRow,MpScrW-20,MpScrH - (Multi_Room.Panel.height + 30 +60 +340));
	setWHV(Multi_Room.SettingsRow,Multi_Room.PlayersRow.width,MpScrH - (Multi_Room.Panel.height + 30 +60 +340));
    setXYV(Multi_Room.SettingsRow.Line,170+169,1);
	--SetWHV(Multi_Room.SettingsRow.Line,MpScrW-20-169-170,2);
	setWHV(Multi_Room.SettingsRow.Line,Multi_Room.PlayersRow.width-169-170,2);

    --SetWHV(Multi_Room.PlayersRow,MpScrW-20,MpScrH - (Multi_Room.Panel.height + 30 +60 +340));
	setWHV(Multi_Room.PlayersRow,Multi_Room.PlayersRow.width,MpScrH - (Multi_Room.Panel.height + 30 +60 +340));
	--SetWHV(room_playes_line,MpScrW-20-169,MpScrH - (Multi_Room.Panel.height + 30 +60 +340));
	setFontColour(setBut,RGBA(150,150,150,255));
	showBuildings();

	setVisible(Multi_Room.SettingsRow,false);
	setVisible(Multi_Room.PlayersRow,true);
	setFontColour(playersBut,WHITEA(255));
	setFontColour(setBut,RGBA(150,150,150,255));
	setFontColour(rankBut,RGBA(150,150,150,255));

	setXYWHV(Multi_Room.Chat.BG,getX(Multi_Room.Chat),getY(Multi_Room.Chat) - 25,146,getHeight(Multi_Room.Chat.BG));
	setXYWHV(Multi_Room.Chat.Button,getX(Multi_Room.Chat),getY(Multi_Room.Chat) - 23,146,getHeight(Multi_Room.Chat.Button));
	setXYWHV(Multi_Room.Chat.IRC.BG,getX(Multi_Room.Chat)+147,getY(Multi_Room.Chat) - 25,146,getHeight(Multi_Room.Chat.IRC.BG));
	setXYWHV(Multi_Room.Chat.IRC.Button,getX(Multi_Room.Chat)+147,getY(Multi_Room.Chat) - 23,146,getHeight(Multi_Room.Chat.IRC.Button));
	setXYWHV(Multi_Room.Chat.Line,146,1,getWidth(Multi_Room.Chat)-(146)+1,2);
	setXYWHV(Multi_Room.Chat.IRC.Line,0,1,147,2);
	setVisible(Multi_Room.Chat.IRC.Line2,false);

	setXYWHV(Multi_Room.RankedRow,Multi_Room.PlayersRow.x,Multi_Room.PlayersRow.y,Multi_Room.PlayersRow.width,MpScrH - (Multi_Room.Panel.height + 30 +60 +340));
	setWidth(Multi_Room.RankedRow.Line, 170+170);
	setXYWHV(Multi_Room.RankedRow.Line2, 170+170+170,1,getWidth(Multi_Room.RankedRow)-170-170-170,2);


  end;

function multiRoomReorganization()
	--	MultiDef.MultiMap.RESTRICTIONS.RESTRICTTECH
	--	MultiDef.MultiMap.RESTRICTIONS.RESTRICTBUILDINGS
	local restAvailable = 0;
	if MultiDef.MultiMap.RESTRICTIONS.RESTRICTTECH and MultiDef.MultiMap.RESTRICTIONS.RESTRICTBUILDINGS then
		restAvailable = 2;
	elseif  MultiDef.MultiMap.RESTRICTIONS.RESTRICTTECH or MultiDef.MultiMap.RESTRICTIONS.RESTRICTBUILDINGS then
		restAvailable =1;
	end;
	if MpScrW >= 2960 then
		if restAvailable == 2 then
			setX(Multi_Room.GameParameters, 0);

			setX(Multi_Room.Restriction,720);
			setWidth(Multi_Room.Restriction,1020);
			setVisible(Multi_Room.Restriction,true);
			setVisible(Multi_Room.Restriction.t_row,true);
			setVisible(Multi_Room.Restriction.b_row,true);
			setVisible(Multi_Room.Restriction.t_but,true);
			setVisible(Multi_Room.Restriction.b_but,true);
			setVisible(Multi_Room.Restriction.Line,true);
			setX(Multi_Room.Restriction.t_row,510);
			setX(Multi_Room.Restriction.t_but,515);
			setX(Multi_Room.Rests,720);
			setVisible(Multi_Room.Rests,true);

			setX(Multi_Room.MapInfo,720+1030);

			setX(Multi_Room.Chat,720+1030+520);

		elseif restAvailable == 1 then
			setX(Multi_Room.GameParameters, 255);

			setX(Multi_Room.Restriction,720+255);
			setWidth(Multi_Room.Restriction,510);

			setVisible(Multi_Room.Restriction,true);
			setX(Multi_Room.Restriction.t_row,0);
			setX(Multi_Room.Restriction.t_but,5);
			setX(Multi_Room.Rests,720+255);
			setVisible(Multi_Room.Rests,true);
			setVisible(Multi_Room.Restriction.Line,false);
			setX(Multi_Room.MapInfo,720+1030-255);

			setX(Multi_Room.Chat,720+1030+520-255);


			if MultiDef.MultiMap.RESTRICTIONS.RESTRICTTECH then
				setVisible(Multi_Room.Restriction.t_row,true);
				setVisible(Multi_Room.Restriction.b_row,false);
				setVisible(Multi_Room.Restriction.t_but,true);
				setVisible(Multi_Room.Restriction.b_but,false);
			else
				setVisible(Multi_Room.Restriction.t_row,false);
				setVisible(Multi_Room.Restriction.b_row,true);
				setVisible(Multi_Room.Restriction.t_but,false);
				setVisible(Multi_Room.Restriction.b_but,true);
			end;
		else
			setX(Multi_Room.GameParameters, 510+10);

			setVisible(Multi_Room.Restriction,false);
			setVisible(Multi_Room.Rests,false);

			setX(Multi_Room.MapInfo,720+1030-510-10);
			setX(Multi_Room.Chat,720+1030+520-510-10);
		end;
		setX(Multi_Room.Chat.BG,getX(Multi_Room.Chat));
		setX(Multi_Room.Chat.Button,getX(Multi_Room.Chat));
		setX(Multi_Room.Chat.IRC.BG,getX(Multi_Room.Chat)+170);
		setX(Multi_Room.Chat.IRC.Button,getX(Multi_Room.Chat)+170);

	elseif MpScrW >= 2450 then
		if restAvailable > 0 then
			setX(Multi_Room.Restriction,720);
			setVisible(Multi_Room.Restriction,true);
			setVisible(Multi_Room.Rests,true);
			setX(Multi_Room.MapInfo,720+520);
			setX(Multi_Room.Chat,720+520+520);
			setX(Multi_Room.GameParameters,10);
			if restAvailable == 2 then
				setX(Multi_Room.Restriction.t_but,170);
				setVisible(Multi_Room.Restriction.t_but,true);
				setVisible(Multi_Room.Restriction.b_but,true);
				setVisible(Multi_Room.Restriction.t_row.Line1, true);
				setVisible(Multi_Room.Restriction.t_row.Line2, true);
				setVisible(Multi_Room.Restriction.t_row.Line3, false);
			elseif MultiDef.MultiMap.RESTRICTIONS.RESTRICTBUILDINGS then
				setVisible(Multi_Room.Restriction.t_but,false);
				setVisible(Multi_Room.Restriction.b_but,true);
				showBuildings();
			else
				showTechs();
				setVisible(Multi_Room.Restriction.t_but,true);
				setVisible(Multi_Room.Restriction.b_but,false);
				setX(Multi_Room.Restriction.t_but,5);
				setVisible(Multi_Room.Restriction.t_row.Line1, false);
				setVisible(Multi_Room.Restriction.t_row.Line2, false);
				setVisible(Multi_Room.Restriction.t_row.Line3, true);
			end;

		else
			setX(Multi_Room.GameParameters, 255+10);

			setVisible(Multi_Room.Restriction,false);
			setVisible(Multi_Room.Rests,false);

			setX(Multi_Room.MapInfo,720+520-255-10);
			setX(Multi_Room.Chat,720+520+520-255-10);
		end;

		setX(Multi_Room.Chat.BG,getX(Multi_Room.Chat));
		setX(Multi_Room.Chat.Button,getX(Multi_Room.Chat));
		setX(Multi_Room.Chat.IRC.BG,getX(Multi_Room.Chat)+170);
		setX(Multi_Room.Chat.IRC.Button,getX(Multi_Room.Chat)+170);

	else
		if restAvailable > 0 then
			--setVisible(Multi_Room.Restriction,true);
			setVisible(Multi_Room.Rests,true);
			setVisible(Multi_Room.RestsBG,true);
			if restAvailable == 2 then
				setX(Multi_Room.Restriction.t_but,170);
				setVisible(Multi_Room.Restriction.t_but,true);
				setVisible(Multi_Room.Restriction.b_but,true);
				setVisible(Multi_Room.Restriction.t_row.Line1, true);
				setVisible(Multi_Room.Restriction.t_row.Line2, true);
				setVisible(Multi_Room.Restriction.t_row.Line3, false);
				setVisible(Multi_Room.Restriction.t_row,false);
				setVisible(Multi_Room.Restriction.b_row,true);
				setFontColour(Multi_Room.Restriction.b_but,WHITEA(255));
				setFontColour(Multi_Room.Restriction.t_but,RGBA(200,200,200,255));
			elseif MultiDef.MultiMap.RESTRICTIONS.RESTRICTBUILDINGS then
				setVisible(Multi_Room.Restriction.t_but,false);
				setVisible(Multi_Room.Restriction.b_but,true);
				--showBuildings();
				setVisible(Multi_Room.Restriction.b_row,true);
				setVisible(Multi_Room.Restriction.t_row,false);
				setFontColour(Multi_Room.Restriction.b_but,WHITEA(255));
			else
				--showTechs();
				setVisible(Multi_Room.Restriction.t_but,true);
				setVisible(Multi_Room.Restriction.b_but,false);
				setX(Multi_Room.Restriction.t_but,5);
				setVisible(Multi_Room.Restriction.t_row.Line1, false);
				setVisible(Multi_Room.Restriction.t_row.Line2, false);
				setVisible(Multi_Room.Restriction.t_row.Line3, true);
				setVisible(Multi_Room.Restriction.t_row,true);
				setVisible(Multi_Room.Restriction.b_row,false);
				setFontColour(Multi_Room.Restriction.t_but,WHITEA(255));
			end;
		else
			if isVisible(Multi_Room.Restriction) then
				showMap();
			end;
			setVisible(Multi_Room.Restriction,false);
			setVisible(Multi_Room.Rests,false);
			setVisible(Multi_Room.RestsBG,false);

		end;
	end;

end;
------------------------  Functions -----------------------------
--- Idk what is it, but it's necesary

Multi_Room.TeamList = AddElement({
	type=TYPE_SCROLLBOX,
	parent=Multi_Room.Panel,
	anchor={top=false,bottom=true,left=true,right=true},
	x=0,
	y=2,
	width=130,
	height=Multi_Room.Panel.height-4-20,
	colour1=BLACKA(200);
	text='bla bla bla bla bla!',
	font_name=Tahoma_12B ,
	visible= false
});

---------------------- Set for OW ------------------------------
OW_ROOM_SETUP(Multi_Room.Chat.TextBox.ID,ChangeMapWindow.ChangeMap.Chat.TextBox.ID,Multi_Room.Panel.Status.ID,Multi_Room.TeamList.ID);
OW_IRC_BOXES(IRCTEXTBOX.ID,IRCLISTBOX.ID,Multi_Room.Chat.IRC.TextBox.ID,Multi_Room.Chat.IRC.LISTBOX.ID);  -- in lobby caused error becouse multi_room isn't exist at time
---------------------- GET FROM OW -----------------------------
function FROMOW_MULTIROOM_TIMEOUT() -- Called by OW
    hideGameRoom();
end;

function FROMOW_MULTIROOM_CONNSTATUS_NOTJOINED() -- Called by OW
	hideGameRoom();
end;

function FROMOW_MULTIROOM_UPDATE_MAP_NAME(Data) -- called by game when multiplayer map name/gametype changes
--[[ DATA Breakdown
 MAP	   - Map Name	       (Send when requesting map change)
 GAMETYPE      - GameType Name	  (Send when requesting map change)
 MAPLOC	- Localized Map Name
 GAMETYPELOC   - Localized Game Type
--]]

	MultiDef.MapName = Data;
	--setText(Multi_Room.Panel.MapTitle,MultiDef.MapName.MAPLOC..' - '..MultiDef.MapName.GAMETYPELOC);

	setInformationsName(MultiDef.MapName.MAPLOC,MultiDef.MapName.GAMETYPELOC);
	setInformationsMapic(MultiDef.MapName.MAP);

	--setInitInformationForCM(MultiDef.MapName);
	ToOW_getCurrentMapInfo();
end;
--for now
include('Menu/multiroom_mapscoor');

function FROMOW_MULTIROOM_GET_MAP_INFO_CALLBACK(Data)       -- získání vygenerovaných informací o gametypu
--[[ DATA Breakdown
    ** MULTIMAP Breakdown
	GAMETYPE	  Integer
	DESCRIPTION      WideString
	RULES	  WideString
	RANDOMNATIONS      Boolean
	ADDCOMPBUTTON     Boolean
	CANSPEC	   Boolean
	MAPPARAMCOUNT     Integer
	RESTRICTTECH	   Boolean
	RESTRICTBUILDINGS	   Boolean

	MAPPARAMS [1..MAPPARAMCOUNT] of
		COUNT     Integer
	    VALUE	  default
	    ITEMS of
			NAMES [1..COUNT] of WideString
			HINTS [1..COUNT] of WideString
	    HINTS [1..2] of WideString	
    **
--]]
	if ifWantSelectMapScreen == true then
		if cache_rules[changedMap.NAME..changedGT.NAME] == nil then
			cache_rules[changedMap.NAME..changedGT.NAME] = Data.MULTIMAP.DESCRIPTION..'\n\n'..Data.MULTIMAP.RULES;
		end;
			set_ChangeMap_setDesc(Data.MULTIMAP.DESCRIPTION..'\n\n'..Data.MULTIMAP.RULES);
	else
		MultiDef.MultiMap = Data.MULTIMAP;
		setInformationsRules(MultiDef.MultiMap.DESCRIPTION,MultiDef.MultiMap.RULES);
		--set_ChangeMap_setDesc(Data.MULTIMAP.DESCRIPTION..'\n\n'..Data.MULTIMAP.RULES);

		local mapName = string.upper(MultiDef.MapName.MAP);
		local gameName = string.upper(MultiDef.MapName.GAMETYPE);
		
		if gameName == nil then gameName = ''; end;
		if mapName == nil then mapName = ''; end;
		--- for now, -1 is System, 0 isn't used, 1 List box, 2 Radio, 3 Progress, 4 Progress with Dissable, 5 Progres from List, 6 Progres from list with Disable
--		if ifIsServer then
--			local toCheck = false;
--		
--			if mapLockedByAchiev[mapName] then
--				toCheck = mapLockedByAchiev[mapName];
--			elseif mapLockedByAchiev[mapName.. "+".. gameName] then
--				toCheck = mapLockedByAchiev[mapName .. "+".. gameName];
--			end;
--			
--			if toCheck then
--				--setInformationsName(toCheck,"");
--				local achieved = checkAchieved(mapLockedByAchiev[toCheck]);			-- Caussing crash
--				if not achievements[mapLockedByAchiev[toCheck]] or achieved == false then
--					toCheck = false;
--				else
--					toCheck = true;
--				end;
--			else
--				toCheck = false;
--			end;
--			if toCheck == true then
--				showChangeMap(true);
--			end;
--		end;
		
		for i=1, MultiDef.MultiMap.MAPPARAMCOUNT do
			if SHVFMP[i] then
				MultiDef.MultiMap.MAPPARAMS[i].TYPE = -1;		-- Using for Interface and inside actions
			elseif MultiDef.MultiMap.MAPPARAMS[i] ~= 0 and MultiDef.MultiMap.MAPPARAMS[i].ITEMS.COUNT == 0 then
				MultiDef.MultiMap.MAPPARAMS[i].TYPE = 0;		-- Isn't used
			elseif MultiDef.MultiMap.MAPPARAMS[i].TYPE == 0 then
				ToOW_Host_setMapParam(i,0); 		-- Isn't used in SGUI
			elseif MultiDef.MultiMap.MAPPARAMS[i].TYPE >= 3 and getMapOptionsAddInfo(mapName,gameName) and getMapOptionsAddInfo(mapName,gameName)[i-1] then
				local MPAI = getMapOptionsAddInfo(mapName,gameName)[i-1];

					MultiDef.MultiMap.MAPPARAMS[i].ENABLENAME = MPAI.ENABLENAME;
					MultiDef.MultiMap.MAPPARAMS[i].ENABLEHINT = MPAI.ENABLEHINT;
					MultiDef.MultiMap.MAPPARAMS[i].MINNAME = MPAI.MINNAME;
					MultiDef.MultiMap.MAPPARAMS[i].MINHINT = MPAI.MINHINT;
					MultiDef.MultiMap.MAPPARAMS[i].MAXNAME = MPAI.MAXNAME;
					MultiDef.MultiMap.MAPPARAMS[i].MAXHINT = MPAI.MAXHINT;
					MultiDef.MultiMap.MAPPARAMS[i].VALUESTRING = MPAI.VALUESTRING;
					MultiDef.MultiMap.MAPPARAMS[i].MIN = MPAI.MIN;
					MultiDef.MultiMap.MAPPARAMS[i].MAX = MPAI.MAX;
					MultiDef.MultiMap.MAPPARAMS[i].STEP = MPAI.STEP;
					MultiDef.MultiMap.MAPPARAMS[i].ITEMS.IMGS = MPAI.IMGS;

					if MPAI.VALUE then
						MultiDef.MultiMap.MAPPARAMS[i].VALUE = MPAI.VALUE;
						ToOW_Host_setMapParam(i,MultiDef.MultiMap.MAPPARAMS[i].VALUE);  -- becouse overrided value can be different then setted
					end;
			elseif MultiDef.MultiMap.MAPPARAMS[i].TYPE == 3 or MultiDef.MultiMap.MAPPARAMS[i].TYPE == 4 then	-- if snt exist AddInfo for type 3 and 4 must by changed to radio or listbox
					if MultiDef.MultiMap.MAPPARAMS[i].ITEMS.COUNT <= 2 then
						MultiDef.MultiMap.MAPPARAMS[i].TYPE = 2;		-- Radio
					else 
						MultiDef.MultiMap.MAPPARAMS[i].TYPE = MultiDef.MultiMap.MAPPARAMS[i].TYPE+2;		-- Ranges from list
					end;

			elseif MultiDef.MultiMap.MAPPARAMS[i].TYPE == 2 then
				if MultiDef.MultiMap.MAPPARAMS[i].ITEMS.COUNT > 4 then
					MultiDef.MultiMap.MAPPARAMS[i].TYPE = 1;
				end;

			elseif MultiDef.MultiMap.MAPPARAMS[i].ITEMS.COUNT <= 2 and MultiDef.MultiMap.MAPPARAMS[i] ~= 0 then
				MultiDef.MultiMap.MAPPARAMS[i].TYPE = 2;		-- Radio
			end;

		end;

		multiplayer_setSettings ( MultiDef.MultiMap.MAPPARAMS);

		if string.find(gameName, 'UNITED NATIONS') then
			MultiDef.MultiMap.UNITED = true;
		else
			MultiDef.MultiMap.UNITED = false;
		end;

		if not (MultiDef.MultiMap.UNITED) then
			MultiDef.NationsText = {[1]= loc(TID_Multi_Random), [2] = loc(TID_Multi_nation1), [3]= loc(TID_Multi_nation2), [4]= loc(TID_Multi_nation3)};
			MultiDef.NationsIcons = {[1]= 'SGUI/Alien/multiplayer/NationR.png', [2] = 'am.png', [3]= 'ar.png', [4]= 'ru.png'};
		else
			MultiDef.NationsText = {[1]= loc(TID_Multi_Random), [2] = loc(TID_Multi_nation1)..'+'.. loc(TID_Multi_nation2), [3]= loc(TID_Multi_nation2) ..'+'.. loc(TID_Multi_nation3), [4]= loc(TID_Multi_nation3) ..'+'.. loc(TID_Multi_nation1)};
			MultiDef.NationsIcons = {[1]= 'SGUI/Alien/multiplayer/NationR.png', [2] = 'amar.png', [3]= 'arru.png', [4]= 'ruam.png'};
		end;

		-- Will be loaded from WRI
		MultiDef.MultiMap.MODIFEDNATIONS = getMapsModifeNations(mapName, gameName);

		if MultiDef.MultiMap.MODIFEDNATIONS == nil then
			MultiDef.MultiMap.MODIFEDNATIONS = {};
		end;


		--MultiDef.MultiMap.ADDCOMPBUTTON = 1;			--for test

		-- check if random nation
		if (not MultiDef.MultiMap.RANDOMNATIONS) then
			updateRandNats(Multi_Room.GameParameters.GameActions.Nats.Checked,false);
			setMapParam_checkbox(op_RandNats ,Multi_Room.GameParameters.GameActions.Nats.Checked);
		else
			updateRandNats(true,true);
			setMapParam_checkbox(op_RandNats ,true);
		end;

		-- Restrictions
		MultiDef.MultiMap.RESTRICTIONS = getRestritions(mapName, gameName);

		if MultiDef.MultiMap.RESTRICTIONS == nil then
			MultiDef.MultiMap.RESTRICTIONS = {};
		end;
		
		
		MultiDef.MultiMap.RESTRICTIONS.RESTRICTTECH = Data.MULTIMAP.RESTRICTTECH;
		MultiDef.MultiMap.RESTRICTIONS.RESTRICTBUILDINGS = Data.MULTIMAP.RESTRICTBUILDINGS;

		if (not MultiDef.MultiMap.RESTRICTIONS.RESTRICTTECH) then
			MultiDef.MultiMap.RESTRICTIONS.LOCKED_T = {};
			for i=1, rest_t_count-1 do
				MultiDef.MultiMap.RESTRICTIONS.LOCKED_T[i] = true;
			end;
			MultiDef.MultiMap.RESTRICTIONS.LOCKED_T_OPT = {};
		end;

		if (not MultiDef.MultiMap.RESTRICTIONS.RESTRICTBUILDINGS) then
			MultiDef.MultiMap.RESTRICTIONS.LOCKED_B = {};
			for i=1, rest_b_count do
				MultiDef.MultiMap.RESTRICTIONS.LOCKED_B[i-1] = true;
			end;
			MultiDef.MultiMap.RESTRICTIONS.LOCKED_B_OPT = {};
		end;

		loadRestritionFromGameType();

		-- Tech Limits
		MultiDef.MultiMap.TECHLIMIT = getTechnologyLimits(mapName, gameName);

		if ifIsServer then
			if MultiDef.MultiMap.TECHLIMIT then
				updateTechLimit(MultiDef.MultiMap.TECHLIMIT[2],MultiDef.MultiMap.TECHLIMIT[1]);
				setMapParam_checkbox(op_TechLimit ,MultiDef.MultiMap.TECHLIMIT[2]);
				setVisible(Multi_Room.GameParameters.GameActions.TechLimit.Query,false);
			else
				updateTechLimit(false,true);
				setMapParam_checkbox(op_TechLimit ,false);
				setVisible(Multi_Room.GameParameters.GameActions.TechLimit.Query,true);
			end;
		end;

		multiRoomReorganization();
	 --[[
	Data.TEAMDEF, Data.SIDEDEF
	TEAMDEF[1..9] of			1 isn't team
		NAME	String				without name isn't avalible 		1 hasn't name
		SIDESMIN    Integer
		SIDESMAX    Integer
		ASSIGNED_POSITIONS[1..8] of Boolean
		ASSIGNED_POSITIONS_COUNT    Integer

	SIDEDEF[1..8] of
		NAME	String
		ENABLED	Boolean
		NATIONS of
			AR    Boolean
			US    Boolean
			RU    Boolean
		--]]

		MultiDef.TeamDef = Data.TEAMDEF;
		MultiDef.SideDef = Data.SIDEDEF;

		-- Will be loaded from WRI
		MultiDef.MultiMap.POSCOORS = getMapsCoors(mapName,gameName);

		createCoors(MultiDef.MultiMap.POSCOORS);


		teamGame = false;
		for i=2, 9 do
			if MultiDef.TeamDef[i].NAME ~= '' then
				teamGame = true;
				break;
			end;
		end;
		--reconstructPlayers();

		MultiDef.TeamDef.LOCKEDTEAMS = getLockedTeams(mapName, gameName);
		if (not MultiDef.TeamDef.LOCKEDTEAMS) then
			updateLockTeams(Multi_Room.GameParameters.GameActions.Nats.Checked,false);
		else
			updateLockTeams(MultiDef.TeamDef.LOCKEDTEAMS[2],MultiDef.TeamDef.LOCKEDTEAMS[1]);
			setMapParam_checkbox(op_LockTeams ,MultiDef.TeamDef.LOCKEDTEAMS[1]);
		end;
		--setText(version,gameName);
		-- reset my nation, becouse isn't called automatically
		setNation(0);
		
		-- automatic kick comps when gametype is changed
		if Players then
			for k, v in pairs(Players) do
				if v.ISCOMP then
					ToOW_Host_KickPlayer(v.ID);
				end;
			end;
		end;
	end;

end;

function fixAVATAR(AVATAR)
	local j = 0;
	for j=2,13 do
		if AVATAR[j] <= 0 then
			AVATAR[j] = 1;
		end;
	end;

	AVATAR[1]  = 0;
	AVATAR[12] = 0;
	AVATAR[14] = 0;

	return AVATAR;
end;

function MULTIROOM_FREEAVATARS()
	if Players ~= nil then
		for i=1, table.getn(Players) do
			if (Players[i] ~= nil) and (Players[i].AVATARTEX ~= nil) and (Players[i].AVATARTEX > 0) then
				OW_XICHT_PORTRAIT_FREETEXTURE(Players[i].AVATARTEX);
			end;
		end;
	end;
end;

TEAMLIST = {};

function FROMOW_MULTIROOM_TEAMLIST(Data)
--[[
DATA Breakdown

 PLAYERCOUNT Integer
 PLAYERSMYPOS Integer
 PLAYERS [1..12] of
	  NAME String
	  ALIVE Boolean
	  ISCOMP Boolean
	  NATION Integer
	  TEAM Integer
	  TEAMPOS Integer
	  SIDE Integer
	  COLOUR Integer
	  READY Boolean
	  LOCKED Boolean
	  TEAMREADY Boolean
	  PLID Integer (PLAYER ID)
	  ISSPEC Boolean
	  ISDEDI Boolean
	  AVATAR Array [1..14] of Byte
	  AVATARSEX Byte
		  --]]

	TEAMLIST = Data;
		  
	MULTIROOM_FREEAVATARS();

	Players = {};
	Spectators = {};
	MyID = 0;
	APositions = {};
	AColours = {};
	firstPlayer = nil;
	local tpos =  {};

	local im = false;
	local wm = nil;

	resetDepotsColours();

	if Data.PLAYERS ~= nil then
		MyID = Data.PLAYERS[Data.PLAYERSMYPOS+1].PLID;

	local tmp = '';

		for i=1, 12 do
	    tmp = tostring(Data.PLAYERS[i].TEAM) ..'+'.. tostring(Data.PLAYERS[i].TEAMPOS);

	    if (tpos[tmp] == nil) or (not Data.PLAYERS[i].ALIVE) or (Data.PLAYERS[i].ISSPEC) then
				im = false;
				wm = nil;
				if ((Data.PLAYERS[i].ALIVE) or (Data.PLAYERS[i].ISCOMP)) and not (Data.PLAYERS[i].ISSPEC) then
					tpos[tmp] = Data.PLAYERS[i].PLID;
				end;
			else
				im = true;
				wm = tpos[tmp];
		--Players[tpos[tmp]].IsMerged   = true;

		Players[tpos[tmp]].withMerged = tpos[tmp];
			end;

			if (Data.PLAYERS[i].ALIVE == true --[[or Data.PLAYERS[i].ISCOMP == true--]]) then
				Data.PLAYERS[i].AVATAR = fixAVATAR(Data.PLAYERS[i].AVATAR);

				Players[Data.PLAYERS[i].PLID] = {
								ID = Data.PLAYERS[i].PLID,
								Name = Data.PLAYERS[i].NAME,
								Nation = Data.PLAYERS[i].NATION,				-- 0 = random
								Side = Data.PLAYERS[i].SIDE,					-- Perharp position
								Team = Data.PLAYERS[i].TEAM,
								tPos = Data.PLAYERS[i].TEAMPOS,
								Lock = Data.PLAYERS[i].LOCKED,
								Ready = Data.PLAYERS[i].READY,
								Colour = Data.PLAYERS[i].COLOUR,
								IsSpec = Data.PLAYERS[i].ISSPEC,
								IsServer = Data.PLAYERS[i].ISDEDI or (i == 1),
								IsComp = Data.PLAYERS[i].ISCOMP,
								AVATAR = Data.PLAYERS[i].AVATAR,
								AVATARSEX = Data.PLAYERS[i].AVATARSEX,
								AVATARTEX = makeAVATARTEX(0,Data.PLAYERS[i].AVATAR,Data.PLAYERS[i].AVATARSEX,Data.PLAYERS[i].NATION),
								IsMerged = im,
								withMerged = wm,		-- ID of player
								};


				--if firstPlayer == nil and not (Data.PLAYERS[i].ISSPEC) then
				--	firstPlayer = Data.PLAYERS[i].PLID;
				--end;

				if not (Data.PLAYERS[i].ISSPEC) and Data.PLAYERS[i].SIDE ~= 0 then
					setDepotColour(Data.PLAYERS[i].SIDE,Data.PLAYERS[i].COLOUR,Data.PLAYERS[i].TEAM);
				end;

				if Data.PLAYERS[i].SIDE > 0 then
					APositions[Data.PLAYERS[i].SIDE] = true;
				end;

				if Data.PLAYERS[i].COLOUR > 0 then
					AColours[Data.PLAYERS[i].COLOUR] = true;
				end;

			end;
		end;
		--[[
		if Players[MyID] ~= nil then
			if (Players[MyID].IsSpec) then
				tmp = tostring(Players[MyID].Team) ..'+'.. tostring(Players[MyID].tPos);
				if (tpos[tmp] ~= nil) then
					if firstPlayer ~= nil then
						OW_MULTIROOM_SET_MYTEAMANDPOS(Players[firstPlayer].Team,Players[firstPlayer].tPos);
					end;
				end;
			end;
		end;
		--]]

		if Players[MyID] ~= nil then
			if (Players[MyID].IsSpec) then
				if (teamGame) then
					if Players[MyID].Team ~= 1 and Players[MyID].tPos ~= 7 then
						OW_MULTIROOM_SET_MYTEAMANDPOS(1,7);
					end;
				else
					if Players[MyID].Team ~= 0 and Players[MyID].tPos ~= 7 then
						OW_MULTIROOM_SET_MYTEAMANDPOS(0,7);
					end;
				end;
			end;

			if Multi_Room.GameParameters.GameActions.Poss.Checked and Players[MyID].Side ~= 0 then
				ToOW_setMySide(0);
			end;

			if Multi_Room.GameParameters.GameActions.Nats.Checked and Players[MyID].Nation ~= 0 then
				ToOW_setMyNation(0);
			end;

			if Multi_Room.GameParameters.GameActions.Colours.Checked and Players[MyID].Colour ~= 0 then
				ToOW_setMyColour(0);
			end;

			setText(Multi_Room.Profile.MPName,Players[MyID].Name);

		refreshPlayers();
		end;
	end;
end;

function FROMOW_MULTIROOM_UPDATE_MAP_SETTINGS(Data) -- called by game when map settings are refreshed (Not nessisarily changed)
--[[ DATA Breakdown
 MAPPARAMS      - A List of the Map Parameter values [1..MAPPARAMCOUNT]
 MAPPARAMCOUNT  - Amount of Parameters
--]]

	MultiDef.MapParams     = Data.MAPPARAMS;
	MultiDef.MapParamCount = Data.MAPPARAMCOUNT;

	setMapParams_toInterface(MultiDef.MapParams,MultiDef.MapParamCount);


end;

function FROMOW_MULTIROOM_UPDATE_MAP_LIST(Data) -- called by game when its building the map list (When you join the game room)
--[[ DATA Breakdown
 MAPLIST       - A List of the Maps [1..MAPLISTCOUNT] = {NAME=,NAMELOC=}
 MAPLISTCOUNT  - Amount of Maps
--]]
	MultiDef.MapList      = Data.MAPLIST;
	MultiDef.MapListCount = Data.MAPLISTCOUNT;
	--setInformationsRules(tostring(MultiDef.MapList[1]),'')
	sgui_deletechildren(ChangeMapWindow.multiplayer_map_scrollbox.ID);
	for i=1, MultiDef.MapListCount  do
		MPmaps_listbox_add(MultiDef.MapList[i],i);
	end;
	--showChangeMap();
end;

function FROMOW_MULTIROOM_UPDATE_MAP_GAMETYPE_LIST(Data) -- called by game when its building the map list (When you join the game room)
--[[ DATA Breakdown
 GAMETYPELIST       - A List of the Game Types of the current map [1..GAMETYPELISTCOUNT] = {NAME=,NAMELOC=}
 GAMETYPELISTCOUNT  - Amount of Game Types of the current map
--]]
	MultiDef.GameTypeList  = Data.GAMETYPELIST;
	MultiDef.GameTypeCount = Data.GAMETYPELISTCOUNT;
	--[[
	for i=1, MultiDef.GameTypeCount  do
		MPmaps_listbox_add(MultiDef.GameTypeList[i],i);
	end; ]]--
--	sgui_deletechildren(ChangeMapWindow.multiplayer_gametype_scrollbox.ID);
--	for i=1, MultiDef.GameTypeCount  do
--		MPGT_listbox_add(MultiDef.GameTypeList[i],i);
--	end;


end;

function FROMOW_MULTIROOM_GET_MAP_GAMETYPES_CALLBACK(Data)
--[[ DATA Breakdown
 GAMETYPELIST       - A List of the Game Types of the current map [1..GAMETYPELISTCOUNT] = {NAME=,NAMELOC=}
 GAMETYPELISTCOUNT  - Amount of Game Types of the current map
--]]
	MultiDef.GameTypeList  = Data.GAMETYPELIST;
	MultiDef.GameTypeCount = Data.GAMETYPELISTCOUNT;
	--[[
	for i=1, MultiDef.GameTypeCount  do
		MPmaps_listbox_add(MultiDef.GameTypeList[i],i);
	end; --]]
	sgui_deletechildren(ChangeMapWindow.multiplayer_gametype_scrollbox.ID);

	if taskOnGT then
		for i=1, MultiDef.GameTypeCount  do
			MPGT_listbox_add2(MultiDef.GameTypeList[i],i);
		end;
	else
		for i=1, MultiDef.GameTypeCount  do
			MPGT_listbox_add(MultiDef.GameTypeList[i],i);
		end;
	end;
end;

function FROMOW_CLANBASE_BADSID(PLAYERID)
	if Players[PLAYERID] then
		OW_MULTI_SENDALLCHATMSG(loc_format(TID_Multi_CB_PlayerNotLoggedIn,{Players[PLAYERID].Name}));
	end;
end;

function FROMOW_ROOM_LAUNCH_ERROR(ID,ERROR)
	Err(ERROR);
end;

function ToOW_getGTList(MapName)
	OW_MULTIROOM_GET_MAP_GAMETYPES(MapName);
end;

function ToOW_getCurrentMapInfo()
	OW_MULTIROOM_GET_CURRENT_MAP_INFO();
end;

function ToOW_Host_setMapParam(id,value)
	if type(id) == 'number' then
		OW_MULTIROOM_HOST_SET_MAPPARAM (id-1,value);
	end;
end;

function ToOW_Host_setMap(map,gt)
	OW_MULTIROOM_HOST_SET_MAP(map,gt);
end;

function ToOW_Host_setClanBase(bool)
	OW_MULTIROOM_HOST_SET_CLANBASEUSED(bool);
end;

function ToOW_RankedLogin(name,pass)
	res = OW_MULTIROOM_CLANBASE_LOGIN(name,pass);
	
	if res == 'OK' then
		setEnabled(Multi_Room.RankedRow.LoginRow.LoginBut,false);
		setEnabled(Multi_Room.RankedRow.LoginRow.Password,false);
		setEnabled(Multi_Room.RankedRow.LoginRow.Name,false);
		setText(Multi_Room.RankedRow.LoginRow.State, loc(TID_Multi_CB_Status) ..' ' .. loc(TID_Multi_CB_LoggedIn)); 
	else
		Err(res);
	end;
end;

function ToOW_Host_setServerLock(bool)
	OW_MULTIROOM_HOST_SET_SERVERLOCKED(bool);
end;


function ToOW_Host_KickPlayer(player)
	OW_MULTIROOM_HOST_KICKPLAYER(player);
end;

function ToOW_Host_AddComputer(team)
	OW_MULTIROOM_HOST_ADDCOMPUTER(team);
end;

function ToOW_setMyName(name)
	OW_MULTIROOM_SET_MYNAME(name);
end;

function ToOW_setMyNation(nation)
	OW_MULTIROOM_SET_MYNATION(nation);
end;

function ToOW_setMySide(side)
	OW_MULTIROOM_SET_MYSIDE(side);
end;

function ToOW_setMyTPos(team,tPos)
	OW_MULTIROOM_SET_MYTEAMANDPOS(team,tPos); -- (1, -1); (-1 pro automatickou pozici)
end;

function ToOW_setMyTeam(team)
	OW_MULTIROOM_SET_MYTEAM(team);
end;

function ToOW_setMyLock(bool)
	OW_MULTIROOM_SET_MYLOCKED(bool);
end;

function ToOW_setMyReady(bool)
	OW_MULTIROOM_SET_MYREADY(bool);
end;

function ToOW_setMyColour(colour)
	OW_MULTIROOM_SET_MYCOLOUR(colour);
end;

function ToOW_setMyAvatar(avatar)
	OW_MULTIROOM_SET_MYAVATAR(avatar);
end;

function ToOW_setMyIsSpec(bool)
	OW_MULTIROOM_SET_MYISSPEC(bool);
end;

function ToOW_getMapInfo(map,gt)
	OW_MULTIROOM_GET_MAP_INFO(map,gt)
end;

function ToOW_SteamInvite()
	--- function not exist
end;

function ToOW_setMyHeroName(name)
	--- function not exist
end;

function ToOW_setMyBaseName(name)
	--- function not exist
end;

-------------------------------------- Other Functions -------------------------
function setMapParam_checkbox(id,cbv)
	local value;
	if cbv == true then
		value = 1;
	else
		value = 0;
	end;
	ToOW_Host_setMapParam(id,value);
end;

function setMapParams_toInterface(mapParams,count)
	for i=1, count do
			if not SHVFMP[i] then
				optionChange(i,mapParams[i]);
			else
				multiplayer_setRoomSettings(i,mapParams[i]);
			end;
	end;
end;

function multiplayer_setRoomSettings(id,value)
	local bool;

	if value == 0 then
		bool = false;
	else
		bool = true;
	end;

	if id== op_TechLimit then
		updateTechLimit(bool, Multi_Room.GameParameters.GameActions.TechLimit.Locked);
	elseif id== op_LockGame then
		updateLock( bool);
	elseif id== op_RndPosition then
		updateRandPoss( bool, Multi_Room.GameParameters.GameActions.Poss.Locked);
	elseif id== op_RndColour then
		updateRandColours( bool, Multi_Room.GameParameters.GameActions.Colours.Locked);
	elseif id==op_RndNation then
		updateRandNats( bool, Multi_Room.GameParameters.GameActions.Nats.Locked);
	elseif id==op_LockTeams then
		updateLockTeams( bool, Multi_Room.GameParameters.GameActions.LockTeams.Locked);
	elseif id==op_Ranked then
		updateRanked(bool);
	end;
end;

function ChangeLock()
	if ifIsServer then
		local check;
		if Multi_Room.GameParameters.GameActions.LockRoom.Checked then
			check = false;
		else
			check = true;
		end;

		setMapParam_checkbox(op_LockGame ,check);
		ToOW_Host_setServerLock(check);

		updateLock(check);
	end;
end;

function updateLock(check)

	Multi_Room.GameParameters.GameActions.LockRoom.Checked = check;

	if check then
		setVisible(Multi_Room.GameParameters.GameActions.LockRoom.Checker,true);
	else
		setVisible(Multi_Room.GameParameters.GameActions.LockRoom.Checker,false);
	end;

end;

function ChangeRandColours()
	if ifIsServer and not Multi_Room.GameParameters.GameActions.Colours.Locked then
		local check;
		if Multi_Room.GameParameters.GameActions.Colours.Checked then
			check = false;
		else
			check = true;
		end;

		setMapParam_checkbox(op_RndColour ,check);

		updateRandColours(check,false);
	end;
end;

function updateRandColours(check,lock)

	Multi_Room.GameParameters.GameActions.Colours.Checked = check;
	if check then
		setVisible(Multi_Room.GameParameters.GameActions.Colours.Checker,true);
	else
		setVisible(Multi_Room.GameParameters.GameActions.Colours.Checker,false);
	end;
	if Multi_Room.GameParameters.GameActions.Colours.Lockable then
		Multi_Room.GameParameters.GameActions.Colours.Locked = lock;
		if lock then
			setVisible(Multi_Room.GameParameters.GameActions.Colours.Locker,true);
			setFontColour(Multi_Room.GameParameters.GameActions.Colours.Name, RGBA(200,200,200,255));
		else
			setVisible(Multi_Room.GameParameters.GameActions.Colours.Locker,false);
			setFontColour(Multi_Room.GameParameters.GameActions.Colours.Name, RGBA(255,255,255,255));
		end;
	end;
end;

function ChangeRandNats()
	if ifIsServer and not Multi_Room.GameParameters.GameActions.Nats.Locked then
		local check;
		if Multi_Room.GameParameters.GameActions.Nats.Checked then
			check = false;
		else
			check = true;
		end;

		setMapParam_checkbox(op_RndNation ,check);

		updateRandNats(check,false);
	end;
end;

function updateRandNats(check,lock)

	Multi_Room.GameParameters.GameActions.Nats.Checked = check;
	if check then
		setVisible(Multi_Room.GameParameters.GameActions.Nats.Checker,true);
	else
		setVisible(Multi_Room.GameParameters.GameActions.Nats.Checker,false);
	end;
	if Multi_Room.GameParameters.GameActions.Nats.Lockable then
		Multi_Room.GameParameters.GameActions.Nats.Locked = lock;
		if lock then
			setVisible(Multi_Room.GameParameters.GameActions.Nats.Locker,true);
			setFontColour(Multi_Room.GameParameters.GameActions.Nats.Name, RGBA(200,200,200,255));
		else
			setVisible(Multi_Room.GameParameters.GameActions.Nats.Locker,false);
			setFontColour(Multi_Room.GameParameters.GameActions.Nats.Name, RGBA(255,255,255,255));
		end;
	end;
end;

function ChangeRandPoss()
	if ifIsServer and not Multi_Room.GameParameters.GameActions.Poss.Locked then
		local check;
		if Multi_Room.GameParameters.GameActions.Poss.Checked then
			check = false;
		else
			check = true;
		end;

		setMapParam_checkbox(op_RndPosition ,check);

		updateRandPoss(check,false);
	end;
end;

function updateRandPoss(check,lock)

	Multi_Room.GameParameters.GameActions.Poss.Checked = check;
	if check then
		setVisible(Multi_Room.GameParameters.GameActions.Poss.Checker,true);
	else
		setVisible(Multi_Room.GameParameters.GameActions.Poss.Checker,false);
	end;
	if Multi_Room.GameParameters.GameActions.Poss.Lockable then
		Multi_Room.GameParameters.GameActions.Poss.Locked = lock;
		if lock then
			setVisible(Multi_Room.GameParameters.GameActions.Poss.Locker,true);
			setFontColour(Multi_Room.GameParameters.GameActions.Poss.Name, RGBA(200,200,200,255));
		else
			setVisible(Multi_Room.GameParameters.GameActions.Poss.Locker,false);
			setFontColour(Multi_Room.GameParameters.GameActions.Poss.Name, RGBA(255,255,255,255));
		end;
	end;
end;

function ChangeTechLimit()
	if ifIsServer and not Multi_Room.GameParameters.GameActions.TechLimit.Locked then
		local check;
		if Multi_Room.GameParameters.GameActions.TechLimit.Checked then
			check = false;
		else
			check = true;
		end;

		setMapParam_checkbox(op_TechLimit ,check);

		updateTechLimit(check,false);
	end;
end;

function updateTechLimit(check,lock)

	Multi_Room.GameParameters.GameActions.TechLimit.Checked = check;
	if check then
		setVisible(Multi_Room.GameParameters.GameActions.TechLimit.Checker,true);
	else
		setVisible(Multi_Room.GameParameters.GameActions.TechLimit.Checker,false);
	end;
	if Multi_Room.GameParameters.GameActions.TechLimit.Lockable then
		Multi_Room.GameParameters.GameActions.TechLimit.Locked = lock;
		if lock then
			setVisible(Multi_Room.GameParameters.GameActions.TechLimit.Locker,true);
			setFontColour(Multi_Room.GameParameters.GameActions.TechLimit.Name, RGBA(200,200,200,255));
		else
			setVisible(Multi_Room.GameParameters.GameActions.TechLimit.Locker,false);
			setFontColour(Multi_Room.GameParameters.GameActions.TechLimit.Name, RGBA(255,255,255,255));
		end;
	end;
end;

function ChangeLockTeams()
	if ifIsServer and not Multi_Room.GameParameters.GameActions.LockTeams.Locked then
		local check;
		if Multi_Room.GameParameters.GameActions.LockTeams.Checked then
			check = false;
		else
			check = true;
		end;

		setMapParam_checkbox(op_LockTeams ,check)

		updateLockTeams(check,false);
	end;
end;

function updateLockTeams(check,lock)

	Multi_Room.GameParameters.GameActions.LockTeams.Checked = check;
	if check then
		setVisible(Multi_Room.GameParameters.GameActions.LockTeams.Checker,true);
		LockedTeams=true;
	else
		setVisible(Multi_Room.GameParameters.GameActions.LockTeams.Checker,false);
		LockedTeams=false;
	end;
	if Multi_Room.GameParameters.GameActions.LockTeams.Lockable then
		Multi_Room.GameParameters.GameActions.LockTeams.Locked = lock;
		if lock then
			setVisible(Multi_Room.GameParameters.GameActions.LockTeams.Locker,true);
			setFontColour(Multi_Room.GameParameters.GameActions.LockTeams.Name, RGBA(200,200,200,255));
		else
			setVisible(Multi_Room.GameParameters.GameActions.LockTeams.Locker,false);
			setFontColour(Multi_Room.GameParameters.GameActions.LockTeams.Name, RGBA(255,255,255,255));
		end;
	end;
end;

function ChangeRanked()
	if ifIsServer  then
		local check;
		if Multi_Room.RankedRow.CheckBox.Checked then
			check = false;
		else
			check = true;
		end;
		ToOW_Host_setClanBase(check);
		setMapParam_checkbox(op_Ranked ,check);
		updateRanked(check);
		--ToOW_getGTList();
		AddSingleUseTimer(0.1,'multiplayer_map_select(1);');
		--OW_MULTIROOM_GET_MAPS();
		--AddSingleUseTimer(0.1,'changeOnFirstGP();');
	end;
end;

-- must be delay for good load gametypes name
function changeOnFirstGP()
	ChangeGameType();
	setVisible(Multi_Window.Clone,false);
	if MultiDef.GameTypeList[1] then
		ToOW_Host_setMap(MultiDef.MapName.MAP,MultiDef.GameTypeList[1].NAME);
	end;
end;

function updateRanked(check)

	Multi_Room.RankedRow.CheckBox.Checked = check;
	RankedGame = true;
	if check then
		setVisible(Multi_Room.RankedRow.CheckBox.Checker,true);
		setVisible(Multi_Room.RankedRow.LoginRow,true);
		
	else
		setVisible(Multi_Room.RankedRow.CheckBox.Checker,false);
		setVisible(Multi_Room.RankedRow.LoginRow,false);
	end;
	
	if ifIsServer  then
		
	end;
end;

inMultiplayer = false;
function multiroom_show()
	ifIsServer = getvalue(OWV_IAMSERVER);
	OW_MULTIROOM_HOST_SET_CLANBASEUSED(false);
	inMultiplayer = true;
	--OW_IRC_BOXES(Multi_Room.Chat.IRC.TextBox.ID,Multi_Room.Chat.IRC.LISTBOX.ID);
	setVisible(menu.side,false);
	if  SteamInitialized then
		--setVisible(steam_bar,false);
		setVisible(Multi_Room.SteamBar.Invite,true);
		setVisible(profile_label,false);
	else
		--setVisible(profilebar,false);
		setVisible(Multi_Room.Profile.MPName,true);
		--setText(Multi_Room.Profile.MPName,getvalue(OWV_USERNAME));
		--setText(Multi_Room.Profile.MPName,OW_SETTING_READ_STRING("MP","NAME",getvalue(OWV_USERNAME)));
		ToOW_setMyName ( OW_SETTING_READ_STRING("MP","NAME",getvalue(OWV_USERNAME)));
	end;
	setText(Multi_Room.RankedRow.LoginRow.Name,OW_SETTING_READ_STRING("MP","RANKED_NAME",''));
	setVisible(profile_button,false);
	--hideElement(profile_button);
	--showElement(Multi_Room.Profile);
	--showElement(Multi_Room.SteamBar.Invite);
	setVisible(Multi_Window,true);
	--showBuildings();
	--hideElement(Multi_Room.Restriction);
	--setBaseInformations();
	if ifIsServer == true then
		setBaseMapparams();
		setVisible(Multi_Room.GameParameters.GameActions.TypeChange,true);
		setVisible(Multi_Room.Panel.StartGame,true);
		setText(Multi_Room.GameParameters.GameActions.MapChange,loc(TID_Multi_button_changeMap));
		setXYV(Multi_Room.Panel.LeaveGame,10,40);
		setXYV(Multi_Room.GameParameters.GameActions.Checkers,5,75);
		--showChangeMap(true);
		--AddSingleUseTimer(0.1,'showChangeMap(true);');
		showChangeMap(true);
	else
		setVisible(Multi_Room.GameParameters.GameActions.TypeChange,false);
		setVisible(Multi_Room.Panel.StartGame,false);
		setText(Multi_Room.GameParameters.GameActions.MapChange,loc(TID_Multi_button_showMap));
		setXYV(Multi_Room.Panel.LeaveGame,10,5);
		setXYV(Multi_Room.GameParameters.GameActions.Checkers,5,40);
	end;

	setEnabled(Multi_Room.RankedRow.LoginRow.LoginBut,true);
	setEnabled(Multi_Room.RankedRow.LoginRow.Password,true);
	setEnabled(Multi_Room.RankedRow.LoginRow.Name,true);
	setText(Multi_Room.RankedRow.LoginRow.State, loc(TID_Multi_CB_Status) ..' ' .. loc(TID_Multi_CB_NotLoggedIn)); 
	--local MPAvatar = OW_SETTING_READ_BOOLEAN('MP', 'AVATAR', 0);
	--setChecked(changeHero.window.ProfileAvatar_CheckBox, MPAvatar);
	--setProfileAvatar(MPAvatar);
end;

function setBaseMapparams()

	updateTechLimit(true,true);
	setMapParam_checkbox(op_TechLimit ,true);

	updateLock(false);
	setMapParam_checkbox(op_LockGame ,false);

	updateRandPoss(false,false);
	setMapParam_checkbox(op_RndPosition ,false);

	updateRandColours(false,false);
	setMapParam_checkbox(op_RndColour ,false);

	updateRandNats(false,false);
	setMapParam_checkbox(op_RndNation ,false);

	updateLockTeams(false,false)
	setMapParam_checkbox(op_LockTeams, false);

	updateLockTeams(false)
	setMapParam_checkbox(op_Ranked, false);

	resetRestStats();
end;

function hideGameRoom()
	setVisible(Multi_Window,false);
	setVisible(Multi_Room,false);
	setVisible(ChangeMapWindow,false);
	setVisible(Lobby,true);
	setVisible(menu.side,true);

	if  SteamInitialized then
		setVisible(Multi_Room.SteamBar.Invite,false);
		setVisible(profile_label,true);
	else
		setVisible(Multi_Room.Profile.MPName,false);
	end;

	setVisible(profile_button,true);
	ifIsServer = false;
	inMultiplayer = false;
end;

function leaveGameRoom()
	OW_ROOM_LEAVE();

end;

function startMPgame()
    if OW_ROOM_LAUNCH_GAME() then
        setVisible(Multi_Window,false);
        setVisible(Multi_Room,false);
        setVisible(ChangeMapWindow,false);
    end;
end;

function FROMOW_MULTIPLAYER_STARTGAME()
    INMULTILOBBY= false;
    OW_IRC_DESTROY();
end;

function showMap()
  if MpScrW < 1920 then
    setVisible(Multi_Room.MapInfo,false);
	setFontColour(desBut,RGBA(150,150,150,255));
  end;
  setVisible(Multi_Room.Restriction,false);
  setVisible(Multi_Room.MapBox,true);
  setVisible(Multi_Room.MapBox.Line,true);
  setFontColour(Multi_Room.MapBut,WHITEA(255));
  setFontColour(Multi_Room.Rests,RGBA(150,150,150,255));

end;

function showDes()
  --hideElement(Multi_Room.MapBox);
  setVisible(Multi_Room.Restriction,false);
  setVisible(Multi_Room.MapBox.Line,false);

  setVisible(Multi_Room.MapInfo,true);
  setFontColour(desBut,WHITEA(255));
  setFontColour(Multi_Room.MapBut,RGBA(150,150,150,255));
  setFontColour(Multi_Room.Rests,RGBA(150,150,150,255));
end;

function showRest()
  if MpScrW < 1920 then
    setVisible(Multi_Room.MapInfo,false);
	setFontColour(desBut,RGBA(150,150,150,255));
  end;
  --hideElement(Multi_Room.MapBox);
  setVisible(Multi_Room.MapBox.Line,false);
  setVisible(Multi_Room.Restriction,true);
  setFontColour(Multi_Room.MapBut,RGBA(150,150,150,255));
  setFontColour(Multi_Room.Rests,RGBA(255,255,255,255));
end;

function showPlayers()

  setVisible(Multi_Room.SettingsRow,false);
  setVisible(Multi_Room.RankedRow,false);
  setVisible(Multi_Room.PlayersRow,true);
  setFontColour(playersBut,WHITEA(255));
  setFontColour(setBut,RGBA(150,150,150,255));
  setFontColour(rankBut,RGBA(150,150,150,255));

end;

function showSets()

		setVisible(Multi_Room.RankedRow,false);
  setVisible(Multi_Room.SettingsRow,true);
  setFontColour(setBut,WHITEA(255));
		setFontColour(rankBut,RGBA(150,150,150,255));
	if MpScrW < 1360 then
		setVisible(Multi_Room.PlayersRow,false);
		setFontColour(playersBut,RGBA(150,150,150,255));
	end;
end;

function showRank()
		setVisible(Multi_Room.SettingsRow,false);
		setVisible(Multi_Room.RankedRow,true);
		setFontColour(rankBut,WHITEA(255));
		setFontColour(setBut,RGBA(150,150,150,255));
	if MpScrW < 1360 then
		setVisible(Multi_Room.PlayersRow,false);
  setFontColour(playersBut,RGBA(150,150,150,255));
	end;
end;


function showRoomChat()

  setVisible(Multi_Room.Chat.IRC,false);
  setVisible(Multi_Room.Chat.Room,true);
  setFontColour(Multi_Room.Chat.Button,WHITEA(255));
  setFontColour(Multi_Room.Chat.IRC.Button,RGBA(150,150,150,255));

end;

function showLobbyChat()

  setVisible(Multi_Room.Chat.Room,false);
  setVisible(Multi_Room.Chat.IRC,true);
  setFontColour(Multi_Room.Chat.IRC.Button,WHITEA(255));
  setFontColour(Multi_Room.Chat.Button,RGBA(150,150,150,255));

end;


function sentToMPRoom(data)
	setText(Multi_Room.Chat.IRC.TextBox,getText(IRCTEXTBOX));
end;

function setInformationsName(mapName,gameType)
	if mapName == '' or mapName == nil then
		mapName = loc(TID_InGame_NoName);
	end;

	if gameType == '' or gameType == nil then
		gameType = loc(TID_InGame_NoName);
	end;

	setText(Multi_Room.Panel.MapTitle,mapName..' - '..gameType);
end;

function setInformationsMapic(mapPic)
	if mapPic == nil or mapPic == '' then
		mapPic = 'skirmish_unknown.png';
	else
		mapPic = '%missions%/\_multiplayer/'..mapPic..'/mappic.png'
	end;
	setTexture(Multi_Room.MapBox.MapImage,mapPic);
	setTextureFallback(Multi_Room.MapBox.MapImage,'skirmish_unknown.png');
end;

function setInformationsRules(des,rul)
  setText(Multi_Room.MapInfo.Rules,des..'\n\n'..rul);
end;

function setPlayersCount(CurrentPlayers,MaxPlayers)
  setText(Multi_Room.Panel.PlayersCount,CurrentPlayers..'/'..MaxPlayers);
end;

function setStatus(status)
  setText(Multi_Room.Panel.Status,status);
end;

function reloadMyAvatarInMP()

end;


pBars = {};
tBars = {};
sBar = {};

teams = {[1]={},[2]={}, [3]={}, [4]={}, [5]={}, [6]={}, [7]={}, [8]={}};
specs = {};
merges = {};
alones = {};
tCount = {[1]=0,[2]=0, [3]=0, [4]=0, [5]=0, [6]=0, [7]=0, [8]=0};
aCount = 0;
sCount = 0;
ifTeams = false;

loadingTeam = {[1]={},[2]={}, [3]={}, [4]={}, [5]={}, [6]={}, [7]={}, [8]={}, [9]={}};
loadingTCount = {[1]=0,[2]=0, [3]=0, [4]=0, [5]=0, [6]=0, [7]=0, [8]=0, [9]=0};
loadingAlones = {};
loadingACount = 0;
loadingTeamsCount = 0;


function refreshPlayers()
	--MyID = 1;
	--players[table.getn(players)+1] = createPlayerBar(table.getn(players)+1);
	teams = {[1]={},[2]={}, [3]={}, [4]={}, [5]={}, [6]={}, [7]={}, [8]={}};
	specs = {};
	merges = {};
	alones = {};
	tCount = {[1]=0,[2]=0, [3]=0, [4]=0, [5]=0, [6]=0, [7]=0, [8]=0};
	aCount = 0;
	sCount = 0;

	for i, v in pairs(Players) do
		--pBars[v.ID] = createPlayerBar(v);
		if v.IsSpec == true then
			specs[sCount+1] = v.ID;
			sCount = sCount+1;

		elseif v.IsMerged == true then
			if merges[v.withMerged] == nil then
				merges[v.withMerged] = {};
			end;
			merges[v.withMerged][table.getn(merges[v.withMerged])+1] = v.ID;

			--if tCount[Players[v.withMerged].tPos] == nil then
			--	tCount[Players[v.withMerged].tPos] = 0;
			--end;
			if Players[v.withMerged].Team > 0 then
				tCount[Players[v.withMerged].Team] = tCount[Players[v.withMerged].Team] +1;
			end;

		elseif v.Team > 0 then
			--if teams[v.tPos] == nil then
			--	teams[v.tPos] = {};
			--end;
			teams[v.Team][table.getn(teams[v.Team])+1] = v.ID;

			--if tCount[v.tPos] == nil then
			--	tCount[v.tPos] = 0;
			--end;
			tCount[v.Team] = tCount[v.Team] +1;
		else
			alones[table.getn(alones)+1] = v.ID;
			aCount = aCount+1;
		end;
	end;

	reconstructPlayers();
	

	loadingTeam = {[1]={},[2]={}, [3]={}, [4]={}, [5]={}, [6]={}, [7]={}, [8]={}, [9]={}};
	loadingTCount = {[1]=0,[2]=0, [3]=0, [4]=0, [5]=0, [6]=0, [7]=0, [8]=0, [9]=0};
--	loadingAlones = {};
--	loadingACount = 0;
	loadingTeamsCount = 0;
	local limit = 0;
	for i, v in pairs(Players) do
		if ifTeams and not v.IsMerged then
			if v.IsSpec and v.Team > 0 then
				loadingTeam[9][table.getn(loadingTeam[9])+1] = v.ID;

				loadingTCount[9] = loadingTCount[9] +1;
				if merges[v.ID] then
					for _, m in pairs(merges[v.ID]) do
						loadingTeam[9][table.getn(loadingTeam[9])+1] = m;
						loadingTCount[9] = loadingTCount[9] +1;
					end;
				end;
			elseif v.Team > 0 then
				loadingTeam[v.Team][table.getn(loadingTeam[v.Team])+1] = v.ID;

				loadingTCount[v.Team] = loadingTCount[v.Team] +1;
				if merges[v.ID] then
					for _, m in pairs(merges[v.ID]) do
						loadingTeam[v.Team][table.getn(loadingTeam[v.Team])+1] = m;
						loadingTCount[v.Team] = loadingTCount[v.Team] +1;
					end;
				end;
			end;
		elseif not v.IsMerged  then
--			loadingAlones[table.getn(loadingAlones)+1] = v.ID;
--			loadingACount = loadingACount+1;
			if v.IsSpec then
				loadingTeam[9][table.getn(loadingTeam[9])+1] = v.ID;

				loadingTCount[9] = loadingTCount[9] +1;
				if merges[v.ID] then
					for _, m in pairs(merges[v.ID]) do
						loadingTeam[9][table.getn(loadingTeam[9])+1] = m;
						loadingTCount[9] = loadingTCount[9] +1;
					end;
				end;
			else
				loadingTeam[limit+1][table.getn(loadingTeam[limit+1])+1] = v.ID;

				loadingTCount[limit+1] = loadingTCount[limit+1] +1;
				
				if merges[v.ID] then
					for _, m in pairs(merges[v.ID]) do
						loadingTeam[limit+1][table.getn(loadingTeam[limit+1])+1] = m;
						loadingTCount[limit+1] = loadingTCount[limit+1] +1;
					end;
				end;
				
				limit = limit+1;
			end;
			
		end;
	end;
	
	for i=1, 9 do
		if loadingTCount[i] > 0 then
			loadingTeamsCount = loadingTeamsCount+1;
		end;
	end;

end;

--	MultiDef.TeamDef = Data.TEAMDEF;
--	MultiDef.SideDef = Data.SIDEDEF;
--[[	Data.TEAMDEF, DATA.SIDEDEF
	TEAMDEF[1..9] of			1 isn't team
		NAME	String				without name isn't avalible 		1 hasn't name
		SIDESMIN    Integer
		SIDESMAX    Integer
		ASSIGNED_POSITIONS[1..8] of Boolean
		ASSIGNED_POSITIONS_COUNT    Integer

	SIDEDEF[1..8] of
		NAME	String
		ENABLED	Boolean
		NATIONS of
			AR    Boolean
			US    Boolean
			RU    Boolean
		--]]

Multi_Room.PlayersRow.joinCompButton = makeCompGradButton(Multi_Room,Multi_Room.PlayersRow.width-150-5+10,Multi_Room.PlayersRow.y-23,150,23,loc(TID_Multi_button_AddComputer),'');
set_Callback(Multi_Room.PlayersRow.joinCompButton.ID,CALLBACK_MOUSECLICK,'JoinComp(0);');
function reconstructPlayers()
	sgui_deletechildren(Multi_Room.PlayersRow.ScrollBox.ID);
	getElementEX(Multi_Room.PlayersRow.ScrollBox,anchorLT,XYWH(0,0,1,1),true,{colour1 = WHITEA(0),});
	
	if MultiDef.TeamDef[1] ~= nil then
		pBars = {};
		tBars = {};
		sBar = {};

		local h=2;
		local th =2;
		ifTeams = false;

		if aCount > 0 then
				for i=1, aCount do
					pBars[Players[alones[i]].ID] = createPlayerBar(Players[alones[i]],th);
					th = th + 27;
				if merges[Players[alones[i]].ID] ~= nil then
						local mCount = table.getn( merges[Players[alones[i]].ID]);
						for v=1, mCount do
							pBars[ merges[Players[alones[i]].ID][v]] = createPlayerBar( Players[merges[Players[alones[i]].ID][v]],th);
						th = th+27;
						end;
						setTexture (pBars[merges[Players[alones[i]].ID][mCount]].mergedMark,"SGUI/Alien/multiplayer/merged.png");
					end;
					th = th+2;
				end;
			th =th + 20;
			end;

		for i=2, 9 do
			if MultiDef.TeamDef[i].NAME ~= '' then
				tBars[i] = createTeamLabel(MultiDef.TeamDef[i],i,tCount[i-1],th);

				--setXYV(tBars[i].row, 2, th);
				th = th + tBars[i].height + 10;
				ifTeams = true;
			end;
		end;

		if ifTeams == true then
			th =th + 20;
			setVisible(Multi_Room.PlayersRow.joinCompButton,false);
		elseif MultiDef.MultiMap.ADDCOMPBUTTON then
			setVisible(Multi_Room.PlayersRow.joinCompButton,true)
			if ifIsServer then
				setEnabled(Multi_Room.PlayersRow.joinCompButton,true);
			else
				setEnabled(Multi_Room.PlayersRow.joinCompButton,false);
			end;
		else
			setVisible(Multi_Room.PlayersRow.joinCompButton,false);

		end;

		--	if aCount > 0 then
		--		for i=1, aCount do
		--			pBars[Players[alones[i]].ID] = createPlayerBar(Players[alones[i]],th);
		--			th = th + 27;
		--		if merges[Players[alones[i]].ID] ~= nil then
		--				local mCount = table.getn( merges[Players[alones[i]].ID]);
		--				for v=1, mCount do
		--					pBars[ merges[Players[alones[i]].ID][v]] = createPlayerBar( Players[merges[Players[alones[i]].ID][v]],th);
		--				th = th+27;
		--				end;
		--				setTexture (pBars[merges[Players[alones[i]].ID][mCount]].mergedMark,"SGUI/Alien/multiplayer/merged.png");
		--			end;
		--			th = th+2;
		--		end;
		--	end;

		th =th + 2;
		if sCount > 0 or MultiDef.MultiMap.CANSPEC then
			sBar = createSpecLabel(sCount,th);--th+20);
		end;

	end;

end

	changeHero = {} ; --[[getElementEX(
							Multi_Room,
							anchorRT,
							XYWH(
								0,
								0,
								ScrWidth,
								ScrHeight
							),
							false,
							{
								colour1=BLACKA(50),
							}
					); ]]--

	changeHero.tabHAvatar = getElementEX(
						profileAvatar,
						anchorRT,
						XYWH(
							LayoutWidth,
							LayoutHeight/2- 130-30,
							(LayoutWidth-menu.side.width-2*55)/2-1,
							30
						),
						true,
						{
							colour1=RGBA(70,70,70,255),
						--	callback_mouseclick='',
							bevel=true,
						}
				);

	changeHero.tabHAvatar.Label = getLabelEX(
							changeHero.tabHAvatar,
							anchorRT,
							XYWH(
								0,
								0,
								changeHero.tabHAvatar.width,
								changeHero.tabHAvatar.height
							),
							true,
							loc(TID_MUltiAvatar_Title),
							{
								font_name = Tahoma_12B,
								font_style_outline=true,
								text_halign=ALIGN_MIDDLE,
								callback_mouseclick='changeMyHero();',
								highlight=true,
							}
					);


	changeHero.window = getWindowEX(
								profileAvatar,
								anchorNone,
								XYWH(
									LayoutWidth - (LayoutWidth-menu.side.width-2*55)/2,
									LayoutHeight/2- 130,
									LayoutWidth-menu.side.width-2*55,
									LayoutHeight-220
								),
								false,
								loc(TID_MUltiAvatar_Title),
								WINDOW_BACKGROUND,
								{

								}
					);


	changeHero.window.cancel = makeGradButton({
		parent=changeHero.window,
		anchor={top=false,bottom=true,left=true,right=true},
		x=10+changeHero.window.width/2,
		y=changeHero.window.height-40,
		width=changeHero.window.width/2-20,
		height=30,
		text=loc(TID_MultiAvatar_Cancel),
		font_colour_disabled=GRAY(127),
		callback_mouseclick='setVisible({ID = '.. profileAvatar.ID ..'},false);',
		},GradButton_Grey_Light);

	changeHero.window.ok =  makeGradButton({
		parent=changeHero.window,
		anchor={top=false,bottom=true,left=true,right=true},
		x=10,
		y=changeHero.window.height-40,
		width=changeHero.window.width/2-20,
		height=30,
		text=loc(TID_MultiAvatar_OK),
		font_colour_disabled=GRAY(127),
		callback_mouseclick='setVisible({ID = '.. profileAvatar.ID ..'},false);changeHero.sendAVATAR();',
		},GradButton_Grey_Light);

	changeHero.window.random = makeGradButton({
		parent=changeHero.window,
		anchor={top=false,bottom=true,left=true,right=true},
		x=10+changeHero.window.width/4,
		y=changeHero.window.height-40*2-2,
		width=changeHero.window.width/2-20,
		height=30,
		text=loc(TID_MultiAvatar_Random),
		font_colour_disabled=GRAY(127),
		callback_mouseclick='',
		},GradButton_Grey_Light);

	changeHero.window.avatar = getElementEX(
							changeHero.window,
							anchorRT,
							XYWH(
								10,
								40,
								160,
								200
							),
							true,
							{
								texture = 'Avatars/unknow.png',
							}
					);

	changeHero.window.heroNameLabel = getLabelEX(
							changeHero.window,
							anchorRT,
							XYWH(
								changeHero.window.avatar.x-2,
								changeHero.window.avatar.y+changeHero.window.avatar.height+10,
								100,
								20
							),
							true,
							loc(TID_MultiAvatar_HeroName),
							{
								font_name = Tahoma_12B,
							}
					);

	changeHero.window.heroName = AddElement({
		type=TYPE_EDIT,
		parent=changeHero.window,
		anchor={top=true,bottom=true,left=false,right=false},
		x=changeHero.window.heroNameLabel.x+2,
		y=changeHero.window.heroNameLabel.y+changeHero.window.heroNameLabel.height,
		width=160,
		height=25,
		colour1=BLACKA(200),
		font_name=Tahoma_12 ,
		wordwrap=false,
		visible=true,
		bevel=true;
		bevel_colour1=GRAYA(14,200),
		bevel_colour2=GRAYA(14,200),
		text=getvalue(OWV_USERNAME),
		--edges=true,
	});


--[[
	changeHero.window.heroClassLabel = getLabelEX(
							changeHero.window,
							anchorRT,
							XYWH(
								changeHero.window.heroName.x,
								changeHero.window.heroName.y+changeHero.window.heroName.height+10,
								100,
								25
							),
							true,
							loc(TID_MultiAvatar_HeroClass),
							{
								font_name = Tahoma_12B,
							}
					);

	changeHero.window.heroClass  = makeCombobox_UI(
							changeHero.window,
							XYWH(
								changeHero.window.heroClassLabel.x + 10,
								changeHero.window.heroClassLabel.y+changeHero.window.heroClassLabel.height+2,
								170,
								16
							),
						''
						);

	 setText(changeHero.window.heroClass.list,loc(TID_MutliAvatar_Soldier)..CHAR13..loc(TID_MultiAvatar_Engineer)..CHAR13..loc(TID_MultiAvatar_Mechanic)..CHAR13..loc(TID_MultiAvatar_Scientist) );

--]]
	changeHero.window.baseNameLabel = getLabelEX(
							changeHero.window,
							anchorRT,
							XYWH(
								changeHero.window.heroNameLabel.x,--changeHero.window.heroClass.x-10,
								changeHero.window.heroName.y+changeHero.window.heroName.height+10,--changeHero.window.heroClass.y+changeHero.window.heroClass.height+10,
								100,
								20
							),
							true,
							loc(TID_MultiAvatar_BaseName),
							{
								font_name = Tahoma_12B,
							}
					);

	changeHero.window.baseName = AddElement({
		type=TYPE_EDIT,
		parent=changeHero.window,
		anchor=anchorTB,
		x=changeHero.window.baseNameLabel.x+2,
		y=changeHero.window.baseNameLabel.y+changeHero.window.baseNameLabel.height,
		width=160,
		height=25,
		colour1=BLACKA(200),
		font_name=Tahoma_12 ,
		wordwrap=false,
		visible=true,
		bevel=true;
		bevel_colour1=GRAYA(14,200),
		bevel_colour2=GRAYA(14,200),
		text=getvalue(OWV_USERNAME),
		--edges=true,
	});



	changeHero.window.ProfileAvatar_CheckBox = getCheckBoxEX_MENU(
										changeHero.window,
										anchorLT,
										XYWH(
											changeHero.window.avatar.x+changeHero.window.avatar.width+30,
											changeHero.window.avatar.y,
											20,
											20
										),
										loc(TID_MultiAvatar_UseProfileAvatar),
										checkbox_merge,
										'',
										{
											checked=false,
--											visible=false,
										}
										);


	changeHero.window.sexLabel = getLabelEX(
							changeHero.window,
							anchorRT,
							XYWH(
								changeHero.window.avatar.x+changeHero.window.avatar.width+30,
								changeHero.window.avatar.y+changeHero.window.ProfileAvatar_CheckBox.height+10,
								200,
								20
							),
							true,
							loc(TID_MultiAvatar_Sex),
							{
								font_name = Tahoma_12B;
							}
					);

	changeHero.window.sex  = makeCombobox_UI(
							changeHero.window,
							XYWH(
								changeHero.window.sexLabel.x+20,
								changeHero.window.sexLabel.y+changeHero.window.sexLabel.height +2,
								180,
								16
							),
						'changeHero.window.sex.onChange();'
						);
	changeHero.setup = true;

	setText(changeHero.window.sex.list,loc(TID_MultiAvatar_Male)..CHAR13..loc(TID_MultiAvatar_Female));

	changeHero.window.skinLabel = getLabelEX(
							changeHero.window,
							anchorRT,
							XYWH(
								changeHero.window.sexLabel.x,
								changeHero.window.sexLabel.y+changeHero.window.sexLabel.height*2 +5,
								200,
								20
							),
							true,
							loc(TID_MultiAvatar_SkinColour),
							{
								font_name = Tahoma_12B;
							}
					);

	changeHero.window.skin  = makeCombobox_UI(
							changeHero.window,
							XYWH(
								changeHero.window.skinLabel.x+20,
								changeHero.window.skinLabel.y+changeHero.window.skinLabel.height +2,
								180,
								16
							),
						''
						);

	changeHero.window.neckLabel = getLabelEX(
							changeHero.window,
							anchorRT,
							XYWH(
								changeHero.window.skinLabel.x,
								changeHero.window.skinLabel.y+changeHero.window.skinLabel.height*2 +5,
								200,
								20
							),
							true,
							loc(TID_MultiAvatar_Neck),
							{
								font_name = Tahoma_12B;
							}
					);

	changeHero.window.neck  = makeCombobox_UI(
							changeHero.window,
							XYWH(
								changeHero.window.neckLabel.x+20,
								changeHero.window.neckLabel.y+changeHero.window.neckLabel.height +2,
								180,
								16
							),
						'changeHero.window.avatar.makePreview()'
						);

	changeHero.window.eyesLabel = getLabelEX(
							changeHero.window,
							anchorRT,
							XYWH(
								changeHero.window.neckLabel.x,
								changeHero.window.neckLabel.y+changeHero.window.neckLabel.height*2 +5,
								200,
								20
							),
							true,
							loc(TID_MultiAvatar_Eyes),
							{
								font_name = Tahoma_12B;
							}
					);

	changeHero.window.eyes  = makeCombobox_UI(
							changeHero.window,
							XYWH(
								changeHero.window.eyesLabel.x+20,
								changeHero.window.eyesLabel.y+changeHero.window.eyesLabel.height +2,
								180,
								16
							),
						'changeHero.window.avatar.makePreview()'
						);

	changeHero.window.cheekLabel = getLabelEX(
							changeHero.window,
							anchorRT,
							XYWH(
								changeHero.window.eyesLabel.x,
								changeHero.window.eyesLabel.y+changeHero.window.eyesLabel.height*2 +5,
								200,
								20
							),
							true,
							loc(  TID_MultiAvatar_Cheeks),
							{
								font_name = Tahoma_12B;
							}
					);

	changeHero.window.cheek  = makeCombobox_UI(
							changeHero.window,
							XYWH(
								changeHero.window.cheekLabel.x+20,
								changeHero.window.cheekLabel.y+changeHero.window.cheekLabel.height +2,
								180,
								16
							),
						'changeHero.window.avatar.makePreview()'
						);

	changeHero.window.noseLabel = getLabelEX(
							changeHero.window,
							anchorRT,
							XYWH(
								changeHero.window.cheekLabel.x,
								changeHero.window.cheekLabel.y+changeHero.window.cheekLabel.height*2 +5,
								200,
								20
							),
							true,
							loc(TID_MultiAvatar_Nose),
							{
								font_name = Tahoma_12B;
							}
					);

	changeHero.window.nose  = makeCombobox_UI(
							changeHero.window,
							XYWH(
								changeHero.window.noseLabel.x+20,
								changeHero.window.noseLabel.y+changeHero.window.noseLabel.height +2,
								180,
								16
							),
						'changeHero.window.avatar.makePreview()'
						);

	changeHero.window.mouthLabel = getLabelEX(
							changeHero.window,
							anchorRT,
							XYWH(
								changeHero.window.noseLabel.x,
								changeHero.window.noseLabel.y+changeHero.window.noseLabel.height*2 +5,
								200,
								20
							),
							true,
							loc(TID_MultiAvatar_Mouth),
							{
								font_name = Tahoma_12B;
							}
					);

	changeHero.window.mouth  = makeCombobox_UI(
							changeHero.window,
							XYWH(
								changeHero.window.mouthLabel.x +20,
								changeHero.window.mouthLabel.y+changeHero.window.mouthLabel.height+2,
								180,
								16
							),
						'changeHero.window.avatar.makePreview()'
						);


	changeHero.window.earsLabel = getLabelEX(
							changeHero.window,
							anchorRT,
							XYWH(
								changeHero.window.sexLabel.x+changeHero.window.sexLabel.width+100,
								changeHero.window.sexLabel.y,
								200,
								20
							),
							true,
							loc(TID_MultiAvatar_Ears),
							{
								font_name = Tahoma_12B;
							}
					);

	changeHero.window.ears  = makeCombobox_UI(
							changeHero.window,
							XYWH(
								changeHero.window.earsLabel.x+20,
								changeHero.window.earsLabel.y+changeHero.window.earsLabel.height +2,
								180,
								16
							),
						'changeHero.window.avatar.makePreview()'
						);

	changeHero.window.eyebrowLabel = getLabelEX(
							changeHero.window,
							anchorRT,
							XYWH(
								changeHero.window.earsLabel.x,
								changeHero.window.earsLabel.y+changeHero.window.earsLabel.height*2 +5,
								200,
								20
							),
							true,
							loc(TID_MultiAvatar_Eyebrows),
							{
								font_name = Tahoma_12B;
							}
					);

	changeHero.window.eyebrow  = makeCombobox_UI(
							changeHero.window,
							XYWH(
								changeHero.window.eyebrowLabel.x+20,
								changeHero.window.eyebrowLabel.y+changeHero.window.eyebrowLabel.height +2,
								180,
								16
							),
						'changeHero.window.avatar.makePreview()'
						);

	changeHero.window.hairLabel = getLabelEX(
							changeHero.window,
							anchorRT,
							XYWH(
								changeHero.window.eyebrowLabel.x,
								changeHero.window.eyebrowLabel.y+changeHero.window.eyebrowLabel.height*2 +5,
								200,
								20
							),
							true,
							loc(TID_MultiAvatar_Hair),
							{
								font_name = Tahoma_12B;
							}
					);

	changeHero.window.hair  = makeCombobox_UI(
							changeHero.window,
							XYWH(
								changeHero.window.hairLabel.x+20,
								changeHero.window.hairLabel.y+changeHero.window.hairLabel.height +2,
								180,
								16
							),
						'changeHero.window.avatar.makePreview()'
						);

	changeHero.window.beardLabel = getLabelEX(
							changeHero.window,
							anchorRT,
							XYWH(
								changeHero.window.hairLabel.x,
								changeHero.window.hairLabel.y+changeHero.window.hairLabel.height*2 +5,
								200,
								20
							),
							true,
							loc(TID_MultiAvatar_Beard),
							{
								font_name = Tahoma_12B;
							}
					);

	changeHero.window.beard  = makeCombobox_UI(
							changeHero.window,
							XYWH(
								changeHero.window.beardLabel.x+20,
								changeHero.window.beardLabel.y+changeHero.window.beardLabel.height +2,
								180,
								16
							),
						'changeHero.window.avatar.makePreview()'
						);

	changeHero.window.cHairLabel = getLabelEX(
							changeHero.window,
							anchorRT,
							XYWH(
								changeHero.window.beardLabel.x,
								changeHero.window.beardLabel.y+changeHero.window.beardLabel.height*2 +5,
								200,
								20
							),
							true,
							loc(TID_MultiAvatar_HaBColour),
							{
								font_name = Tahoma_12B;
							}
					);

	changeHero.window.cHair  = makeCombobox_UI(
							changeHero.window,
							XYWH(
								changeHero.window.cHairLabel.x+20,
								changeHero.window.cHairLabel.y+changeHero.window.cHairLabel.height +2,
								180,
								16
							),
						''
						);

	changeHero.window.glassesLabel = getLabelEX(
							changeHero.window,
							anchorRT,
							XYWH(
								changeHero.window.cHairLabel.x,
								changeHero.window.cHairLabel.y+changeHero.window.cHairLabel.height*2 +5,
								200,
								20
							),
							true,
							loc(TID_MultiAvatar_Glasses),
							{
								font_name = Tahoma_12B;
							}
					);

	changeHero.window.glasses  = makeCombobox_UI(
							changeHero.window,
							XYWH(
								changeHero.window.glassesLabel.x+20,
								changeHero.window.glassesLabel.y+changeHero.window.glassesLabel.height +2,
								180,
								16
							),
						'changeHero.window.avatar.makePreview()'
						);

	changeHero.window.faceLabel = getLabelEX(
							changeHero.window,
							anchorRT,
							XYWH(
								changeHero.window.glassesLabel.x,
								changeHero.window.glassesLabel.y+changeHero.window.glassesLabel.height*2 +5,
								200,
								20
							),
							true,
							loc(TID_MultiAvatar_Face),
							{
								font_name = Tahoma_12B;
							}
					);

	changeHero.window.face  = makeCombobox_UI(
							changeHero.window,
							XYWH(
								changeHero.window.faceLabel.x + 20,
								changeHero.window.faceLabel.y+changeHero.window.faceLabel.height+2,
								180,
								16
							),
						'changeHero.window.avatar.makePreview()'
						);
XICHT_PARTS = {};
XICHT_AVATAR_TEXID = 0;
XICHT_NOPREVIEW = false;

function chwIndex(LISTBOX)
	return getIndex(LISTBOX.list)+1;
end;

function chwSetIndex(LISTBOX,VALUE)
	if VALUE > 1 then
		setIndex(LISTBOX.list,VALUE-1);
	else
		setIndex(LISTBOX.list,0);
	end;
end;

function changeHero.freeAvatarTexture()
	if XICHT_AVATAR_TEXID > 0 then
		OW_XICHT_PORTRAIT_FREETEXTURE(XICHT_AVATAR_TEXID);
		XICHT_AVATAR_TEXID = 0;
	end;
end;

function makeAVATARTEX(VALUE,AVATAR,AVATARSEX,NATION)
	if (VALUE ~= nil) and (VALUE > 0) then
		OW_XICHT_PORTRAIT_FREETEXTURE(VALUE);
	end;
	if NATION < 0 then
		NATION = 0;
	end;
	return OW_XICHT_PORTRAIT(0,AVATARSEX,NATION,true,0,AVATAR[2],AVATAR[3],AVATAR[4],AVATAR[5],AVATAR[6],AVATAR[7],AVATAR[8],AVATAR[9],AVATAR[10],AVATAR[11],0,AVATAR[13],0);
end;

function changeHero.window.avatar.makePreview()
	changeHero.freeAvatarTexture();

	if not XICHT_NOPREVIEW then
		if not changeHero.setup then
			local NATION = 0;
			local chw = changeHero.window;
			XICHT_AVATAR_TEXID = OW_XICHT_PORTRAIT(changeHero.window.avatar.ID,chwIndex(chw.sex)-1,NATION,false,0,chwIndex(chw.neck),chwIndex(chw.face),chwIndex(chw.cheek),chwIndex(chw.eyes),chwIndex(chw.nose),chwIndex(chw.mouth),chwIndex(chw.ears),chwIndex(chw.eyebrow),chwIndex(chw.hair),chwIndex(chw.beard),0,chwIndex(chw.glasses),0);
		end;
	end;
end;

function changeHero.setList(LISTBOX,VALUE)
	setText(LISTBOX.list,VALUE);
	if changeHero.setup then
		setIndex(LISTBOX.list,0);
	end;
end;

function changeHero.window.sex.onChange()
	local TEMP = {};
	if (getIndex(changeHero.window.sex.list) == 0) then
		TEMP = XICHT_PARTS.MALE;
	else
		TEMP = XICHT_PARTS.FEMALE;
	end;

	changeHero.setList(changeHero.window.neck,TEMP.NECK.STR);
	changeHero.setList(changeHero.window.face,TEMP.FACE.STR);
	changeHero.setList(changeHero.window.cheek,TEMP.CHEEK.STR);
	changeHero.setList(changeHero.window.eyes,TEMP.EYE.STR);
	changeHero.setList(changeHero.window.nose,TEMP.NOSE.STR);
	changeHero.setList(changeHero.window.mouth,TEMP.MOUTH.STR);
	changeHero.setList(changeHero.window.ears,TEMP.EAR.STR);
	changeHero.setList(changeHero.window.eyebrow,TEMP.EYEBROW.STR);
	changeHero.setList(changeHero.window.hair,TEMP.HAIR.STR);
	changeHero.setList(changeHero.window.beard,TEMP.BEARD.STR);
	changeHero.setList(changeHero.window.glasses,TEMP.GLASSES.STR);

	changeHero.window.avatar.makePreview();
end;

function updateXichtParts()
	OW_XICHT_PORTRAIT_GETPARTS(0);
	XICHT_PARTS.MALE   = TEMPPARTS;
	OW_XICHT_PORTRAIT_GETPARTS(1);
	XICHT_PARTS.FEMALE = TEMPPARTS;
end;

function changeHero.setAVATAR()
	XICHT_NOPREVIEW = true;
	local chw = changeHero.window;
	setIndex(chw.sex.list,Players[MyID].AVATARSEX);
	chwSetIndex(chw.neck,Players[MyID].AVATAR[2]);
	chwSetIndex(chw.face,Players[MyID].AVATAR[3]);
	chwSetIndex(chw.cheek,Players[MyID].AVATAR[4]);
	chwSetIndex(chw.eyes,Players[MyID].AVATAR[5]);
	chwSetIndex(chw.nose,Players[MyID].AVATAR[6]);
	chwSetIndex(chw.mouth,Players[MyID].AVATAR[7]);
	chwSetIndex(chw.ears,Players[MyID].AVATAR[8]);
	chwSetIndex(chw.eyebrow,Players[MyID].AVATAR[9]);
	chwSetIndex(chw.hair,Players[MyID].AVATAR[10]);
	chwSetIndex(chw.beard,Players[MyID].AVATAR[11]);

	chwSetIndex(chw.glasses,Players[MyID].AVATAR[13]);
	XICHT_NOPREVIEW = false;

	changeHero.window.avatar.makePreview();
	--setTexture(profileXichtAvatar,getTexture(changeHero.window.avatar));
	profileXichtAvatar_setAvatar();
end;

function changeMyHero()
	updateXichtParts();
	if inMultiplayer == true then
		changeHero.setAVATAR();
	end;

	setText(changeHero.window.heroName , OW_SETTING_READ_STRING("MP","HERO",getText(Multi_Room.Profile.MPName)));
	setText(changeHero.window.baseName , OW_SETTING_READ_STRING("MP","BASE",getText(Multi_Room.Profile.MPName)));

--setXY(changeHero.window,ScrWidth/2 - (LayoutWidth-menu.side.width-2*55)/2,ScrHeight/2-(LayoutHeight-220)/2);

	setVisible(profileAvatar,true);
	setVisible(profileAvatar.window,false);
	setEnabled(profileAvatar.tabPAvatar.Label , true);
	setVisible(changeHero.window,true);
	setEnabled(changeHero.tabHAvatar.Label, false);

	if selectedAvatar == 1 then
		setVisible(changeHero.window.ProfileAvatar_CheckBox,false);
		setVisible(changeHero.window.ProfileAvatar_CheckBox.label,false);
	else
		setVisible(changeHero.window.ProfileAvatar_CheckBox,true);
		setVisible(changeHero.window.ProfileAvatar_CheckBox.label,true);
	end;
end;

function changeHero.sendAVATAR()
	local chw = changeHero.window;
	MULTIPLAYER_SETAVATAR(chwIndex(chw.sex)-1,chwIndex(chw.neck),chwIndex(chw.face),chwIndex(chw.cheek),chwIndex(chw.eyes),chwIndex(chw.nose),chwIndex(chw.mouth),chwIndex(chw.ears),chwIndex(chw.eyebrow),chwIndex(chw.hair),chwIndex(chw.beard),chwIndex(chw.glasses));

	ToOW_setMyHeroName( getText(changeHero.window.heroName));
	ToOW_setMyBaseName( getText(changeHero.window.baseName));
	----  Cab'T be changed, so can't be saved
	--OW_SETTING_WRITE("MP","HERO",  getText(changeHero.window.heroName));
	--OW_SETTING_WRITE("MP","BASE",  getText(changeHero.window.baseName));
end;

function MULTIPLAYER_SETAVATAR(SEX,NECK,FACE,CHEEK,EYE,NOSE,MOUTH,EAR,EYEBROW,HAIR,BEARD,GLASSES)
	OW_MULTIROOM_SET_MYAVATAR(SEX,'0;'..NECK..';'..FACE..';'..CHEEK..';'..EYE..';'..NOSE..';'..MOUTH..';'..EAR..';'..EYEBROW..';'..HAIR..';'..BEARD..';0;'..GLASSES..';0;');
end;

TEMPPARTS = {};

function FROMOW_XICHT_PORTRAIT_PARTS(DATA)
	TEMPPARTS = DATA;
end;

if not getvalue(OWV_ISEDITOR) then
	changeHero.setup = true;
	updateXichtParts();
	setIndex(changeHero.window.sex.list,0);
	changeHero.setup = false;

	--- Always Disabled, because can not be changed for now
	setEnabled(changeHero.window.skin ,false);
	setEnabled(changeHero.window.cHair ,false);
	setEnabled(changeHero.window.heroName ,false);
	setEnabled(changeHero.window.baseName ,false);
	setEnabled(changeHero.window.ProfileAvatar_CheckBox,false);
	setEnabled(changeHero.window.ProfileAvatar_CheckBox.label,false);
end;
