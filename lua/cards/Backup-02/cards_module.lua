cards    = {LIST={},COUNT=0,USED=0,ELE=nil,ANIMTIME=1,ANIMMOVETIME=0.5,CARDHEIGHT=300,CARDWIDTH=250};
cardPool = {LIST={},COUNT=0};

function cards.add(DATA)
	cards.COUNT=cards.COUNT+1;
        DATA.ID = cards.COUNT;
	cards.LIST[cards.COUNT] = DATA;
        return cards.COUNT;
end;

function cards.addCards(COUNT)
	cards.COUNT = 0;
        cards.USED  = 0;
        cards.LIST  = {};
	for i = 1,COUNT do
        	cards.add({V=false});
        end;
end;

function cards.findFree()
	if cards.USED < cards.COUNT then
		for i = 1,cards.COUNT do
	        	if (not cards.LIST[i].V) then
                		return i;
        	        end;
	        end;
        end;

        return 0;
end;

-- AM deck =
-- AR deck =
-- RU deck =

function cards.addCard(DATA)
	local id = cards.findFree();
        if id < 1 then
        	return;
        end;
		
		cards.LIST[id].V = true;
	
		if cards.State == 'Game' then
		
			if interfaceSide == 1 then -- Américain
				if id ~= 5 or id ~= 6 then cards.LIST[id].CARD = cardPool.LIST[math.random(16,42)]; end;
				if id == 5 then cards.LIST[id].CARD = cardPool.LIST[math.random(97,112)]; end;
				if id == 6 then	cards.LIST[id].CARD = cardPool.LIST[138]; end;	
			end;
				
			if interfaceSide == 3 then -- Arabe
				if id ~= 5 or id ~= 6 then cards.LIST[id].CARD = cardPool.LIST[math.random(43,69)]; end;
				if id == 5 then cards.LIST[id].CARD = cardPool.LIST[math.random(113,124)]; end;
				if id == 6 then	cards.LIST[id].CARD = cardPool.LIST[138]; end;
			end;
			
			if interfaceSide == 2 then -- Russe
				if id ~= 5 or id ~= 6 then cards.LIST[id].CARD = cardPool.LIST[math.random(70,96)]; end; 
				if id == 5 then cards.LIST[id].CARD = cardPool.LIST[math.random(125,137)]; end;
				if id == 6 then	cards.LIST[id].CARD = cardPool.LIST[138]; end;	
			end;	

		end;
		
		if cards.State == 'Base Building' then
			if interfaceSide == 1 then cards.LIST[id].CARD = cardPool.LIST[id]; end;
			if interfaceSide == 3 then cards.LIST[id].CARD = cardPool.LIST[id+5]; end;
			if interfaceSide == 2 then cards.LIST[id].CARD = cardPool.LIST[id+10]; end;			
		end;
		
        cards.LIST[id].animtime = cards.ANIMTIME+cards.ANIMMOVETIME;
        if cards.LIST[id].ELE == nil then
        	cards.createCard(id);
        else
                cards.updateCard(id);
        end;

        setY(cards.LIST[id].ELE,cards.CARDHEIGHT);
        setY(cards.LIST[id].ELE.BACK,cards.CARDHEIGHT);

        cards.USED = cards.USED + 1;
end;

function cards.createOutlineLabel(PARENT,POSSIZE,FONT1,FONT2,ID)
	local ELE = getLabelEX(PARENT,anchorNone,POSSIZE,FONT1,'',{font_colour=cards.LIST[ID].CARD.COL,shadowtext=true,text_halign=ALIGN_MIDDLE});
        ELE.o = getLabelEX(PARENT,anchorNone,POSSIZE,FONT2,'',{font_colour=cards.LIST[ID].CARD.COL,shadowtext=false,text_halign=ALIGN_MIDDLE});
        return ELE;
end;

function cards.setOutlineLabelCol(ELE,COL1,COL2) -- COL1 is outline, COL2 is inner colour
	setFontColour(ELE,COL1);
    setFontColour(ELE.o,COL2);
end;

function cards.createCard(ID)
	if cards.State ~= 'Base Building' then 
		if ID == 5 then 
			cards.LIST[ID].ELE = getElementEX(cards.ELE,anchorLT,XYWH((ID-1)*cards.CARDWIDTH,150,142,138),true,{}); 
			cards.LIST[ID].ELE.BACK = getElementEX(cards.ELE,anchorLT,XYWH((ID-1)*250,0,142,138),true,{texture='cardtest1.png',callback_mouseclick='cards.onCardClick('..ID..')'}); 
		end;
		
		if ID == 6 then 
			cards.LIST[ID].ELE = getElementEX(cards.ELE,anchorLT,XYWH((ID-1)*cards.CARDWIDTH,150,142,138),true,{}); 
			cards.LIST[ID].ELE.BACK = getElementEX(cards.ELE,anchorLT,XYWH((ID-1)*250,0,142,138),true,{texture='cardtest1.png',callback_mouseclick='cards.onCardClick('..ID..')'}); 
		end;
		
		if ID < 5 then cards.LIST[ID].ELE = getElementEX(cards.ELE,anchorLT,XYWH((ID-1)*cards.CARDWIDTH,150,cards.CARDWIDTH,cards.CARDHEIGHT),true,{}); end;
		cards.LIST[ID].ELE.xicht  = getElementEX(cards.LIST[ID].ELE,anchorNone,XYWH(22,166-125,100,125),true,{});
		cards.LIST[ID].ELE.title  = cards.createOutlineLabel(cards.LIST[ID].ELE,XYWH(0,24,cards.CARDWIDTH,0),'Tahoma_70_Fixed_16_2.DFF;0.74;0.2;0.5;1','Tahoma_70_Fixed_16_2.DFF;0.74;0.4;0.5;1',ID);
		cards.LIST[ID].ELE.attrib = getLabelEX(cards.LIST[ID].ELE,anchorNone,XYWH(106,66,120,74),Tahoma_16B,'',{font_colour=purple,font_style_outline=true,shadowtext=true,text_halign=ALIGN_MIDDLE});
		cards.LIST[ID].ELE.desc   = getLabelEX(cards.LIST[ID].ELE,anchorNone,XYWH(28,189,201,88),Tahoma_13B,'',{font_colour=description,font_style_outline=true,shadowtext=true,wordwrap=true,text_halign=ALIGN_MIDDLE});
		if ID < 5 then cards.LIST[ID].ELE.BACK = getElementEX(cards.ELE,anchorLT,XYWH((ID-1)*250,0,cards.CARDWIDTH,cards.CARDHEIGHT),true,{texture='cardtest1.png',callback_mouseclick='cards.onCardClick('..ID..')'}); end;
	end;
		
	if cards.State == 'Base Building' then 
		cards.LIST[ID].ELE = getElementEX(cards.ELE,anchorNone,XYWH(225*ID,75,cards.LIST[ID].CARD.SUBTW,cards.LIST[ID].CARD.SUBTH),true,{colour1=BLACKA(0)});
		cards.LIST[ID].ELE.xicht  = getElementEX(cards.LIST[ID].ELE,anchorNone,XYWH(0,0,0,0),true,{});
		cards.LIST[ID].ELE.subtexture = getElementEX(cards.LIST[ID].ELE,anchorNone,XYWH(cards.LIST[ID].CARD.SUBTW/2,0,cards.LIST[ID].CARD.SUBTW,cards.LIST[ID].CARD.SUBTH),true,{texture=cards.LIST[ID].CARD.SUBTEXTURE});
		cards.LIST[ID].ELE.attrib = getLabelEX(cards.LIST[ID].ELE,anchorNone,XYWH(0,0,0,0),Tahoma_16B,'',{font_colour=purple,font_style_outline=true,shadowtext=true,text_halign=ALIGN_MIDDLE});
		cards.LIST[ID].ELE.desc   = getLabelEX(cards.LIST[ID].ELE,anchorNone,XYWH(0,0,0,0),Tahoma_13B,'',{font_colour=description,font_style_outline=true,shadowtext=true,wordwrap=true,text_halign=ALIGN_MIDDLE});
		cards.LIST[ID].ELE.title  = cards.createOutlineLabel(cards.LIST[ID].ELE,XYWH(0,-50,cards.LIST[ID].CARD.SUBTW,0),'Tahoma_70_Fixed_16_2.DFF;0.74;0.2;0.5;1','Tahoma_70_Fixed_16_2.DFF;0.74;0.4;0.5;1',ID);
		cards.LIST[ID].ELE.BACK = getElementEX(cards.ELE,anchorNone,XYWH(225*ID,75,cards.LIST[ID].CARD.SUBTW,cards.LIST[ID].CARD.SUBTH),true,{colour1=BLACKA(0),callback_mouseclick='cards.onCardClick('..ID..')'});
	end;

	if cards.State ~= 'Base Building' and cards.LIST[ID].CARD.SAILCATEGORY == 8 then 
		cards.LIST[ID].ELE.subtexture  = getElementEX(cards.LIST[ID].ELE,anchorNone,XYWH(cards.CARDWIDTH/2,100,0,0),true,{});
	end;			

        cards.updateCard(ID);
end;

discard1= getImageButtonEX(nil,anchorNone,XYWH(cards.CARDWIDTH-5,ScrHeight-cards.CARDHEIGHT-25,60,25),'Discard','','for i = 1,2 do if MULTI_PLAYERINFO_CURRENT_PLID[MyID].COLOUR == Players[i][4] and Players[i][2] > 10 then cards.freeCard(1); OW_CUSTOM_COMMAND(889,Players[i][4],10); end; end;',SKINTYPE_BUTTON,{colour2=BLACKA(350), font_colour_disabled=GRAY(127),});	
discard2= getImageButtonEX(nil,anchorNone,XYWH(cards.CARDWIDTH*2-5,ScrHeight-cards.CARDHEIGHT-25,60,25),'Discard','','for i = 1,2 do if MULTI_PLAYERINFO_CURRENT_PLID[MyID].COLOUR == Players[i][4] and Players[i][2] > 10 then cards.freeCard(2); OW_CUSTOM_COMMAND(889,Players[i][4],10); end; end;',SKINTYPE_BUTTON,{colour2=BLACKA(350), font_colour_disabled=GRAY(127),});	
discard3= getImageButtonEX(nil,anchorNone,XYWH(cards.CARDWIDTH*3-5,ScrHeight-cards.CARDHEIGHT-25,60,25),'Discard','','for i = 1,2 do if MULTI_PLAYERINFO_CURRENT_PLID[MyID].COLOUR == Players[i][4] and Players[i][2] > 10 then cards.freeCard(3); OW_CUSTOM_COMMAND(889,Players[i][4],10); end; end;',SKINTYPE_BUTTON,{colour2=BLACKA(350), font_colour_disabled=GRAY(127),});	
discard4= getImageButtonEX(nil,anchorNone,XYWH(cards.CARDWIDTH*4-5,ScrHeight-cards.CARDHEIGHT-25,60,25),'Discard','','for i = 1,2 do if MULTI_PLAYERINFO_CURRENT_PLID[MyID].COLOUR == Players[i][4] and Players[i][2] > 10 then cards.freeCard(4); OW_CUSTOM_COMMAND(889,Players[i][4],10); end; end;',SKINTYPE_BUTTON,{colour2=BLACKA(350), font_colour_disabled=GRAY(127),});	
discard5= getImageButtonEX(nil,anchorNone,XYWH(cards.CARDWIDTH*5-5,ScrHeight-cards.CARDHEIGHT-25,60,25),'Discard','','for i = 1,2 do if MULTI_PLAYERINFO_CURRENT_PLID[MyID].COLOUR == Players[i][4] and Players[i][2] > 10 then cards.freeCard(5); OW_CUSTOM_COMMAND(889,Players[i][4],10); end; end;',SKINTYPE_BUTTON,{colour2=BLACKA(350), font_colour_disabled=GRAY(127),});	

setVisible(discard1,false);
setVisible(discard2,false);
setVisible(discard3,false);
setVisible(discard4,false);
setVisible(discard5,false);

function cards.updateCard(ID)
	if cards.State ~= 'Base Building' then setTexture(cards.LIST[ID].ELE,cards.LIST[ID].CARD.TEXTURE); end;
	
	setTexture(cards.LIST[ID].ELE.xicht,cards.LIST[ID].CARD.XICHT);
	
		if cards.LIST[ID].CARD.SAILCATEGORY == 8 then
			setTexture(cards.LIST[ID].ELE.subtexture,cards.LIST[ID].CARD.SUBTEXTURE);
			setWH(cards.LIST[ID].ELE.subtexture,cards.LIST[ID].CARD.SUBTW,cards.LIST[ID].CARD.SUBTH);
			setX(cards.LIST[ID].ELE.subtexture,getX(cards.LIST[ID].ELE.subtexture)-cards.LIST[ID].CARD.SUBTW/2);
		end;
		
        setRotation(cards.LIST[ID].ELE,SetRotate(1,0,true));
        setRotation(cards.LIST[ID].ELE.BACK,SetRotate(1,0,false));
        setVisible(cards.LIST[ID].ELE,true);
        setVisible(cards.LIST[ID].ELE.BACK,true);

        setText(cards.LIST[ID].ELE.title,cards.LIST[ID].CARD.TITLE);
        setText(cards.LIST[ID].ELE.title.o,cards.LIST[ID].CARD.TITLE);

        setText(cards.LIST[ID].ELE.attrib,cards.LIST[ID].CARD.ATTRIB);
        setText(cards.LIST[ID].ELE.desc,cards.LIST[ID].CARD.DESC);		

        cards.onUpdateCard(ID);
end;

function cards.setRotation(ID)
	local a = (cards.ANIMTIME-cards.LIST[ID].animtime)/cards.ANIMTIME*180;
  	setRotation(cards.LIST[ID].ELE,SetRotate(1,a,true));
        setRotation(cards.LIST[ID].ELE.BACK,SetRotate(1,a,false));
        setY(cards.LIST[ID].ELE,0);
        setY(cards.LIST[ID].ELE.BACK,0);
end;

function cards.setMovement(ID)
	local y = (cards.LIST[ID].animtime-cards.ANIMTIME)/cards.ANIMMOVETIME*cards.CARDHEIGHT;
        setY(cards.LIST[ID].ELE,y);
        setY(cards.LIST[ID].ELE.BACK,y);
end;

function cards.animate(ID,FRAMETIME)
	if (cards.LIST[ID].V) and (cards.LIST[ID].animtime > 0) then
        	cards.LIST[ID].animtime = cards.LIST[ID].animtime - FRAMETIME;
                if cards.LIST[ID].animtime < 0 then
                	cards.LIST[ID].animtime = 0;
                end;
                if cards.LIST[ID].animtime > cards.ANIMTIME then
                	cards.setMovement(ID);
                else
        		cards.setRotation(ID);
                end;
        end;
end;

function cards.freeCard(ID)
	if not cards.LIST[ID].V then
        	return;
        end;

        cards.LIST[ID].V = false;
        setVisible(cards.LIST[ID].ELE,false);
        setVisible(cards.LIST[ID].ELE.BACK,false);

        if cards.State ~= 'Base Building' then cards.USED = cards.USED - 1; end;
end;

function cards.tick(FRAMETIME)
	if cards.USED > 0 then
        	for i=1,cards.COUNT do
                	cards.animate(i,FRAMETIME);
                end;
        end;
		
		if cards.State == "Game" then 
			if (cards.USED < cards.COUNT) and (math.random(50) == 1) then
        	cards.addCard(cardPool.randomCard(),0);
			end;		
		end;

		if cards.State == "Base Building" then 
			if (cards.USED < cards.COUNT) then
        	cards.addCard(cardPool.randomCard(),0);
			end;		
		end;
end;

function cardPool.add(DATA)
	cardPool.COUNT=cardPool.COUNT+1;
	cardPool.LIST[cardPool.COUNT] = DATA;
        return cardPool.COUNT;
end;

function cardPool.randomCard()
	return cardPool.LIST[math.random(1,cardPool.COUNT)];
end;

regTickCallback('cards.tick(%frametime);');

-----------------------------------------------------

cards.ELE = getElementEX(nil,anchorNone,XYWH(150,ScrHeight-cards.CARDHEIGHT,5*cards.CARDWIDTH,cards.CARDHEIGHT+25),true,{colour1=BLACKA(0),scissor=false});
BuildingsPlaced = {0,0};

function cards.onCardClick(ID)

if not getvalue(OWV_MULTIPLAYER) then 
	
	if cards.LIST[ID].CARD.SAILCATEGORY == 3 or cards.LIST[ID].CARD.SAILCATEGORY == 8 then CallPreview(1,cards.LIST[ID].CARD.PREVIEW,1); end;
	
	if cards.LIST[ID].animtime == 0 then

		if cards.State == 'Game' --[[and Players[i][1] >= cards.LIST[ID].CARD.PRICE--]] then
			OW_CUSTOM_COMMAND(cards.LIST[ID].CARD.SAILCATEGORY,cards.LIST[ID].CARD.SAILIDENT,cards.LIST[ID].CARD.PRICE,cards.LIST[ID].CARD.VEHICLEPART,cards.LIST[ID].CARD.NATION);				
			cards.freeCard(ID);
		end;
			
		if cards.State == 'Base Building' then
				OW_CUSTOM_COMMAND(cards.LIST[ID].CARD.SAILCATEGORY,cards.LIST[ID].CARD.SAILIDENT,cards.LIST[ID].CARD.NATION,0,0);
				cards.freeCard(ID); --setVisible(cards.LIST[ID].ELE,false);
				if ID == 2 or ID == 3 then BuildingsPlaced[1] = BuildingsPlaced[1] +2; end;
				if ID == 1 or ID == 4 or ID == 5 then BuildingsPlaced[1] = BuildingsPlaced[1] +1; end;				
				if BuildingsPlaced[1] == 7 or BuildingsPlaced[1] > 7 then cards.State = 'Game'; 
					setVisible(discard1,true);
					setVisible(discard2,true);
					setVisible(discard3,true);
					setVisible(discard4,true);
					setVisible(discard5,true);
					cards.freeCard(1);
					cards.freeCard(2);
					cards.freeCard(3);
					cards.freeCard(4);
					cards.freeCard(5);
					cards.addCards(6);
			end;
		end;
	end;
end;


if getvalue(OWV_MULTIPLAYER) then 
	OW_CUSTOM_COMMAND(999,MULTI_PLAYERINFO_CURRENT_PLID[MyID].COLOUR);
	
	if cards.LIST[ID].CARD.SAILCATEGORY == 3 or cards.LIST[ID].CARD.SAILCATEGORY == 8 then CallPreview(1,cards.LIST[ID].CARD.PREVIEW,MULTI_PLAYERINFO_CURRENT_PLID[MyID].COLOUR); end;
	
	if cards.LIST[ID].animtime == 0 then
		for i = 1,2 do
			if MULTI_PLAYERINFO_CURRENT_PLID[MyID].COLOUR == Players[i][4] then
				if cards.State == 'Game' and Players[i][1] >= cards.LIST[ID].CARD.PRICE then
					OW_CUSTOM_COMMAND(cards.LIST[ID].CARD.SAILCATEGORY,cards.LIST[ID].CARD.SAILIDENT,cards.LIST[ID].CARD.PRICE,cards.LIST[ID].CARD.VEHICLEPART,cards.LIST[ID].CARD.NATION);				
					cards.freeCard(ID);
				end;
			end;
			
			if MULTI_PLAYERINFO_CURRENT_PLID[MyID].COLOUR == Players[i][4] then
				if cards.State == 'Base Building' then
					OW_CUSTOM_COMMAND(cards.LIST[ID].CARD.SAILCATEGORY,cards.LIST[ID].CARD.SAILIDENT,cards.LIST[ID].CARD.NATION,0,0);
					cards.freeCard(ID); --setVisible(cards.LIST[ID].ELE,false);
					if ID == 2 or ID == 3 then BuildingsPlaced[i] = BuildingsPlaced[i] +2; end;
					if ID == 1 or ID == 4 or ID == 5 then BuildingsPlaced[i] = BuildingsPlaced[i] +1; end;				
					if BuildingsPlaced[i] == 7 or BuildingsPlaced[i] > 7 then cards.State = 'Game'; 
						setVisible(discard1,true);
						setVisible(discard2,true);
						setVisible(discard3,true);
						setVisible(discard4,true);
						setVisible(discard5,true);
						cards.freeCard(1);
						cards.freeCard(2);
						cards.freeCard(3);
						cards.freeCard(4);
						cards.freeCard(5);
						cards.addCards(6);
					end;
				end;
			end;
		end;
	end;
end;
end;

function cards.onUpdateCard(ID)
	-- Add code to change looks/etc here

	if cards.State == 'Game' then 

		if ID == 6 then setVisible(cards.LIST[6].ELE,false); end;
		
		if ID == 5 then
			setXY(cards.LIST[ID].ELE.title,-30,5);
			setXY(cards.LIST[ID].ELE.title.o,-30,5);
			setXY(cards.LIST[ID].ELE.desc,5,90);
			setXY(cards.LIST[ID].ELE.attrib,110,35);
		end;

		setFontColour(cards.LIST[ID].ELE.title,cards.LIST[ID].CARD.COL);
		setFontColour(cards.LIST[ID].ELE.title.o,cards.LIST[ID].CARD.COL);
	end;
	
end;

Players = {};

function VerifyResources(DEPOT, CRATES, OIL, ALASKITE, SIDE)
	if DEPOT == 1 then Players[1][1] = CRATES; Players[1][2] = OIL; Players[1][3] = ALASKITE; Players[1][4] = SIDE; end;
	if DEPOT == 2 then Players[2][1] = CRATES; Players[2][2] = OIL; Players[2][3] = ALASKITE; Players[2][4] = SIDE; end;	
	if DEPOT == 3 then Players[1][1] = CRATES; Players[1][2] = OIL; Players[1][3] = ALASKITE; end;
	if DEPOT == 4 then Players[2][1] = CRATES; Players[2][2] = OIL; Players[2][3] = ALASKITE; end;	
end;

-- translations
TID_CARDS_Soldier = 48100;
TID_CARDS_Engineer = 48101;
TID_CARDS_Mechanic = 48102;
TID_CARDS_Scientist = 48103;
TID_CARDS_Sniper = 48104;
TID_CARDS_Mortar = 48105;
TID_CARDS_Bazooker = 48106;
TID_CARDS_Sheikh = 48107;

TID_CARDS_Apeman = 48108;
TID_CARDS_ExtremeApeman = 48109;
TID_CARDS_Kamikaze = 48110;
TID_CARDS_Machairodus = 48111;

TID_CARDS_Breastworks = 48125;
TID_CARDS_Derrick = 48126;
TID_CARDS_AlaskiteMine = 48127;
TID_CARDS_Laboratory = 48128;
TID_CARDS_Workshop = 48129;
TID_CARDS_CombustionPowerPlant = 48130;
TID_CARDS_Armoury = 48131;
TID_CARDS_Barracks = 48132;
TID_CARDS_Behemoth = 48329;

TID_CARDS_YourLobbyHero = 48999;

TID_CARDS_Crates = 48950;

TID_CARDS_ArthurPowell = 49000;
TID_CARDS_HughStevens = 49001;
TID_CARDS_PeterRoth = 49002;
TID_CARDS_RonHarrison = 49003;
TID_CARDS_JohnMacmillan = 49004;
TID_CARDS_FrankForsyth = 49005;
TID_CARDS_JeffBrown = 49006;
TID_CARDS_AndyCornell = 49007;
TID_CARDS_BobbyBrandon = 49008;
TID_CARDS_CathySimms = 49009;
TID_CARDS_ConnieTraverse = 49010;
TID_CARDS_CyrusParker = 49011;
TID_CARDS_DenisPeterson = 49012;
TID_CARDS_GaryGrant = 49013;
TID_CARDS_JeremySikorski = 49014;
TID_CARDS_JoanFergusson = 49015;
TID_CARDS_LisaLawson = 49016;
TID_CARDS_LucyDonaldson = 49017;
TID_CARDS_PaulKhattam = 49018;
TID_CARDS_PeterVanHouten = 49019;
TID_CARDS_TimGladstone = 49020;
TID_CARDS_YamokoKikuchi = 49021;

TID_CARDS_AbdulShariff = 49500;
TID_CARDS_RolfBergkamp = 49501;
TID_CARDS_DietrichGensher = 49502;
TID_CARDS_RobertFarmer = 49503;
TID_CARDS_HeikeSteyer = 49504;
TID_CARDS_KurtSchmidt = 49505;
TID_CARDS_SheikhOmar = 49506;
TID_CARDS_Sheherezade = 49507;
TID_CARDS_RaulXavier = 49508;

TID_CARDS_BurlakGorky = 50000;
TID_CARDS_MajorPlatonov = 50001;
TID_CARDS_SergeyPopov = 50002;
TID_CARDS_MarshalYashin = 50003;
TID_CARDS_IvanKurin = 50004;
TID_CARDS_Belkov = 50005;
TID_CARDS_OlegGleb = 50006;
TID_CARDS_Borodin = 50007;
TID_CARDS_Bystrov = 50008;
TID_CARDS_DmitriGladkov = 50009;
TID_CARDS_Eisenstein = 50010;
TID_CARDS_FyodorFurmanov = 50011;
TID_CARDS_Gnyevko = 50012;
TID_CARDS_Gossudarov = 50013;
TID_CARDS_Grishko = 50014;
TID_CARDS_IlyaKovalyuk = 50015;
TID_CARDS_Karamazov = 50016;
TID_CARDS_Kirilenkova = 50017;
TID_CARDS_KonstantinFadeev = 50018;
TID_CARDS_Kuzmov = 50019;
TID_CARDS_LazarDolgov = 50020;
TID_CARDS_LeonidOblukov = 50021;
TID_CARDS_LevTitov = 50022;
TID_CARDS_Lipshchin = 50023;
TID_CARDS_MikhailBerezov = 50024;
TID_CARDS_Morozov = 50025;
TID_CARDS_NikitaKozlov = 50026;
TID_CARDS_OlgaKapitsova = 50027;
TID_CARDS_Petrosyan = 50028;
TID_CARDS_Petrovova = 50029;
TID_CARDS_Pokryshkin = 50030;
TID_CARDS_PyotrDavidov = 50031;
TID_CARDS_Scholtze = 50032;
TID_CARDS_Stolypin = 50033;
TID_CARDS_TimurGaydar = 50034;
TID_CARDS_Tsarytsyn = 50035;
TID_CARDS_VsevolodGorky = 50036;

-- Colours
blue = RGB(100,149,237);
red =  RGB(240,128,128);
yellow = RGB(255,255,0);
description = RGB(210,180,140);
purple = RGB(230,230,250);

-- Directories
EMPTY = 'SGUI/Nao/cards/Skeletons/EmptyXicht.png';
ANIMALS = 'SGUI/Nao/cards/Animals/';
BUILDINGS = 'SGUI/Nao/cards/Buildings/';
VEHICLES = 'SGUI/Nao/cards/Vehicles/';
AMERICAN = 'skeleton_vehicles_american.png';
ARABIAN = 'skeleton_vehicles_arabian.png';
RUSSIAN = 'skeleton_vehicles_russian.png';
HUMANS = 'SGUI/Nao/cards/Humans/';
POWERS = 'SGUI/Nao/cards/Powers/';
RESOURCES = 'SGUI/Nao/cards/Resources/';
SKELETONS = 'SGUI/Nao/cards/Skeletons/Cardfront.png';
Start = 'SGUI/Nao/cards/Starting/';

-- American starter pack 1-5
cardPool.add({SAILCATEGORY=8,SAILIDENT=1,NATION=1,PREVIEW=10,--[[TEXTURE=Start..'backAmerican.png',--]]SUBTEXTURE=BUILDINGS..'b-u-warehouse.png',SUBTW=176,SUBTH=220,XICHT=EMPTY,COL=blue,TITLE='Warehouse',ATTRIB='',DESC=''});
cardPool.add({SAILCATEGORY=8,SAILIDENT=2,NATION=1,PREVIEW=14,--[[TEXTURE=Start..'backAmerican.png',--]]SUBTEXTURE=BUILDINGS..'b-u-bunker.png',SUBTW=88,SUBTH=76,XICHT=EMPTY,COL=blue,TITLE='Turret',ATTRIB='',DESC=''});
cardPool.add({SAILCATEGORY=8,SAILIDENT=3,NATION=1,PREVIEW=14,--[[TEXTURE=Start..'backAmerican.png',--]]SUBTEXTURE=BUILDINGS..'b-u-bunker.png',SUBTW=88,SUBTH=76,XICHT=EMPTY,COL=blue,TITLE='Turret',ATTRIB='',DESC=''});
cardPool.add({SAILCATEGORY=8,SAILIDENT=5,NATION=1,PREVIEW=18,--[[TEXTURE=Start..'backAmerican.png',--]]SUBTEXTURE=BUILDINGS..'b-u-oilpower.png',SUBTW=84,SUBTH=146,XICHT=EMPTY,COL=blue,TITLE='Power',ATTRIB='',DESC=''});
cardPool.add({SAILCATEGORY=8,SAILIDENT=4,NATION=1,PREVIEW=15,--[[TEXTURE=Start..'backAmerican.png',--]]SUBTEXTURE=BUILDINGS..'b-u-oilmine.png',SUBTW=84,SUBTH=82,XICHT=EMPTY,COL=blue,TITLE='Derrick',ATTRIB='',DESC=''});
-- Arabian starter pack 6-10
cardPool.add({SAILCATEGORY=8,SAILIDENT=1,NATION=2,PREVIEW=10,--[[TEXTURE=Start..'backArabian.png',--]]SUBTEXTURE=BUILDINGS..'b-a-warehouse.png',SUBTW=138,SUBTH=208,XICHT=EMPTY,COL=yellow,TITLE='Warehouse',ATTRIB='',DESC=''});
cardPool.add({SAILCATEGORY=8,SAILIDENT=2,NATION=2,PREVIEW=14,--[[TEXTURE=Start..'backArabian.png',--]]SUBTEXTURE=BUILDINGS..'b-a-bunker.png',SUBTW=82,SUBTH=72,XICHT=EMPTY,COL=yellow,TITLE='Turret',ATTRIB='',DESC=''});
cardPool.add({SAILCATEGORY=8,SAILIDENT=3,NATION=2,PREVIEW=14,--[[TEXTURE=Start..'backArabian.png',--]]SUBTEXTURE=BUILDINGS..'b-a-bunker.png',SUBTW=82,SUBTH=72,XICHT=EMPTY,COL=yellow,TITLE='Turret',ATTRIB='',DESC=''});
cardPool.add({SAILCATEGORY=8,SAILIDENT=5,NATION=2,PREVIEW=18,--[[TEXTURE=Start..'backArabian.png',--]]SUBTEXTURE=BUILDINGS..'b-a-oilpower.png',SUBTW=102,SUBTH=164,XICHT=EMPTY,COL=yellow,TITLE='Power',ATTRIB='',DESC=''});
cardPool.add({SAILCATEGORY=8,SAILIDENT=4,NATION=2,PREVIEW=15,--[[TEXTURE=Start..'backArabian.png',--]]SUBTEXTURE=BUILDINGS..'b-a-oilmine.png',SUBTW=82,SUBTH=158,XICHT=EMPTY,COL=yellow,TITLE='Derrick',ATTRIB='',DESC=''});
-- Russian starter pack 11-15
cardPool.add({SAILCATEGORY=8,SAILIDENT=1,NATION=3,PREVIEW=10,--[[TEXTURE=Start..'backRussian.png',--]]SUBTEXTURE=BUILDINGS..'b-r-warehouse.png',SUBTW=164,SUBTH=230,XICHT=EMPTY,COL=red,TITLE='Warehouse',ATTRIB='',DESC=''});
cardPool.add({SAILCATEGORY=8,SAILIDENT=2,NATION=3,PREVIEW=14,--[[TEXTURE=Start..'backRussian.png',--]]SUBTEXTURE=BUILDINGS..'b-r-bunker.png',SUBTW=86,SUBTH=70,XICHT=EMPTY,COL=red,TITLE='Turret',ATTRIB='',DESC=''});
cardPool.add({SAILCATEGORY=8,SAILIDENT=3,NATION=3,PREVIEW=14,--[[TEXTURE=Start..'backRussian.png',--]]SUBTEXTURE=BUILDINGS..'b-r-bunker.png',SUBTW=86,SUBTH=70,XICHT=EMPTY,COL=red,TITLE='Turret',ATTRIB='',DESC=''});
cardPool.add({SAILCATEGORY=8,SAILIDENT=5,NATION=3,PREVIEW=18,--[[TEXTURE=Start..'backRussian.png',--]]SUBTEXTURE=BUILDINGS..'b-r-oilpower.png',SUBTW=82,SUBTH=102,XICHT=EMPTY,COL=red,TITLE='Power',ATTRIB='',DESC=''});
cardPool.add({SAILCATEGORY=8,SAILIDENT=4,NATION=3,PREVIEW=15,--[[TEXTURE=Start..'backRussian.png',--]]SUBTEXTURE=BUILDINGS..'b-r-oilmine.png',SUBTW=88,SUBTH=122,XICHT=EMPTY,COL=red,TITLE='Derrick',ATTRIB='',DESC=''});

-- American Deck 16-34
	-- Resources/
	cardPool.add({PRICE=10,SAILCATEGORY=6,SAILIDENT=1,TEXTURE=RESOURCES..'Crates.png',ATTRIB='',XICHT=EMPTY,DESC=loc(TID_CARDS_Crates),COL=description});
	-- Animals/
	cardPool.add({PRICE=0,SAILCATEGORY=2,SAILIDENT=1,TEXTURE=ANIMALS..'Apeman.png',ATTRIB='x0 crates',XICHT=EMPTY,DESC=loc(TID_CARDS_Apeman),COL=description});
	cardPool.add({PRICE=0,SAILCATEGORY=2,SAILIDENT=2,TEXTURE=ANIMALS..'ExtremeApeman.png',ATTRIB='x0 crates',XICHT=EMPTY,DESC=loc(TID_CARDS_ExtremeApeman),COL=description});
	cardPool.add({PRICE=8,SAILCATEGORY=2,SAILIDENT=3,TEXTURE=ANIMALS..'Kamikaze.png',ATTRIB='x8 crates',XICHT=EMPTY,DESC=loc(TID_CARDS_Kamikaze),COL=description});
	cardPool.add({PRICE=0,SAILCATEGORY=2,SAILIDENT=4,TEXTURE=ANIMALS..'Machairodus.png',ATTRIB='x0 crates',XICHT=EMPTY,DESC=loc(TID_CARDS_Machairodus),COL=description});
	-- Buildings/
	cardPool.add({PRICE=20,SAILCATEGORY=3,SAILIDENT=31,PREVIEW=15,NATION=1,TEXTURE=BUILDINGS..'AmericanBreastwork.png',ATTRIB='x20 crates',XICHT=EMPTY,COL=blue,DESC=loc(TID_CARDS_Breastworks)});
	cardPool.add({PRICE=20,SAILCATEGORY=3,SAILIDENT=29,PREVIEW=15,NATION=1,TEXTURE=BUILDINGS..'AmericanDerrick.png',ATTRIB='x20 crates',XICHT=EMPTY,COL=blue,DESC=loc(TID_CARDS_Derrick)});
	cardPool.add({PRICE=20,SAILCATEGORY=3,SAILIDENT=30,PREVIEW=15,NATION=1,TEXTURE=BUILDINGS..'AmericanAlaskiteMine.png',ATTRIB='x20 crates',XICHT=EMPTY,COL=blue,DESC=loc(TID_CARDS_AlaskiteMine)});
	cardPool.add({PRICE=30,SAILCATEGORY=3,SAILIDENT=6,PREVIEW=15,NATION=1,TEXTURE=BUILDINGS..'AmericanLaboratory.png',ATTRIB='x30 crates',XICHT=EMPTY,COL=blue,DESC=loc(TID_CARDS_Laboratory)});
	cardPool.add({PRICE=30,SAILCATEGORY=3,SAILIDENT=2,PREVIEW=15,NATION=1,TEXTURE=BUILDINGS..'AmericanWorkshop.png',ATTRIB='x30 crates',XICHT=EMPTY,COL=blue,DESC=loc(TID_CARDS_Workshop)});
	cardPool.add({PRICE=25,SAILCATEGORY=3,SAILIDENT=26,PREVIEW=15,NATION=1,TEXTURE=BUILDINGS..'AmericanCombustionPowerPlant.png',ATTRIB='x25 crates',XICHT=EMPTY,COL=blue,DESC=loc(TID_CARDS_CombustionPowerPlant)});
	cardPool.add({PRICE=45,SAILCATEGORY=3,SAILIDENT=4,PREVIEW=19,NATION=1,TEXTURE=BUILDINGS..'AmericanArmoury.png',ATTRIB='x45 crates',XICHT=EMPTY,COL=blue,DESC=loc(TID_CARDS_Armoury)});
	cardPool.add({PRICE=25,SAILCATEGORY=3,SAILIDENT=5,PREVIEW=15,NATION=1,TEXTURE=BUILDINGS..'AmericanBarracks.png',ATTRIB='x25 crates',XICHT=EMPTY,COL=blue,DESC=loc(TID_CARDS_Barracks)});
	-- Humans/
	cardPool.add({PRICE=0 ,SAILCATEGORY=5,SAILIDENT=1,NATION=1,TEXTURE=HUMANS..'AmericanSoldier.png',XICHT=EMPTY,COL=blue,TITLE='American Soldier',ATTRIB='1x soldier\nx0 crates',DESC=loc(TID_CARDS_Soldier)});
	cardPool.add({PRICE=10,SAILCATEGORY=5,SAILIDENT=2,NATION=1,TEXTURE=HUMANS..'AmericanEngineer.png',XICHT=EMPTY,COL=blue,TITLE='American Engineer',ATTRIB='1x engineer\nx10 crates',DESC=loc(TID_CARDS_Engineer)});
	-- (heroes)
	cardPool.add({PRICE=10,SAILCATEGORY=4,SAILIDENT=99,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/LobbyHeroXicht.png',COL=blue,TITLE='Your Lobby Hero',ATTRIB='x10 crates',DESC=loc(TID_CARDS_YourLobbyHero)});
	cardPool.add({PRICE=20,SAILCATEGORY=4,SAILIDENT=100,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/ArthurPowellXicht.png',COL=blue,TITLE='Arthur Powell',ATTRIB='x20 crates',DESC=loc(TID_CARDS_ArthurPowell)});
	cardPool.add({PRICE=20,SAILCATEGORY=4,SAILIDENT=101,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/HuckStevensXicht.png',COL=blue,TITLE='Hugh Stevens',ATTRIB='x20 crates',DESC=loc(TID_CARDS_HughStevens)});
	cardPool.add({PRICE=15,SAILCATEGORY=4,SAILIDENT=102,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/PeterRothXicht.png',COL=blue,TITLE='Peter Roth',ATTRIB='x15 crates',DESC=loc(TID_CARDS_PeterRoth)});
	cardPool.add({PRICE=18,SAILCATEGORY=4,SAILIDENT=103,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/RonHarrisonXicht.png',COL=blue,TITLE='Ron Harrison',ATTRIB='x18 crates',DESC=loc(TID_CARDS_RonHarrison)});
	cardPool.add({PRICE=60,SAILCATEGORY=4,SAILIDENT=104,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/JohnMacmillanXicht.png',COL=blue,TITLE='John Macmillan',ATTRIB='x60 crates',DESC=loc(TID_CARDS_JohnMacmillan)});
	cardPool.add({PRICE=24,SAILCATEGORY=4,SAILIDENT=105,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/FrankForsythXicht.png',COL=blue,TITLE='Frank Forsyth',ATTRIB='x24 crates',DESC=loc(TID_CARDS_FrankForsyth)});
	cardPool.add({PRICE=9,SAILCATEGORY=4,SAILIDENT=106,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/JeffBrownXicht.png',COL=blue,TITLE='Jeff Brown',ATTRIB='x9 crates',DESC=loc(TID_CARDS_JeffBrown)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=120,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/TimGladstoneXicht.png',COL=blue,TITLE='Tim Gladstone',ATTRIB='x0 crates',DESC=loc(TID_CARDS_TimGladstone)});
	cardPool.add({PRICE=18,SAILCATEGORY=4,SAILIDENT=116,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/LisaLawsonXicht.png',COL=blue,TITLE='Lisa Lawson',ATTRIB='x18 crates',DESC=loc(TID_CARDS_LisaLawson)});
	--[[cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=107,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/AndyCornellXicht.png',COL=blue,TITLE='Andy Cornell',ATTRIB='Andy Cornell\nx9 crates',DESC=loc(TID_CARDS_AndyCornell)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=108,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/BobbyBrandonXicht.png',COL=blue,TITLE='Bobby Brandon',ATTRIB='Bobby Brandon\nx9 crates',DESC=loc(TID_CARDS_BobbyBrandon)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=109,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/CathySimmsXicht.png',COL=blue,TITLE='Cathy Simms',ATTRIB='Cathy Simms\nx9 crates',DESC=loc(TID_CARDS_CathySimms)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=110,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/ConnieTraverseXicht.png',COL=blue,TITLE='Connie Traverse',ATTRIB='Connie Traverse\nx9 crates',DESC=loc(TID_CARDS_ConnieTraverse)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=111,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/CyrusParkerXicht.png',COL=blue,TITLE='Cyrus Parker',ATTRIB='Cyrus Parker\nx9 crates',DESC=loc(TID_CARDS_CyrusParker)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=112,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/DenisPetersonXicht.png',COL=blue,TITLE='Denis Peterson',ATTRIB='Denis Peterson\nx9 crates',DESC=loc(TID_CARDS_DenisPeterson)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=113,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/GaryGrantXicht.png',COL=blue,TITLE='Gary Grant',ATTRIB='Gary Grant\nx9 crates',DESC=loc(TID_CARDS_GaryGrant)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=114,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/JeremySikorskiXicht.png',COL=blue,TITLE='Jeremy Sikorski',ATTRIB='Jeremy Sikorski\nx9 crates',DESC=loc(TID_CARDS_JeremySikorski)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=115,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/JoanFergussonXicht.png',COL=blue,TITLE='Joan Fergusson',ATTRIB='Joan Fergusson\nx9 crates',DESC=loc(TID_CARDS_JoanFergusson)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=117,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/LucyDonaldsonXicht.png',COL=blue,TITLE='Lucy Donaldson',ATTRIB='Lucy Donaldson\nx9 crates',DESC=loc(TID_CARDS_LucyDonaldson)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=118,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/PaulKhattamXicht.png',COL=blue,TITLE='Paul Khattam',ATTRIB='Paul Khattam\nx9 crates',DESC=loc(TID_CARDS_PaulKhattam)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=119,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/PeterVanHoutenXicht.png',COL=blue,TITLE='Peter Van Houten',ATTRIB='Peter Van Houten\nx9 crates',DESC=loc(TID_CARDS_PeterVanHouten)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=121,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/YamokoKikuchiXicht.png',COL=blue,TITLE='Yamoko Kikuchi',ATTRIB='Yamoko Kikuchi\nx9 crates',DESC=loc(TID_CARDS_YamokoKikuchi)});--]]
	cardPool.add({PRICE=75,SAILCATEGORY=4,SAILIDENT=2001,TEXTURE=SKELETONS,XICHT=EMPTY,COL=blue,TITLE='(temp card)',ATTRIB='x75 crates',DESC=loc(TID_CARDS_Eisenstein)});
	cardPool.add({PRICE=75,SAILCATEGORY=4,SAILIDENT=2002,TEXTURE=SKELETONS,XICHT=EMPTY,COL=blue,TITLE='(temp card)',ATTRIB='x75 crates',DESC=loc(TID_CARDS_Tsarytsyn)});
	
-- Arabian Deck 35-53
	-- Resources/
	cardPool.add({PRICE=10,SAILCATEGORY=6,SAILIDENT=1,TEXTURE=RESOURCES..'Crates.png',ATTRIB='x10 crates.',XICHT=EMPTY,DESC=loc(TID_CARDS_Crates),COL=description});
	-- Animals/
	cardPool.add({PRICE=0,SAILCATEGORY=2,SAILIDENT=1,TEXTURE=ANIMALS..'Apeman.png',ATTRIB='x0 crates',XICHT=EMPTY,DESC=loc(TID_CARDS_Apeman),COL=description});
	cardPool.add({PRICE=0,SAILCATEGORY=2,SAILIDENT=2,TEXTURE=ANIMALS..'ExtremeApeman.png',ATTRIB='x0 crates',XICHT=EMPTY,DESC=loc(TID_CARDS_ExtremeApeman),COL=description});
	cardPool.add({PRICE=8,SAILCATEGORY=2,SAILIDENT=3,TEXTURE=ANIMALS..'Kamikaze.png',ATTRIB='x8 crates',XICHT=EMPTY,DESC=loc(TID_CARDS_Kamikaze),COL=description});
	cardPool.add({PRICE=0,SAILCATEGORY=2,SAILIDENT=4,TEXTURE=ANIMALS..'Machairodus.png',ATTRIB='x0 crates',XICHT=EMPTY,DESC=loc(TID_CARDS_Machairodus),COL=description});
	-- Buildings/
	cardPool.add({PRICE=20,SAILCATEGORY=3,SAILIDENT=31,PREVIEW=15,NATION=2,TEXTURE=BUILDINGS..'ArabianBreastwork.png',ATTRIB='x20 crates',XICHT=EMPTY,COL=yellow,DESC=loc(TID_CARDS_Breastworks)});
	cardPool.add({PRICE=20,SAILCATEGORY=3,SAILIDENT=29,PREVIEW=15,NATION=2,TEXTURE=BUILDINGS..'ArabianDerrick.png',ATTRIB='x20 crates',XICHT=EMPTY,COL=yellow,DESC=loc(TID_CARDS_Derrick)});
	cardPool.add({PRICE=20,SAILCATEGORY=3,SAILIDENT=30,PREVIEW=15,NATION=2,TEXTURE=BUILDINGS..'ArabianAlaskiteMine.png',ATTRIB='x20 crates',XICHT=EMPTY,COL=yellow,DESC=loc(TID_CARDS_AlaskiteMine)});
	cardPool.add({PRICE=30,SAILCATEGORY=3,SAILIDENT=6,PREVIEW=15,NATION=2,TEXTURE=BUILDINGS..'ArabianLaboratory.png',ATTRIB='x30 crates',XICHT=EMPTY,COL=yellow,DESC=loc(TID_CARDS_Laboratory)});
	cardPool.add({PRICE=30,SAILCATEGORY=3,SAILIDENT=2,PREVIEW=15,NATION=2,TEXTURE=BUILDINGS..'ArabianWorkshop.png',ATTRIB='x30 crates',XICHT=EMPTY,COL=yellow,DESC=loc(TID_CARDS_Workshop)});
	cardPool.add({PRICE=25,SAILCATEGORY=3,SAILIDENT=26,PREVIEW=15,NATION=2,TEXTURE=BUILDINGS..'ArabianCombustionPowerPlant.png',ATTRIB='x25 crates',XICHT=EMPTY,COL=yellow,DESC=loc(TID_CARDS_CombustionPowerPlant)});
	cardPool.add({PRICE=45,SAILCATEGORY=3,SAILIDENT=4,PREVIEW=19,NATION=2,TEXTURE=BUILDINGS..'ArabianArmoury.png',ATTRIB='x45 crates',XICHT=EMPTY,COL=yellow,DESC=loc(TID_CARDS_Armoury)});
	cardPool.add({PRICE=25,SAILCATEGORY=3,SAILIDENT=5,PREVIEW=15,NATION=2,TEXTURE=BUILDINGS..'ArabianBarracks.png',ATTRIB='x25 crates',XICHT=EMPTY,COL=yellow,DESC=loc(TID_CARDS_Barracks)});
	-- Humans/
	cardPool.add({PRICE=0 ,SAILCATEGORY=5,SAILIDENT=1, NATION=2,TEXTURE=HUMANS..'ArabianSoldier.png',XICHT=EMPTY,COL=yellow,TITLE='Arabian Soldier',ATTRIB='1x soldier\nx0 crates',DESC=loc(TID_CARDS_Soldier)});
	cardPool.add({PRICE=10,SAILCATEGORY=5,SAILIDENT=2 ,NATION=2,TEXTURE=HUMANS..'ArabianEngineer.png',XICHT=EMPTY,COL=yellow,TITLE='Arabian Engineer',ATTRIB='1x engineer\nx10 crates',DESC=loc(TID_CARDS_Engineer)});
	cardPool.add({PRICE=0, SAILCATEGORY=5,SAILIDENT=11,NATION=2,TEXTURE=HUMANS..'ArabianSheikh.png',XICHT=EMPTY,COL=yellow,TITLE='Arabian Sheikh',ATTRIB='1x sheikh\nx0 crates',DESC=loc(TID_CARDS_Sheikh)});
	-- (heroes)
	cardPool.add({PRICE=10,SAILCATEGORY=4,SAILIDENT=99,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/LobbyHeroXicht.png',COL=yellow,TITLE='Your Lobby Hero',ATTRIB='x10 crates',DESC=loc(TID_CARDS_YourLobbyHero)});
	cardPool.add({PRICE=30,SAILCATEGORY=4,SAILIDENT=200,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/AbdulShariffXicht.png',COL=yellow,TITLE='Abdul Shariff',ATTRIB='x30 crates',DESC=loc(TID_CARDS_AbdulShariff)});
	--cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=201,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/RolfBergkampXicht.png',COL=yellow,TITLE='Rolf Bergkamp',ATTRIB='\nx20 crates',DESC=loc(TID_CARDS_RolfBergkamp)});
	cardPool.add({PRICE=30,SAILCATEGORY=4,SAILIDENT=202,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/DietrichGensherXicht.png',COL=yellow,TITLE='Dietrich Gensher',ATTRIB='x30 crates',DESC=loc(TID_CARDS_DietrichGensher)});
	cardPool.add({PRICE=30,SAILCATEGORY=4,SAILIDENT=203,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/RobertFarmerXicht.png',COL=yellow,TITLE='Robert Farmer',ATTRIB='x30 crates',DESC=loc(TID_CARDS_RobertFarmer)});
	cardPool.add({PRICE=48,SAILCATEGORY=4,SAILIDENT=204,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/HeikeSteyerXicht.png',COL=yellow,TITLE='Heike Steyer',ATTRIB='x48 crates',DESC=loc(TID_CARDS_HeikeSteyer)});
	cardPool.add({PRICE=24,SAILCATEGORY=4,SAILIDENT=205,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/KurtSchmidtXicht.png',COL=yellow,TITLE='Kurt Schmidt',ATTRIB='x24 crates',DESC=loc(TID_CARDS_KurtSchmidt)});
	cardPool.add({PRICE=15,SAILCATEGORY=4,SAILIDENT=206,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/SheikhOmarXicht.png',COL=yellow,TITLE='Sheikh Omar',ATTRIB='x15 crates',DESC=loc(TID_CARDS_SheikhOmar)});
	cardPool.add({PRICE=25,SAILCATEGORY=4,SAILIDENT=207,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/SheherezadeXicht.png',COL=yellow,TITLE='Sheherezade',ATTRIB='x25 crates',DESC=loc(TID_CARDS_Sheherezade)});
	cardPool.add({PRICE=24,SAILCATEGORY=4,SAILIDENT=208,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/RaulXavierXicht.png',COL=yellow,TITLE='Raul Xavier',ATTRIB='x24 crates',DESC=loc(TID_CARDS_RaulXavier)});
	cardPool.add({PRICE=75,SAILCATEGORY=4,SAILIDENT=2001,TEXTURE=SKELETONS,XICHT=EMPTY,COL=yellow,TITLE='(temp card)',ATTRIB='x75 crates',DESC=loc(TID_CARDS_Eisenstein)});
	cardPool.add({PRICE=75,SAILCATEGORY=4,SAILIDENT=2002,TEXTURE=SKELETONS,XICHT=EMPTY,COL=yellow,TITLE='(temp card)',ATTRIB='x75 crates',DESC=loc(TID_CARDS_Tsarytsyn)});
	
-- Russian Deck 54-74
	-- Resources/
	cardPool.add({PRICE=10,SAILCATEGORY=6,SAILIDENT=1,TEXTURE=RESOURCES..'Crates.png',ATTRIB='x10 crates.',XICHT=EMPTY,DESC=loc(TID_CARDS_Crates),COL=description});
	-- Animals/
	cardPool.add({PRICE=0,SAILCATEGORY=2,SAILIDENT=1,TEXTURE=ANIMALS..'Apeman.png',ATTRIB='x0 crates',XICHT=EMPTY,DESC=loc(TID_CARDS_Apeman),COL=description});
	cardPool.add({PRICE=0,SAILCATEGORY=2,SAILIDENT=2,TEXTURE=ANIMALS..'ExtremeApeman.png',ATTRIB='x0 crates',XICHT=EMPTY,DESC=loc(TID_CARDS_ExtremeApeman),COL=description});
	cardPool.add({PRICE=8,SAILCATEGORY=2,SAILIDENT=3,TEXTURE=ANIMALS..'Kamikaze.png',ATTRIB='x8 crates',XICHT=EMPTY,DESC=loc(TID_CARDS_Kamikaze),COL=description});
	cardPool.add({PRICE=0,SAILCATEGORY=2,SAILIDENT=4,TEXTURE=ANIMALS..'Machairodus.png',ATTRIB='x0 crates',XICHT=EMPTY,DESC=loc(TID_CARDS_Machairodus),COL=description});
	-- Buildings/
	cardPool.add({PRICE=20,SAILCATEGORY=3,SAILIDENT=31,PREVIEW=15,NATION=3,TEXTURE=BUILDINGS..'RussianBreastwork.png',ATTRIB='x20 crates',XICHT=EMPTY,COL=red,DESC=loc(TID_CARDS_Breastworks)});
	cardPool.add({PRICE=20,SAILCATEGORY=3,SAILIDENT=29,PREVIEW=15,NATION=3,TEXTURE=BUILDINGS..'RussianDerrick.png',ATTRIB='x20 crates',XICHT=EMPTY,COL=red,DESC=loc(TID_CARDS_Derrick)});
	cardPool.add({PRICE=20,SAILCATEGORY=3,SAILIDENT=30,PREVIEW=15,NATION=3,TEXTURE=BUILDINGS..'RussianAlaskiteMine.png',ATTRIB='x20 crates',XICHT=EMPTY,COL=red,DESC=loc(TID_CARDS_AlaskiteMine)});
	cardPool.add({PRICE=30,SAILCATEGORY=3,SAILIDENT=6,PREVIEW=15,NATION=3,TEXTURE=BUILDINGS..'RussianLaboratory.png',ATTRIB='x30 crates',XICHT=EMPTY,COL=red,DESC=loc(TID_CARDS_Laboratory)});
	cardPool.add({PRICE=30,SAILCATEGORY=3,SAILIDENT=2,PREVIEW=15,NATION=3,TEXTURE=BUILDINGS..'RussianWorkshop.png',ATTRIB='x30 crates',XICHT=EMPTY,COL=red,DESC=loc(TID_CARDS_Workshop)});
	cardPool.add({PRICE=25,SAILCATEGORY=3,SAILIDENT=26,PREVIEW=15,NATION=3,TEXTURE=BUILDINGS..'RussianCombustionPowerPlant.png',ATTRIB='x25 crates',XICHT=EMPTY,COL=red,DESC=loc(TID_CARDS_CombustionPowerPlant)});
	cardPool.add({PRICE=45,SAILCATEGORY=3,SAILIDENT=4,PREVIEW=19,NATION=3,TEXTURE=BUILDINGS..'RussianArmoury.png',ATTRIB='x45 crates',XICHT=EMPTY,COL=red,DESC=loc(TID_CARDS_Armoury)});
	cardPool.add({PRICE=25,SAILCATEGORY=3,SAILIDENT=5,PREVIEW=15,NATION=3,TEXTURE=BUILDINGS..'RussianBarracks.png',ATTRIB='x25 crates',XICHT=EMPTY,COL=red,DESC=loc(TID_CARDS_Barracks)});
	cardPool.add({PRICE=0,SAILCATEGORY=3,SAILIDENT=37,PREVIEW=15,NATION=3,TEXTURE=VEHICLES..'/AllSet/RussianBehemoth.png',ATTRIB='x0 crates\ncurrently deprecated',XICHT=EMPTY,COL=red,DESC=loc(TID_CARDS_Behemoth)});
	-- Humans/
	cardPool.add({PRICE=0, SAILCATEGORY=5,SAILIDENT=1,NATION=3,TEXTURE=HUMANS..'RussianSoldier.png',XICHT=EMPTY,COL=red,TITLE='Russian Soldier',ATTRIB='x0 crates',DESC=loc(TID_CARDS_Soldier)});
	cardPool.add({PRICE=10,SAILCATEGORY=5,SAILIDENT=2,NATION=3,TEXTURE=HUMANS..'RussianEngineer.png',XICHT=EMPTY,COL=red,TITLE='Russian Engineer',ATTRIB='1x engineer\nx10 crates',DESC=loc(TID_CARDS_Engineer)});
	-- (heroes)
	cardPool.add({PRICE=10,SAILCATEGORY=4,SAILIDENT=99,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/LobbyHeroXicht.png',COL=red,TITLE='Your Lobby Hero',ATTRIB='x10 crates',DESC=loc(TID_CARDS_YourLobbyHero)});
	cardPool.add({PRICE=60,SAILCATEGORY=4,SAILIDENT=300,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/BurlakGorkyXicht.png',COL=red,TITLE='Burlak Gorky',ATTRIB='x60 crates',DESC=loc(TID_CARDS_BurlakGorky)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=301,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/MajorPlatonovXicht.png',COL=red,TITLE='Major Platonov',ATTRIB='x0 crates',DESC=loc(TID_CARDS_MajorPlatonov)});
	cardPool.add({PRICE=18,SAILCATEGORY=4,SAILIDENT=302,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/SergeyPopovXicht.png',COL=red,TITLE='Sergey I. Popov',ATTRIB='x18 crates',DESC=loc(TID_CARDS_SergeyPopov)});
	cardPool.add({PRICE=36,SAILCATEGORY=4,SAILIDENT=303,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/MarshalYashinXicht.png',COL=red,TITLE='Marshal Yashin',ATTRIB='x36 crates',DESC=loc(TID_CARDS_MarshalYashin)});
	cardPool.add({PRICE=30,SAILCATEGORY=4,SAILIDENT=304,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/IvanKurinXicht.png',COL=red,TITLE='Ivan Kurin',ATTRIB='x30 crates',DESC=loc(TID_CARDS_IvanKurin)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=314,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/GrishkoXicht.png',COL=red,TITLE='Grishko',ATTRIB='x0 crates',DESC=loc(TID_CARDS_Grishko)});
	cardPool.add({PRICE=12,SAILCATEGORY=4,SAILIDENT=305,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/BelkovXicht.png',COL=red,TITLE='P. M. Belkov',ATTRIB='x12 crates',DESC=loc(TID_CARDS_Belkov)});
	cardPool.add({PRICE=75,SAILCATEGORY=4,SAILIDENT=310,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/EisensteinXicht.png',COL=red,TITLE='Eisenstein',ATTRIB='x75 crates',DESC=loc(TID_CARDS_Eisenstein)});
	cardPool.add({PRICE=75,SAILCATEGORY=4,SAILIDENT=335,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/TsarytsynXicht.png',COL=red,TITLE='Tsarytsyn',ATTRIB='x75 crates',DESC=loc(TID_CARDS_Tsarytsyn)});
	cardPool.add({PRICE=24,SAILCATEGORY=4,SAILIDENT=329,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/PetrovovaXicht.png',COL=red,TITLE='Petrovova',ATTRIB='x24 crates',DESC=loc(TID_CARDS_Petrovova)});
		
	--[[cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=306,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/OlegGlebXicht.png',COL=red,TITLE='Oleg Gleb',ATTRIB='\nx30 crates',DESC=loc(TID_CARDS_OlegGleb)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=307,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/BorodinXicht.png',COL=red,TITLE='Borodin',ATTRIB='Borodin\nx30 crates',DESC=loc(TID_CARDS_Borodin)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=308,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/BystrovXicht.png',COL=red,TITLE='Bystrov',ATTRIB='Bystrov\nx30 crates',DESC=loc(TID_CARDS_Bystrov)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=309,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/DmitriGladkovXicht.png',COL=red,TITLE='Dmitri Gladkov',ATTRIB='Dmitri Gladkov\nx30 crates',DESC=loc(TID_CARDS_DmitriGladkov)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=311,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/FyodorFurmanovXicht.png',COL=red,TITLE='Fyodor Furmanov',ATTRIB='Fyodor Furmanov\nx30 crates',DESC=loc(TID_CARDS_FyodorFurmanov)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=312,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/GnyevkoXicht.png',COL=red,TITLE='Gnyevko',ATTRIB='Gnyevko\nx30 crates',DESC=loc(TID_CARDS_Gnyevko)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=313,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/GossudarovXicht.png',COL=red,TITLE='Gossudarov',ATTRIB='Gossudarov\nx30 crates',DESC=loc(TID_CARDS_Gossudarov)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=315,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/IlyaKovalyukXicht.png',COL=red,TITLE='Ilya Kovalyuk',ATTRIB='Ilya Kovalyuk\nx30 crates',DESC=loc(TID_CARDS_IlyaKovalyuk)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=316,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/KaramazovXicht.png',COL=red,TITLE='Karamazov',ATTRIB='Karamazov\nx30 crates',DESC=loc(TID_CARDS_Karamazov)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=317,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/KirilenkovaXicht.png',COL=red,TITLE='Kirilenkova',ATTRIB='Kirilenkova\nx30 crates',DESC=loc(TID_CARDS_Kirilenkova)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=318,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/KonstantinFadeevXicht.png',COL=red,TITLE='Konstantin Fadeev',ATTRIB='Konstantin Fadeev\nx30 crates',DESC=loc(TID_CARDS_KonstantinFadeev)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=319,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/KuzmovXicht.png',COL=red,TITLE='Kuzmov',ATTRIB='Kuzmov\nx30 crates',DESC=loc(TID_CARDS_Kuzmov)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=320,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/LazarDolgovXicht.png',COL=red,TITLE='Lazar Dolgov',ATTRIB='Lazar Dolgov\nx30 crates',DESC=loc(TID_CARDS_LazarDolgov)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=321,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/LeonidOblukovXicht.png',COL=red,TITLE='Leonid Oblukov',ATTRIB='Leonid Oblukov\nx30 crates',DESC=loc(TID_CARDS_LeonidOblukov)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=322,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/LevTitovXicht.png',COL=red,TITLE='Lev Titov',ATTRIB='Lev Titov\nx30 crates',DESC=loc(TID_CARDS_LevTitov)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=323,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/LipshchinXicht.png',COL=red,TITLE='Lipshchin',ATTRIB='Lipshchin\nx30 crates',DESC=loc(TID_CARDS_Lipshchin)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=324,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/MikhailBerezovXicht.png',COL=red,TITLE='Mikhail Berezov',ATTRIB='Mikhail Berezov\nx30 crates',DESC=loc(TID_CARDS_MikhailBerezov)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=325,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/MorozovXicht.png',COL=red,TITLE='Morozov',ATTRIB='Morozov\nx30 crates',DESC=loc(TID_CARDS_Morozov)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=326,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/NikitaKozlovXicht.png',COL=red,TITLE='Nikita Kozlov',ATTRIB='Nikita Kozlov\nx30 crates',DESC=loc(TID_CARDS_NikitaKozlov)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=327,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/OlgaKapitsovaXicht.png',COL=red,TITLE='Olga Kapitsova',ATTRIB='Olga Kapitsova\nx30 crates',DESC=loc(TID_CARDS_OlgaKapitsova)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=328,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/PetrosyanXicht.png',COL=red,TITLE='Petrosyan',ATTRIB='Petrosyan\nx30 crates',DESC=loc(TID_CARDS_Petrosyan)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=330,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/PokryshkinXicht.png',COL=red,TITLE='Pokryshkin',ATTRIB='Pokryshkin\nx30 crates',DESC=loc(TID_CARDS_Pokryshkin)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=331,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/PyotrDavidovXicht.png',COL=red,TITLE='Pyotr Davidov',ATTRIB='Pyotr Davidov\nx30 crates',DESC=loc(TID_CARDS_PyotrDavidov)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=332,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/ScholtzeXicht.png',COL=red,TITLE='Scholtze',ATTRIB='Scholtze\nx30 crates',DESC=loc(TID_CARDS_Scholtze)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=333,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/StolypinXicht.png',COL=red,TITLE='Stolypin',ATTRIB='Stolypin\nx30 crates',DESC=loc(TID_CARDS_Stolypin)});
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=334,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/TimurGaydarXicht.png',COL=red,TITLE='Timur Gaydar',ATTRIB='Timur Gaydar\nx30 crates',DESC=loc(TID_CARDS_TimurGaydar)});
	
	cardPool.add({PRICE=0,SAILCATEGORY=4,SAILIDENT=336,TEXTURE=SKELETONS,XICHT=HUMANS..'dev/VsevolodGorkyXicht.png',COL=red,TITLE='Vsevolod Gorky',ATTRIB='Vsevolod Gorky\nx30 crates',DESC=loc(TID_CARDS_VsevolodGorky)});--]]

-- Powers/
--cardPool.add({PRICE=0,SAILCATEGORY=5,SAILIDENT=1,TEXTURE=POWERS..'AccessViolation.png',XICHT=EMPTY,COL=description});
--cardPool.add({PRICE=10,SAILCATEGORY=5,SAILIDENT=2,TEXTURE=POWERS..'Explosion.png',XICHT=EMPTY,COL=description});
	
-- Vehicles/ 75-92 93-104 119-120
cardPool.add({PRICE=10,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=5,TEXTURE=VEHICLES..'AmericanDoubleGun.png',XICHT=EMPTY,COL=blue,TITLE='Double Gun',ATTRIB='x10 crates',DESC='The next vehicle you call will be equiped with an American double gun. Consumable. No refund.'});
cardPool.add({PRICE=5,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=9,TEXTURE=VEHICLES..'AmericanSimpleLaser.png',XICHT=EMPTY,COL=blue,TITLE='Simple Laser',ATTRIB='x5 crates',DESC='The next vehicle you call will be equiped with an American laser. Consumable. No refund.'});
cardPool.add({PRICE=20,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=6,TEXTURE=VEHICLES..'AmericanHeavyGun.png',XICHT=EMPTY,COL=blue,TITLE='Heavy Gun',ATTRIB='x20 crates',DESC='The next vehicle you call will be equiped with an American heavy gun. Consumable. No refund.'});
cardPool.add({PRICE=10,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=4,TEXTURE=VEHICLES..'AmericanDoubleGun.png',XICHT=EMPTY,COL=blue,TITLE='Double Gun',ATTRIB='x10 crates',DESC='The next vehicle you call will be equiped with an American double gun. Consumable. No refund.'});
cardPool.add({PRICE=0,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=8,TEXTURE=VEHICLES..'AmericanSiberiteBomb.png',XICHT=EMPTY,COL=blue,TITLE='Siberite Bomb',ATTRIB='x0 crates\ndoes not attack in this\nversion of the prototype',DESC='The next vehicle you call will be equiped with an American siberite bomb. Consumable. No refund.'});
cardPool.add({PRICE=10,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=7,TEXTURE=VEHICLES..'AmericanRockets.png',XICHT=EMPTY,COL=blue,TITLE='Rockets',ATTRIB='x10 crates',DESC='The next vehicle you call will be equiped with American rockets. Consumable. No refund.'});
cardPool.add({PRICE=2,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=2,TEXTURE=VEHICLES..'AmericanMachineGun.png',XICHT=EMPTY,COL=blue,TITLE='Machine Gun',ATTRIB='x2 crates',DESC='The next vehicle you call will be equiped with an American machine gun. Consumable. No refund.'});
cardPool.add({PRICE=3,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=3,TEXTURE=VEHICLES..'AmericanLightGun.png',XICHT=EMPTY,COL=blue,TITLE='Light Gun',ATTRIB='x3 crates',DESC='The next vehicle you call will be equiped with an American light gun. Consumable. No refund.'});
cardPool.add({PRICE=5,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=4,TEXTURE=VEHICLES..'AmericanGatling.png',XICHT=EMPTY,COL=blue,TITLE='Gatling',ATTRIB='x5 crates',DESC='The next vehicle you call will be equiped with an American gatling. Consumable. No refund.'});
cardPool.add({PRICE=7,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=10,TEXTURE=VEHICLES..'AmericanDoubleLaser.png',XICHT=EMPTY,COL=blue,TITLE='Double Laser',ATTRIB='x7 crates',DESC='The next vehicle you call will be equiped with an American double laser. Consumable. No refund.'});
cardPool.add({PRICE=0,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=14,TEXTURE=VEHICLES..'AmericanBulldozer.png',XICHT=EMPTY,COL=blue,TITLE='Bulldozer',ATTRIB='x0 crates',DESC='The next vehicle you call will be equiped with an American bulldozer. Consumable. No refund.'});
cardPool.add({PRICE=2,SAILCATEGORY=7,SAILIDENT=4,VEHICLEPART=1,TEXTURE=VEHICLES..'AmericanLightWheeled.png',XICHT=EMPTY,COL=blue,TITLE='Light Wheels',ATTRIB='x2 crates',DESC='The next vehicle you call will be equiped with American light wheels. Consumable. No refund.'});
cardPool.add({PRICE=10,SAILCATEGORY=7,SAILIDENT=4,VEHICLEPART=5,TEXTURE=VEHICLES..'AmericanMorphling.png',XICHT=EMPTY,COL=blue,TITLE='Morphling',ATTRIB='x10 crates',DESC='The next vehicle you call will be equiped with an American morphling. Consumable. No refund.'});
cardPool.add({PRICE=9,SAILCATEGORY=7,SAILIDENT=4,VEHICLEPART=4,TEXTURE=VEHICLES..'AmericanHeavyTracked.png',XICHT=EMPTY,COL=blue,TITLE='Heavy Tracks',ATTRIB='x9 crates',DESC='The next vehicle you call will be equiped with American heavy tracks. Consumable. No refund.'});
cardPool.add({PRICE=7,SAILCATEGORY=7,SAILIDENT=4,VEHICLEPART=3,TEXTURE=VEHICLES..'AmericanMediumTracked.png',XICHT=EMPTY,COL=blue,TITLE='Medium Tracks',ATTRIB='x7 crates',DESC='The next vehicle you call will be equiped with American medium tracks. Consumable. No refund.'});
cardPool.add({PRICE=5,SAILCATEGORY=7,SAILIDENT=4,VEHICLEPART=2,TEXTURE=VEHICLES..'AmericanMediumWheeled.png',XICHT=EMPTY,COL=blue,TITLE='Medium Wheels',ATTRIB='x5 crates',DESC='The next vehicle you call will be equiped with American medium wheels. Consumable. No refund.'});

cardPool.add({PRICE=5,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=27,TEXTURE=VEHICLES..'ArabianGun.png',XICHT=EMPTY,COL=yellow,TITLE='Simple Gun',ATTRIB='x5 crates',DESC='The next vehicle you call will be equiped with an Arabian gun. Consumable. No refund.'});
cardPool.add({PRICE=10,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=28,TEXTURE=VEHICLES..'ArabianRockets.png',XICHT=EMPTY,COL=yellow,TITLE='Rockets',ATTRIB='x10 crates',DESC='The next vehicle you call will be equiped with Arabian rockets. Consumable. No refund.'});
cardPool.add({PRICE=2,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=22,TEXTURE=VEHICLES..'ArabianBallista.png',XICHT=EMPTY,COL=yellow,TITLE='Ballista',ATTRIB='x2 crates',DESC='The next vehicle you call will be equiped with an Arabian ballista. Consumable. No refund.'});
cardPool.add({PRICE=15,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=29,TEXTURE=VEHICLES..'ArabianSelfPropelledBomb.png',XICHT=EMPTY,COL=yellow,TITLE='Self-propelled bomb',ATTRIB='x15 crates',DESC='The next vehicle you call will be equiped with an Arabian self-propelled bomb. Consumable. No refund.'});
cardPool.add({PRICE=3,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=23,TEXTURE=VEHICLES..'ArabianLightGun.png',XICHT=EMPTY,COL=yellow,TITLE='Light Gun',ATTRIB='x3 crates',DESC='The next vehicle you call will be equiped with an Arabian light gun. Consumable. No refund.'});
cardPool.add({PRICE=5,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=25,TEXTURE=VEHICLES..'ArabianGatling.png',XICHT=EMPTY,COL=yellow,TITLE='Gatling',ATTRIB='x5 crates',DESC='The next vehicle you call will be equiped with an Arabian gatling. Consumable. No refund.'});
cardPool.add({PRICE=8,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=26,TEXTURE=VEHICLES..'ArabianFlameThrower.png',XICHT=EMPTY,COL=yellow,TITLE='Flamethrower',ATTRIB='x8 crates',DESC='The next vehicle you call will be equiped with an Arabian flamethrower. Consumable. No refund.'});
cardPool.add({PRICE=3,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=24,TEXTURE=VEHICLES..'ArabianDoubleMachineGun.png',XICHT=EMPTY,COL=yellow,TITLE='Double Machine Gun',ATTRIB='x3 crates',DESC='The next vehicle you call will be equiped with an Arabian double machine gun. Consumable. No refund.'});
cardPool.add({PRICE=2,SAILCATEGORY=7,SAILIDENT=4,VEHICLEPART=12,TEXTURE=VEHICLES..'ArabianLightTrike.png',XICHT=EMPTY,COL=yellow,TITLE='Light Wheels',ATTRIB='x2 crates',DESC='The next vehicle you call will be equiped with Arabian light wheels. Consumable. No refund.'});
cardPool.add({PRICE=5,SAILCATEGORY=7,SAILIDENT=4,VEHICLEPART=14,TEXTURE=VEHICLES..'ArabianHalfTracks.png',XICHT=EMPTY,COL=yellow,TITLE='Half Tracks',ATTRIB='x5 crates',DESC='The next vehicle you call will be equiped with Arabian half tracks. Consumable. No refund.'});
cardPool.add({PRICE=3,SAILCATEGORY=7,SAILIDENT=4,VEHICLEPART=13,TEXTURE=VEHICLES..'ArabianMediumTrike.png',XICHT=EMPTY,COL=yellow,TITLE='Medium Trike',ATTRIB='x3 crates',DESC='The next vehicle you call will be equiped with Arabian half tracks. Consumable. No refund.'});
cardPool.add({PRICE=3,SAILCATEGORY=7,SAILIDENT=4,VEHICLEPART=11,TEXTURE=VEHICLES..'ArabianHovercraft.png',XICHT=EMPTY,COL=yellow,TITLE='Hovercraft',ATTRIB='x3 crates',DESC='The next vehicle you call will be equiped with Arabian hovercraft. Consumable. No refund.'});

cardPool.add({PRICE=0,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=48,TEXTURE=VEHICLES..'RussianAlaskiteBomb.png',XICHT=EMPTY,COL=red,TITLE='Alaskite Bomb',ATTRIB='x0 crates',DESC='Assemble Russian heavy tracks for the next vehicle.\n\nCreate vehicle\n(costs x10 crates):'});
cardPool.add({PRICE=15,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=45,TEXTURE=VEHICLES..'RussianHeavyRockets.png',XICHT=EMPTY,COL=red,TITLE='Heavy Rockets',ATTRIB='x15 crates',DESC='Assemble Russian heavy tracks for the next vehicle.\n\nCreate vehicle\n(costs x10 crates):'});
cardPool.add({PRICE=10,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=47,TEXTURE=VEHICLES..'RussianRockets.png',XICHT=EMPTY,COL=red,TITLE='Rockets',ATTRIB='x10 crates',DESC='Assemble Russian heavy tracks for the next vehicle.\n\nCreate vehicle\n(costs x10 crates):'});
cardPool.add({PRICE=25,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=49,TEXTURE=VEHICLES..'RussianTimeLapser.png',XICHT=EMPTY,COL=red,TITLE='Time Lapser',ATTRIB='x25 crates\nSlows down allies and enemies',DESC='Assemble Russian heavy tracks for the next vehicle.\n\nCreate vehicle\n(costs x10 crates):'});
cardPool.add({PRICE=3,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=42,TEXTURE=VEHICLES..'RussianHeavyMachineGun.png',XICHT=EMPTY,COL=red,TITLE='Heavy Machine Gun',ATTRIB='x3 crates',DESC='Assemble Russian heavy tracks for the next vehicle.\n\nCreate vehicle\n(costs x10 crates):'});
cardPool.add({PRICE=20,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=46,TEXTURE=VEHICLES..'RussianHeavyGun.png',XICHT=EMPTY,COL=red,TITLE='Heavy Gun',ATTRIB='x20 crates',DESC='Assemble Russian heavy tracks for the next vehicle.\n\nCreate vehicle\n(costs x10 crates):'});
cardPool.add({PRICE=5,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=44,TEXTURE=VEHICLES..'RussianGun.png',XICHT=EMPTY,COL=red,TITLE='Simple Gun',ATTRIB='x5 crates',DESC='Assemble Russian heavy tracks for the next vehicle.\n\nCreate vehicle\n(costs x10 crates):'});
cardPool.add({PRICE=5,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=43,TEXTURE=VEHICLES..'RussianGatling.png',XICHT=EMPTY,COL=red,TITLE='Gatling',ATTRIB='x5 crates',DESC='Assemble Russian heavy tracks for the next vehicle.\n\nCreate vehicle\n(costs x10 crates):'});
cardPool.add({PRICE=0,SAILCATEGORY=7,SAILIDENT=1,VEHICLEPART=53,TEXTURE=VEHICLES..'RussianBulldozer.png',XICHT=EMPTY,COL=red,TITLE='Bulldozer',ATTRIB='x0 crates',DESC='Assemble Russian heavy tracks for the next vehicle.\n\nCreate vehicle\n(costs x10 crates):'});
cardPool.add({PRICE=5,SAILCATEGORY=7,SAILIDENT=4,VEHICLEPART=21,TEXTURE=VEHICLES..'RussianMediumWheeled.png',XICHT=EMPTY,COL=red,TITLE='Medium Wheels',ATTRIB='x5 crates',DESC='Assemble Russian heavy tracks for the next vehicle.\n\nCreate vehicle\n(costs x10 crates):'});
cardPool.add({PRICE=5,SAILCATEGORY=7,SAILIDENT=4,VEHICLEPART=22,TEXTURE=VEHICLES..'RussianMediumTracked.png',XICHT=EMPTY,COL=red,TITLE='Medium Tracks',ATTRIB='x5 crates',DESC='Assemble Russian heavy tracks for the next vehicle.\n\nCreate vehicle\n(costs x10 crates):'});
cardPool.add({PRICE=5,SAILCATEGORY=7,SAILIDENT=4,VEHICLEPART=23,TEXTURE=VEHICLES..'RussianHeavyWheeled.png',XICHT=EMPTY,COL=red,TITLE='Heavy Wheels',ATTRIB='x5 crates',DESC='Assemble Russian heavy tracks for the next vehicle.\n\nCreate vehicle\n(costs x10 crates):'});
cardPool.add({PRICE=9,SAILCATEGORY=7,SAILIDENT=4,VEHICLEPART=24,TEXTURE=VEHICLES..'RussianHeavyTracked.png',XICHT=EMPTY,COL=red,TITLE='Heavy Tracks',ATTRIB='x9 crates',DESC='Assemble Russian heavy tracks for the next vehicle.\n\nCreate vehicle\n(costs x10 crates):'});

cardPool.add({PRICE=10,SAILCATEGORY=7,SAILIDENT=999,TEXTURE=VEHICLES..'spawnVehicle.png',XICHT=EMPTY,COL=description,TITLE='ii',DESC='ii'});