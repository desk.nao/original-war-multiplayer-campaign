queryCreate = function()
	local self = {}

	self.init = function(Title, Options)
		if self.element ~= nil then
			sgui_delete(sgui.element.ID)
		end
		
		self.element 			= getDialogEX(nil, anchorNone, XYWH(300+LayoutWidth/2,LayoutHeight/2,300,100 + 25 * (#Options-1)), SKINTYPE_DIALOG1, {tile=true})
		self.element.label 		= getLabelEX(self.element, anchorNone, XYWH(0,25,300,25), Tahoma_13, Title, {nomouseeventhis=true, text_halign=ALIGN_MIDDLE, colour1=BLACKA(0)})

		for i = 1, #Options do
			getImageButtonEX(self.element, anchorNone, XYWH(15,55 + 25 * (i-1),270,25), Options[i], '', 'sgui_delete(framework.Popup.ID)', SKINTYPE_BUTTON, {})
		end

		setVisible(self.element, true)
		AddSingleUseTimer(1,'ChangeInterface(3)')
		AddSingleUseTimer(2,'ChangeInterface(1)')
	end

	return self
end
	ChangeInterface(1)
	Query = queryCreate()
	Query.init('What do you want to do?', {"Abandon your troops.", "Send reinforcements."})
--ChangeInterface(3)
--Query = queryCreate()
--AddSingleUseTimer(2,'Query.init()')