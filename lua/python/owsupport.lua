logoscale = 1;

logos.owsupport = getElementEX(logos,anchorLTRB,XYWH(0,0,getWidth(logos),getHeight(logos)),false,{colour1=RGB(25,77,58)});

logos.owsupport.logo = getElementEX(logos.owsupport,anchorNone,XYWH(getWidth(logos.owsupport)/2-480/2,getHeight(logos.owsupport)/2-60/2,480,60),true,{colour1=BLACKA(0)});

logos.owsupport.blurb = getLabelEX(logos.owsupport,anchorLTRB,XYWH(0,0,getWidth(logos.owsupport),getHeight(logos.owsupport)),
				  Inconsolata_70_12,'Maintaining and Developing'..CHAR13.. ' Original War',{font_colour=WHITE(),text_halign=ALIGN_MIDDLE,shadowtext=true});

--470x44

-- O = 52 W = 84 SUPPORT = 136
logos.owsupport.logo.O = getLabelEX(logos.owsupport.logo,anchorLTRB,XYWH(0,0,0,60),
				  	    Eurostile_70_16,'O',{font_colour=WHITE(),font_scale=1.61,font_style_outline=true,shadowtext=true});
logos.owsupport.logo.riginal = getLabelEX(logos.owsupport.logo,anchorLTRB,XYWH(32,0,0,60),
				  	    Eurostile_70_16,'riginal',{font_colour=WHITE(),font_scale=1.61,font_style_outline=true,shadowtext=true});

logos.owsupport.logo.W = getLabelEX(logos.owsupport.logo,anchorLTRB,XYWH(181,0,0,60),
				  	    Eurostile_70_16,'W',{font_colour=WHITE(),font_scale=1.61,font_style_outline=true,shadowtext=true});
logos.owsupport.logo.ar = getLabelEX(logos.owsupport.logo,anchorLTRB,XYWH(233,0,0,60),
				  	    Eurostile_70_16,'ar',{font_colour=WHITE(),font_scale=1.61,font_style_outline=true,shadowtext=true});

logos.owsupport.logo.Support = getLabelEX(logos.owsupport.logo,anchorLTRB,XYWH(295,0,0,60),
				  	    Eurostile_70_16,'Support',{font_colour=RGB(239,243,82),font_scale=1.61,font_style_outline=true,shadowtext=true});

logos.owsupport.logo.com = getLabelEX(logos.owsupport.logo,anchorLTRB,XYWH(313,0,0,60),
				  	    Eurostile_70_16,'.com',{font_colour=WHITE(),font_scale=1.61,font_style_outline=true,shadowtext=true});

logos.owsupport.abort = true;
logos.owsupport.timer = 0;
logos.owsupport.music = -1;

function logos.owsupport.doAbort()
        logos.owsupport.finish();
        StopTimer(logos.owsupport.timer);
end;

function logos.owsupport.finish()
	if logos.owsupport.abort then
		return;
        end;

        logos.owsupport.abort = true; -- Prevent Abort

	OW_OAL_FREE(logos.owsupport.music);
        logos.owsupport.music = -1;
        setVisible(logos.owsupport,false);
        playLogo(logos.owsupport.nextLogoID);
end;

function logos.owsupport.stage6()
	if logos.owsupport.abort then
		return;
        end;

	AddEventFade(logos.owsupport.logo.ID,0,1,'');
        logos.owsupport.timer = AddRepeatableTimer(1.25,'logos.owsupport.finish()',logos.owsupport.timer);
end;

function logos.owsupport.stage5()
	if logos.owsupport.abort then
		return;
        end;

	AddEventFade(logos.owsupport.logo.com.ID,255,0.5,'');
        logos.owsupport.timer = AddRepeatableTimer(3,'logos.owsupport.stage6()',logos.owsupport.timer);
end;

function logos.owsupport.stage4()
	if logos.owsupport.abort then
		return;
        end;

	AddEventSlideXF(logos.owsupport.logo.O.ID,52*logoscale,0.5,'');
        AddEventSlideXF(logos.owsupport.logo.W.ID,84*logoscale,0.5,'');
        AddEventSlideXF(logos.owsupport.logo.Support.ID,136*logoscale,0.5,'');
        logos.owsupport.timer = AddRepeatableTimer(0.5,'logos.owsupport.stage5()',logos.owsupport.timer);
end;

function logos.owsupport.stage3()
	if logos.owsupport.abort then
		return;
        end;

	AddEventFade(logos.owsupport.logo.riginal.ID,0,0.40,'');
        AddEventFade(logos.owsupport.logo.ar.ID,0,0.25,'');
        logos.owsupport.timer = AddRepeatableTimer(0.25,'logos.owsupport.stage4()',logos.owsupport.timer);
end;

function logos.owsupport.stage2()
	if logos.owsupport.abort then
		return;
        end;

	AddEventFade(logos.owsupport.logo.ID,255,2,'');
        logos.owsupport.timer = AddRepeatableTimer(2,'logos.owsupport.stage3()',logos.owsupport.timer);
end;

function logos.owsupport.stage1()
	if logos.owsupport.abort then
		return;
        end;

	AddEventFade(logos.owsupport.blurb.ID,0,0.5,'');
        logos.owsupport.timer = AddRepeatableTimer(1,'logos.owsupport.stage2()',logos.owsupport.timer);
end;

function logos.owsupport.run(NEXTLOGOID)
	logos.owsupport.nextLogoID = NEXTLOGOID;
        logos.owsupport.abort = false;

        logoscale = ScrHeight/900;

        setXYWHV(logos.owsupport.logo,getWidth(logos.owsupport)/2-480*logoscale/2,getHeight(logos.owsupport)/2-60*logoscale/2,480*logoscale,60*logoscale);

	setX(logos.owsupport.logo.O,0);
        setX(logos.owsupport.logo.riginal,32*logoscale);
        setX(logos.owsupport.logo.W,181*logoscale);
        setX(logos.owsupport.logo.ar,233*logoscale);
        setX(logos.owsupport.logo.Support,295*logoscale);
        setX(logos.owsupport.logo.com,323*logoscale);

        setFontScale(logos.owsupport.logo.O,1.61*logoscale);
        setFontScale(logos.owsupport.logo.riginal,1.61*logoscale);
        setFontScale(logos.owsupport.logo.W,1.61*logoscale);
        setFontScale(logos.owsupport.logo.ar,1.61*logoscale);
        setFontScale(logos.owsupport.logo.Support,1.61*logoscale);
        setFontScale(logos.owsupport.logo.com,1.61*logoscale);
        setFontScale(logos.owsupport.blurb,1.2*logoscale);

        setAlpha(logos.owsupport.logo.riginal,255);
        setAlpha(logos.owsupport.logo.ar,255);
        setAlpha(logos.owsupport.logo,0);
        setAlpha(logos.owsupport.logo.com,0);
        setAlpha(logos.owsupport.blurb,0);
        setVisible(logos.owsupport,true);

        setFocus(logos);

        if logos.owsupport.music ~= -1 then
        	OW_OAL_FREE(logos.owsupport.music);
        end;

        logos.owsupport.music = OW_OAL_LOAD('Hudba/ArPruD.wav');
        OW_OAL_PLAY(logos.owsupport.music);

        AddSingleUseTimer(0.5,'AddEventFade(logos.owsupport.blurb.ID,255,2,"");');
        logos.owsupport.timer = AddRepeatableTimer(5.5,'logos.owsupport.stage1()',logos.owsupport.timer);
end;
