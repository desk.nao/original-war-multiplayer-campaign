wait_openai = false
openai_toggle = false

function CheckForKey(FRAMETIME)
  if SGUI_getkeystate(98) then
    if wait_openai then return end
    wait_openai = true
    AddSingleUseTimer(1, "wait_openai = false")
    openai_toggle = not openai_toggle
    if openai_toggle then
      ToDebug("Open AI answer: ON")
    else
      ToDebug("Open AI answer: OFF")
    end
  end
end