local debug_string = getLabelEX(nil, anchorNone, XYWH(5,450,300,25), Tahoma_13, "", {font_colour_background=BLACKA(OW_GSETTING_READ_NUMBER(getvalue(OWV_USERNAME), 'GS_subBG', 127)),nomouseeventhis=true,text_halign=ALIGN_LEFT,colour1=BLACKA(0)})

ToDebug = function(string)
  setText(debug_string, string)
end

--timer:single(1,"SGUI_register_tick_callback('ToDebug(getText(game.xicht.say))')")

local createFramework = function()
  local self = {isBusy = false, current_index=0,FROM_OW_DATA=nil,queue={},letter=0,data_temp='',reactingToResearch=false, allowAnswer=false, mouse_coords={x=0,y=0}, isMovingCurrently=nil, isInitialised=false, isRadio=false, selected={}, progress=0, isRunning=false}
  
  --[[
  FIXED: Keyboard focus for Popup.
  TODO: String break on <'>
  TODO: String colour?
  TODO: CommPanel buttons on Nations & None
  TODO: Dialogs history
  TODO: Network code
  ]]

  self.element = getElementEX(nil, anchorLTR, XYWH(0,0,0,0) ,true, {})
  self.text = getLabelEX(nil, anchorNone, XYWH(0,0,0,0), Tahoma_13, "", {wordwrap=true})
  setVisible(self.text, false)
  self.temp = getLabelEX(nil, anchorNone, XYWH(0,0,0,0), Tahoma_13, "", {wordwrap=true})
  setVisible(self.temp, false)

  self.Xicht = 
  {
    name = getLabelEX(nil, anchorNone, XYWH(7,217,500,25), Tahoma_13, "", {wordwrap=true, text_halign=ALIGN_LEFT, text_valign=ALIGN_TOP, nomouseeventthis=true, font_colour_background=BLACKA(OW_GSETTING_READ_NUMBER(getvalue(OWV_USERNAME), 'GS_subBG', 127))}),
    label = getLabelEX(nil, anchorNone, XYWH(7,15+217,500,375), Tahoma_13, "", {wordwrap=true, text_halign=ALIGN_LEFT, text_valign=ALIGN_TOP, nomouseeventthis=true, font_colour_background=BLACKA(OW_GSETTING_READ_NUMBER(getvalue(OWV_USERNAME), 'GS_subBG', 127))})
  }

  self.Colours = {RGB(144,144,144),RGB(64,128,255),RGB(255,255,128),RGB(224,64,64),RGB(128,255,255),RGB(255,192,64),RGB(255,128,255),RGB(64,255,64),RGB(240,240,240),RGB(166,162,136)}

  --self.Xicht.static = getElementEX(nil, anchorNone, XYWH(10,115,80,100), true, {texture='SGUI/static.png', nomouseeventhis=true, subtexture=true, subcoords=SUBCOORD(0,0,80,100), canAnimate=true, value=-100, animtime=88.24})

  self.initSay = function(texture)
    self.Xicht.element = getElementEX(nil, anchorNone, XYWH(10,115,80,100), true, {texture="SGUI/"..texture, nomouseeventhis=true, subtexture=true, subcoords=SUBCOORD(0,0,80,100), canAnimate=true, value=-100, animtime=88.24})
    setVisible(self.Xicht.element, true)
    setVisible(self.Xicht.name, true)
    if self.xichted_name == nil then
      self.Xichted_name = 'Major D. N. Platonov'
    end
    setText(self.Xicht.name, self.Xichted_name .. ':')
  end

  self.updateXichted = function(tick)
    if self.Xicht.element ~= nil then
      if self.Xicht.element.canAnimate == true then
        self.Xicht.element.value = self.Xicht.element.value + 100
        if self.Xicht.element.value == 1500 then self.Xicht.element.value = 0 end
        setSubCoords(self.Xicht.element, SUBCOORD(0, 100 + self.Xicht.element.value, 80, 100))
        self.Xicht.element.canAnimate = false
        AddSingleUseTimer(0.08824, "framework.Xicht.element.canAnimate = true")
      end
    end

    -- if self.Xicht.static ~= nil then
    --   if self.Xicht.static.canAnimate == true then
    --     self.Xicht.static.value = self.Xicht.static.value + 100
    --     if self.Xicht.static.value == 1500 then self.Xicht.static.value = 0 end
    --     setSubCoords(self.Xicht.static, SUBCOORD(0, 100 + self.Xicht.static.value, 80, 100))
    --     self.Xicht.static.canAnimate = false
    --     AddSingleUseTimer(0.08824, "framework.Xicht.static.canAnimate = true")
    --   end
    -- end
  end

  self.ShowPopup = function(Title, Button1_Callback, Button2_Callback, Checkbox_Callback, Checkbox2_OpenAI)
    self.isRadio = false
    self.allowAnswer = false

    OW_SET_OW_IGNORE_KEYBOARD(true)

    if self.Popup ~= nil then
      sgui_delete(self.Popup.ID)
    end

    self.Popup = getDialogEX(nil, anchorNone, XYWH(-150+LayoutWidth/2,LayoutHeight/2-150,300,100), SKINTYPE_DIALOG1, {tile=true})
    self.Popup.Children =
    {
      Label     = getLabelEX(self.Popup, anchorNone, XYWH(0,5,300,25), Tahoma_13, Title, {nomouseeventhis=true,text_halign=ALIGN_MIDDLE,colour1=BLACKA(0)}),
      Edit      = getEditEX(self.Popup, anchorNone, XYWH(15,35,270,25), Tahoma_10B, '', '', COLOURS_DIALOG_EDIT, {"if (%k == VK_RETURN) then OW_SET_OW_IGNORE_KEYBOARD(false) " .. Button1_Callback .. " setText(framework.Popup.Children.Edit,'') end"}),
      Checkbox  = getCheckBoxEX_MENU(self.Popup, anchorNone, XYWH(200,65,12.5,12.5), "Radio", checkbox_merge, Checkbox_Callback, {checked=false,}),
      Close     = getImageButtonEX(self.Popup, anchorNone, XYWH(270,5,25,25), 'X', '', 'OW_SET_OW_IGNORE_KEYBOARD(false) sgui_delete(framework.Popup.ID)', SKINTYPE_BUTTON, {})
    }

    if Button2_Callback ~= false then
      self.Popup.Buttons = 
      {
        getImageButtonEX(self.Popup, anchorNone, XYWH(15,65,135,25), 'Yes', '', Button1_Callback, SKINTYPE_BUTTON, {}),
        getImageButtonEX(self.Popup, anchorNone, XYWH(160,65,135,25), 'No', '', Button2_Callback, SKINTYPE_BUTTON, {})
      }
    else
      self.Popup.Buttons =
      {
        getImageButtonEX(self.Popup, anchorNone, XYWH(15,65,135,25), 'Start', '', 'OW_SET_OW_IGNORE_KEYBOARD(false)' .. Button1_Callback, SKINTYPE_BUTTON, {})
      }
    end

    if Checkbox_Callback == false then
      setVisible(self.Popup.Children.Checkbox, false)
    end

    if Checkbox2_OpenAI == true then
      self.Popup.Children.Checkbox2 = getCheckBoxEX_MENU(self.Popup, anchorNone, XYWH(145,65,12.5,12.5), "OpenAI", checkbox_merge, "framework.allowAnswer = %value", {checked=false,})
    end

    set_Callback(self.Popup.ID, CALLBACK_MOUSECLICK, "if framework.isMovingCurrently == framework.Popup then framework.isMovingCurrently = nil else framework.isMovingCurrently = framework.Popup setNoMouseEventThis(framework.Popup, true) end") 

    setVisible(self.Popup, true)
    AddSingleUseTimer(0.25,"setFocus(framework.Popup.Children.Edit)")

    DoInterfaceChange(interface.current)
  end

  self.moveElement = function(element)
    if element.isMoving == nil then
      element.isMoving = false
    end
    element.isMoving = not element.isMoving

    if element.isMoving then
      self.isMovingCurrently = element
    else
      self.isMovingCurrently = nil
    end
  end

  self.getMouseCoords = function(x, y)
    self.mouse_coords = {x=x, y=y}
  end

  self.moveElementFrametime = function()
    if self.isMovingCurrently ~= nil then
      setXY(self.isMovingCurrently, self.mouse_coords.x, self.mouse_coords.y)
    end
  end

  timer:single(1,"SGUI_register_tick_callback('framework.moveElementFrametime()')")
  SGUI_register_tick_callback('framework.updateXichted(%frametime)')

  self.initStart = function(Command, Param1, Param2, Param3)
    clearFocus()
    if self.Popup ~= nil then setVisible(self.Popup, false) end
    if Command == "process_player_dialog" then
      local v = 0
      if self.isRadio then v = 1 else v = 0 end
      if self.allowAnswer then self.allowAnswer = 1 else self.allowAnswer = 0 end
      framework.addToQueue( {Command, getText(self.Popup.Children.Edit), self.selected.id, self.FROM_OW_DATA, v, self.allowAnswer, nil})
    end
    if Command == "react_to_research" then 
      framework.addToQueue( {Command, Param1, Param2, Param3, 0, 0, nil}) -- Command, Lua dialog, Unit, Unit_voice, Radio, Answer, Filename
    end
    --if Command == "Prompt" then
    --  Command = Command .."|".. getText(self.Popup.Children.Edit)
    --end
  end

  -- Custom Command Panels
  self.Comm1 = function(Side, Units)
    if Side ~= nil and InfoPanelLastData.SIDE ~= nil then 
      if Side == InfoPanelLastData.SIDE then
          if Side == InfoPanelLastData.SIDE then
            framework.ShowPopup("Send a custom dialog", "framework.initStart('process_player_dialog', nil, nil)", false, "framework.isRadio = %value", true)
          end
      end
    end
  end

  self.Comm2 = function()
    framework.ShowPopup("Send a custom prompt", "framework.initStart('Prompt')", false, false, false)
  end

  self.ReactToResearch = function(Research, Unit, Unit_VoiceID)
    self.allowAnswer = false
    self.reactingToResearch = true
    self.initStart('react_to_research', Research, Unit, Unit_VoiceID)
  end

  self.setOpenAiAnswer = function()

  end

  self.runFeature = function(index)
    self.isRunning = true
    self.current_index = index
    setText(self.Xicht.label, "Test...")
    setVisible(self.Xicht.label, true)
    self.string = self.queue[index][2]
    self.progress = 0


    if self.isOpenAi then
      self.initSay("Major D. N. Platonov.png")
    else
      self.initSay(self.Xichted_name_png)
    end

    self.playAudio(self.queue[index][7], index)
    
    self.letter = 0

    
  end

  self.addToQueue = function(DATA)
    table.insert(self.queue, DATA)
  end

  self.listen_to_queue = function()
    -- Command, Lua dialog or Research ID, Unit, Unit_voice, Radio, Answer, Filename

    if #self.queue > 0 then
      for i = 1, #self.queue do

          if self.queue[i][1] == 'process_player_dialog' then
            if self.queue[i][7] == nil then
              self.queue[i][7] = false -- call xichted face on all
              OW_AI_SENDSTRING(10, "retrieve_xichted_face|"..self.Xichted_name..'|'..self.Xichted_list)
              local Values = self.queue[i][1] .. '|' .. self.queue[i][2] .. '|' .. self.queue[i][3] .. '|' .. self.queue[i][4] .. '|' .. self.queue[i][5] .. '|' .. self.queue[i][6] .. '|' .. i .. '|' .. self.Xichted_name
              AddSingleUseTimer(2, "OW_AI_SENDSTRING(5, '" .. Values .. "')")
            elseif not self.isBusy and not self.isRunning and self.queue[i][7] ~= nil and self.queue[i][7] ~= false then
              self.isBusy = true
              framework.runFeature(i)
            end
          end

          if self.queue[i][1] == 'process_openai_dialog' then
            if not self.isBusy and not self.isRunning and self.queue[i][7] ~= nil and self.queue[i][7] ~= false then
              self.isBusy = true
              framework.isOpenAi = true
              framework.runFeature(i)
            end
          end

          if self.queue[i][1] == 'react_to_research' then
            if self.queue[i][7] == nil then
              self.queue[i][7] = false
              OW_AI_SENDSTRING(4, self.queue[i][1] .. '|' .. self.queue[i][2] .. '|' .. self.queue[i][3] .. '|' .. i .. '|' .. self.queue[i][4])
            elseif not self.isBusy and not self.isRunning and self.queue[i][7] ~= nil and self.queue[i][7] ~= false then
              self.isBusy = true
              framework.runFeature(i)
            end
          end

      end
    end
  end

  timer:single(1,"SGUI_register_tick_callback('framework.listen_to_queue()')")

  self.setVanilla = function()
    OW_AI_SENDSTRING(3, "delete_audio|" .. self.queue[self.current_index][7])
    table.remove(self.queue, self.current_index)
    self.isRunning = false
    self.isOpenAi = false
    setVisible(self.Xicht.label, false)
    sgui_delete(self.Xicht.element.ID)
    self.Xichted_name = 'Someone...'
    AddSingleUseTimer(2.5,'framework.isBusy = false')
    setVisible(self.Xicht.name, false)
  end

  self.playAudio = function(file, index)
    --OW_CUSTOM_COMMAND(1, self.queue[index][3], self.queue[index][5])
    self.volume_speech = OW_settings_getvolume(VOLUME_SPEECH)
    sound.play('sound/generations/'..file, 'AddSingleUseTimer(1,"framework.setVanilla()")', VOLUME_SPEECH)
  end

  self.animate = function(FRAMETIME)
    if self.isRunning then
      self.progress = self.progress + 30 * FRAMETIME

      if self.letter < string.len(self.string) then
        self.letter = self.letter +1
        set_Colour(self.Xicht.label.ID, PROP_FONT_COL, RGB(186,196,252))
        set_Colour(self.Xicht.name.ID, PROP_FONT_COL, RGB(186,196,252))
        setText(self.Xicht.label, '- ' .. string.sub(self.string, 1, self.letter))
      else
        self.isRunning = false
        self.letter = 0
      end

    end
  end

  self.retrieveUnitName = function(fullString)
    if fullString ~= nil then
      fullString = string.match(fullString,"^(.-)%s-:")
      return fullString
    else
      return '(error)'
    end
  end

  return self
end

framework = createFramework()

timer:single(1,"SGUI_register_tick_callback('framework.animate(%frametime)')")
set_Callback(gamewindow.ID, CALLBACK_MOUSEMOVE, 'framework.getMouseCoords(%x, %y)')
set_Callback(gamewindow.ID, CALLBACK_MOUSECLICK, 'if framework.Popup ~= nil and framework.isMovingCurrently == framework.Popup then setNoMouseEventThis(framework.Popup, false) end')

function FROMOW_AI_INPUT(ID, P1, P2, P3, P4, P5, P6)

  if not framework.isInitialised then
    ToDebug("Framework initialised (contact .py <> OWAR_AI.exe OK!")
    framework.isInitialised = true
  else

    if ID == 5 then -- Process a dialog as generated by a custom unit or an open AI answer -- "{self.OPEN_AI_STRING}" 999 1 "{self.OPEN_AI_AUDIO}" {self.SGUI_QUEUE}',
      if P2 == 999 or P2 == "999" then
        local DATA = {STRING=P1, UID=P2, RADIO=P3, FILE=P4}
        table.insert(framework.queue, {"process_openai_dialog", DATA.STRING, DATA.UID, nil, DATA.RADIO, 0, DATA.FILE})
      else
        framework.queue[P5][2] = P1
        framework.queue[P5][3] = P2
        framework.queue[P5][5] = P3
        framework.queue[P5][7] = P4
        framework.Xichted_name = P6 .. ':'
      end
    end
 
    if ID == 7 then
      local DATA = {STRING=P1, UID=P2, RADIO=P3, FILE=P4, NAME=P6}
      table.insert(framework.queue, {"process_player_dialog", DATA.STRING, DATA.UID, nil, DATA.RADIO, 0, DATA.FILE})
      framework.Xichted_name = P6 .. ':'
    end

    -- 1: Dialogue Prompt, 2: Unit, 3: Filename, 4: Index
    if ID == 4 then -- Process a dialog as a reaction to a Research Complete event (locally)
      framework.queue[P4][2] = P1
      framework.queue[P4][3] = P2
      framework.queue[P4][5] = 0
      framework.queue[P4][7] = P3
    end

    local DATA = {STRING=P2, UID=P3, RADIO=P4, FILE=P5, COMMAND=P1}

    if ID == 6 then
      ToDebug("Received by the server: COMMAND> " .. DATA.COMMAND .. ' | STRING> ' .. DATA.STRING .. ' | UID> ' .. DATA.UID .. ' | RADIO>' .. DATA.RADIO .. ' | FILE>' .. DATA.FILE)
      table.insert(framework.queue,  {DATA.COMMAND, DATA.STRING, DATA.UID, nil, DATA.RADIO, 0, DATA.FILE})
    end

    if ID == 10 then
      ToDebug("Sail Status: "..P1)
      OW_CUSTOM_COMMAND(P1)
    end
    -- Ask Y/N Dialog Box for Prompt
    -- if Command == "11" then
    --  framework.showPopup(framework.filterCharacters(from_python).." is the recognized value index. Do you want to continue?", "framework.initStart('Confirmation|1')", "framework.initStart('Confirmation|0')", false, false)
    --end

end
end

function dothetesttest(unit_id)
  ToDebug("Annihiliation event. Unit_ID: " .. unit_id .. " and framework.queue: " .. #framework.queue)
  framework.addToQueue({"process_player_dialog", "There it is! You should have surrendered, American bastards! The WORLD belongs to US!!", unit_id, 15, #framework.queue+1, 0, nil})
  OW_CUSTOM_COMMAND(1)
end;

function FROMOW_INFOPANEL_UPDATE(DATA)
  game_info.Update(DATA)
  LUA_TO_DEBUGLOG(serializeTable(DATA,"FROMOW_INFOPANEL_UPDATE"))
  if DATA.ID > 0 then
    framework.selected = {name=DATA.INFO[1], side=framework.Colours[DATA.SIDE+1], id=DATA.ID}
    Xichted = OW_GET_UNIT_PORTRAIT_PARTS(DATA.ID)
    framework.Xichted_list = string.sub(Xichted.TYP,1,4)
    framework.Xichted_name = DATA.INFO[1]
    framework.Xichted_name_png = DATA.INFO[1]..'.png'
    for i = 1, #Xichted.PARTS do
      framework.Xichted_list = framework.Xichted_list..'|'..Xichted.PARTS[i]
    end
  end
end

include('_keyPressed')
--include('_queries')