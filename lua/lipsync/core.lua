-- Lipsync
face = {};

-- Change this value to specify the amount of XichtEd faces which were adapted for LIPSYNC.
face.load = 2;
LIPSYNC_DATA = {};

-- Easy face loader
for i = 1,face.load do include('lipsync/face-'..i); end;

-- Call this function to animate lips (id = face ID, number = longer of arrays in generated lipsync list)
function AnimateLips(id,number)
	for i = 1,number do
		AddSingleUseTimer(LIPSYNC_DATA[id][i][1],"focusMouth("..id..","..LIPSYNC_DATA[id][i][2]..","..i..");");
	end;
end;

-- Modifies mouth texture (Need to automate mouth.png with correct loaded face / optimise lines into 1)
-- Need to use setOffset instead of multiple files (first time ever that I'm gonna try that)
function focusMouth(id,texture,number)
	if LIPSYNC_DATA[id][number][2] == 'A' then setTexture(body.mouth,'SGUI/face/mouths/1usta.png'); end;
	if LIPSYNC_DATA[id][number][2] == 'X' then setTexture(body.mouth,'SGUI/face/mouths/1usta.png'); end;
	if LIPSYNC_DATA[id][number][2] == 'B' then setTexture(body.mouth,'SGUI/face/mouths/1ustaa.png'); end;
	if LIPSYNC_DATA[id][number][2] == 'E' then setTexture(body.mouth,'SGUI/face/mouths/1ustab.png'); end;
	if LIPSYNC_DATA[id][number][2] == 'F' then setTexture(body.mouth,'SGUI/face/mouths/1ustab.png'); end;
	if LIPSYNC_DATA[id][number][2] == 'C' then setTexture(body.mouth,'SGUI/face/mouths/1ustac.png'); end;
	if LIPSYNC_DATA[id][number][2] == 'D' then setTexture(body.mouth,'SGUI/face/mouths/1ustac.png'); end;
end;

-- Audio file used for the test. Will be moved to a library when more dubbings are used.
face.voice = OW_OAL_LOAD('Sound/voice.wav',false,true,'');
-- "Play" button used for the test. Trigger to play lipsync.
face.play = getImageButtonEX(face,anchorNone,XYWH(ScrWidth/2,ScrHeight/2,80,25),'PLAY!','','OW_OAL_PLAY(face.voice); AnimateLips(2,102);',SKINTYPE_BUTTON,{colour2=BLACKA(350), font_colour_disabled=GRAY(127),});	

-- Quick Face textures lines used for the test.
-- Need to automate getElementEX for each body part (like in cards module).
-- Replace nil with body and give right position.
-- When all face elements imported and given their right x,y positions, make it so face elements can be changed and positioned correctly.
body = getElementEX(nil,anchorNone,XYWH(600,200,420,285),true,{colour1=BLACKA(0)});
body.backclothes = getElementEX(body,anchorNone,XYWH(139,body.y-117,158,165),true,{texture='SGUI/face/clothes/Rusol_z.png'});
body.torso = getElementEX(nil,anchorNone,XYWH(body.x,200,420,285),true,{texture='SGUI/face/body/1krk.png'});

body.clothes = getElementEX(nil,anchorNone,XYWH(body.x-7,body.y+body.height-193,431,165),true,{texture='SGUI/face/clothes/Rusol_s.png'});
body.hairp2 = getElementEX(nil,anchorNone,XYWH(body.x+250+20-46-104+171,body.y-100+46+62+10+75-202,38,111),true,{texture='SGUI/face/hair/pacino_z.png'});
body.head = getElementEX(nil,anchorNone,XYWH(body.x+117,body.y+-123,205,250),true,{texture='SGUI/face/faces/rek.png'});

body.eye1 = getElementEX(nil,anchorNone,XYWH(body.x+130+66,body.y-100+72,86,49),true,{texture='SGUI/face/eyes/004_p.png'});
body.eye2 = getElementEX(nil,anchorNone,XYWH(body.x+250+20,body.y-100+62+10,61,49),true,{texture='SGUI/face/eyes/004_z.png'});

body.mouth = getElementEX(nil,anchorNone,XYWH(body.x+250+20-46,body.y-100+62+10+75,75,70),true,{texture='SGUI/face/mouths/1usta.png'});

body.hair_sub = getElementEX(nil,anchorNone,XYWH(body.x+250+20-46-104-6-2,body.y-100+71-1+62+10+75-202,81,152),true,{texture='SGUI/face/hair/pacino_z_shd.png'});
body.hair = getElementEX(nil,anchorNone,XYWH(body.x+250+20-46-104,body.y-100+62+10+75-202,181,108),true,{texture='SGUI/face/hair/pacino_p.png'});

body.ear = getElementEX(nil,anchorNone,XYWH(body.x+250+20-46-104+13,body.y-100+62+10+75-202+138,54,82),true,{texture='SGUI/face/ears/rek.png'});

--[[ Face elements will look like this:


Face.elements = {
{ 
	{ face.png, x, y, w, h },
	{ face2.png, x, y, w, h },
	{ face3.png, x, y, w, h },
	{ face4.png, x, y, w, h }
},

{ 
	{ mouth1.png, x, y, w, h },
	{ mouth1a.png, x, y, w, h },
	{ mouth1b.png, x, y, w, h },
	{ mouth1c.png, x, y, w, h }
}

etc...

};

Adjust all face elements x & y according to face element.

--]]